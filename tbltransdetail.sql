-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:20 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table users
-- 

LOCK TABLES users WRITE;
INSERT INTO users(ID, UserID, FullName, PIN, LastAccess, Enabled, CreatedDate, LastAccTime, CreatedTime, AccessCount, CustID, access_type) VALUES
  (1, 1, '2014-09-09', 0, 1),
  (2, 2, '2014-09-09', 0, 1),
  (3, 3, '2014-09-09', 0, 1);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
