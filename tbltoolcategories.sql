-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:21 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table tbltools
-- 

LOCK TABLES tbltools WRITE;
INSERT INTO tbltools(id, Part__No, Description, catID, Category, subCATID, Subcategory, Latestcost, Pop_Up_Alert, Suppliername, Serialno, Calibrate_Expiry_days, Dimensions, CustID, SupplierID) VALUES
  (1, 'Insulated Tools', 1),
  (2, 'Knives & Scissors', 1),
  (3, 'Soldering Equipment', 1),
  (4, 'Pliers', 1),
  (5, 'Spanners & Wrenches', 1),
  (6, 'Safety Wear/PPE', 1),
  (7, 'Deburring Tools', 1),
  (8, 'Vacuum Cleaners', 1),
  (9, 'Sockets & Accessories', 1),
  (10, 'Tool Kits', 1);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
