-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:20 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table tbltransdetail
-- 

LOCK TABLES tbltransdetail WRITE;
INSERT INTO tbltransdetail(ID, TransID, TransDate, TransType, CustID) VALUES
  (1, 1, 'VDE Wrench ', 1),
  (2, 1, 'VDE  Sockets', 1),
  (3, 1, 'VDE Ratchet', 1),
  (4, 1, 'VDE Torque Wrench', 1),
  (5, 1, 'VDE Extension Bar', 1),
  (6, 1, 'Art Knife', 1),
  (7, 1, 'Utility', 1),
  (8, 1, 'Retractable Trimming Knives', 1),
  (9, 1, 'Snap Off', 1),
  (10, 1, 'Solder Wire', 1),
  (11, 1, 'Scissors', 1),
  (12, 1, 'Combination Pliers', 1),
  (13, 1, 'Long Nose', 1),
  (14, 1, 'Side Cutters', 1),
  (15, 1, 'Scotch-Lock Plier', 1),
  (16, 1, 'Electronic Series Cutters', 1),
  (17, 1, 'Forming Pliers', 1),
  (18, 1, 'Cable Cutters', 1),
  (19, 1, 'Double Open End', 1),
  (20, 1, 'Single Open End', 1),
  (21, 1, 'Flare Nut', 1),
  (22, 1, 'Adjustable Wrench', 1),
  (23, 1, 'Hook Spanner', 1),
  (24, 1, 'Combination', 1),
  (25, 1, 'Double Ring', 1),
  (26, 1, 'Arc Flash Protection', 1),
  (27, 1, 'Burraway Tools', 1),
  (28, 1, 'Burraway, Spare Blades', 1),
  (29, 1, 'Insulated Safety Prod.', 1),
  (30, 1, 'Gloves', 1),
  (31, 1, 'Industrial Vacuums', 1),
  (32, 1, 'Combi, Ratcheting', 1),
  (33, 1, 'Electronic Series Long Nose', 1),
  (34, 1, '1/4� Drive', 1),
  (35, 1, '3/8� Drive', 1),
  (36, 1, '1/2� Drive', 1),
  (37, 1, '3/4� Drive', 1),
  (38, 1, 'Kapton Tape', 1),
  (39, 1, 'Kits', 1),
  (40, 1, 'Solder Braid, Wick', 1),
  (41, 1, 'Soldering Accessories', 1),
  (42, 1, 'Solder Tips', 1),
  (43, 1, 'Solder Stations', 1),
  (44, 1, 'Solder Paste', 1),
  (45, 1, 'Desolder Braid', 1),
  (46, 1, 'Portable Solder Irons', 1),
  (47, 1, 'Flux', 1),
  (48, 1, 'Solder Stations and Tips', 1);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
