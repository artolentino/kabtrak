-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:20 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table users
-- 

LOCK TABLES users WRITE;
INSERT INTO users(ID, UserID, FullName, PIN, LastAccess, Enabled, CreatedDate, LastAccTime, CreatedTime, AccessCount, CustID, access_type) VALUES
  (3, '1111', 'Administrator', '1234', NULL, 1, '2014-08-08 00:00:00', '2014-09-12 15:10:14', '1899-12-30 07:45:28', 934, 1, 1),
  (501, '001501', 'ALISDAIR CHAMBERS', '1111', NULL, 1, '2014-09-05 00:00:00', '2014-09-11 13:08:37', '1899-12-30 08:44:16', 78, 1, 0),
  (508, '17000468', 'MIRIAM ARAOS', '1111', NULL, 0, '2014-08-27 00:00:00', NULL, '1899-12-30 12:39:37', 0, 1, 0);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
