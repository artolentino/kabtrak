program TWR_Demo;

uses
  Forms,
  main in 'main.pas' {fMain},
  TWRSDK in 'TWRSDK.pas',
  TWRdbg in 'TWRdbg.pas',
  utils in 'utils.pas',
  ExtInfo in 'ExtInfo.pas' {FExtInfo};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := 'D-Logic - TWR_SDK Demo';
  Application.CreateForm(TfMain, fMain);
  Application.CreateForm(TFExtInfo, FExtInfo);
  Application.Run;
end.
