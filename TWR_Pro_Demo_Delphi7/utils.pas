unit utils;

//******************************************************************************
//  common utility functions and procedures
//  intentionally here for better readability of source code in main.pas
//******************************************************************************

interface
    uses Windows,sysutils, strutils, DateUtils, Math;

type
  TWR_CARD_ACTIONS = record
      code      : integer;
      name      : AnsiString; 
      end;  

Function Pad (str:AnsiString; ch: char; lr:char;  n:Integer):AnsiString;
Function GetTimeZoneBias  :LongInt;
Function GetDiff    (twrtime:LongInt; loctime:TDateTime)  :Integer;
function checkTime  (starttext,endtext:AnsiString)  :boolean;
Function CheckCardNo(card:AnsiString) :boolean;
Function DefSec() :AnsiString;
Function LabtoHex (labtext:AnsiString) :AnsiString;
Function ByteToHex(bt:AnsiString) : AnsiString;
Function LabToByte (labtext:Ansistring):AnsiString;
Function CheckSecPass (old,new:AnsiString):boolean;
Function CardAction (a:Integer):AnsiString;
Function GetSecLength(secdata:AnsiString):integer;
Function BTH ( bt: array  of ansichar) : Ansistring;

var   FormatSettings              : TFormatSettings;  
      Custom_Card_Actions         : array [1..8] of TWR_CARD_ACTIONS;
      // FormatSettings, for local settings of your PC
      
implementation

Function BTH ( bt: array  of ansichar) : Ansistring;
var niz:array [0..6] of ansichar;
    i:integer;
    st:Ansistring;
begin
    for i:=0 to 6 do niz[i]:=bt[i];
    for i:=0 to 6 do st:=st+' '+inttostr(i)+' '+format('%x',[ord(niz[i])])+' '+inttostr(ord(niz[i]));
    result:=st;
end;


//******************************************************************************
    {This function 'pads' string (adds char on left or right)
    where 'str' is string to be formatted, 'ch' is char to be added 
    on 'l' or 'r' side, to gain 'n'-size  of returned string }
//******************************************************************************

function Pad( str:String;  ch: char;  lr:char;  n:Integer):String;
var pomstr, res: AnsiString;
    i,j,k      :Integer;
begin
  j:=length(trim(str));
  k:=n-j;
  pomstr:='';
  for i:=1 to k do pomstr:=pomstr+ch;
  if lr='l' then res:= pomstr+trim(str)
            else res:=trim(str)+pomstr;
  Result:=res;
end;

//******************************************************************************
    {This function gets length of passed string 'secdata'; result is:
      first occassion of '�' - value 255 (0xFF) if exists,
      or absolute length of string!
      Used for reading of size of SecData.}
//******************************************************************************
      
Function GetSecLength(secdata:AnsiString):integer;
var i,j:integer;
  a:AnsiString;
begin
  result:=-1;
  j:=length(trim(secdata));
  if j>0 then
        begin
        a:='';
        setlength(a,1);
        a[1]:=chr(255);
        i:=posex(a,secdata);
        if i>0 then result:=i-1
                else result:=j;
        end
        else result:=0;
        
end;

//******************************************************************************
   {This function gets TimeZoneBias info, depending on your time zone;
   result is difference in minutes from UTC.}
//******************************************************************************
   
function  GetTimeZoneBias:longint;
var
    TZoneInfo: TTimeZoneInFormation;
    TimeZoneBias: longint;
begin
    GetTimeZoneInFormation(TZoneInfo);
    TimeZoneBias := TZoneInfo.Bias;
    Result:=TimeZoneBias;
end;

//******************************************************************************
   {This function gets difference in seconds between local PC time (UTC)
      and time in TWR_SDK device}
//******************************************************************************

Function getdiff(twrtime:longint;loctime:tdatetime):Integer;
var
    localtime,b,bias,utclocal,twrutc:longint;
begin

    localtime:=datetimetounix(loctime);
    bias:=(gettimeZoneBias*60);
    b:=abs(bias);
    if bias>0   then utclocal:=localtime+b
                else utclocal:=localtime-b;

    twrutc:=twrtime;
    Result:=utclocal-twrutc;

end;

//******************************************************************************
  {This function checks if passed string arguments 
    can be safely converted into TDateTime}
//******************************************************************************
 
function CheckTime(starttext,endtext:ansistring):boolean;
var testtime:tdatetime;
begin
if  (trystrtotime(startText,testtime,FormatSettings))=False
          or
    (trystrtotime(EndText,testtime,FormatSettings))=False
 then Result:=False
 else Result:=True;
end;

//******************************************************************************
  {This function checks if card number is in valid range}
//******************************************************************************

Function CheckCardNo (card:String):boolean; overload;
var c:Integer;
begin
  result:=false;
  trystrtoint(card,c);
  if c>65534 then c:=0;
  case c                
      of
      0         :    Result:=False;
      1..65634  :    Result:=True;
  end; //case
end;

//******************************************************************************
  {This function returns formatted AnsiString with 8 chars of 0xFF,
      '��������', since it can not be input from keyboard}
//******************************************************************************

Function DefSec():AnsiString;
Var a:array [1..8] of AnsiChar;
    i: Integer;
begin
  for i:=1 to 8 do a[i]:='1';
  Result:=AnsiString(a);
end;

//******************************************************************************
  {This function returns 'labtext' as formatted string, which represents
   the same value of chars in 'labtext', but in hexadecimal notation; 
   breaks on 16th digit and adds chr(13) for new line}
//******************************************************************************
   
Function LabtoHex (labtext:AnsiString) : AnsiString;
Var ch:AnsiChar;
    c,s,h:AnsiString;
    i,j:Integer;

begin
  s:='';
  for i:=1 to length(labtext) do
        begin
        c:=MidStr(labtext,i,1);
        ch:= c[1];
        j:=ord(ch);
        h:= format('%x',[j]);
        if (i mod 16)=0 
                    then s:=s+pad((h),' ','l',4)+#13
                    else s:= s+pad((h),' ','l',4);
        end;
  Result:=s;
end;
//******************************************************************************




Function ByteToHex(bt:AnsiString) : AnsiString;
Var ch:AnsiChar;
    c,s,h,m:AnsiString;
    i :Integer;
    j :Byte;
begin
  s:='';
  for i:=1 to length(bt) do
        begin
        c:=MidStr(bt,i,1);  //uzmi bajt kao string[1]
        ch:= c[1];           // pretvori u char
        j:=ord(ch);          // uzmi vrednost char u byte
        h:=rightstr('00'+format('%x',[j]),2);
        s:=h+s;
        end;
   m:=rightstr(trim(s),6);
   if m<>'000000' then result:=ansistring(s)        //7B UID
                  else result:=leftstr(trim(s),8);  //4B UID         
   
  //result:=s;     
  
end;

//******************************************************************************
  {This function returns 'labtext' as formatted string, which represents
   the same value of chars in 'labtext', but in DECIMAL notation; 
   breaks on 16th digit and adds chr(13) for new line}
//******************************************************************************

Function LabToByte (labtext:Ansistring):AnsiString;
Var ch:AnsiChar;
    c,s:AnsiString;
    i,j:Integer;

begin
  s:='';
  for i:=1 to length(labtext) do
        begin
        c:=MidStr(labtext,i,1);
        ch:= c[1];
        j:= ord(ch);
        if (i mod 16)=0 
                    then s:=s+pad(inttostr(j),' ','l',4)+#13
                    else s:= s+pad(inttostr(j),' ','l',4);
  end;
  Result:=s;
end;

//******************************************************************************
{This function checks for valid length of SecPass; must be 8 chars!}
//******************************************************************************

Function CheckSecPass (old,new:AnsiString):boolean;
begin
Result:=(length(old)=8)
            and  (length(new)=8);

end;

//******************************************************************************
{This function returns descriptions for card actions, depending on value}
//******************************************************************************

Function CardAction (a:integer):AnsiString;
var i: integer;
begin

if a in [0..255]//[32,128,144,160,176]
      then
          begin
            case a
              of
              32:   result:='032 - Blacklisted card login attempt';
              64:   result:='064 - Hacked card!';
              128:  result:='128 - Regular login';
            end;// case

            for i:= 1 to 8 do 
                if a=Custom_Card_Actions[i].code 
                      then 
                          result:= 
                                inttostr(Custom_Card_Actions[i].code)+
                                ' - '+
                                Custom_Card_Actions[i].name;
      
          end         
      
       else result:='Action: '+IntToStr(a)+' - UNKOWN action!';
end;

//******************************************************************************
//      END of UTILS.pas 
//******************************************************************************     

end.
