object fMain: TfMain
  Left = 468
  Top = 241
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'D-Logic - 2012  - TWR_Pro Demo V2.5.0'
  ClientHeight = 616
  ClientWidth = 872
  Color = clBtnFace
  Constraints.MaxHeight = 650
  Constraints.MaxWidth = 880
  Constraints.MinHeight = 644
  Constraints.MinWidth = 878
  DockSite = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcTWRSDK: TPageControl
    Left = 0
    Top = 0
    Width = 872
    Height = 400
    ActivePage = tsUSB
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 0
    TabWidth = 100
    object tsUSB: TTabSheet
      Caption = 'USB - INFO'
      OnShow = tsUSBShow
      object gbUSBInfo: TGroupBox
        Left = 10
        Top = 10
        Width = 399
        Height = 311
        Caption = ' USB RF INFO '
        Enabled = False
        TabOrder = 0
        object mUSB: TMemo
          Left = 10
          Top = 18
          Width = 377
          Height = 273
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object mUSBTBL: TMemo
          Left = 0
          Top = 256
          Width = 65
          Height = 33
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Lines.Strings = (
            'mUSBTBL')
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object bUSBOpen: TButton
        Tag = 1
        Left = 685
        Top = 14
        Width = 150
        Height = 59
        Caption = 'OPEN USB_RF'
        TabOrder = 1
        OnClick = bUSBOpenClick
      end
      object bUSBRestart: TButton
        Tag = 2
        Left = 685
        Top = 223
        Width = 150
        Height = 40
        Caption = 'RESTART USB_RF'
        TabOrder = 2
        OnClick = bUSBRestartClick
      end
      object bUSBClose: TButton
        Tag = 3
        Left = 685
        Top = 281
        Width = 150
        Height = 40
        Caption = 'CLOSE USB_RF Comm'
        TabOrder = 3
        OnClick = bUSBCloseClick
      end
      object bUSBInfo: TButton
        Tag = 4
        Left = 685
        Top = 108
        Width = 150
        Height = 40
        Caption = 'GET USB_RF INFO'
        TabOrder = 4
        OnClick = bUSBInfoClick
      end
    end
    object tsTWR: TTabSheet
      Caption = 'TWR - INFO'
      ImageIndex = 1
      OnShow = tsTWRShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object gbTWRInfo: TGroupBox
        Left = 10
        Top = 5
        Width = 423
        Height = 252
        Caption = ' TWR_Pro  INFO '
        Enabled = False
        TabOrder = 0
        object mTWR: TMemo
          Left = 3
          Top = 12
          Width = 414
          Height = 234
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object mTWRTBL: TMemo
          Left = 8
          Top = 187
          Width = 65
          Height = 33
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Lines.Strings = (
            'mTwrTbl')
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
        object mExtTbl: TMemo
          Left = 96
          Top = 187
          Width = 65
          Height = 33
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Lines.Strings = (
            'mExtTbl')
          ParentFont = False
          TabOrder = 2
          Visible = False
        end
      end
      object gbTWR: TGroupBox
        Left = 10
        Top = 256
        Width = 423
        Height = 88
        Caption = ' TWR Pro Login '
        TabOrder = 1
        object lTWRId: TLabel
          Left = 15
          Top = 24
          Width = 67
          Height = 13
          Caption = 'TWR Pro  ID'
          Transparent = True
        end
        object lTWRPass: TLabel
          Left = 14
          Top = 53
          Width = 108
          Height = 13
          Caption = 'TWR Pro  Password'
          Transparent = True
        end
        object eTWRPass: TEdit
          Left = 159
          Top = 51
          Width = 66
          Height = 21
          Hint = 'Default password 1111|Default password 1111'
          HelpContext = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '1111'
          OnKeyPress = CheckIntKey
        end
        object bTWRInfo: TButton
          Tag = 11
          Left = 295
          Top = 10
          Width = 122
          Height = 39
          Caption = 'INFO TWR'
          TabOrder = 0
          OnClick = bTWRInfoClick
          OnEnter = bTWRInfoEnter
        end
        object eTWRid: TEdit
          Left = 160
          Top = 22
          Width = 50
          Height = 21
          Hint = 'Default ID 1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '1'
          OnKeyPress = CheckIntKey
        end
        object udTWRid: TUpDown
          Left = 210
          Top = 22
          Width = 16
          Height = 21
          Associate = eTWRid
          Position = 1
          TabOrder = 3
        end
        object btExt: TButton
          Tag = 18
          Left = 296
          Top = 56
          Width = 121
          Height = 25
          Caption = 'Extended INFO'
          TabOrder = 4
          OnClick = btExtClick
        end
      end
      object gbTWRPass: TGroupBox
        Left = 452
        Top = 5
        Width = 380
        Height = 132
        Caption = ' TWR Pro PASSWORD '
        TabOrder = 2
        object lTWROldPass: TLabel
          Left = 6
          Top = 22
          Width = 79
          Height = 13
          Caption = 'OLD Password'
          Transparent = True
        end
        object lTWRNewPass: TLabel
          Left = 6
          Top = 46
          Width = 81
          Height = 13
          Caption = 'NEW password'
          Transparent = True
        end
        object lAgain: TLabel
          Left = 6
          Top = 70
          Width = 41
          Height = 13
          Caption = 'Repeat'
          Transparent = True
        end
        object labmatch: TLabel
          Left = 8
          Top = 96
          Width = 3
          Height = 13
        end
        object eTWROldPass: TEdit
          Left = 126
          Top = 17
          Width = 87
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = eTWROldPassChange
          OnExit = eTWROldPassExit
          OnKeyPress = CheckIntKey
        end
        object eTWRNewPass: TEdit
          Left = 126
          Top = 41
          Width = 87
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnExit = eTWRNewPassExit
          OnKeyPress = CheckIntKey
        end
        object bTWRPass: TButton
          Tag = 13
          Left = 228
          Top = 59
          Width = 140
          Height = 25
          Caption = 'Submit pass'
          Enabled = False
          TabOrder = 4
          OnClick = bTWRPassClick
        end
        object ETWRAgain: TEdit
          Left = 126
          Top = 65
          Width = 87
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnChange = ETWRAgainChange
          OnExit = ETWRAgainExit
          OnKeyPress = CheckIntKey
        end
        object BtwrCHPass: TButton
          Tag = 12
          Left = 228
          Top = 16
          Width = 140
          Height = 25
          Caption = 'Change password'
          TabOrder = 0
          OnClick = BtwrCHPassClick
        end
        object bCNCpass: TButton
          Tag = 14
          Left = 228
          Top = 91
          Width = 140
          Height = 25
          Caption = 'Cancel pass change'
          TabOrder = 5
          Visible = False
          OnClick = bCNCpassClick
        end
      end
      object gbTWRTime: TGroupBox
        Left = 452
        Top = 138
        Width = 380
        Height = 207
        Caption = ' TWR Pro DATE - TIME  (UTC)   '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object lTWRDate: TLabel
          Left = 7
          Top = 22
          Width = 159
          Height = 13
          Caption = 'DateTime at TWR Pro (UTC) '
          Transparent = True
        end
        object lDate: TLabel
          Left = 7
          Top = 72
          Width = 90
          Height = 13
          Caption = 'Local DateTime '
          Transparent = True
        end
        object lOffset: TLabel
          Left = 7
          Top = 128
          Width = 128
          Height = 39
          Caption = 'TimeZone Offset (UTC-Local) in minutes  : '
          Color = clInfoBk
          ParentColor = False
          Transparent = True
          WordWrap = True
        end
        object labOffset: TLabel
          Left = 158
          Top = 136
          Width = 8
          Height = 16
          Caption = '0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lDiff: TLabel
          Left = 7
          Top = 160
          Width = 98
          Height = 26
          Caption = 'TWR / UTC time difference (sec)  :'
          Color = clInfoBk
          ParentColor = False
          WordWrap = True
        end
        object LabDiff: TLabel
          Left = 158
          Top = 168
          Width = 8
          Height = 16
          Caption = '0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object labtimewarn: TLabel
          Left = 194
          Top = 152
          Width = 167
          Height = 49
          AutoSize = False
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          WordWrap = True
        end
        object bTWRTime: TButton
          Tag = 16
          Left = 228
          Top = 88
          Width = 140
          Height = 25
          Caption = 'Submit time'
          Enabled = False
          TabOrder = 0
          OnClick = bTWRTimeClick
        end
        object dtpTWRDate: TDateTimePicker
          Left = 7
          Top = 42
          Width = 197
          Height = 21
          Date = 41044.440972222220000000
          Format = 'dd.MM.yyyy HH:mm:ss'
          Time = 41044.440972222220000000
          Enabled = False
          TabOrder = 1
        end
        object dtpTWRLocal: TDateTimePicker
          Left = 7
          Top = 93
          Width = 197
          Height = 21
          Date = 41044.440972222220000000
          Format = 'dd.MM.yyyy HH:mm:ss'
          Time = 41044.440972222220000000
          Enabled = False
          TabOrder = 2
          TabStop = False
        end
        object bTWRChTime: TButton
          Tag = 15
          Left = 228
          Top = 40
          Width = 140
          Height = 25
          Caption = 'Change time'
          TabOrder = 3
          OnClick = bTWRChTimeClick
        end
        object bCncTime: TButton
          Tag = 17
          Left = 228
          Top = 120
          Width = 140
          Height = 25
          Caption = 'Cancel time change'
          TabOrder = 4
          Visible = False
          OnClick = bCncTimeClick
        end
      end
    end
    object tsLOG: TTabSheet
      Caption = 'TWR - LOG'
      ImageIndex = 2
      OnShow = tsLOGShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object gbLOG1: TGroupBox
        Left = 15
        Top = 5
        Width = 850
        Height = 369
        Caption = ' LOG Manipulation  '
        TabOrder = 0
        object bLogALL: TButton
          Tag = 21
          Left = 39
          Top = 67
          Width = 154
          Height = 30
          Caption = 'Get whole log'
          TabOrder = 0
          WordWrap = True
          OnClick = bLogALLClick
        end
        object gbLogPass: TGroupBox
          Left = 2
          Top = 14
          Width = 200
          Height = 49
          Caption = 'TWR Pro ID / PASS'
          TabOrder = 1
          object lLOGId: TLabel
            Left = 10
            Top = 18
            Width = 26
            Height = 13
            Caption = 'TWR'
            Transparent = True
          end
          object lLOGPass: TLabel
            Left = 105
            Top = 17
            Width = 29
            Height = 13
            Caption = 'PASS'
            Transparent = True
          end
          object eLOGId: TEdit
            Left = 40
            Top = 14
            Width = 45
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = '1'
            OnKeyPress = CheckIntKey
          end
          object udLOGid: TUpDown
            Left = 85
            Top = 14
            Width = 16
            Height = 21
            Associate = eLOGId
            Position = 1
            TabOrder = 1
          end
          object eLOGPass: TEdit
            Left = 140
            Top = 14
            Width = 50
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            Text = '1111'
            OnKeyPress = CheckIntKey
          end
        end
        object gbLogIdx: TGroupBox
          Left = 2
          Top = 105
          Width = 200
          Height = 71
          Caption = 'Get Log by Indexed Range'
          TabOrder = 2
          object lLogStartIdx: TLabel
            Left = 10
            Top = 26
            Width = 52
            Height = 13
            Caption = 'Start IDX'
            Transparent = True
          end
          object lLogEndIndx: TLabel
            Left = 11
            Top = 48
            Width = 43
            Height = 13
            Caption = 'End IDX'
            Transparent = True
          end
          object eLOGId1: TEdit
            Left = 67
            Top = 18
            Width = 47
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnKeyPress = CheckIntKey
          end
          object eLOGId2: TEdit
            Left = 67
            Top = 42
            Width = 47
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnKeyPress = CheckIntKey
          end
          object bLogIdx: TButton
            Tag = 22
            Left = 135
            Top = 16
            Width = 58
            Height = 48
            Caption = 'IDX LOG'
            TabOrder = 2
            WordWrap = True
            OnClick = bLogIdxClick
          end
        end
        object gbLogTime: TGroupBox
          Left = 2
          Top = 178
          Width = 200
          Height = 169
          Caption = '   Get Log by Time Range '
          TabOrder = 3
          object lLOGStartDate: TLabel
            Left = 8
            Top = 22
            Width = 29
            Height = 13
            Caption = 'Start'
            Transparent = True
          end
          object lLogEndDate: TLabel
            Left = 8
            Top = 90
            Width = 20
            Height = 13
            Caption = 'End'
            Transparent = True
          end
          object dtpStart: TDateTimePicker
            Left = 5
            Top = 38
            Width = 113
            Height = 24
            Date = 41044.000000000000000000
            Format = 'dd.MM.yyyy'
            Time = 41044.000000000000000000
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'System'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object bLogTime: TButton
            Tag = 23
            Left = 135
            Top = 26
            Width = 58
            Height = 48
            Caption = 'TIME LOG'
            TabOrder = 1
            WordWrap = True
            OnClick = bLogTimeClick
          end
          object meStart: TMaskEdit
            Left = 5
            Top = 64
            Width = 113
            Height = 24
            EditMask = '!90:00:00;1;_'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'System'
            Font.Style = [fsBold]
            MaxLength = 8
            ParentFont = False
            TabOrder = 2
            Text = '  :  :  '
          end
          object dtpEnd: TDateTimePicker
            Left = 6
            Top = 110
            Width = 111
            Height = 24
            Date = 41044.000000000000000000
            Format = 'dd.MM.yyyy'
            Time = 41044.000000000000000000
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'System'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object meEnd: TMaskEdit
            Left = 5
            Top = 136
            Width = 113
            Height = 24
            EditMask = '!90:00:00;1;_'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'System'
            Font.Style = [fsBold]
            MaxLength = 8
            ParentFont = False
            TabOrder = 4
            Text = '  :  :  '
          end
        end
      end
      object gbLogList: TGroupBox
        Left = 220
        Top = 14
        Width = 642
        Height = 338
        TabOrder = 1
        object lLogHeader: TLabel
          Left = 3
          Top = 7
          Width = 635
          Height = 16
          AutoSize = False
          Caption = 
            'Index    Action    Reader     Card      Sys        Date         ' +
            '          Time              MiFare-UID'
          Color = clInfoBk
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'System'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object mCardTBL: TMemo
          Left = 8
          Top = 280
          Width = 100
          Height = 38
          Lines.Strings = (
            'mCard1')
          TabOrder = 0
          Visible = False
        end
        object mCard: TRichEdit
          Left = 3
          Top = 24
          Width = 636
          Height = 312
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 2147483645
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
      end
    end
    object tsRTE: TTabSheet
      Caption = 'TWR - RTE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ImageIndex = 3
      ParentFont = False
      OnShow = tsRTEShow
      object gbRTEList: TGroupBox
        Left = 216
        Top = 8
        Width = 649
        Height = 338
        TabOrder = 0
        object lRTEHeader: TLabel
          Left = 3
          Top = 7
          Width = 639
          Height = 16
          AutoSize = False
          Caption = 
            'Index    Action    Reader     Card      Sys        Date         ' +
            '          Time              MiFare-UID'
          Color = clInfoBk
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'System'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object mRTETbl: TMemo
          Left = 8
          Top = 296
          Width = 65
          Height = 33
          Lines.Strings = (
            'mRTE1')
          TabOrder = 0
          Visible = False
        end
        object mRTE: TRichEdit
          Left = 3
          Top = 24
          Width = 636
          Height = 312
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 2147483645
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
      end
      object gbRTE: TGroupBox
        Left = 0
        Top = 8
        Width = 210
        Height = 338
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        object lRTEaction: TLabel
          Left = 4
          Top = 312
          Width = 251
          Height = 13
          AutoSize = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Consolas'
          Font.Style = []
          ParentFont = False
        end
        object gbRTEStatus: TGroupBox
          Left = 8
          Top = 246
          Width = 190
          Height = 51
          Enabled = False
          TabOrder = 0
          object mInd: TMemo
            Left = 5
            Top = 9
            Width = 164
            Height = 37
            TabStop = False
            Alignment = taCenter
            Color = clRed
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Consolas'
            Font.Style = [fsBold]
            Lines.Strings = (
              'Status : STOPPED')
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            WantReturns = False
          end
        end
        object gbRTEComm: TGroupBox
          Left = 8
          Top = 7
          Width = 190
          Height = 242
          TabOrder = 1
          object bRTE: TButton
            Tag = 31
            Left = 5
            Top = 15
            Width = 164
            Height = 218
            Caption = 'START REAL TIME EVENT MONITORING'
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            WordWrap = True
            OnClick = bRTEClick
          end
        end
      end
    end
    object tsBLACKLIST: TTabSheet
      Caption = 'TWR - BLACKLIST'
      ImageIndex = 4
      OnShow = tsBLACKLISTShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lBLwarn: TLabel
        Left = 264
        Top = 16
        Width = 245
        Height = 13
        Caption = 'You should read blacklist after each action !'
      end
      object bReadBlacklist: TButton
        Tag = 41
        Left = 2
        Top = 5
        Width = 239
        Height = 31
        Caption = 'Read TWR Pro BlackList'
        TabOrder = 0
        WordWrap = True
        OnClick = bReadBlacklistClick
      end
      object gbBlacklist: TGroupBox
        Left = 0
        Top = 40
        Width = 857
        Height = 329
        Enabled = False
        TabOrder = 1
        object gbDirectCard: TGroupBox
          Left = 22
          Top = 17
          Width = 220
          Height = 301
          Caption = 'Direct Card Blacklist Actions'
          TabOrder = 0
          object lCardBL: TLabel
            Left = 14
            Top = 35
            Width = 46
            Height = 13
            Caption = 'Card No.'
          end
          object bBlock: TButton
            Tag = 42
            Left = 14
            Top = 70
            Width = 188
            Height = 30
            Caption = 'Block Card'
            TabOrder = 1
            OnClick = bBlockClick
          end
          object eBlock: TEdit
            Left = 67
            Top = 30
            Width = 132
            Height = 21
            TabOrder = 0
            OnChange = eBlockChange
            OnKeyPress = CheckIntKey
          end
          object bUnblock: TButton
            Tag = 43
            Left = 14
            Top = 120
            Width = 188
            Height = 30
            Caption = 'UnBlock Card'
            TabOrder = 2
            OnClick = bUnblockClick
          end
        end
        object gbBLList: TGroupBox
          Left = 266
          Top = 17
          Width = 314
          Height = 301
          TabOrder = 1
          object mBlackList: TMemo
            Left = 10
            Top = 10
            Width = 295
            Height = 285
            TabStop = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'System'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
        end
        object gbListCard: TGroupBox
          Left = 610
          Top = 17
          Width = 220
          Height = 301
          Caption = ' Blacklist Actions with Card List  '
          TabOrder = 2
          object lCardBL2: TLabel
            Left = 14
            Top = 35
            Width = 46
            Height = 13
            Caption = 'Card No.'
          end
          object eBList: TEdit
            Left = 69
            Top = 30
            Width = 135
            Height = 21
            TabOrder = 0
            OnKeyPress = CheckIntKey
          end
          object bAdd: TButton
            Tag = 44
            Left = 14
            Top = 70
            Width = 189
            Height = 30
            Caption = 'Add Card to List'
            TabOrder = 1
            WordWrap = True
            OnClick = bAddClick
          end
          object bWriteBL: TButton
            Tag = 46
            Left = 14
            Top = 160
            Width = 189
            Height = 30
            Caption = 'Write existing list to TWR Pro'
            TabOrder = 3
            WordWrap = True
            OnClick = bWriteBLClick
          end
          object bClearBL: TButton
            Tag = 45
            Left = 14
            Top = 120
            Width = 189
            Height = 30
            Caption = 'Clear List'
            TabOrder = 2
            OnClick = bClearBLClick
          end
          object bImportBL: TButton
            Tag = 48
            Left = 14
            Top = 260
            Width = 189
            Height = 30
            Caption = 'Import from .CSV'
            TabOrder = 5
            WordWrap = True
            OnClick = bImportBLClick
          end
          object bExportBL: TButton
            Tag = 47
            Left = 14
            Top = 220
            Width = 189
            Height = 30
            Caption = 'Export List to .CSV'
            TabOrder = 4
            WordWrap = True
            OnClick = bExportBLClick
          end
        end
      end
    end
    object tsSecData: TTabSheet
      Caption = 'USB - Sec Pass'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ImageIndex = 5
      ParentFont = False
      OnShow = tsSecDataShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object gbSecPass: TGroupBox
        Left = 0
        Top = 1
        Width = 405
        Height = 368
        Caption = ' USB Sec Pass '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object Lsec1: TLabel
          Left = 32
          Top = 32
          Width = 126
          Height = 28
          Alignment = taCenter
          Caption = 'Sec Pass '#13#10'(must be 8 bytes!)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object lSecOld: TLabel
          Left = 5
          Top = 103
          Width = 42
          Height = 16
          Caption = 'Actual'
        end
        object lSecNew: TLabel
          Left = 5
          Top = 178
          Width = 21
          Height = 16
          Caption = 'NEW'
        end
        object lSec2: TLabel
          Left = 184
          Top = 30
          Width = 203
          Height = 16
          Caption = '            Bytes            '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
        end
        object lSecBytes: TLabel
          Left = 169
          Top = 54
          Width = 217
          Height = 16
          Caption = '  1   2   3   4   5   6   7   8'
        end
        object lOldByte: TLabel
          Left = 169
          Top = 90
          Width = 224
          Height = 16
          Caption = '                                '
        end
        object lNewByte: TLabel
          Left = 169
          Top = 165
          Width = 224
          Height = 16
          Caption = '                                '
        end
        object lOldHex: TLabel
          Left = 169
          Top = 114
          Width = 224
          Height = 16
          Caption = '                                '
        end
        object lNewHex: TLabel
          Left = 169
          Top = 189
          Width = 224
          Height = 16
          Caption = '                                '
        end
        object lWas: TLabel
          Left = 6
          Top = 72
          Width = 35
          Height = 14
          Caption = '     '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object eOldSec: TEdit
          Left = 54
          Top = 94
          Width = 103
          Height = 26
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          TabOrder = 0
          OnChange = eOldSecChange
        end
        object eNewSec: TEdit
          Left = 54
          Top = 169
          Width = 103
          Height = 26
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          MaxLength = 8
          ParentFont = False
          TabOrder = 1
          OnChange = eNewSecChange
        end
        object bDefSecPass: TButton
          Tag = 52
          Left = 32
          Top = 260
          Width = 121
          Height = 41
          Caption = 'Generate default Sec Pass'
          TabOrder = 2
          WordWrap = True
          OnClick = bDefSecPassClick
        end
        object bChangeSecPass: TButton
          Tag = 51
          Left = 184
          Top = 260
          Width = 201
          Height = 41
          Caption = 'Change Sec Pass'
          TabOrder = 3
          OnClick = bChangeSecPassClick
        end
      end
      object gbSecData: TGroupBox
        Left = 406
        Top = 1
        Width = 457
        Height = 368
        Caption = 'USB Sec Data '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object lSecDataOld: TLabel
          Left = 120
          Top = 10
          Width = 91
          Height = 16
          Caption = 'Current Value'
        end
        object lSecDataNew: TLabel
          Left = 13
          Top = 189
          Width = 224
          Height = 16
          Caption = 'Enter New Value (max. 32 chars!)'
        end
        object lSize1: TLabel
          Left = 389
          Top = 10
          Width = 28
          Height = 16
          Caption = 'Size'
        end
        object lSize2: TLabel
          Left = 389
          Top = 189
          Width = 28
          Height = 16
          Caption = 'Size'
        end
        object lSecSizeOld: TLabel
          Left = 375
          Top = 30
          Width = 49
          Height = 16
          Caption = '       '
        end
        object lSecSizeNew: TLabel
          Left = 375
          Top = 210
          Width = 49
          Height = 16
          Caption = '       '
        end
        object bReadSecData: TButton
          Tag = 53
          Left = 8
          Top = 153
          Width = 438
          Height = 29
          Caption = 'Read Sec Data'
          TabOrder = 0
          OnClick = bReadSecDataClick
        end
        object eSecDataOld: TEdit
          Left = 8
          Top = 27
          Width = 353
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          MaxLength = 32
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          OnChange = eSecDataOldChange
        end
        object eSecDataNew: TEdit
          Left = 8
          Top = 207
          Width = 353
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          MaxLength = 32
          ParentFont = False
          TabOrder = 2
          OnChange = eSecDataNewChange
        end
        object bWriteSecData: TButton
          Tag = 54
          Left = 7
          Top = 332
          Width = 438
          Height = 29
          Caption = 'Write Sec Data'
          TabOrder = 3
          OnClick = bWriteSecDataClick
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 52
          Width = 452
          Height = 97
          TabOrder = 4
          object lOldSecHex: TLabel
            Left = 1
            Top = 52
            Width = 448
            Height = 38
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
          object lOldSecDec: TLabel
            Left = 1
            Top = 8
            Width = 448
            Height = 38
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 232
          Width = 452
          Height = 97
          TabOrder = 5
          object lNewSecDec: TLabel
            Left = 1
            Top = 8
            Width = 448
            Height = 38
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
          object lNewSecHex: TLabel
            Left = 1
            Top = 52
            Width = 448
            Height = 38
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
        end
      end
    end
  end
  object mStatus: TMemo
    Left = 0
    Top = 400
    Width = 872
    Height = 188
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object msublog: TMemo
    Left = 0
    Top = 432
    Width = 185
    Height = 89
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 588
    Width = 872
    Height = 28
    Align = alBottom
    TabOrder = 3
    object lcnt: TLabel
      Left = 800
      Top = 5
      Width = 64
      Height = 16
      Caption = '        '
      Color = clBlack
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lprg: TLabel
      Left = 684
      Top = 4
      Width = 56
      Height = 16
      Caption = '              '
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'System'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object sb: TStatusBar
      Left = 1
      Top = -2
      Width = 680
      Height = 27
      Align = alNone
      Color = clBlack
      Panels = <
        item
          Style = psOwnerDraw
          Width = 10
        end>
      OnDrawPanel = sbDrawPanel
    end
  end
  object sTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = sTimerTimer
    Left = 671
    Top = 517
  end
  object LocalClock: TTimer
    Enabled = False
    OnTimer = LocalClockTimer
    Left = 713
    Top = 517
  end
  object OpDlg: TOpenDialog
    DefaultExt = 'csv'
    FileName = 'blacklist'
    Filter = 'CSV files|*.csv|All files|*.*'
    Left = 764
    Top = 515
  end
  object SvDlg: TSaveDialog
    DefaultExt = 'csv'
    FileName = 'blacklist'
    Filter = 'CSV files|*.csv|All files|*.*'
    Left = 804
    Top = 515
  end
end
