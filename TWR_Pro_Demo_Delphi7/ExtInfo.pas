unit ExtInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  StdCtrls, utils;

type
  TFExtInfo = class(TForm)
    mExt: TMemo;
    Button2: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FExtInfo: TFExtInfo;

implementation

uses main;

{$R *.dfm}

procedure TFExtInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
FextInfo:=nil;
end;

procedure TFExtInfo.FormActivate(Sender: TObject);
var i: integer;
begin
mExt.lines.clear;
for i := 0 to 19 do
    begin
     mExt.Lines.Add( pad(Ext[1,i],' ','r',24)+'='+
                              pad(Ext[2,i],' ','l',10));


   
    end;

end;

procedure TFExtInfo.Button2Click(Sender: TObject);
begin
fExtInfo.Close;
end;

end.

