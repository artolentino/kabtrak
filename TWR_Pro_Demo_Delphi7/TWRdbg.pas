unit TWRdbg;

interface

//******************************************************************************
// application actions, events, twr & usb actions defined here as constant codes
//  intentionally here for better readability of source code in main.pas
//******************************************************************************

  const

    CCommand          = 0 ;
    CSuccess          = 1 ;
    CError            = 2 ;
    CTimeout          = 3 ;
    CInfo             = 4 ;
    CStarted          = 5 ;
    CClosed           = 6 ;
    CForced           = 7 ;
    CTime_delta_OK    = 8 ;
    CTime_delta_big   = 9 ;
    CStopped          = 10;


   CActionOK                      = 400 ;
   CAppClose                      = 401;
   CAppOpen                       = 402;
   
  
   CTWR_MainLoop                  = 200 ;
   CTWR_USB_Open                  = 100 ;
   CTWR_USB_Restart               = 101 ;
   CTWR_USB_Close                 = 102 ;
   CTWR_USB_Info                  = 103 ;
   CTWR_GetInfo                   = 110 ;
   CTWR_GetInfo_getResult         = 111 ;
   CTWR_SetPassword               = 112 ;
   CTWR_SetPassword_getExecStatus = 113 ;
   CTWR_SetTime                   = 114 ;
   CTWR_SetTime_getExecStatus     = 115 ;
   CTWR_GetInfo_Extended          = 116 ;
   CTWR_GetLog                    = 120 ;
   CTWR_GetLogByIndex             = 121 ;
   CTWR_GetLogByTime              = 122 ;
   CTWR_GetLog_getExecStatus      = 123 ;
   CTWR_ReadLog                   = 124 ;
   CRTE                           = 130 ;
   CTWR_ReadRTE                   = 131 ;
   CTWR_BlackList_Block           = 140 ;
   CTWR_BlackList_getExecStatus   = 141 ;
   CTWR_BlackList_Read            = 142 ;
   CTWR_BlackList_getSize         = 143 ;
   CTWR_BlackList_getResult       = 144 ;
   CTWR_BlackList_Unblock         = 145 ;
   CTWR_BlackList_EraseWrite      = 146 ;
   CSecData_PassChange            = 150 ;
   CSecData_ReadData              = 151 ;
   CSecData_WriteData             = 152 ;
   CTimeDoEvents                  = 199 ;                
                                                
                                                
implementation

end.
