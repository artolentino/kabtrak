unit main;

Interface

uses
  Windows,  Messages, SysUtils, Variants,
  Classes,  Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, ComCtrls, DiaLogs,
  IniFiles, Mask,     DateUtils,strutils,
  TwrSDK,   Twrdbg,   Utils;

type


    TSearchOption  = (soIgnoreCase, soFromStart, soWrap);
    TSearchOptions = set of TSearchOption;

    log_record =  record
      idx     : AnsiString;
      action  : AnsiString;
      reader  : AnsiString;
      card    : AnsiString;
      sys     : AnsiString;
      date    : AnsiString;
      time    : AnsiString;
      direct  : AnsiString;
      mifare_uid  : AnsiString;
      end;

  TWR_STATUS_DEBUG = record
      code      : integer;
      name      : AnsiString;    
      end;

 

  TfMain = class(TForm)
      pcTwrSDK    : TPageControl;
      tsUsb       : TTabSheet;
      tsTwr       : TTabSheet;
      tsLog       : TTabSheet;
      tsRte       : TTabSheet;
      tsBlackList : TTabSheet;
      tsSecData   : TTabSheet;
 //GROUPBOXES :
      gbUsbInfo   : TGroupBox;
      gbTwr       : TGroupBox;
      gbTwrInfo   : TGroupBox;
      gbTwrPass   : TGroupBox;
      gbTwrTime   : TGroupBox;

      gbLog1      : TGroupBox;
      gbLogPass   : TGroupBox;
      gbLogIdx    : TGroupBox;
      gbLogTime   : TGroupBox;
      gbLogList   : TGroupBox;

      gbRte       : TGroupBox;
      gbRteComm   : TGroupBox;
      gbRteStatus : TGroupBox;
      gbRteList   : TGroupBox;

      gbBlacklist : TGroupBox;
      gbDirectCard: TGroupBox;
      gbListCard  : TGroupBox;
      gbBLList    : TGroupBox;

      gbSecPass   : TGroupBox;
      gbSecData   : TGroupBox;
      GroupBox1   : TGroupBox;
      GroupBox2   : TGroupBox;
      
 // EDITS
      eTwrid      : TEdit;
      eTwrPass    : TEdit;
      eTwrOldPass : TEdit;
      eTwrNewPass : TEdit;
      eTwrAgain   : TEdit;

      eLogId      : TEdit;
      eLogPass    : TEdit;
      eLogId1     : TEdit;
      eLogId2     : TEdit;
      meStart     : TMaskEdit;
      meEnd       : TMaskEdit;

      eBlock      : TEdit;
      eBList      : TEdit;
      eOldSec     : TEdit;
      eNewSec     : TEdit;
      eSecDataOld : TEdit;
      eSecDataNew : TEdit;

 //BUTTONS :
      bUsbOpen      :  TButton;
      bUsbRestart   :  TButton;
      bUsbClose     :  TButton;
      bUsbInfo      :  TButton;

      bTwrInfo      :  TButton;
      BTwrCHPass    :  TButton;
      bTwrPass      :  TButton;
      bCNCPass      :  TButton;
      bTwrChTime    :  TButton;
      bTwrTime      :  TButton;
      bCncTime      :  TButton;

      bLogALL       :  TButton;
      bLogIdx       :  TButton;
      bLogTime      :  TButton;

      bRte          :  TButton;

      bReadBlacklist:  TButton;
      bBlock        :  TButton;
      bUnblock      :  TButton;
      bAdd          :  TButton;
      bWriteBL      :  TButton;
      bClearBL      :  TButton;
      bImportBL     :  TButton;
      bExportBL     :  TButton;

      bChangeSecPass:  TButton;
      bDefSecPass   :  TButton;
      bReadSecData  :  TButton;
      bWriteSecData :  TButton;
 //MEMOS :
      mStatus     :  TMemo;
      mUsb        :  TMemo;
      mUsbTbl     :  TMemo;
      mTwr        :  TMemo;
      mTwrTbl     :  TMemo;
      msubLog     :  TMemo;
      mInd        :  TMemo;
      mCardTbl    :  TMemo;
      mRteTbl     :  TMemo;
      mBlackList  :  TMemo;
 // TimeRS :
      sTimer        : TTimer;
      LocalClock    : TTimer;
 // LABELS :
      lTwrId        : TLabel;
      lTwrPass      : TLabel;
      lTwrOldPass   : TLabel;
      lTwrNewPass   : TLabel;
      lTwrDate      : TLabel;
      lAgain        : TLabel;
      lDate         : TLabel;
      lOffset       : TLabel;
      labOffset     : TLabel;
      labmatch      : TLabel;
      lDiff         : TLabel;
      LabDiff       : TLabel;
      labTimewarn   : TLabel;
      lLogHeader    : TLabel;
      lLogId        : TLabel;
      lLogPass      : TLabel;
      lLogStartIdx  : TLabel;
      lLogEndIndx   : TLabel;
      lLogStartDate : TLabel;
      lLogEndDate   : TLabel;
      lRteHeader    : TLabel;
      Lsec1         : TLabel;
      lCardBL2      : TLabel;
      lCardBL       : TLabel;
      lRteAction    : TLabel;
      lSecOld       : TLabel;
      lSecNew       : TLabel;
      lSec2         : TLabel;
      lSecBytes     : TLabel;
      lOldByte      : TLabel;
      lNewByte      : TLabel;
      lSecDataOld   : TLabel;
      lSecDataNew   : TLabel;
      lBLwarn       : TLabel;
      lOldHex       : TLabel;
      lNewHex       : TLabel;
      lSize1        : TLabel;
      lSize2        : TLabel;
      lSecSizeOld   : TLabel;
      lSecSizeNew   : TLabel;
      lWas          : TLabel;
      lOldSecHex    : TLabel;
      lOldSecDec    : TLabel;
      lNewSecDec    : TLabel;
      lNewSecHex    : TLabel;

      dtpTwrDate    : TDateTimePicker;
      dtpTwrLocal   : TDateTimePicker;
      dtpStart      : TDateTimePicker;
      dtpEnd        : TDateTimePicker;
      udTwrid       : TUpDown;
      udLogid       : TUpDown;
      OpDlg         : TOpenDiaLog;
      SvDlg         : TSaveDiaLog;
    Panel1: TPanel;
    sb: TStatusBar;
    lcnt: TLabel;
    mExtTbl: TMemo;
    btExt: TButton;
    lprg: TLabel;
    mCard: TRichEdit;
    mRTE: TRichEdit;
    

    


// app events
    procedure FormCreate      (Sender: TObject);
    procedure FormActivate    (Sender: TObject);
    procedure FormClose       (Sender: TObject; var Action: TCloseAction);

    procedure sTimerTimer     (Sender: TObject);
    procedure LocalClockTimer (Sender: TObject);

    procedure sbDrawPanel     (Statusbar: TStatusbar; Panel: TStatusPanel;
                                         const Rect: TRect);
    procedure AppAction           (Sender: Tobject);
    procedure AppLog              (Sender: Tobject);
                                         
//page control events
    procedure tsUsbShow       (Sender: TObject);
    procedure tsTwrShow       (Sender: TObject);
    procedure tsLogShow       (Sender: TObject);
    procedure tsRteShow       (Sender: TObject);
    procedure tsBlackListShow (Sender: TObject);
    procedure tsSecDataShow   (Sender: TObject);
// Usb tab events
//          ---

// Twr tab events
    procedure bTwrInfoEnter    (Sender: TObject);
    procedure eTwrOldPassChange(Sender: TObject);
    procedure eTwrAgainChange  (Sender: TObject);
    procedure eTwrOldPassExit  (Sender: TObject);
    procedure eTwrNewPassExit  (Sender: TObject);
    procedure eTwrAgainExit    (Sender: TObject);

// Log tab events
//          ---

// Rte tab events
    procedure mRTEoldChange       (Sender: TObject);

// BlackList tab events
    procedure eBlockChange        (Sender: TObject);

// SecPass tab events
    procedure eOldSecChange       (Sender: TObject);
    procedure eNewSecChange       (Sender: TObject);
    procedure eSecDataOldChange   (Sender: TObject);
    procedure eSecDataNewChange   (Sender: TObject);
    
//utils

    Procedure ReadIni;
    procedure WriteIni;
    Procedure CheckIntKey         (Sender: Tobject; var Key: Char);
    Function  SearchText          (Control: TCustomEdit; Search: String;
                                  SearchOptions: TSearchOptions): Boolean;

    procedure DebugLog            (TwrEvent, TwrAction, code: Integer);

    procedure Show_Pass_Dlg;
    procedure Hide_Pass_Dlg;
    procedure Show_Time_Dlg;
    procedure Hide_Time_Dlg;
    procedure btExtClick(Sender: TObject);
    procedure bUSBOpenClick(Sender: TObject);
    procedure bUSBInfoClick(Sender: TObject);
    procedure bUSBRestartClick(Sender: TObject);
    procedure bUSBCloseClick(Sender: TObject);
    procedure bTWRInfoClick(Sender: TObject);
    procedure BtwrCHPassClick(Sender: TObject);
    procedure bTWRPassClick(Sender: TObject);
    procedure bCNCpassClick(Sender: TObject);
    procedure bTWRChTimeClick(Sender: TObject);
    procedure bTWRTimeClick(Sender: TObject);
    procedure bCncTimeClick(Sender: TObject);
    procedure bLogALLClick(Sender: TObject);
    procedure bLogIdxClick(Sender: TObject);
    procedure bLogTimeClick(Sender: TObject);
    procedure bRTEClick(Sender: TObject);
    procedure bReadBlacklistClick(Sender: TObject);
    procedure bBlockClick(Sender: TObject);
    procedure bUnblockClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bClearBLClick(Sender: TObject);
    procedure bWriteBLClick(Sender: TObject);
    procedure bExportBLClick(Sender: TObject);
    procedure bImportBLClick(Sender: TObject);
    procedure bChangeSecPassClick(Sender: TObject);
    procedure bDefSecPassClick(Sender: TObject);
    procedure bReadSecDataClick(Sender: TObject);
    procedure bWriteSecDataClick(Sender: TObject);




    
   private

// calling procedures   
  // Usb
    procedure OpenUsb;
    procedure CloseUsb;
    procedure RstUsb;
    procedure InfoUsb;
    procedure WRmUsb;
    procedure FmtUsbTbl;
  //Twr
    procedure InfoTwr;
    procedure ChangePass;
    procedure ChangeTime;
    procedure WRmTwr;
    procedure FmtTwrTbl;
  //Log
    procedure LogAll;
    procedure LogByIndex;
    procedure LogByTime;
    procedure ReadLog;
    procedure Init_CardLog;
    procedure MakeTime;
  // Rte
    procedure Init_RteLog;
    procedure Rte;
  // blacklist
    procedure block_Card  (Card: String);
    procedure Unblock_Card(Card: String);
    procedure read_BlackList;
    procedure WriteBlackList;
  // SecData
    procedure ReadSecData;
    procedure SecPassChange;
    procedure WriteSecData;
    function TWR_WaitMainLoop_PB(wait_time: integer;to_seq:integer): Integer;
    procedure FmtExt;
    procedure ExtendedInfo;
    procedure wrmExt;
    function FMTUID(uid: array of byte): Ansistring;

    { Private declarations }
  public
    { Public declarations }
  
  end;

var

  fMain                       : TfMain;

  TWR_RESULT                  : Twr_Status;

  TWR_SM                      : array [0..43] of TWR_STATUS_DEBUG;

  
  
  LogRec,
  RteRec                      : Log_record;

  PUsb_rf                     : PLongword;

  SystemRun,
  Usb_Status                  : Boolean;

  LOOP_Result, 
  Diff, BtnTag, 
  UsbCount, TwrCount,rteaction          : Integer;

  Path, DataPath,
  LogPath,BLPath,
  DbgLog, SubLog,
  UsbLog, UsbTbl,
  TwrLog, TwrTbl, ExtTbl,
  RteLog, CardLog,
  GlobalPass, secOld, SecNew  : AnsiString;

  Init                        : TiniFile;

  StartTime,
  EndTime                     : TDateTime;

  Usb                         : array [1..2,0..14] of AnsiString;
  Twr                         : array [1..2,0..12] of AnsiString;
  Ext                         : array [1..2,0..19] of AnsiString;


  //// Usb_RF INFO VARS
  Usb_hw_type                 : Integer;
  Usb_hw_ver                  : Integer;
  Usb_fw_ver                  : Integer;
  Usb_fw_sn                   : array[0..8] of AnsiChar;
  Rf_settings_group           : Integer;
  Rf_channel_group            : Integer;
  Usb_Rf_power                : Integer;
  Usb_Rf_cmd_reTry_num        : Integer;
  Usb_Rf_speed                : Integer;
  Last_rssi                   : Integer;
  Last_lqi                    : Integer;
  Uart_Timeout                : Integer;

  //// Twr_SDK INFO VARS
  Hw_type                     : Integer;
  Hw_ver                      : Integer;
  Fw_ver                      : Integer;
  Ser_nr                      : array[0..8] of AnsiChar;
  Mode                        : Integer;
  Time, extime                : Integer;
  Battery_Status              : Integer;
  System_ID                   : Integer;

  // Extende Info vars

  cycle_counter    ,   
  optical_counter  ,
  wake_up_counter  ,
  upload_counter   ,
  download_counter ,
  log_size         ,
  twr_rf_power     ,
  twr_rf_rssi      ,
  twr_rf_lqi       ,
  usb_rf_power_ext ,
  usb_rf_rssi      ,
  usb_rf_lqi        : Integer;
  twr_sn                      : array[0..8] of AnsiChar;




  
                                 

implementation

uses Extinfo;

{$R *.dfm}


//******************************************************************************
//        app events 
//******************************************************************************

PROCEDURE TfMain.FormCreate(Sender: TObject);
begin
  pcTwrSDK.ActivePageIndex := 0;   //set active tabsheet on startup
  mCard.MaxLength := System.MaxInt-2;
end;

    {initalize application; RTE is not running;
      set date in DateTimePicker component;
      ASSIGN memory for pUsb_rf pointer ! ;
      get application paths for data, log, blacklist dirs; create them ;
      read prameters from 'var.ini'  file;
      reset counters; clear memos; load usb and twr .csv tables, if exists}

PROCEDURE TfMain.FormActivate(Sender: TObject);
begin  
  SystemRun := False;
  dtpTwrDate.DateTime := EncodeDateTime(2000,1,1,1,0,0,0);
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT,FormatSettings);
  New(pUsb_rf);
  Usb_Status:=False;
  Path:=extractfilePath(application.ExeName);
  dataPath:=Path+'data\';
  LogPath:=Path+'Log\';
  blPath:=Path+'BlackList\';
  if not directoryexists(dataPath) then createdir(dataPath);
  if not directoryexists(LogPath) then createdir(LogPath);
  if not directoryexists(BLPath) then createdir(BLPath);
  dbgLog:=LogPath+'debug.txt';
  subLog:=LogPath+'debug_'+IntToStr(DateTimeToUnix(Now))+'.Log';
  UsbLog:=LogPath+'Usb_'+IntToStr(DateTimeToUnix(Now))+'.Log';
  UsbTbl:=dataPath+'Usbtb.csv';
  TwrLog:=LogPath+'Twr_'+IntToStr(DateTimeToUnix(Now))+'.Log';
  TwrTbl:=dataPath+'Twrtb.csv';
  ExtTbl:=dataPath+'Extended_Twr.csv';
  mStatus.Clear;
  readini;
  DebugLog(CAppOpen,CSuccess,CActionOK);
  UsbCount:=0; 
  TwrCount:=0;
  mUsbTbl.Clear;
  mTwrTbl.Clear;
  if fileexists(UsbTbl)
              then  mUsbTbl.Lines.LoadFromFile(UsbTbl)
              else  FmtUsbTbl;
  if fileexists(TwrTbl)
              then  mTwrTbl.Lines.LoadFromFile(TwrTbl)
              else  FmtTwrTbl;  
  if fileexists(ExtTbl)
              then  mExtTbl.Lines.LoadFromFile(ExtTbl)
              else  FmtExt;                           
end;


PROCEDURE TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
var i : integer;

begin
  sb.Visible:=False;
  if Usb_Status
      then
          begin
          mStatus.Lines.Append('');
          mStatus.Font.Size:=10;
          pcTwrSDK.Hide;
          fMain.height:=120;
          DebugLog(CAppClose,CForced,CActionOK);
          CloseUsb;
          fMain.Cursor:=crHourGlass;
          mStatus.Lines.Append('');
          mstatus.Lines.add('Exiting app.....');
          for i:=1 to 10 do begin
          mstatus.Lines.Add(dupestring('.',10-i));
          sleep(100);
          end;
          
          fMain.Cursor:=crDefault;
          end
      else    DebugLog(CAppClose,CSuccess,CActionOK);
      
  Application.Terminate; 
end;

  {timer procedure for reading RTE events;
  if RTE events >0 then read it from buffer}

PROCEDURE TfMain.sTimerTimer(Sender: TObject);
var
  cmdResponses                : Integer;
  RealTimeEvents              : Integer;
  LogAvailable                : Integer;
  TimeoutOccurred             : Integer;
  LogCounter                  : Integer;
  Log_idx                     : Integer;
  Log_Action                  : Integer;
  Log_reader_id               : Integer;
  Log_Card_id                 : Integer;
  Log_system_id               : Integer;
  Timestamp                   : Integer;
  Mf_UID                      : array[0..7] of byte;
  i                           : Integer;
  line,direct, datestr, timestr : AnsiString;
begin
  //Twr_ClearLog();
  TWR_RESULT :=
                Twr_MainLoop
                            ( 
                            @cmdResponses,
                            @RealTimeEvents,
                            @LogAvailable,
                            @TimeoutOccurred
                            );
  if  (TWR_RESULT = TWR_OK) and (RealTimeEvents > 0) 
      then
          begin
          DebugLog(CTWR_MainLoop,CSuccess,TWR_Result);
          sTimer.Enabled := False;
          LogCounter := Twr_ReadRte_Count();
          for i := 1 to LogCounter 
            do
              begin
              timestamp:=0;
              line:='';
              
              TWR_RESULT :=
                Twr_ReadRte(
                            @Log_idx,
                            @Log_Action,
                            @Log_reader_id,
                            @Log_Card_id,
                            @Log_system_id,
                            @Timestamp,
                            @mf_uid
                            );
              if TWR_RESULT = TWR_OK 
                  then
                      begin

              
                 

                  if log_reader_id=1 then direct:='IN'
                                     else
                                     
                  case (Log_reader_id mod 2)
                          of
                            1:   direct:='IN';
                            0:   direct:='OUT';
                        end; 
                 datestr:= FormatDateTime('dd.mm.yyyy',UnixToDateTime(Timestamp));
                 timestr:= FormatDateTime('HH:mm:ss',UnixToDateTime(Timestamp));
                 rteaction:=log_action;
                line:=format('%4d | %4d | %3d/%3s | %5d | %5d  | %10s | %8s | %22s',
                  [Log_idx,Log_action,Log_reader_id,
                  direct,log_card_id,Log_system_id,datestr,timestr,FMTUID(mf_uid)]);
       
                      DebugLog(CTWR_ReadRTE,CSuccess,TWR_Result);
                      mrte.Lines.add( line );            // add line
                      mrte.SelStart := mrte.GetTextLen;   // here
                      mrte.Perform(EM_SCROLLCARET, 0, 0);  // scroll caret to bottom
                      
                      mrteTbl.Lines.append(Stringreplace(line,'|',',',[rfReplaceall]));
                      mrteTbl.Lines.SaveToFile(rteLog);
                      end
              else    DebugLog(CTWR_ReadRTE,CError,TWR_Result);

              end;

          sTimer.Enabled := True;
          end;
end;

    {local clock timer; starts when action for change time is initiated; 
      its purpose is to provide accurate time to be written in TWR
      during time change procedure; DateTimePicker components ticks 
      every second until Submit or Cancel is pressed}  

PROCEDURE TfMain.LocalClockTimer(Sender: TObject);
begin
  dtpTwrDate.DateTime := UnixToDateTime(DateTimeToUnix(Now)+(getTimezonebias*60));
  dtpTwrLocal.DateTime:= UnixToDateTime(DateTimeToUnix(Now));
  Application.ProcessMessages;
end;

  {cosmetics - util procedure for StatusBar component, 
    provides 'old console look', black background with green font}

PROCEDURE TfMain.sbDrawPanel(Statusbar: TStatusbar; 
                              Panel: TStatusPanel;const Rect: TRect);
begin
  with Statusbar.Canvas 
    do
      begin
      Brush.Color := clBlack;
      Font.Color := clLime;
      Font.Style := [fsBold];
                    //Panel background color
      FillRect(Rect) ;
                     //Panel Text
      TextRect(Rect,2 + Statusbar.panels[0].width
                      + Rect.Left, 2
                      + Rect.Top,Panel.Text) ;
      end;
end;

//******************************************************************************
//  Main procedure for application behaviour; 
//  depending on button tag, predefined actions happens;
//  This way is much more comfortable than calling ButtonXClick procedure
//  for each button; similar to Action list & Action Manager usage in Delphi
//******************************************************************************

PROCEDURE TfMain.AppAction(Sender:Tobject);
//var Ltag:integer;
begin
  {
  if (sender is Tbutton) then
    begin

    Ltag:=(sender as tbutton).tag;
    if Ltag in 
        [1..4,11..17, 21..23,31, 41..48, 51..54] 
                          then  AppLog(sender);

    case Ltag 
        of
           1: OpenUsb;    // Open USB_RF 

           2: RstUsb;   // Restart USB_RR

           3: CloseUsb;    // Close USB_RF

           4: InfoUsb;   //  Get USB INFO

          11: InfoTwr;     // Get TWR info

          12: Show_Pass_Dlg ;      // Start Change password  and show pass dlg
                 
          13: begin       //SUBMIT PASS  - Change password and Hide pass Dlg
              if    (
                    (trim(eTwrNewPass.text)<>'')
                          and
                    (trim(eTwrAgain.text)<>'')
                          and
                    (trim(eTwrNewPass.text)=trim(eTwrAgain.text))
                    ) 
                   then begin
                        ChangePass;
                        Hide_Pass_Dlg;
                        end
                   else begin
                        ShowMessage('Password can not be empty!');
                        eTwrNewPass.SetFocus;
                        end;
              end;

          14: begin     //CANCEL PASS - Do nothing and Hide pass dlg
              Hide_Pass_Dlg;
              eTwrOLDPass.Text      :=eTwrPass.Text;
              eTwrNewPass.Text      :='';
              eTwrAgain.Text        :='';
              end;

          15: Show_Time_Dlg;    // Start Change time and  show time dlg

          16: begin     // Submit change time and Hide Time Dlg
              changeTime;
              Hide_Time_Dlg;
              end;

          17: Hide_Time_Dlg;     // Cancel change time  -  Do Nothing and Hide Time Dlg

          21: begin           // Get Whole Log
              btntag:=lTag;
              Logall;
              end;

          22: begin           // Get Idx Log
              btntag:=Ltag;
              Logbyindex;
              end;    

          23: begin          // Get Time Log
              btntag:=Ltag;
              if CheckTime(mestart.Text,meend.Text) 
                  then LogbyTime
                  else meStart.SetFocus;
              end;

          31: RTE;         // Start/Stop RTE monitoring

          41: Read_BlackList;         // REad Blacklist
              
          42: begin        // Check CardNo. and Block Card if CardNo. OK
                if CheckCardNo(eblock.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlock.SetFocus;
                        end
                    else
                        begin
                            if searchText(mBlackList,eblock.Text,[soFromStart]) 
                                then
                                    begin
                                    showmessage('Already in list!');
                                    eblock.SetFocus;
                                    end
                                else block_Card(trim(eblock.Text));
                        end;
              end;

          43: begin      // Check CardNo. and UnBlock Card if CardNo. OK
                if CheckCardNo(eblock.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlock.SetFocus;
                        end
                    else
                        begin
                        if searchText(mBlackList,eblock.Text,[soFromStart])=False
                           then
                               begin
                               showmessage('Card NOT in list!');
                               eblock.SetFocus;
                               end
                           else  Unblock_Card(trim(eblock.Text));
                        end;
              end;   

          44: begin            //Check CardNo. and ADD to list if CardNo. OK
                if CheckCardNo(eBlist.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlist.SetFocus;
                        end
                    else
                        begin
                        if searchText(mBlackList,eblist.Text,[soFromStart]) 
                           then
                                begin
                                showmessage('Already in list!');
                                eblist.SetFocus;
                                end
                           else
                                mBlackList.Lines.DelimitedText:=
                                  mBlackList.Lines.DelimitedText+','+eblist.Text;
                        end;
              end;

          45: begin            // Clear Working List
              mBlackList.Lines.Clear;
              mBlackList.Lines.DelimitedText:='TWR_BLACKLIST,';
              end;    

          46: WriteBlackList;   // Write Blacklist to TWR
              

          47: begin            // Open SaveDialog and save working list  to .csv
              if SvDlg.Execute 
                  then
                    mBlackList.Lines.SaveToFile(svDlg.FileName);
              end;      

          48: begin            // Open OpenDialog and put .csv list to working list
              MBlackList.Lines.Clear;
              If OpDlg.Execute 
                  then
                    mBlackList.Lines.LoadFromFile(OpDlg.FileName);
              end;

          51: begin
              if CheckSecPass(eOldSec.Text,eNewSec.Text)
                  then
                      SecPassChange                 // change SecPass
                  else
                      begin
                      showmessage('Wrong length of Passwords!');
                      eOldSec.SetFocus;
                      end;
              end;

          52: begin
              eNewSec.Text:='';
              eNewSec.Text:=DefSec;            // put default SecPass in EditBox
              end;

          53: ReadSecData;                    

          54: WriteSecData;

    end; //CASE
  end;  // ENDIF
    }
  end;

    { writes button actions to memo and log file;
    buttons are marked with 'tag' property; depending on tag, read caption
    of button and write messaeg in memo; then write memo lines to log file}

PROCEDURE TfMain.AppLog(Sender:Tobject);
var 
    ms:AnsiString;
begin
  ms:=  ( 
      FormatDateTime('HH:mm:ss:zzz',now)
      +' - ('+
      pad(inttostr((sender as tbutton).tag),'0','l',3)
      +') - '''+
      (sender as tbutton).Caption 
      +''' button pressed - OK'
    );
  sb.panels[0].text:=ms;  
  mstatus.Lines.Append(ms);
  mStatus.Lines.SaveToFile(dbgLog);  
end;
//******************************************************************************
//    end of app actions block
//******************************************************************************


//******************************************************************************
//    Page control events block
//******************************************************************************

 { on show of tsUSB tabsheet }

PROCEDURE TfMain.tsUsbShow(Sender: TObject);
begin
 if bUsbOpen.Enabled then  bUsbOpen.SetFocus
                        else bUsbClose.SetFocus;
end;

 { on show of tsTWR tabsheet; prevents further actions if USB_Rf is closed! }

PROCEDURE TfMain.tsTwrShow(Sender: TObject);
begin
  if Usb_Status=False
      then
          begin
          tsTwr.Enabled:=False;
          showmessage('OPEN Usb_RF first!');
          end
      else
          begin
          tsTwr.Enabled:=True;
          bTwrInfo.SetFocus;
          dtpTwrlocal.DateTime:=Now;
          laboffset.Caption:=IntToStr(getTimezonebias);
          eTwrNewPass.Enabled:=False;
          eTwrAgain.Enabled:=False;
          eTWRPass.Text:=GlobalPass;
          end;
end;

 { on show of tsLog tabsheet; prevents further actions if USB_Rf is closed! }

PROCEDURE TfMain.tsLogShow(Sender: TObject);
begin
  if Usb_Status=False
      then
          begin
          gbLog1.Enabled:=False;
          tsTwr.Enabled:=False;
          showmessage('OPEN Usb_RF first!');
          end
      else
          begin
          gbLog1.Enabled:=True;
          tsTwr.Enabled:=True;
          dtpStart.Date:= Now;      //- EncodeTime(23,59,59,999)
          dtpEnd.Date:= Now+EncodeTime(0,0,0,0);
          meStart.Text:='00:00:00';
          meEnd.Text:='23:59:59';
          eLogPass.Text:=GlobalPass;
          ELogId1.Text:='0';
          ELogid2.Text:='0';
          end;
end;

 { on show of tsRTE tabsheet; prevents further actions if USB_Rf is closed! }

PROCEDURE TfMain.tsRteShow(Sender: TObject);
begin
  if Usb_Status=False
      then
          begin
          tsRte.Enabled:=False;
          showmessage('OPEN Usb_RF first!');
          end
      else    tsRte.Enabled:=True;
         
end;

 { on show of tsBlacklist tabsheet; prevents further actions if USB_Rf is closed! }

PROCEDURE TfMain.tsBlackListShow(Sender: TObject);
begin
  if Usb_Status=False
      then
          begin
          tsBlacklist.Enabled:=False;
          gbBlacklist.Enabled:=False;
          bReadBlackList.Enabled:=False;
          showmessage('OPEN Usb_RF first!');
          end
      else
          begin
          tsBlackList.Enabled:=True;
          gbBlacklist.Enabled:=False;
          bReadBlackList.Enabled:=True;
          bReadBlackList.SetFocus;
          OpDlg.InitialDir:=BLPath;
          SvDlg.InitialDir:=BLPath;
          end;
end;

 { on show of tsSecData tabsheet; prevents further actions if USB_Rf is closed! }
 
procedure TfMain.tsSecDataShow(Sender: TObject);
begin
if Usb_Status=False
      then
          begin
          gbSecPass.Enabled:=False;
          gbSecData.Enabled:=False;
          showmessage('OPEN Usb_RF first!');
          end
      else
          begin
          gbSecPass.Enabled:=True;
          gbSecData.Enabled:=True;
          eOldSec.Text:=SecNew;
          eNewSec.Text:='';
          lWas.Caption:='Old : '+SecOld;
          eOldSec.SetFocus;
          end;
end;
//******************************************************************************
//    end of page control events block
//******************************************************************************


//******************************************************************************
//  TWR tabsheet events - cosmetics and validations
//******************************************************************************

PROCEDURE TfMain.bTwrInfoEnter(Sender: TObject);
begin
  bTwrPass.Enabled:=False;
end;


PROCEDURE TfMain.eTwrOldPassChange(Sender: TObject);
begin
    if length(TRIM(eTwrOldPass.Text))>0 
        then
            begin
            eTwrNewPass.Enabled  :=True;
            eTwrAgain.Enabled    :=True;
            end
        else
            begin
            eTwrNewPass.Enabled  :=False;
            eTwrAgain.Enabled    :=False;
            end;

end;

PROCEDURE TfMain.eTwrAgainChange(Sender: TObject);
begin
    if  (length(TRIM(eTwrNewPass.Text)))
                <>
        (length(trim(eTwrAgain.Text)))
          then
              begin
              labmatch.Caption    :='Pass does not match!';
              labmatch.Font.color :=clred;
              end
          else
              begin
              labmatch.Caption    :='Match OK!';
              labmatch.Font.color :=clBlack;
              end;
end;

PROCEDURE TfMain.eTwrOldPassExit(Sender: TObject);
begin
  if length(trim(eTwroldPass.Text))=0 then eTwroldPass.SetFocus;
end;


PROCEDURE TfMain.eTwrNewPassExit(Sender: TObject);
begin
  //if length(trim(eTwrNewPass.Text))=0 then eTwrNewPass.SetFocus;
end;


PROCEDURE TfMain.eTwrAgainExit(Sender: TObject);
begin
 // if length(trim(eTwrAgain.Text))=0 then eTwrAgain.SetFocus;
end;

//******************************************************************************
//  RTE tabsheet events - cosmetics
//******************************************************************************

PROCEDURE TfMain.mRTEoldChange(Sender: TObject);
begin
  lRteAction.Caption:=CardAction((RteAction)); 
                              //CardAction procedure - Utils.pas
end;

//******************************************************************************
// Blacklist tabsheet events - cosmetics and validations
//******************************************************************************

PROCEDURE TfMain.eBlockChange(Sender: TObject);
begin
  if length((sender as tedit).Text)>=5 
      then
          begin
          if CheckCardNo((sender as tedit).Text)=False
                        then  (sender as tedit).SelectAll;
          end;
end;

//******************************************************************************
//  SecData tabsheet events  - cosmetics and validations
//******************************************************************************

procedure TfMain.eSecDataOldChange(Sender: TObject);
begin
  if (sender as tedit).enabled then 
          begin
          lOldSecDec.Caption:=(labtobyte(eSecDataOld.Text));
          lOldSecHex.Caption:=(labtoHex(eSecDataOld.Text));
          lSecSizeOld.Caption:=inttostr(GetSecLength(eSecDataOld.Text))+' / 32';
          end;
end;

procedure TfMain.eSecDataNewChange(Sender: TObject);
begin
 if (sender as tedit).enabled then 
          begin
          lNewSecDec.Caption:=(labtobyte(eSecDataNew.Text));
          lNewSecHex.Caption:=(LabtoHex(eSecDataNew.Text));
          lSecSizeNew.Caption:=inttostr(GetSecLength(eSecDataNew.Text))+' / 32';
          end;
end;


PROCEDURE TfMain.eOldSecChange(Sender: TObject);
begin
  lOldByte.Caption:=LabTobyte(eOldSec.text);
  lOldHex.Caption:=LabTOHex(eOldSec.Text);
end;


PROCEDURE TfMain.eNewSecChange(Sender: TObject);
begin
  lNewByte.Caption:=LabToByte(eNewSec.Text);
  lNewHex.Caption:=LabTOHex(eNewSec.Text);
end;

//******************************************************************************
//  Utility procedures; not too common, so they are not in Utils.pas
//******************************************************************************

    {Read parameters from 'var.ini';}

PROCEDURE TfMain.readini;
var 
    i, custCardAct:Integer;
begin
  Init:=Tinifile.Create(Path+'var.ini');
  for i:=0 to 14 do Usb[1,i]:=Init.ReadString('RFCaption',IntToStr(i),IntToStr(i));
  for i:=0 to 11 do Twr[1,i]:=Init.ReadString('TwrCaption',IntToStr(i),IntToStr(i));
  for i:= 0 to 19 do Ext[1,i]:=Init.ReadString('ExtCaption',IntToStr(i),IntToStr(i));
  for i:= 0 to 43 do 
            begin
            TWR_SM[i].code:=i;
            TWR_SM[i].name:=Init.ReadString('TWR_STATUS_DEBUG',intToStr(i),inttostr(i));
            end;
  GlobalPass:= Init.ReadString('Default','Pass','1111');
  SecOld:=Init.ReadString('SEC','old','��������');
  SecNew:=Init.ReadString('SEC','new','��������');
  for i:=1 to 8 do 
                begin
                CustCardAct:= init.ReadInteger('CARD_ACTION_CODES',inttostr(i),255);
                Custom_Card_Actions[i].code:=  CustCardAct;
                            
                Custom_Card_Actions[i].name:=
                            (init.ReadString('CARD_ACTION_NAMES',inttostr(i),'CUSTOM'+inttostr(custCardAct)));
                            
                end;
  Init.free;
end;

    {write passwords in 'var.ini' for security purpose;
      if you forget your passwords during testing, you can find it in var.ini }

PROCEDURE TfMain.writeini;
begin
  Init:=Tinifile.Create(Path+'var.ini');
  Init.WriteString('Default','Pass',GlobalPass);
  Init.WriteString('SEC','old',SecOld);
  Init.WriteString('SEC','new',SecNew);
  Init.free;
end;

    { some editbox components calls this procedure instead of 
    standard KeyPress; only numbers, ESCAPE, BackSpace and Enter
      are valid inputs}
      
PROCEDURE TfMain.CheckIntKey(Sender: TObject; var Key:Char);
begin
  if not( Key in ['0'..'9',#13,#8,#27]) then key:=#0;
end;

    {search specific text in memo control; 
      usage: find if card no. is already in blacklist data written in memo, 
      so avoid duplicate entries}
      
Function TfMain.SearchText(
    Control: TCustomEdit;
    Search: AnsiString;
    SearchOptions: TSearchOptions): Boolean;
var
  Text: String;
  Index: Integer;
begin
  if soIgnoreCase in SearchOptions 
    then
        begin
        Search := UpperCase(Search);
        Text := UpperCase(Control.Text);
        end
    else    Text := Control.Text;

  Index := 0;
  if not (soFromStart in SearchOptions) 
      then
          Index := PosEx(Search, Text,
                  Control.SelStart + Control.SelLength + 1);

  if (Index = 0) and
      ((soFromStart in SearchOptions) or
       (soWrap in SearchOptions)) 
       then
            Index := PosEx(Search, Text, 1);

  Result := Index > 0;
  if Result 
      then
          begin
          Control.SelStart  := Index - 1;
          Control.SelLength := Length(Search);
          end;
end;

    {main procedure for writing logs;
      TwrEvents and TwrActions are passed as argument from each
      TWR-related procedure as constants with descriptive names
        (defined in TWRdbg.pas).
        Third parameter is TWR_status, defined as constant
        in TWRSDK.pas;
        replu codes and names are written at 'var.ini',
        in section [TWR_STATUS_DEBUG];
        at last, every action is written in log file;
        log files are named 'debug_XXXXXXXXXX.log', where 'X'-s
        represent Unix datetime}

PROCEDURE TfMain.DebugLog( TwrEvent,TwrAction,code:Integer);
var 
    init    : tinifile;
    ms, pom :Ansistring;
begin
  init:=Tinifile.Create(Path+'var.ini');
                          
   if code<43 then  pom:= twr_sm[code].name;
   if code>43 then  pom:= 'OK';
   if code=43 then begin
                  pom:=  twr_sm[code].name;
                  ms:= FormatDateTime('HH:mm:ss:zzz',now)+' - ('+
        inttostr(TwrEvent)+') - '+
        init.ReadString('event',inttostr(TwrEvent),'')+ ' : '+
        inttostr(TwrAction)+' ms ['+
          pom+'] ';
                  end
            else       
   ms:= FormatDateTime('HH:mm:ss:zzz',now)+' - ('+
        inttostr(TwrEvent)+') - '+
        init.ReadString('event',inttostr(TwrEvent),'')+ ' - ('+
        inttostr(TwrAction)+') - '+
        init.ReadString('action',inttostr(twraction),'')+' - ('+
        inttostr(code)+') - '+
        pom;

  sb.panels[0].text:=ms;  
  mstatus.Lines.Append(ms);
  mStatus.Lines.SaveToFile(dbgLog); 
  mStatus.Lines.SaveToFile(SubLog);  
  init.Free;
end; 

    {cosmetics - show and enable edits and buttons 
      needed for pass change procedure}

PROCEDURE TfMain.show_pass_dlg;
begin
  eTwrOldPass.Enabled :=True;
  eTwrNewPass.Enabled :=True;
  eTwrAgain  .Enabled :=True;
  bTwrPass   .Enabled :=True;
  bCNCPass   .visible :=True;
  BTwrCHPass .Visible :=False;
  eTwrOldPass.Text    :=eTwrPass.Text;
  eTwrNewPass.Text    :='';
  eTwrAgain.Text      :='';
  eTwrOldPass.SetFocus;
end;

    {cosmetics - hide and diable edits and buttons 
      needed for pass change procedure, after submit or cancel action}

PROCEDURE TfMain.Hide_pass_dlg;
begin
  bTwrinfo.SetFocus;
  BTwrCHPass.Visible  :=True;  
  eTwrNewPass.Enabled :=False;
  eTwroldPass.Enabled :=False;
  eTwrAgain.Enabled   :=False;
  bTwrPass.Enabled    :=False; 
  bCNCPass.Visible    :=False; 
end;

    {cosmetics - show and enable edits and buttons 
      needed for time change procedure}
      
PROCEDURE TfMain.Show_time_dlg;
begin
  dtpTwrDate.Enabled  := false; //True;          
  LocalClock.Enabled  :=True;
  bTwrTime.Enabled    :=True;
  bCnCTime.Visible    :=True;
  bTwrTime.SetFocus;
  //dtpTwrDate.SetFocus;
end;

    {cosmetics - hide and diable edits and buttons 
      needed for time change procedure, after submit or cancel action}

PROCEDURE TfMain.Hide_Time_Dlg;
begin
  LocalClock.Enabled  :=False;
  bTwrTime.Enabled    :=False;
  dtpTwrDate.Enabled  :=False;
  bcncTime.Visible    :=False;
  bTwrInfo.SetFocus;
end;




//******************************************************************************
//  private procedures - intentionally here for better readability of code
//    almost all of following procedures are examples haw to call
//    procedures for twr_dll.dll, which are in 'TWRSDK.pas';
//    NOTE: DebugLog (CSomeEvent, CSomeAction, CSomeResult) are
//    only for better user interaction and logging purposes;
//******************************************************************************


//******************************************************************************
//      USB
//******************************************************************************  

    {Open USB Rf}
    
PROCEDURE TfMain.OpenUsb;
begin
  TWR_RESULT := Twr_Usb_Open(pUsb_rf);         // call Twr_Usb_Open and pass pUsb_rf as nil to get HANDLE
  if TWR_RESULT = TWR_OK                       // if OK
      then
          begin
          DebugLog(CTWR_Usb_open,CSuccess,TWR_RESULT);  // write to log
          Usb_Status:=True;                             // put flag on TRUE
          InfoUsb;
          end
      else
          begin
          DebugLog(CTWR_Usb_open,Cerror,TWR_RESULT);    // write to log
          Usb_Status:=False;                            // put flag on FALSE
          end;
end;

    {Close USB RF}
    
PROCEDURE TfMain.CloseUsb;
begin
  TWR_RESULT := Twr_Usb_Close(pUsb_rf);        // call  Twr_Usb_Close
  if TWR_RESULT = TWR_OK
      then
          begin
          DebugLog(CTWR_Usb_Close,CSuccess,TWR_RESULT);
          Usb_Status:=False;
          end
      else
          begin
          DebugLog(CTWR_Usb_Close,Cerror,TWR_RESULT);
          Usb_Status:=True;
          end;
end;

    {Soft restart of USB RF}
    
PROCEDURE TfMain.RstUsb;
begin
  TWR_RESULT := Twr_Restart(pUsb_rf);       // call  Twr_Restart
  if TWR_RESULT = TWR_OK
      then DebugLog(CTWR_Usb_Restart,CSuccess,TWR_RESULT) 
      else DebugLog(CTWR_Usb_Restart,Cerror,TWR_RESULT);  
end;

    {calls Info Usb procedure}
  
PROCEDURE TfMain.InfoUsb;
begin
  TWR_RESULT :=                            // call Usb info function
                Twr_Usb_Info( pUsb_rf,
                              @Usb_hw_type,
                              @Usb_hw_ver,
                              @Usb_fw_ver,
                              @Usb_fw_sn,       
                              @Rf_settings_group,
                              @Rf_channel_group,
                              @Usb_Rf_power,
                              @Usb_Rf_cmd_reTry_num,
                              @Usb_Rf_speed,
                              @Last_rssi,
                              @Last_lqi,
                              @uart_Timeout);

  if TWR_RESULT = TWR_OK 
      then                                                // put results in USB string array
          begin
          inc(UsbCount);
          Usb [2,0 ] := FormatDateTime('dd.mm.yyyy HH:mm:ss',Now);
          Usb [2,1 ] := IntToStr(UsbCount);
          Usb [2,2 ] := IntToStr(Usb_hw_type);
          Usb [2,3 ] := IntToStr(Usb_hw_ver);
          Usb [2,4 ] := IntToStr(Usb_fw_ver);
          Usb [2,5 ] := AnsiString(Usb_fw_sn);
          Usb [2,6 ] := IntToStr(Rf_settings_group);
          Usb [2,7 ] := IntToStr(Rf_channel_group);
          Usb [2,8 ] := IntToStr(Usb_Rf_power);
          Usb [2,9 ] := IntToStr(Usb_Rf_cmd_retry_num);
          Usb [2,10] := IntToStr(Usb_Rf_speed);
          Usb [2,11] := IntToStr(Last_rssi);
          Usb [2,12]:= IntToStr(Last_lqi);
          Usb [2,13]:= IntToStr(uart_Timeout);
          Usb [2,14]:= Format('0x%x',[pUsb_rf^]);
          WRmUsb;                                       // write output to file
          DebugLog(CTWR_Usb_Info,CSuccess,TWR_RESULT);  // and write to log
          end
      else    DebugLog(CTWR_Usb_Info,Cerror,TWR_RESULT);
end;

    { Writes content of mUsb TMemo into file as 
      it was in memo and also writes data in *.csv file
      as transposed table  - \data\usbtb.csv}
    
PROCEDURE TfMain.WRmUsb;
var i:Integer;
    block:AnsiString;
begin
  mUsb.Lines.Add('--');
  for i:=0 to 14 
        do
          begin
          mUsb.Lines.Add(
                          pad(Usb[1,i],' ','r',24)
                          +'='+
                          pad(Usb[2,i],' ','l',10)
                          );
          block:=   block+
                    pad(
                        trim(Usb[2,i])
                        ,' '
                        ,'l'
                        ,length(trim(Usb[2,i]))+2
                        )
                    +',';
          end;
  mUsb.Lines.SaveToFile(UsbLog);
  mUsbTbl.Lines.append(block);
  mUsbTbl.Lines.SaveToFile(UsbTbl);
end;

    {formats UsbTbl memo for first use, if it does not exists}

PROCEDURE TfMain.FmtUsbTbl;
var i:Integer;
    spom:AnsiString;
begin
  spom:='';
  for i:=0 to 14 
      do  spom:=spom+pad(trim(Usb[1,i]),' ','l',length(trim(Usb[1,i]))+2)+',';
  mUsbTbl.Lines.Add(spom);
  mUsbTbl.Lines.SaveToFile(UsbTbl);
end;

//******************************************************************************
//    TWR 
//******************************************************************************


    {calls Info Twr procedure, checks time offset}

PROCEDURE TfMain.InfoTwr;
begin
  time:=0;
  sTimer.Enabled := False;
  TWR_RESULT :=                       // call TWR_GetInfo
                Twr_GetInfo(pUsb_rf,
                            PInteger(StrToInt(eTwrId.Text))
                            );
  fMain.Cursor:=crhourGlass;

  if TWR_RESULT = TWR_OK              // if OK, poll MainLoop to get result
      then
          begin
          DebugLog(CTWR_GetInfo,CSuccess,TWR_RESULT);
          LOOP_Result := Twr_WaitMainLoop_PB (1,3);
          if LOOP_Result > 0
              then
                  begin
                  DebugLog(CTWR_MainLoop,CTimeout,Loop_Result);;
                  Exit;
                  end
              else    DebugLog(CTWR_MainLoop,Csuccess,Loop_Result);
          end;

  TWR_RESULT :=                      // now take results from dll buffer
                Twr_GetInfo_getResult(pUsb_rf,
                                      @hw_type,
                                      @hw_ver,
                                      @fw_ver,
                                      @ser_nr,
                                      @System_ID,
                                      @mode, //  new
                                      @Time,
                                      @battery_Status);

      if TWR_RESULT = TWR_OK             // and put them into TWR string array
      then
          begin
          inc(TwrCount);
          diff :=getdiff(Time,Now);
          Twr [2,0 ] := FormatDateTime('dd.mm.yyyy HH:mm:ss',Now);
          Twr [2,1 ] := IntToStr(TwrCount);
          Twr [2,2 ] := IntToStr(hw_type);
          Twr [2,3 ] := IntToStr(hw_ver);
          Twr [2,4 ] := IntToStr(fw_ver);
          Twr [2,5 ] := AnsiString(ser_nr);
          Twr [2,6 ] := FormatDateTime('dd.MM.yyyy', UnixToDateTime(Time));
          Twr [2,7 ] := FormatDateTime('HH:mm:ss', UnixToDateTime(Time));
          Twr [2,8 ] := IntToStr(battery_Status);
          Twr [2,9 ] := IntToStr(diff);
          Twr [2,10 ]:= Format('0x%x',[pUsb_rf^]);
          Twr [2,11 ]:= IntToStr(mode);
          wrMTwr;
          dtpTwrDate.DateTime:=UnixToDateTime(Time);
          DebugLog(CTWR_GetInfo_GetResult,Csuccess,TWR_RESULT);
          labdiff.Caption:=IntToStr(diff);

          if abs(diff)>3600        // if time difference is too big, warning
              then
                  begin
                  labTimewarn.Caption:=' Need to accurate TWR_SDK clock !';
                  labTimewarn.Font.Color:=clred;
                  DebugLog(CTWR_GetInfo_GetResult,CTime_delta_big,TWR_RESULT);
                  end
              else
                  begin
                  labTimewarn.Caption:=' No need to sync Time ! (diff < 1 min)';
                  labTimewarn.Font.Color:=clblack;
                  DebugLog(CTWR_GetInfo_GetResult,CTime_delta_OK,TWR_RESULT);
                  end;
          
          end

      else DebugLog(CTWR_GetInfo_GetResult,CError,TWR_RESULT);

  fMain.Cursor:=crDefault;
  sTimer.Enabled := SystemRun;
end;

PROCEDURE TfMain.ExtendedInfo;
begin
  Extime:=0;
  sTimer.Enabled := False;
  TWR_RESULT :=                       // call TWR_GetInfo
                Twr_GetInfo(pUsb_rf,
                            PInteger(StrToInt(eTwrId.Text))
                            );
  fMain.Cursor:=crhourGlass;

  if TWR_RESULT = TWR_OK              // if OK, poll MainLoop to get result
      then
          begin
          DebugLog(CTWR_GetInfo_Extended,CSuccess,TWR_RESULT);
          LOOP_Result := Twr_WaitMainLoop_PB (1,3);
          if LOOP_Result > 0
              then
                  begin
                  DebugLog(CTWR_MainLoop,CTimeout,Loop_Result);;
                  Exit;
                  end
              else    DebugLog(CTWR_MainLoop,Csuccess,Loop_Result);
          end;

  TWR_RESULT :=                      // now take results from dll buffer
                TWR_GetInfo_getExtendedResult
                                              (pUsb_rf,
                                              @hw_type,
                                              @hw_ver,
                                              @fw_ver,
                                              @twr_sn,
                                              @System_ID,
                                              @mode, //  new
                                              @exTime,
                                          @cycle_counter,    
                                          @optical_counter,  
                                          @wake_up_counter,  
                                          @upload_counter,   
                                          @download_counter, 
                                          @log_size,         
                                          @twr_rf_power,     
                                          @twr_rf_rssi,      
                                          @twr_rf_lqi,       
                                          @usb_rf_power_ext,     
                                          @usb_rf_rssi,      
                                          @usb_rf_lqi);       
  if TWR_RESULT = TWR_OK             // and put them into TWR string array
      then
          begin
          Ext [2,0 ] := FormatDateTime('dd.mm.yyyy HH:mm:ss',Now);
          Ext [2,1 ] := Format('0x%x',[pUsb_rf^]);
          Ext [2,2 ] := IntToStr(hw_type);
          Ext [2,3 ] := IntToStr(hw_ver);
          Ext [2,4 ] := IntToStr(fw_ver);
          Ext [2,5 ] := AnsiString(twr_sn);
          Ext [2,6 ] := IntToStr(mode);
          Ext [2,7 ] := FormatDateTime('dd.MM.yyyy HH:mm:ss', UnixToDateTime(exTime));
          Ext [2,8 ] := inttostr(cycle_counter);   
          Ext [2,9 ] := inttostr(optical_counter); 
          Ext [2,10 ]:= inttostr(wake_up_counter); 
          Ext [2,11 ]:= inttostr(upload_counter);  
          Ext [2,12 ]:= inttostr(download_counter);
          Ext [2,13 ]:= inttostr(log_size);        
          Ext [2,14 ]:= inttostr(twr_rf_power);    
          Ext [2,15 ]:= inttostr(twr_rf_rssi);     
          Ext [2,16 ]:= format('0x%x',[twr_rf_lqi]);      
          Ext [2,17 ]:= inttostr(usb_rf_power);    
          Ext [2,18 ]:= inttostr(usb_rf_rssi);     
          Ext [2,19 ]:= inttostr(usb_rf_lqi);     
          WrmExt;
          DebugLog(CTWR_GetInfo_GetResult,CSuccess,TWR_RESULT);
          end
          
      else DebugLog(CTWR_GetInfo_GetResult,CError,TWR_RESULT);

  fMain.Cursor:=crDefault;
  sTimer.Enabled := SystemRun;
end;




    {change password for TWR }
  
PROCEDURE TfMain.changePass;
var newpass: AnsiString;
begin
  GlobalPass := AnsiString(eTwrOldPass.text);
  NewPass   :=  AnsiString(eTwrNewPass.text);
  if trim(NewPass)='' then NewPass:='1111';
  sTimer.Enabled := False;
  TWR_RESULT :=                     // call Twr_SetPassword
                Twr_SetPassword(pUsb_rf,
                                PInteger(StrToInt(eTwrId.Text)),
                                PAnsiChar(globalPass),
                                PAnsiChar(NewPass));
  if TWR_RESULT = TWR_OK           // if Ok, poll MainLoop to get exec status
    then
        begin
        DebugLog(CTWR_SetPassword, CSuccess, TWR_Result); 
        //LOOP_Result := Twr_WaitMainLoop;
          LOOP_Result := Twr_WaitMainLoop_PB(10,5);

        if LOOP_Result > 0 
          then
              begin
              DebugLog(CTWR_MainLoop,CTimeout,Loop_Result);
              Exit;
              end
          else    DebugLog(CTWR_MainLoop,CSuccess,Loop_Result);
        end;

  TWR_RESULT := Twr_SetPassword_getExecStatus(pUsb_rf);
  if TWR_RESULT = TWR_OK 
    then
        begin
        DebugLog(CTWR_SetPassword_getExecStatus, CSuccess, TWR_Result);
        GlobalPass:= eTwrNewPass.Text; 
        eTwrPass.Text:=GlobalPass;
        end
    else 
        begin
        DebugLog(CTWR_SetPassword_getExecStatus, CError, TWR_Result);
        GlobalPass:=eTwrOldPass.Text;      
        end;
  writeini;                       // write new password to ini file for safety
  sTimer.Enabled      := SystemRun;       
end;


    {change time at TWR! Be careful, always is about UTC time}
   
PROCEDURE TfMain.changeTime;
begin

  sTimer.Enabled := False;
  TWR_RESULT := Twr_SetTime
                            (
                            pUsb_rf,
                            PInteger(StrToInt(eTwrId.Text)),
                            PAnsiChar(GlobalPass),
                            PInteger(DateTimeToUnix(dtpTwrDate.DateTime))
                            );
  if TWR_RESULT = TWR_OK then
      begin
      DebugLog(CTWR_SetTime,CSuccess,TWR_Result); 
      //LOOP_Result := Twr_WaitMainLoop;
      LOOP_Result:=Twr_WaitMainLoop_PB(10,5);
      if LOOP_Result > 0
          then
              begin
              DebugLog(CTWR_MainLoop,Ctimeout,LOOP_result);
              Exit;
              end
          else    DebugLog(CTWR_MainLoop,Csuccess,LOOP_result);
      end;
      
  TWR_RESULT := Twr_SetTime_getExecStatus(pUsb_rf);
 
  if TWR_RESULT = TWR_OK  then DebugLog(CTwr_SetTime_getExecStatus,CSuccess,TWR_Result) 
                          else DebugLog(CTwr_SetTime_getExecStatus,CError,TWR_Result);      
  sTimer.Enabled := SystemRun; 
end;


    {Writes content of mTwr TMemo into file as 
      it was in memo and also writes data in *.csv file
      as transposed table  - \data\twrtb.csv}
 
PROCEDURE TfMain.wrmTwr;
var i:Integer;
    block:AnsiString;
begin

  mTwr.Lines.Add('--');
  for i:=0 to 11 do
              begin
              mTwr.Lines.Add(
                              pad(Twr[1,i],' ','r',24)+'='+
                              pad(Twr[2,i],' ','l',10)
                              );
              block:=block+
                        pad(trim(Twr[2,i]),' ','l',length(trim(Twr[2,i]))+2)
                        +',';
              end;
  mTwr.Lines.SaveToFile(TwrLog);
  mTwrTbl.Lines.append(block);
  mTwrTbl.Lines.SaveToFile(TwrTbl);
end;

PROCEDURE TfMain.wrmExt;
var i:Integer;
    block:AnsiString;
begin

 
  for i:=0 to 19 do
              begin
             
              block:=block+
                        pad(trim(Ext[2,i]),' ','l',length(trim(Ext[2,i]))+2)
                        +',';
              end;

  mExtTbl.Lines.append(block);
  mExtTbl.Lines.SaveToFile(ExtTbl);
end;

    { formats mTwr memo for first use}

PROCEDURE TfMain.FmtTwrTbl;
var i:Integer;
    spom:AnsiString;
begin
  spom:='';
  for i:=0 to 11 do 
        spom:=spom+pad(trim(Twr[1,i]),' ','l',length(trim(Twr[1,i]))+2)+',';
  mTwrTbl.Lines.Add(spom);
  mTwrTbl.Lines.SaveToFile(TwrTbl);
  //
end;

Procedure TfMain.FmtExt;
var i:Integer;
    spom:AnsiString;
begin
  spom:='';
  mExtTbl.Lines.Clear;
  for i:=0 to 19 do
        spom:=spom+pad(trim(Ext[1,i]),' ','l',length(trim(Ext[1,i]))+2)+',';
  mExtTbl.Lines.Add(spom);
  mExtTbl.Lines.SaveToFile(ExtTbl);
  //
end;



//******************************************************************************
//     LOG
//******************************************************************************


    {get whole log procedure}
 
PROCEDURE TfMain.Logall;
var id     :Integer;
       Pass:AnsiString;
begin
  LOOP_Result := 99;
  TryStrToInt(eLogID.Text,id);
  Pass:=AnsiString(trim(eLogPass.Text));
  sTimer.Enabled := False;
  TWR_RESULT := Twr_ClearLog();
  TWR_RESULT := Twr_GetLog(
                            pUsb_rf,
                            PInteger(ID),
                            PAnsiChar(Pass)
                            );
   if TWR_RESULT = TWR_OK
          then
              begin
              DebugLog(CTWR_GetLog,CSuccess,TWR_Result);
              LOOP_Result :=    Twr_WaitMainLoop_PB(10,5);
                              //Twr_WaitMainLoop;
              end
          else    DebugLog(CTWR_GetLog,CError,TWR_Result);

   if LOOP_Result > 0
          then
              begin
              DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
              Exit;
              end
          else    DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);

   TWR_RESULT := Twr_GetLog_getExecStatus(pUsb_rf);

   if TWR_RESULT = TWR_OK then DebugLog(CTWR_GetLog_GetExecStatus,CSuccess,TWR_Result)
                          else DebugLog(CTWR_GetLog_GetExecStatus,CError,TWR_Result);

   Init_CardLog;
   ReadLog;
   sTimer.Enabled := SystemRun;
end;


    {get logByIndex }
 
PROCEDURE TfMain.LogByIndex;
var Pass:AnsiString;
    id,first,Last:Integer;
begin
  LOOP_Result := 99;
  TryStrToInt(eLogID.Text,id);
  TryStrToInt(eLogID1.Text,first);
  TryStrToInt(eLogID2.Text,Last);
  Pass:=AnsiString(trim(eLogPass.Text));
  sTimer.Enabled := False;
  TWR_RESULT := Twr_ClearLog();
  TWR_RESULT := Twr_GetLogByIndex(
                            pUsb_rf,
                            PInteger(ID),
                            PAnsiChar(Pass),
                            PInteger(First),
                            PInteger(Last)
                            );
   if TWR_RESULT = TWR_OK
        then
            begin
            DebugLog(CTWR_GetLogByIndex,CSuccess,TWR_Result);
            LOOP_Result :=  Twr_WaitMainLoop_PB(10,10);
                          //Twr_WaitMainLoop;
            end
        else    DebugLog(CTWR_GetLogByIndex,CError,TWR_Result);

   if LOOP_Result > 0
        then
            begin
            DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
            Exit;
            end
        else    DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);

   TWR_RESULT := Twr_GetLog_getExecStatus(pUsb_rf);

   if TWR_RESULT = TWR_OK then DebugLog(CTWR_GetLog_GetExecStatus,CSuccess,TWR_Result)
                          else DebugLog(CTWR_GetLog_GetExecStatus,CError,TWR_Result);

   Init_CardLog;
   READLog;
   sTimer.Enabled := SystemRun;
end;


    {getLogByTime}
 
PROCEDURE TfMain.LogByTime;
var Pass:AnsiString;
    id:Integer;
    startUTCTime,endUTCTime:longInt;
begin
  MakeTime;
  LOOP_Result := 99;
  TryStrToInt(eLogID.Text,id);
  startUTCTime:=DateTimeToUnix(StartTime);
  endUTCTime:=DateTimeToUnix(EndTime);
  Pass:=AnsiString(trim(eLogPass.Text));
  sTimer.Enabled := False;
  TWR_RESULT := Twr_ClearLog();
  TWR_RESULT := Twr_GetLogByTime(
                            pUsb_rf,
                            PInteger(ID),
                            PAnsiChar(Pass),
                            PInteger(StartUTCTime),
                            PInteger(EndUTCTime)
                            );
   if TWR_RESULT = TWR_OK
       then
          begin
          DebugLog(CTWR_GetLogByTime,CSuccess,TWR_Result);
          LOOP_Result :=   Twr_WaitMainLoop_PB(10,5);
                          //Twr_WaitMainLoop;
          end
       else    DebugLog(CTWR_GetLogByIndex,CError,TWR_Result);

   if LOOP_Result > 0
       then
          begin
          DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
          Exit;
          end
       else    DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);

   TWR_RESULT := Twr_GetLog_getExecStatus(pUsb_rf);

   if TWR_RESULT = TWR_OK then DebugLog(CTWR_GetLog_GetExecStatus,CSuccess,TWR_Result)
                          else DebugLog(CTWR_GetLog_GetExecStatus,CError,TWR_Result);
   Init_CardLog;
   READLog;
   sTimer.Enabled := SystemRun;
end;

     {Read Log events from dll buffer }

PROCEDURE TfMain.READLog;
var 
  Log_idx                     : Integer;
  Log_Action                  : Integer;
  Log_reader_id               : Integer;
  Log_Card_id                 : Integer;
  Log_system_id               : Integer;
  Timestamp                   : Integer;

  mf_uid                  : array [0..7] of byte;
  str_uid,tmp_pom :AnsiString;
  line,tmpuid, direct,datestr,timestr : AnsiString;
begin

   repeat
        begin
        
        TWR_RESULT :=
                      Twr_ReadLog(@Log_idx,
                                  @Log_Action,
                                  @Log_reader_id,
                                  @Log_Card_id,
                                  @Log_system_id,
                                  @Timestamp,
                                  @mf_uid);
         
        tmpuid:=''; 
        str_uid:='';  
        if TWR_RESULT = TWR_OK
              then
                  begin
               
                 

                  if log_reader_id=1 then direct:='IN'
                                     else
                                     
                  case (Log_reader_id mod 2)
                          of
                            1:   direct:='IN';
                            0:   direct:='OUT';
                        end; 
             
                  datestr:= FormatDateTime('dd.mm.yyyy',UnixToDateTime(Timestamp));
                  timestr:= FormatDateTime('HH:mm:ss',UnixToDateTime(Timestamp));
                  line:=format('%4d | %4d | %3d/%3s | %5d | %5d  | %10s | %8s | %22s',
                  [Log_idx,Log_action,Log_reader_id,
                  direct,log_card_id,Log_system_id,datestr,timestr,FMTUID(mf_uid)]);
                 
                        

                  mCARD.Lines.add(line);       // add new line
                  mCARD.SelStart := mCARD.GetTextLen;   // here
                  mCARD.Perform(EM_SCROLLCARET, 0, 0);  // scroll caret to bottom
                
                  mCardTbl.Lines.Add(Stringreplace(line,'|',',',[rfReplaceall]));
                 
                  end
              else
                  begin
                  if TWR_RESULT<>Log_EMPTY then DebugLog(CTWR_ReadLog,CError,TWR_Result);
                  end ;
        end;
   until TWR_RESULT = Log_EMPTY;
   DebugLog(CTWR_ReadLog,CSuccess,TWR_Result);
   mCARDTbl.Lines.SaveToFile(CardLog);
end;

Function TfMain.FMTUID(uid:array of byte):Ansistring;
VAR ln,cnt,tmp_int:integer;
    tmp_pom:Ansistring;
    
begin
result:='';

for cnt:=0 to 6 do
  begin
  tmp_int:=0;
  tmp_pom:='';
  tmp_int:=uid[cnt];
    if tmp_int=0 then tmp_pom:='0'
      else
        begin
          if ((tmp_int<16)) 
                then 
                  tmp_pom:=rightstr('00'+Format('%x',[tmp_int]),2) 
                else 
                  tmp_pom:= Format('%x',[tmp_int]);
        end;
  if cnt=0 then result:=tmp_pom
            else result:=result+':'+tmp_pom;
  end;
 
 

end;

    {prepend prefix to .csv file depending on action taken : whole log, index log, time log;
      filename is integer value of unix time at the moment of file creation}
 
PROCEDURE TfMain.Init_CardLog;
var pref:AnsiString;
begin
    case btntag 
        of
          0 :   pref:='';
          21:  pref:='all_';
          22:  pref:='idx_';
          23:  pref:='Time_';
    end;
  mCARD.Lines.Clear;
  mCardTbl.Lines.Clear;
  CardLog:=dataPath+pref+'Cards_'+IntToStr(DateTimeToUnix(Now))+'.csv';
end;

    {creates time from DateTimePicker and MaskEdit components
      on Log tabsheet, so query for GetLogByTime is always correct}
  
PROCEDURE TfMain.MakeTime;
begin
  StartTime:= dtpStart.DateTime+StrToTime(meStart.Text);
  EndTime  := dtpEnd.DateTime+StrToTime(meEnd.Text);
end;


//******************************************************************************
//      RTE
//******************************************************************************


    {prepend prefix to .csv file for RTE log;
      filename is integer value of unix time at the moment of file creation}
  
PROCEDURE TfMain.Init_rteLog;
begin
  mRte.Lines.Clear;
  mRteTbl.Lines.Clear;
  rteLog:=dataPath+'rte_'+IntToStr(DateTimeToUnix(Now))+'.csv';
end;

    {Start/Stop RTE ; 'SystemRun' is flag if RTE is running or not}
    
PROCEDURE TfMain.Rte;
begin
  lRteAction.Caption:='';
  if sTimer.Enabled
     then
         begin
         mInd.Color:=clRed;
         mInd.Lines.append('Status : STOPPED');
         bRte.Caption := 'START REAL TIME EVENT MONITORING';
         sTimer.Enabled := False;
         SystemRun := False;
         DebugLog(CRTE,CStopped,CActionOk);
         end
     else
         begin
         Init_rteLog;
         mInd.Color:=clLime;
         mInd.Lines.append('Status : STARTED');
         bRte.Caption := 'STOP REAL TIME EVENT MONITORING';
         sTimer.Enabled := True;
         SystemRun := True;
         DebugLog(CRTE,CStarted,CActionOk);
         end;
end;


//******************************************************************************
//    BlackList
//******************************************************************************


    { block card, e.g. add it to blacklist}
  
PROCEDURE TfMain.block_Card(Card:String);
begin
  sTimer.Enabled := False;
  TWR_RESULT :=
                Twr_BlackList_Block(pUsb_rf,
                                PInteger(StrToInt(eTwrId.Text)),
                                PAnsiChar(AnsiString(eTwrPass.Text)),
                                PAnsiChar(Card));
  if TWR_RESULT = TWR_OK then
      begin
      DebugLog(CTWR_BlackList_Block,CSuccess,TWR_Result);
      LOOP_Result :=   Twr_WaitMainLoop_PB(5,5);
                     // Twr_WaitMainLoop;
      if LOOP_Result > 0 
          then
              begin
              DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
              Exit;
              end
          else      DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);
      end;

  TWR_RESULT := Twr_BlackList_getExecStatus(pUsb_rf);
  if TWR_RESULT = TWR_OK 
      then  DebugLog(CTWR_BlackList_GetExecStatus,CSuccess,TWR_Result)
      else  DebugLog(CTWR_BlackList_GetExecStatus,CError,TWR_Result);
      
  sTimer.Enabled := SystemRun;
end;

    { UnBlock card, e.g. remove it from blacklist}
 
PROCEDURE TfMain.Unblock_Card (Card:String);
begin
  sTimer.Enabled := False;
  TWR_RESULT :=
                Twr_BlackList_Unblock(pUsb_rf,
                                PInteger(StrToInt(eTwrId.Text)),
                                PAnsiChar(Ansistring(eTwrPass.Text)),
                                PAnsiChar(trim(Card)));
  if TWR_RESULT = TWR_OK then
      begin
      DebugLog(CTWR_BlackList_UnBlock,CSuccess,TWR_Result);
      LOOP_Result := Twr_WaitMainLoop_PB(5,5);
                      //Twr_WaitMainLoop;
      if LOOP_Result > 0 
          then
              begin
              DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
              Exit;
              end
          else    DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);
      end;

  TWR_RESULT := Twr_BlackList_getExecStatus(pUsb_rf);
  if TWR_RESULT = TWR_OK 
      then  DebugLog(CTWR_BlackList_GetExecStatus,CSuccess,TWR_Result)
      else  DebugLog(CTWR_BlackList_GetExecStatus,CError,TWR_Result);
      
   sTimer.Enabled := SystemRun;
end;

    {Read Blacklist from TWR}
 
PROCEDURE TfMain.read_BlackList;
var bl_size :Integer;
    BlackListOut: PAnsiChar;
    bs: AnsiString;
begin
  gbBlackList.Enabled:=True;
  sTimer.Enabled := False;
  TWR_RESULT :=
                Twr_BlackList_Read(pUsb_rf,
                                PInteger(StrToInt(eTwrId.Text)),
                                PAnsiChar(AnsiString(eTwrPass.Text)));
  if TWR_RESULT = TWR_OK 
      then
          begin
          DebugLog(CTWR_BlackList_Read,CSuccess,TWR_Result);
          LOOP_Result := Twr_WaitMainLoop_PB(10,5);
                          //Twr_WaitMainLoop;
          if LOOP_Result > 0 
              then
                  begin
                  DebugLog(CTWR_MainLoop,CTimeout,LOOP_Result);
                  Exit;
                  end
              else   DebugLog(CTWR_MainLoop,CSuccess,LOOP_Result);

          end;
  Application.ProcessMessages;
  TWR_RESULT := Twr_BlackList_getExecStatus(pUsb_rf);
  if TWR_RESULT = TWR_OK 
      then  DebugLog(CTWR_BlackList_GetExecStatus,CSuccess,TWR_Result)
      else  DebugLog(CTWR_BlackList_GetExecStatus,CError,TWR_Result);
    
  sTimer.Enabled := SystemRun;

  bs:='';
  bl_size:=Twr_BlackList_getSize(pUsb_rf);

  BlackListOut:=nil;
  getmem(BlackListOut,bl_size+1);
  Application.ProcessMessages;
  TWR_RESULT :=
                Twr_BlackList_getResult
                        (pUsb_rf,
                        BlackListOut);
  if TWR_RESULT = TWR_OK 
      then
            begin
            DebugLog(CTWR_BlackList_GetResult, CSuccess, TWR_Result);
            bs:=AnsiString(BlackListOut);
            mBlackList.Lines.Delimiter:=',';
            mblackList.Lines.DelimitedText:=bs;
            end
      else    DebugLog(CTWR_BlackList_GetResult, CError, TWR_Result);                      

end;

    {Write data from memo to TWR blacklist. Be careful: this way you are going to 
      erase all previous data int TWR blacklist, 
      meaning that actual data is what you are writing now!
      So, old blacklist is erased with new blacklist data! 
      First row of CSV file can be "TWR_BLACKLIST"; also, you can write raw data 
      separated with comma; TWR automatically  adds first row (header) in list.}
  
PROCEDURE TfMain.WriteBlackList;
var
    BlackListIn:pAnsiChar;
    bs:AnsiString;
begin
  bs:=mBlackList.Lines.DelimitedText;
  BlackListIn:=pAnsiChar(bs);
  sTimer.Enabled := False;
  TWR_RESULT :=
                Twr_BlackList_EraseWrite(pUsb_rf,
                                PInteger(StrToInt(eTwrId.Text)),
                                PAnsiChar(AnsiString(eTwrPass.Text)),
                                BlackListIn);
  if TWR_RESULT = TWR_OK 
      then
          begin
          DebugLog(CTWR_BlackList_EraseWrite, CSuccess, TWR_Result);
          LOOP_Result := Twr_WaitMainLoop_PB(10,5);
                          //Twr_WaitMainLoop;
          if LOOP_Result > 0 
              then
                  begin
                  DebugLog(CTWR_MainLoop, CTimeout, LOOP_Result);
                  Exit;
                  end
              else    DebugLog(CTWR_MainLoop, CSuccess, LOOP_Result);
          end;
  Application.ProcessMessages;
  TWR_RESULT := Twr_BlackList_getExecStatus(pUsb_rf);
  if TWR_RESULT = TWR_OK 
      then    DebugLog(CTWR_BlackList_GetExecStatus,CSuccess,TWR_Result)
      else    DebugLog(CTWR_BlackList_GetExecStatus,CError,TWR_Result);

   sTimer.Enabled := SystemRun;
end;

//******************************************************************************
//    Secure data in USB EEPROM - SecData
//******************************************************************************


      {read Secure Data written in EEPROM of USB RF;
        no password needed for that action; 
        common usage is to write your own data as some sort of key or 
        protection, so you can bind that key with your app or something.}

PROCEDURE TfMain.ReadSecData;
Var
  size, i :Integer;
  data : array [1..32] of AnsiChar;
  secData: AnsiString;
begin
  size:=32;
  for i:=1 to size do data[i]:=chr(0);
  TWR_RESULT:= SecData_ReadData(pUsb_rf,@data,size);
  if TWR_RESULT=TWR_OK
      then
          begin
          secData:='';
          setlength(secdata,size);            
          DebugLog(CSecData_ReadData,CSuccess,TWR_Result);
          secData:=AnsiString(data);
          eSecDataOld.Text:=secData;
          end
      else    DebugLog(CSecData_ReadData,CError,TWR_Result);

end;

    {change Pass for Secure Data access; 
    don't mess with TWR pass, it's totally different;
    Pass for Secure Data must be 8 bytes long; default is 8 x FF ( 8 times FF) }

PROCEDURE TfMain.SecPassChange;
var 
    OldStr, NewStr: AnsiString;
begin
  SetLength(OldStr,8);
  SetLength(NewStr,8);
  OldStr:=AnsiString(trim(eOldSec.Text));
  NewStr:=AnsiString(trim(eNewSec.Text));

  TWR_RESULT:=SecData_PassChange
                  (
                    pUsb_rf,
                    PAnsiChar(OldStr),
                    PAnsiChar(NewStr)
                  );
  if TWR_RESULT=TWR_OK 
      then  begin
            DebugLog(CSecData_PassChange,CSuccess,TWR_Result);
            SecOld:=AnsiString(OldStr);
            SecNew:=AnsiString(NewStr);
            WriteIni;
            end
      else  DebugLog(CSecData_PassChange,CError,TWR_Result);
end;

    {write SecData to USB EEPROM; 
      limitations: SecPass must be 8 chars long!
                    SecData max length is 32 bytes!
                    Number of writes/attempts to EEPROM
                      is about 100k times, so be careful
                      about wasting that amount!
                      
      Recommendation: 
      Always try to read and write whole array of 32 bytes in EEPROM. 
      Example : if you write 10 bytes in EEPROM and read whole
      space of 32 bytes after that, you will notice that bytes
      from position 11 to position 32 are values of 
      dec. 255 - hex. 0xFF  char - '�'    ;
      that char is not common on any keyboard, so it can not be enter.
      Simply, read the whole string of 32 bytes;
      first occasion of '�' ( chr(255) ) is also the end of your data.
      AnsiString usage is intentionally here for compatibility with other
      Delphi platforms.}
                              


PROCEDURE TfMain.WriteSecData;
var 
    d,ps,ppom : AnsiString;
    i         : Integer;
begin
  d:= trim(eSecDataNew.Text);
  if length(d)>32 then setlength(d,32);
  if trim(eNewSec.text)<>'' then ps:=AnsiString(eNewSec.Text)
                            else ps:=AnsiString(eOldSec.Text);
  ppom:='';
  setlength(ppom,32);
  for i:=1 to 32 do ppom[i]:=chr(0);   // make array empty with null char
  d:=leftstr(d+ppom,32);   // cut 32 chars from left side
  setlength(d,32);         // make sure it is 32 bytes long
  TWR_RESULT:=SecData_WriteData         // write it to EEPROM
                (
                  pUsb_rf,
                  pansichar(d),
                  length(d),        // should always be 32
                  pansichar(ps)
                );

  if TWR_RESULT=TWR_OK
      then    DebugLog(CSecData_WriteData,CSuccess,TWR_Result)
      else    DebugLog(CSecData_WriteData,CError,TWR_Result);             
end;

//******************************************************************************
//      END
//******************************************************************************

function tFmain.TWR_WaitMainLoop_PB(wait_time:integer;to_seq:integer): Integer;
var
  cmdResponses                : Integer;
  RealTimeEvents              : Integer;
  LogAvailable                : Integer;
  TimeoutOccurred             : Integer;
  TimeoutEvent                : Integer;
  progress                    : Integer;
  sad,posle                   : TDatetime;
  razlika                     : integer;

begin
  progress:=0;
  Result := 40;         // put result on Error until get real result
  TimeoutEvent := 0;
  cmdResponses := 0;
  sad:=now;
  lcnt.caption:='';
  lprg.caption:='';
//  pb1.position:=0;

  //Application.ProcessMessages;
  
  while cmdResponses = 0 do
  begin
    Application.ProcessMessages;
    TWR_MainLoop(@cmdResponses, @RealTimeEvents, @LogAvailable, @TimeoutOccurred);
    TimeoutEvent := TimeoutEvent + to_seq;
    if TimeoutEvent > 30000 then      // if timeout is more than 30 secs
    begin
      Result := 41;                   // result TIMEOUT
      Exit;
    end;
    inc(progress); 
    
    Application.ProcessMessages;

    if (progress mod 16)=0 
          then lprg.Caption:=''
          else
    //if (progress mod 2)=0   then     
    lprg.Caption:=lprg.Caption+'|'; 
    //Application.ProcessMessages;
    
    Sleep(Wait_time);  // Wait a little (best effort < 10 msec)
  end;
  posle:=now;
  razlika:=millisecondsbetween(sad,posle);
  lcnt.Caption:=inttostr(razlika)+' ms';
  lprg.caption:='';
  DebugLog(CtimedoEvents,razlika,43);
  Result := 0;                        // Else, TWR_OK
end;


procedure TfMain.btExtClick(Sender: TObject);
begin
AppLog(sender);
extendedInfo;
If not assigned(FextInfo) then FextInfo:=TFextInfo.Create(application);
FExtInfo.ShowModal;

end;


procedure TfMain.bUSBOpenClick(Sender: TObject);
begin
AppLog(sender);
OpenUsb;    // Open USB_RF
if usb_status then bUSBOpen.Enabled:=false
              else bUSBOpen.Enabled:=true;

end;


procedure TfMain.bUSBInfoClick(Sender: TObject);
begin
AppLog(sender);
InfoUsb;   //  Get USB INFO

end;


procedure TfMain.bUSBRestartClick(Sender: TObject);
begin
AppLog(sender);
RstUsb;   // Restart USB_RR

end;


procedure TfMain.bUSBCloseClick(Sender: TObject);
begin
AppLog(sender);
CloseUsb;    // Close USB_RF
if not usb_status then bUSBOpen.Enabled:=true
              else bUSBOpen.Enabled:=false;

end;


procedure TfMain.bTWRInfoClick(Sender: TObject);
begin
AppLog(sender);
InfoTwr;     // Get TWR info

end;


procedure TfMain.BtwrCHPassClick(Sender: TObject);
begin
AppLog(sender);
Show_Pass_Dlg ;      // Start Change password  and show pass dlg
end;


procedure TfMain.bTWRPassClick(Sender: TObject);
begin
AppLog(sender);
       //SUBMIT PASS  - Change password and Hide pass Dlg
              if    (
                    (trim(eTwrNewPass.text)<>'')
                          and
                    (trim(eTwrAgain.text)<>'')
                          and
                    (trim(eTwrNewPass.text)=trim(eTwrAgain.text))
                    ) 
                   then begin
                        ChangePass;
                        Hide_Pass_Dlg;
                        end
                   else begin
                        ShowMessage('Password can not be empty!');
                        eTwrNewPass.SetFocus;
                        end;       
              end;


              
procedure TfMain.bCNCpassClick(Sender: TObject);
begin
//CANCEL PASS - Do nothing and Hide pass dlg
AppLog(sender);
              Hide_Pass_Dlg;
              eTwrOLDPass.Text      :=eTwrPass.Text;
              eTwrNewPass.Text      :='';
              eTwrAgain.Text        :='';
end;


procedure TfMain.bTWRChTimeClick(Sender: TObject);
begin
AppLog(sender);
Show_Time_Dlg;    // Start Change time and  show time dlg
end;


procedure TfMain.bTWRTimeClick(Sender: TObject);
begin
// Submit change time and Hide Time Dlg
              AppLog(sender);
              changeTime;
              Hide_Time_Dlg;
end;


procedure TfMain.bCncTimeClick(Sender: TObject);
begin
AppLog(sender);
Hide_Time_Dlg;     // Cancel change time  -  Do Nothing and Hide Time Dlg

end;


procedure TfMain.bLogALLClick(Sender: TObject);
begin
btntag:=(sender as tbutton).tag;
AppLog(Sender);
Logall;

end;


procedure TfMain.bLogIdxClick(Sender: TObject);
begin
btntag:=(sender as tbutton).tag;
AppLog(Sender);
LogByIndex;

end;


procedure TfMain.bLogTimeClick(Sender: TObject);
begin
btntag:=(sender as tbutton).tag;
AppLog(Sender);
if CheckTime(mestart.Text,meend.Text) 
                  then LogbyTime
                  else meStart.SetFocus;

end;


procedure TfMain.bRTEClick(Sender: TObject);
begin
AppLog(sender);
RTE;         // Start/Stop RTE monitoring

end;


procedure TfMain.bReadBlacklistClick(Sender: TObject);
begin
AppLog(sender);
Read_BlackList;         // REad Blacklist

end;


procedure TfMain.bBlockClick(Sender: TObject);
begin
AppLog(sender);
        // Check CardNo. and Block Card if CardNo. OK
                if CheckCardNo(eblock.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlock.SetFocus;
                        end
                    else
                        begin
                            if searchText(mBlackList,eblock.Text,[soFromStart]) 
                                then
                                    begin
                                    showmessage('Already in list!');
                                    eblock.SetFocus;
                                    end
                                else block_Card(trim(eblock.Text));
                        end;
             
end;


procedure TfMain.bUnblockClick(Sender: TObject);
begin
AppLog(sender);
    // Check CardNo. and UnBlock Card if CardNo. OK
                if CheckCardNo(eblock.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlock.SetFocus;
                        end
                    else
                        begin
                        if searchText(mBlackList,eblock.Text,[soFromStart])=False
                           then
                               begin
                               showmessage('Card NOT in list!');
                               eblock.SetFocus;
                               end
                           else  Unblock_Card(trim(eblock.Text));
                        end;
end;


procedure TfMain.bAddClick(Sender: TObject);
begin
AppLog(sender);
  //Check CardNo. and ADD to list if CardNo. OK
                if CheckCardNo(eBlist.Text)=False
                    then
                        begin
                        showmessage('Enter Card No. (1-65534)!');
                        eBlist.SetFocus;
                        end
                    else
                        begin
                        if searchText(mBlackList,eblist.Text,[soFromStart]) 
                           then
                                begin
                                showmessage('Already in list!');
                                eblist.SetFocus;
                                end
                           else
                                mBlackList.Lines.DelimitedText:=
                                  mBlackList.Lines.DelimitedText+','+eblist.Text;
                        end;
end;


procedure TfMain.bClearBLClick(Sender: TObject);
begin
AppLog(sender);
  // Clear Working List
              mBlackList.Lines.Clear;
              mBlackList.Lines.DelimitedText:='TWR_BLACKLIST,';
end;


procedure TfMain.bWriteBLClick(Sender: TObject);
begin
AppLog(sender);
WriteBlackList;   // Write Blacklist to TWR
end;


procedure TfMain.bExportBLClick(Sender: TObject);
begin
AppLog(sender);
 // Open SaveDialog and save working list  to .csv
              if SvDlg.Execute 
                  then
                    mBlackList.Lines.SaveToFile(svDlg.FileName);
end;


procedure TfMain.bImportBLClick(Sender: TObject);
begin
AppLog(sender);
    // Open OpenDialog and put .csv list to working list
              MBlackList.Lines.Clear;
              If OpDlg.Execute 
                  then
                    mBlackList.Lines.LoadFromFile(OpDlg.FileName);
end;


procedure TfMain.bChangeSecPassClick(Sender: TObject);
begin
AppLog(sender);
if CheckSecPass(eOldSec.Text,eNewSec.Text)
                  then
                      SecPassChange                 // change SecPass
                  else
                      begin
                      showmessage('Wrong length of Passwords!');
                      eOldSec.SetFocus;
                      end;
end;


procedure TfMain.bDefSecPassClick(Sender: TObject);
begin
AppLog(sender);
eNewSec.Text:='';
eNewSec.Text:=DefSec;            // put default SecPass in EditBox
             
end;


procedure TfMain.bReadSecDataClick(Sender: TObject);
begin
AppLog(sender);
ReadSecData;    //REad SecData from EEPROM

end;


procedure TfMain.bWriteSecDataClick(Sender: TObject);
begin      
AppLog(sender);
WriteSecData;

end;


end.

