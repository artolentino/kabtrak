unit TWRSDK;
  
//******************************************************************************
//  predefined function for calling twr_dll.dll;
//  applies to twr_dll.dll Version 2.1.3.36 and higher!
//
//  static calling method is preferable;
//  
//  'usb_rf' is LongWord type pointer;
//  on first use (TWR_USB_Open) you pass NIL and it returns
//  pointer of DLLs Handle
//******************************************************************************

interface

uses
  SysUtils;

type


  TWR_STATUS = Integer;   // is used as index of status messages,
                          // detailed definitions later in CONST section

//******************************************************************************
// TWR status messages
//******************************************************************************

const
  // NAME                     CODE
  TWR_OK                      = 0;
  TIMEOUT_ERROR               = 1;
  SM_NOT_IDLE                 = 2;
  SM_IDLE__NO_RESPONSE        = 3;
  SM_COMMAND_IN_PROGRESS      = 4;
  FILE_OVERSIZE               = 5;
  FILE_EMPTY                  = 6;
  FILE_LOCKED                 = 7;
  LOG_EMPTY                   = 8;
  NO_RF_PACKET_DATA           = 9;
  TRANSFER_WRITING_ERROR      = 10;
  EVENT_WAKEUP_BUSY           = 11;
  UART_READ_FTDI_ERROR        = 12;
  UART_READ_LESS_DATA         = 13;
  UART_WRITE_FTDI_ERROR       = 14;
  UART_WRITE_LESS_DATA        = 15;
  NO_USB_RF_DEVICES           = 16;
  NO_TWR_DEVICES              = 17;
  CRC_ERROR                   = 18;
  PARAMETERS_ERROR            = 19;
  TIMESTAMP_INVALID           = 20;
  LOG_BUFFER_OVERFLOW         = 21;
  CMD_RESPONSE_UNSUCCESS      = 22;
  CMD_RESPONSE_WRONG_RF_ADR   = 23;
  CMD_RESPONSE_WRONG_CMD      = 24;
  CMD_RESPONSE_SIZE_OVERFLOW  = 25;
  UART_BAD_DATA               = 26;
  MEMORY_ALOC_ERROR           = 27;
  BUFFER_OVERFLOW             = 28;
  RFCMD_SUCCESS_REPORT_FAILED = 29;
  RFCMD_PASSWORD_WRONG        = 30;
  RFCMD_LOG_NODATA            = 31;
  NULL_POINTER                = 32;
  USB_CLOSE_ERROR             = 33;
  USB_RF_ACK_FAILED           = 34;
  RESOURCE_BUSY               = 35;
  MAIN_LOOP_ERROR             = 40;
  MAIN_LOOP_TIMEOUT           = 41;

//******************************************************************************
 
// --- MainLoop function ---

function TWR_WaitMainLoop(): Integer;             // Executing TWR_MainLoop, result is TWR_STATUS_MESSAGE

//  --- D L L   functions ---

function TWR_MainLoop                             // executing MainLoop, expecting response, OUT params, called by previos function
  (
  cmdResponses: PInteger;               // 0 - IDLE, elsewhere command has been executed
  RealTimeEvents: PInteger;             // RTE record exists in DLL buffer
  LogAvailable: PInteger;               // LOG record(s) exists in DLL buffer
  TimeoutOccurred: PInteger             // Timeout occured
  ): TWR_STATUS stdcall;

//******************************************************************************
// --- USB RF Functions ---    usb_rf: Handle as mandatory
//******************************************************************************

function TWR_USB_Open                             // OPEN USB comm  = pass NIL as IN, expect Handle address as OUT
  (
  usb_rf: PLongWord                    // HANDLE USB
  ): TWR_STATUS stdcall;

function TWR_USB_Close                            // CLOSE USB comm
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

function TWR_Restart                              // RESTART USB comm (soft restart)
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

function TWR_USB_Info(                            // Info on USB_RF device
  usb_rf: PLongWord;                    // HANDLE USB
  usb_hw_type: PInteger;                // Hardware type
  usb_hw_ver: PInteger;                 // Hardware version
  usb_fw_ver: PInteger;                 // Firmware version
  usb_fw_sn: PAnsiChar;                 // Serial No. USB tranceiver
  rf_settings_group: PInteger;          // RF settings group
  rf_channel_group: PInteger;           // RF channel group
  usb_rf_power: PInteger;               // Current RF power
  usb_rf_cmd_retry_num: PInteger;       // Number of RF packet retries for better transmission
  usb_rf_speed: PInteger;               // RF speed in Kbps
  last_rssi: PInteger;                  // Signal strength of last received packet (-128 .. +127)
  last_lqi: PInteger;                   // Signal quality of last received packet(+255 .. +128)
  uart_timeout: PInteger                // Timeout settings for USB_RF communication while opening port
  ): TWR_STATUS stdcall;

//******************************************************************************  
// --- TWR_SDK Functions ---
//******************************************************************************

    // --- Password ---

function TWR_SetPassword                          // set password for TWR_SDK device
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_old_password: PAnsiChar;          // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  str_new_password: PAnsiChar           // New password, Pointer to NullTerminatedString
  ): TWR_STATUS stdcall;

function TWR_SetPassword_getExecStatus            // execution result of  TWR_SetPassword
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

    // --- Time ---

function TWR_SetTime                              // set time for TWR_SDK device
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  new_time: PInteger                    // New time (UTC) Unix format
  ): TWR_STATUS stdcall;

function TWR_SetTime_getExecStatus                // execution result of TWR_SetTime
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

    //  --- INFO ---

function TWR_GetInfo                              // Get TWR info data to buffer
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger                      // Address (ID) of TWR_SDK device
  ): TWR_STATUS stdcall;

function TWR_GetInfo_getResult                    // read TWR info data from buffer
  (
  usb_rf: PLongWord;                    // HANDLE USB
  hw_type: PInteger;                    // Hardware type of TWR_SDK device
  hw_ver: PInteger;                     // Hardware version of  TWR_SDK device
  fw_ver: PInteger;                     // Firmware version of TWR_SDK device
  ser_nr: PAnsiString;                  // Serial No. TWR_SDK device
  system_ID:PInteger;                   // system ID
  mode: Pinteger;                       // mode / new
  time: PLongint;                       // Current time of TWR_SDK device (UTC) Unix format
  battery_status: PInteger              // Battery status in percents ( 0 - 100 )
  ): TWR_STATUS stdcall;

function TWR_GetInfo_getExtendedResult
  (
  usb_rf          : PLongWord;          // HANDLE USB
  hw_type         : PInteger;           // Hardware type of TWR_SDK device
  hw_ver          : PInteger;           // Hardware version of  TWR_SDK device
  fw_ver          : PInteger;           // Firmware version of TWR_SDK device
  twr_sn          : PAnsiString;        // Serial No. TWR_SDK device
  system_ID       :PInteger;                   // system ID
  mode            : Pinteger;           // mode 
  time            : PLongint;           // Current time of TWR_SDK device (UTC) Unix format
  cycle_counter   : PInteger;           // read mifare card cycles
  optical_counter : PInteger;           // read error mifare card cycles
	wake_up_counter : PInteger;           // number of wake up
	upload_counter  : PInteger;           // received RF packet
  download_counter: PInteger;           // transmitted RF packet
  log_size        : PInteger;           // log size in B
  twr_rf_power    : PInteger;           // TWR RF Power
  twr_rf_rssi     : PInteger;           // TWR RSSI
  twr_rf_lqi      : PInteger;           // TWR LQI
  usb_rf_power    : PInteger;           // USB RF Power
  usb_rf_rssi     : PInteger;           // USB RSSI
  usb_rf_lqi      : Pinteger            // USB LQI
  ): TWR_STATUS stdcall;

  
//******************************************************************************  
// --- TWR RTE Functions ---
//******************************************************************************

function TWR_ReadRTE_Count                        // OUT - number of RTE events in buffer
  (): Integer stdcall;

function TWR_ReadRTE                              // OUT - read RTE record
  (
  log_idx: PInteger;                    // Index of current record - autoinc by TWR_SDK
  log_action: PInteger;                 // Action defined by card (Dec. 128 - Regular, 7 more could be defined )  ??????
  log_reader_id: PInteger;              // ID of reader (commonly, first reader is 1; if TWR_SDK is in checkin/out mode, second reader is 2 etc)
  log_card_id: PInteger;                // Card ID (user defined number written on card; don't mess with generic serial nr. of MiFare card
  log_system_id: PInteger;              // System ID (readers group, default is 0, user defined)
  timestamp: PInteger ;                  // Timestamp of card logging event, UTC, Unix format
  mifare: PAnsiChar
  ): TWR_STATUS stdcall;

//******************************************************************************
// --- TWR LOG Functions ---
//******************************************************************************

function TWR_GetLog                               // Get the whole LOG to DLL buffer eg. all LOG records in TWR
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address(ID) TWR_SDK device
  str_password: PAnsiChar               // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  ): TWR_STATUS stdcall;

function TWR_GetLogByIndex                        // Get LOG records in range of indexes and put in DLL buffer
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address(ID) TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  startIdx: PInteger;                   // Starting Index of LOG record, start of range
  endIdx: PInteger                      // Ending Index of LOG record, end of range
  ): TWR_STATUS stdcall;

function TWR_GetLogByTime                         // Get LOG records in range of time  and put in DLL buffer
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address(ID) TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  start_time: PInteger;                 // Starting time of LOG record, start of time range  (UTC)
  end_time: PInteger                    // Ending time of LOG record, end of time range  (UTC)
  ): TWR_STATUS stdcall;

function TWR_GetLog_getExecStatus                 // result of execution of previos LOG commands
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

function TWR_ReadLog_Count                        // OUT - number of log records in DLL buffer
  (): Integer stdcall;

function TWR_ReadLog                              // Read LOG record from DLL buffer, OUT params
  (
  log_idx: PInteger;                    // Index of current record - autoinc by TWR_SDK
  log_action: PInteger;                 // Action defined by card (Dec. 128 - Regular, 7 more could be defined )  ??????
  log_reader_id: PInteger;              // ID of reader (commonly, first reader is 1; if TWR_SDK is in checkin/out mode, second reader is 2 etc)
  log_card_id: PInteger;                // Card ID (user defined number written on card; don't mess with generic serial nr. of MiFare card
  log_system_id: PInteger;              // System ID (readers group, default is 0, user defined)
  timestamp: PInteger; 
  mifare:Pansichar                  // Timestamp of card logging event, UTC, Unix format
  ): TWR_STATUS stdcall;

function TWR_ClearLog(                  // Clear DLL buffer, doesn't clear RTE logs
  ): TWR_STATUS stdcall;

{function Convert_MifareUID_to_32bitInt
    (mifare :Pansichar;
    hi:Pinteger;
    lo:pinteger):integer stdcall;  }
//******************************************************************************  
// --- TWR Blacklist functions ---
//******************************************************************************


function TWR_BlackList_getExecStatus            // execution result of  TWR_BLackList Commands
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): TWR_STATUS stdcall;

function TWR_BlackList_getSize                        // OUT - number of BlackList Entries
  (
  usb_rf: PLongWord                     // HANDLE USB
  ): Integer stdcall;


function TWR_BlackList_getResult
  (
  usb_rf: PLongWord;                    // HANDLE USB
  str_out_csv_blacklist : PansiChar
  ): TWR_STATUS stdcall;

function TWR_BlackList_Read
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_password: PAnsiChar               // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  ): TWR_STATUS stdcall;

function TWR_BlackList_EraseWrite
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS stdcall;

function TWR_BlackList_Block
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS stdcall;

function TWR_BlackList_UnBlock
  (
  usb_rf: PLongWord;                    // HANDLE USB
  twr_id: PInteger;                     // Address (ID) of TWR_SDK device
  str_password: PAnsiChar;              // Current password for accessing TWR_SDK device, Pointer to NullTerminatedString
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS stdcall;

//******************************************************************************  
// --- USB EEPROM Sec Pass Functions ---
//******************************************************************************

function SecData_PassChange
  (
  usb_rf: PLongWord;                  // HANDLE USB  
  actualPass: PAnsiChar;              // Current Sec password , default 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF// Current Sec password , default 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
  NewPass : PansiChar                 // New Sec password
  ): TWR_STATUS stdcall;

function SecData_WriteData
  (
  usb_rf: PLongWord;                  // HANDLE USB 
  data: PansiChar;                    // data to be written in USB_RF EEPROM , max 32 bytes
  size: Integer;                      // size of data, max 32
  actualPass: PAnsiChar               // Current Sec password , default 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
  ): TWR_STATUS stdcall;

function SecData_ReadData
  (
  usb_rf: PLongWord;                  // HANDLE USB  
  data: PAnsiChar;                    // data to be read, written in USB_RF EEPROM, 32 bytes 
  size: Integer                       // size of data, max 32
  ): TWR_STATUS stdcall;


implementation

//******************************************************************************
//******************************************************************************
//******************************************************************************

const                                                // Static loading of  twr_comm.dll
  TWR_DLL_File                = 'twr_comm.dll';       // File Version 3.0.3.116 and above


function TWR_WaitMainLoop(): Integer;
var
  cmdResponses                : Integer;
  RealTimeEvents              : Integer;
  LogAvailable                : Integer;
  TimeoutOccurred             : Integer;
  TimeoutEvent                : Integer;
begin
  Result := 40;
  TimeoutEvent := 0;
  cmdResponses := 0;
  while cmdResponses = 0 do
  begin
    TWR_MainLoop(@cmdResponses, @RealTimeEvents, @LogAvailable, @TimeoutOccurred);
    TimeoutEvent := TimeoutEvent + 5;
    if TimeoutEvent > 30000 then      // if timeout is more than 30 secs
    begin
      Result := 41;                   // result TIMEOUT
      Exit;
    end;
    SysUtils.Sleep(10);               // Wait a little (10 msec)
  end;
  Result := 0;                        // Else, TWR_OK
end;


function TWR_USB_Open (usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_USB_Close(usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_Restart  (usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_USB_Info
  (
  usb_rf: PLongWord;
  usb_hw_type: PInteger;
  usb_hw_ver: PInteger;
  usb_fw_ver: PInteger;
  usb_fw_sn: PAnsiChar;
  rf_settings_group: PInteger;
  rf_channel_group: PInteger;
  usb_rf_power: PInteger;
  usb_rf_cmd_retry_num: PInteger;
  usb_rf_speed: PInteger;
  last_rssi: PInteger;
  last_lqi: PInteger;
  uart_timeout: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetInfo_getExtendedResult
  (
  usb_rf              : PLongWord;                
  hw_type             : PInteger;                
  hw_ver              : PInteger;                 
  fw_ver              : PInteger;                 
  twr_sn              : PAnsiString;              
  system_ID           : PInteger;                   
  mode                : Pinteger;                   
  time                : PLongint;                   
  cycle_counter       : PInteger;         
  optical_counter     : PInteger;       
  wake_up_counter     : PInteger;       
  upload_counter      : PInteger;       
  download_counter    : PInteger;       
  log_size            : PInteger;              
  twr_rf_power        : PInteger;          
  twr_rf_rssi         : PInteger;          
  twr_rf_lqi          : PInteger;          
  usb_rf_power        : PInteger;          
  usb_rf_rssi         : PInteger;          
  usb_rf_lqi          : Pinteger           
  ) : TWR_STATUS; external TWR_DLL_File;
  
function TWR_SetPassword
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_old_password: PAnsiChar;
  str_new_password: PAnsiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_SetPassword_getExecStatus (usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_SetTime
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar;
  new_time: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_SetTime_getExecStatus (usb_rf: PLongWord) : TWR_STATUS; external TWR_DLL_File;

function TWR_GetInfo
  (
  usb_rf: PLongWord;
  twr_id: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetInfo_getResult
  (
  usb_rf: PLongWord;
  hw_type: PInteger;
  hw_ver: PInteger;
  fw_ver: PInteger;
  ser_nr: PAnsiString;
  system_ID:PInteger;                   // system ID
  mode: Pinteger;
  time: PLongint;
  battery_status: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_MainLoop
  (
  cmdResponses: PInteger;
  RealTimeEvents: PInteger;
  LogAvailable: PInteger;
  TimeoutOccurred: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_ReadRTE_Count(): Integer; external TWR_DLL_File;

function TWR_ReadRTE
  (
  log_idx: PInteger;
  log_action: PInteger;
  log_reader_id: PInteger;
  log_card_id: PInteger;
  log_system_id: PInteger;
  timestamp: PInteger ;
  mifare  :PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetLog
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetLogByIndex
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar;
  startIdx: PInteger;
  endIdx: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetLogByTime
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar;
  start_time: PInteger;
  end_time: PInteger
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_GetLog_getExecStatus(usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_ReadLog_Count(): Integer; external TWR_DLL_File;

function TWR_ReadLog
  (
  log_idx: PInteger;
  log_action: PInteger;
  log_reader_id: PInteger;
  log_card_id: PInteger;
  log_system_id: PInteger;
  timestamp: PInteger;
  mifare: PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_ClearLog(): TWR_STATUS; external TWR_DLL_File;

function Convert_MifareUID_to_32bitInt
    (mifare :Pansichar;
    hi:Pinteger;
    lo:Pinteger):integer; external TWR_DLL_file;

function TWR_BlackList_getExecStatus (usb_rf: PLongWord): TWR_STATUS; external TWR_DLL_File;

function TWR_BlackList_getSize (usb_rf: PLongWord): Integer; external TWR_DLL_File;

function TWR_BlackList_getResult
  (
  usb_rf: PlongWord;
  str_out_csv_blacklist : PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_BlackList_Read
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_BlackList_EraseWrite
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar;
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_BlackList_Block
  (
  usb_rf: PLongWord;
  twr_id: PInteger;
  str_password: PAnsiChar;
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function TWR_BlackList_UnBlock
  (
  usb_rf: PLongWord;                  
  twr_id: PInteger;                   
  str_password: PAnsiChar;            
  str_in_csv_blacklist : PansiChar
  ): TWR_STATUS; external TWR_DLL_File;

function SecData_PassChange
  (
  usb_rf: PLongWord;                  
  actualPass: PAnsiChar;              
  NewPass : PansiChar                 
  ): TWR_STATUS; external TWR_DLL_File;

function SecData_WriteData
  (
  usb_rf: PLongWord;                    
  data: PAnsiChar;                      
  size: Integer;                        
  actualPass: PAnsiChar                 
  ): TWR_STATUS; external TWR_DLL_File;

function SecData_ReadData
  (
  usb_rf: PLongWord;                    
  data: PAnsiChar;                      
  size: Integer                         
  ): TWR_STATUS; external TWR_DLL_File;

//******************************************************************************
//      END
//******************************************************************************
  
end.


