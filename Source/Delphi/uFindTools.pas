unit uFindTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage, DB, MemDS, DBAccess, MyAccess;

type
  TfrmFindTools = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    img1: TImage;
    lbl1: TLabel;
    edtPartNo: TEdit;
    lbl2: TLabel;
    edtDescription: TEdit;
    tblLookupTools: TMyTable;
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFindTools: TfrmFindTools;

implementation

uses uDataModule, uTools;

{$R *.dfm}

procedure TfrmFindTools.btnCancelClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmFindTools.btnOKClick(Sender: TObject);

begin
  //ShowMessage(IntToStr(rgLocateOptions.ItemIndex));

  // Check if the option is not empty
  if (edtPartNo.Text <> '') and (edtDescription.Text <> '') then
    begin
      if frmTools.qryTools.Locate('Part__No;Description' , VarArrayOf([edtPartNo.Text , edtDescription.Text]), [loPartialKey,loCaseInsensitive]) <> True then

         raise(Exception.Create('Part Number ' + '''' + edtPartNo.Text + '''' + ' and Description ' + '''' + edtDescription.Text + '''' + ' not found'))

      else
        Self.Close;
    end
   else
 if (edtPartNo.Text <> '') and (edtDescription.Text = '') then
    begin
      if frmTools.qryTools.Locate('Part__No', edtPartNo.Text, [loPartialKey,loCaseInsensitive]) <> true then
            raise(Exception.Create('Part Number ' + '''' + edtPartNo.Text + '''' + ' not found'))
        else
         Self.Close;

    end
   else
    if (edtPartNo.Text = '') and (edtDescription.Text <> '') then
    begin
       if frmTools.qryTools.locate('Description', edtDescription.Text, [loPartialKey,loCaseInsensitive]) <> true then
        raise(Exception.Create('Part Description ' + '''' + edtDescription.Text + '''' + ' not found'))
       else
        Self.Close;
    end;


end;

procedure TfrmFindTools.FormCreate(Sender: TObject);
begin
   if not tblLookupTools.active then
   begin
    tblLookupTools.Connection := DM.conHenchman;
    tblLookupTools.Active := true;
  end;
end;

end.
