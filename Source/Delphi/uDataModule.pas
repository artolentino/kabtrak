unit uDataModule;

interface

uses
  SysUtils, Classes, DBAccess, MyDacVcl, DB, MyAccess, MemDS, DASQLMonitor,
  MyDacMonitor, MySQLMonitor,inifiles,Forms, Dialogs;

type
  TDM = class(TDataModule)
    conHenchman: TMyConnection;
    mysqlmntr1: TMySQLMonitor;
    dsCustomer: TMyDataSource;
    tblLookUpSupplier: TMyTable;
    dsLookupSupplier: TMyDataSource;
    tblLookUpToolCategories: TMyTable;
    dsLookupToolCategories: TMyDataSource;
    tblToolSubCat: TMyTable;
    dsToolSubCat: TMyDataSource;
    tblLookupDept: TMyTable;
    dsDept: TMyDataSource;
    qryCustomer: TMyQuery;
    qryLoggedUser: TMyQuery;
    qryCabinet: TMyQuery;
    dsCabinet: TMyDataSource;
    qryLookupItems: TMyQuery;
    dsLookupItems: TMyDataSource;
    qryDrawer: TMyQuery;
    dsDrawer: TMyDataSource;
    qryBin: TMyQuery;
    dsBin: TMyDataSource;
    qryCabinetKabID: TIntegerField;
    qryCabinetDescription: TStringField;
    qryCabinetLocation: TStringField;
    dtmfldCabinetCreatedDate: TDateTimeField;
    dtmfldCabinetCreatedTime: TDateTimeField;
    dtmfldCabinetLastAccess: TDateTimeField;
    dtmfldCabinetLastAccessTime: TDateTimeField;
    qryCabinetUserID: TIntegerField;
    qryCabinetModelNumber: TStringField;
    qryCabinetCustID: TIntegerField;
    qryCabinetAssigned: TIntegerField;
    qryLoggedUserID: TIntegerField;
    qryLoggedUserUserID: TStringField;
    qryLoggedUserFullName: TStringField;
    qryLoggedUserPIN: TStringField;
    dtmfldLoggedUserLastAccess: TDateTimeField;
    qryLoggedUserEnabled: TIntegerField;
    dtmfldLoggedUserCreatedDate: TDateTimeField;
    dtmfldLoggedUserLastAccTime: TDateTimeField;
    dtmfldLoggedUserCreatedTime: TDateTimeField;
    qryLoggedUserAccessCount: TIntegerField;
    qryLoggedUserCustID: TIntegerField;
    qryLoggedUseraccess_type: TIntegerField;
    qryDrawerID: TIntegerField;
    qryDrawerDrawerID: TStringField;
    qryDrawerKabID: TStringField;
    qryDrawerLockCode: TStringField;
    qryDrawerStatus: TIntegerField;
    qryDrawerRemarks: TStringField;
    qryDrawerUserID: TIntegerField;
    dtmfldDrawerCreatedDate: TDateTimeField;
    dtmfldDrawerCreatedTime: TDateTimeField;
    dtmfldDrawerLastAccess: TDateTimeField;
    dtmfldDrawerLastAccessTime: TDateTimeField;
    qryDrawerM1ID: TStringField;
    qryDrawerCustID: TIntegerField;
    qryDrawerDrawerCode: TIntegerField;
    qryCabinetNumberDrawers: TIntegerField;
    qryCabinetNumberBins: TIntegerField;
    qry: TMyQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation



{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
var
  MTDIni: TiniFile;
  LocalDB : Boolean;
begin
  MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
  // Make sure that the connection is read here
   with MTDIni do
     begin
       LocalDB := ReadBool('General', 'localDB', true);
       // Load apprpriate database profile from the result
       if LocalDB then
         begin
           // Local Database connection
           with conHenchman do
             begin
               Server := ReadString('Database', 'Server', 'localhost');
               Port := ReadInteger('Database','Port',3306);
               Database := ReadString('Database','Schema','mtd');
               Username := ReadString('Database','Username','root');
               Password := ReadString('Database','Password','');
               try
                 Connect; // Try to connect to database profile
               except
                // Error encountered, shwo the error and terminate the application
                 on E: Exception do
                    begin
                      //  Catch the error
                       ShowMessage('Exception class name = '+E.ClassName);
                       ShowMessage('Exception message = '+E.Message);
                       Application.Terminate;
                    end;
               end;


             end;
         end
       else
         begin
         // Online connection here
            with conHenchman do
             begin
               Server := ReadString('Cloud', 'Server', 'localhost');
               Port := ReadInteger('Cloud','Port',3306);
               Database := ReadString('Cloud','Schema','mtd');
               Username := ReadString('Cloud','Username','root');
               Password := ReadString('Cloud','Password','');
               try
                 Connect; // Try to connect to database profile
               except
                // Error encountered, shwo the error and terminate the application
                 on E: Exception do
                    begin
                      // Catch the error
                       ShowMessage('Exception class name = '+E.ClassName);
                       ShowMessage('Exception message = '+E.Message);
                       Application.Terminate;
                    end;
               end;
             end;
         end;
     end;

  MTDIni.Free;


end;

end.
