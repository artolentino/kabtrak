object frmTool: TfrmTool
  Left = 2445
  Top = 849
  BorderStyle = bsDialog
  Caption = 'Tool Details'
  ClientHeight = 280
  ClientWidth = 465
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 249
    Width = 465
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 296
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 6
        Top = 1
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 1
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 465
    Height = 1
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 1
    Width = 465
    Height = 248
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 455
      Height = 238
      ActivePage = TabSheet1
      Align = alClient
      TabHeight = 20
      TabOrder = 0
      TabWidth = 100
      object TabSheet1: TTabSheet
        Caption = 'General'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label14: TLabel
          Left = 34
          Top = 62
          Width = 70
          Height = 13
          Caption = 'Serial Number:'
          FocusControl = editSerialNo
        end
        object Label2: TLabel
          Left = 261
          Top = 178
          Width = 66
          Height = 13
          Caption = 'Current Cost:'
        end
        object Label7: TLabel
          Left = 34
          Top = 178
          Width = 42
          Height = 13
          Caption = 'Supplier:'
        end
        object Label1: TLabel
          Left = 34
          Top = 91
          Width = 57
          Height = 13
          Caption = 'Description:'
        end
        object Label4: TLabel
          Left = 34
          Top = 122
          Width = 49
          Height = 13
          Caption = 'Category:'
        end
        object Label5: TLabel
          Left = 34
          Top = 149
          Width = 70
          Height = 13
          Caption = 'Sub Category:'
        end
        object lblPartNumber: TLabel
          Left = 34
          Top = 22
          Width = 64
          Height = 13
          Caption = 'Part Number:'
          FocusControl = editSerialNo
        end
        object dbtxtPartNumber: TDBText
          Left = 110
          Top = 20
          Width = 4
          Height = 16
          AutoSize = True
          DataField = 'Part__No'
          DataSource = dsTools
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object bvl1: TBevel
          Left = 34
          Top = 41
          Width = 387
          Height = 8
          Shape = bsTopLine
        end
        object editSerialNo: TDBEdit
          Left = 110
          Top = 58
          Width = 313
          Height = 21
          DataField = 'Serialno'
          DataSource = dsTools
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
        end
        object editCost: TDBEdit
          Left = 333
          Top = 174
          Width = 90
          Height = 21
          DataField = 'Latestcost'
          DataSource = dsTools
          ParentShowHint = False
          ShowHint = False
          TabOrder = 4
        end
        object editDescription: TDBEdit
          Left = 110
          Top = 87
          Width = 313
          Height = 21
          CharCase = ecUpperCase
          DataField = 'Description'
          DataSource = dsTools
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
        end
        object dblkcbbcatID: TDBLookupComboBox
          Left = 110
          Top = 118
          Width = 311
          Height = 21
          DataField = 'catID'
          DataSource = dsTools
          KeyField = 'CatID'
          ListField = 'Description'
          ListSource = dsCategories
          TabOrder = 2
        end
        object dblkcbbSupplierID: TDBLookupComboBox
          Left = 110
          Top = 174
          Width = 145
          Height = 21
          DataField = 'SupplierID'
          DataSource = dsTools
          KeyField = 'id'
          ListField = 'SupplierName'
          ListSource = dsSuppliers
          TabOrder = 3
        end
        object dblkcbbSubCat: TDBLookupComboBox
          Left = 110
          Top = 145
          Width = 313
          Height = 21
          DataField = 'subCatID'
          DataSource = dsTools
          KeyField = 'SubCatID'
          ListField = 'Description'
          ListSource = dsSubCategories
          TabOrder = 5
        end
      end
    end
  end
  object dsTools: TMyDataSource
    DataSet = qryTools
    Left = 280
    Top = 72
  end
  object dsCategories: TMyDataSource
    DataSet = qryCategories
    Left = 80
    Top = 72
  end
  object dsSubCategories: TMyDataSource
    DataSet = qrySubCat
    Left = 200
    Top = 24
  end
  object dsSuppliers: TMyDataSource
    DataSet = qrySuppliers
    Left = 168
    Top = 152
  end
  object qryTools: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltools')
    AfterInsert = qryToolsAfterInsert
    AfterPost = qryToolsAfterPost
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 376
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object qryCategories: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltoolcategories')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 128
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object qrySubCat: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltoolsubcatogories')
    MasterSource = dsTools
    MasterFields = 'subCatID'
    DetailFields = 'SubCatID'
    Active = True
    Left = 128
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'subCatID'
        ParamType = ptInput
      end>
  end
  object qrySuppliers: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tblsuppliers')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 176
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object qryCounter: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tblcounterid')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 328
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
end
