unit uChangePin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, MemDS, DBAccess, MyAccess;

type
  TfrmChangePin = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Image1: TImage;
    labUsername: TLabel;
    edtNewPin: TEdit;
    Label1: TLabel;
    edtRepeat: TEdit;
    tblUser: TMyTable;
    tblUserUserID: TIntegerField;
    tblUserUserName: TStringField;
    tblUserFullName: TStringField;
    tblUserPassword: TStringField;
    tblUserLastAccess: TDateTimeField;
    tblUserEnabled: TStringField;
    tblUserCreatedDate: TDateTimeField;
    tblUserLastAccTime: TDateTimeField;
    tblUserCreatedTime: TDateTimeField;
    tblUserAccessCount: TIntegerField;
    tblUserCustID: TIntegerField;
    tblUseraccess_type: TIntegerField;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmChangePin: TfrmChangePin;

implementation

uses uDataModule, main;

{$R *.dfm}

procedure TfrmChangePin.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmChangePin.btnOKClick(Sender: TObject);
begin
   if (edtNewPin.Text = '') or (edtRepeat.Text = '' ) or (edtNewPin.Text <> edtRepeat.Text)  then
     begin
       ShowMessage('Please enter your NEW PIN number');
     end
 else
    begin
       ShowMessage(frmMain.strUserName);
      tblUser.Active := True;
      if tblUser.Locate('UserID',frmMain.intUserID, []) then
        begin
          ShowMessage('Found');
          tblUser.Edit;
          tblUser.FieldByName('Password').Value := edtNewPin.Text;
          tblUser.Post;

        end;
       Close;
   end;
end;

procedure TfrmChangePin.FormShow(Sender: TObject);
begin
// with frmMain.kbQwerty do
//   begin
//       Show;
//       Keyboard.LoadKeybdLayout('Keyboard.txt');
//       Keyboard.Width := 161;
//       Keyboard.Height := 201;
//
//
//
//   end;
end;

end.
