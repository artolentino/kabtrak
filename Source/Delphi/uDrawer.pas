unit uDrawer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DBCtrls, Mask, ComCtrls, DB, MemDS, DBAccess,
  MyAccess;

type
  TfrmDrawer = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    lbl3: TLabel;
    editDrawerCode: TDBEdit;
    grp1: TGroupBox;
    lbl1: TLabel;
    editLockCode: TDBEdit;
    lbl2: TLabel;
    editM1ID: TDBEdit;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDrawer: TfrmDrawer;

implementation

uses uSetupCabinet, uDataModule;

{$R *.dfm}

procedure TfrmDrawer.btnCancelClick(Sender: TObject);
var
msg : string;
begin

 msg := 'kab Drawer record have been modified. Do you really want to cancel the current operations ?' ;
  if frmSetupCabinet.qryDrawers.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOK then
        begin
          frmSetupCabinet.qryDrawers.cancel;
          Close;
        end;
    end
  else
    close;
end;

procedure TfrmDrawer.btnOKClick(Sender: TObject);
begin
     if frmSetupCabinet.qryDrawers.State in [dsInsert, dsEdit] then
    begin
      frmSetupCabinet.qryDrawers.post;
    end;
  frmSetupCabinet.qryDrawers.Refresh;
  Close;
end;

end.
