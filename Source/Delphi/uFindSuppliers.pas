unit uFindSuppliers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage, DB;

type
  TfrmFindSuppliers = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    img1: TImage;
    lbl1: TLabel;
    edtCompany: TEdit;
    lbl2: TLabel;
    edtPerson: TEdit;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFindSuppliers: TfrmFindSuppliers;

implementation

uses uSuppliers;

{$R *.dfm}

procedure TfrmFindSuppliers.btnCancelClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmFindSuppliers.btnOKClick(Sender: TObject);

begin
    // Check if the option is not empty
  if (edtCompany.Text <> '') and (edtPerson.Text <> '') then
    begin
      if frmSuppliers.tblSuppliers.Locate('SupplierName;ContactPerson' , VarArrayOf([edtCompany.Text , edtPerson.Text]), [loPartialKey,loCaseInsensitive]) <> True then

         raise(Exception.Create('Supplier ' + '''' + edtCompany.Text + '''' + ' and Contact person ' + '''' + edtPerson.Text + '''' + ' not found'))

      else
        Self.Close;
    end
   else
 if (edtCompany.Text <> '') and (edtPerson.Text = '') then
    begin
      if frmSuppliers.tblSuppliers.Locate('SupplierName', edtCompany.Text, [loPartialKey,loCaseInsensitive]) <> true then
            raise(Exception.Create('Supplier ' + '''' + edtCompany.Text + '''' + ' not found'))
        else
         Self.Close;

    end
   else
    if (edtCompany.Text = '') and (edtPerson.Text <> '') then
    begin
       if frmSuppliers.tblSuppliers.locate('ContactPerson', edtPerson.Text, [loPartialKey,loCaseInsensitive]) <> true then
        raise(Exception.Create('Contact Name ' + '''' + edtPerson.Text + '''' + ' not found'))
       else
        Self.Close;
    end;


end;

end.
