unit uSubCategory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Mask, DBCtrls, MemDS, DBAccess, MyAccess, ExtCtrls;

type
  TfrmSubCategory = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    tblSubCategory: TMyTable;
    tblSubCategorySubCatID: TIntegerField;
    tblSubCategoryCatID: TIntegerField;
    tblSubCategoryDescription: TStringField;
    tblSubCategoryCustID: TIntegerField;
    lbl1: TLabel;
    editDescription: TDBEdit;
    ds1: TDataSource;
    pnl3: TPanel;
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSubCategory: TfrmSubCategory;

implementation

uses uDataModule, uCategories;

{$R *.dfm}

procedure TfrmSubCategory.btnCancelClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmSubCategory.btnOKClick(Sender: TObject);
begin
    if tblSubCategory.State in [dsInsert, dsEdit] then
    begin
      tblSubCategory.post;
    end;

    frmCategories.tblSubCategories.Refresh;
  Self.Close;
end;

procedure TfrmSubCategory.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
