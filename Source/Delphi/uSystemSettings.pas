unit uSystemSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExForms, JvCustomItemViewer, JvImagesViewer, ComCtrls, StdCtrls,
  ExtCtrls, IniFiles, ieview, iemview, DB, Mask, DBCtrls, MemDS, DBAccess,
  MyAccess, Dropbox, pngimage, Buttons, ImgList;

type
  TfrmSystemSettings = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgcSystem: TPageControl;
    ts1: TTabSheet;
    tsInterface: TTabSheet;
    btnSelect: TButton;
    rg1: TRadioGroup;
    tsCounterID: TTabSheet;
    tblCounter: TMyTable;
    tblCountertoolsID: TIntegerField;
    tblCountersupplierID: TIntegerField;
    tblCounternotificationID: TIntegerField;
    tblCounterjobID: TIntegerField;
    tblCounteremployeeID: TIntegerField;
    tblCounteruserID: TIntegerField;
    tblCounterkabID: TIntegerField;
    tblCounterkabDrawerID: TIntegerField;
    tblCounterkabBinID: TIntegerField;
    tblCountertransactionID: TIntegerField;
    lbl8: TLabel;
    editkabID: TDBEdit;
    lbl9: TLabel;
    editkabDrawerID: TDBEdit;
    lbl10: TLabel;
    editkabBinID: TDBEdit;
    lbl11: TLabel;
    edittransactionID: TDBEdit;
    dsCounte: TDataSource;
    lbl15: TLabel;
    edittransactionID1: TDBEdit;
    grp1: TGroupBox;
    lbl12: TLabel;
    editkabID1: TDBEdit;
    lbl13: TLabel;
    editkabDrawerID1: TDBEdit;
    lbl14: TLabel;
    editkabBinID1: TDBEdit;
    grp2: TGroupBox;
    lbl2: TLabel;
    edittoolsID: TDBEdit;
    lbl3: TLabel;
    editsupplierID: TDBEdit;
    grp3: TGroupBox;
    lbl6: TLabel;
    editemployeeID: TDBEdit;
    lbl7: TLabel;
    edituserID: TDBEdit;
    grp4: TGroupBox;
    lbl5: TLabel;
    editjobID: TDBEdit;
    lbl16: TLabel;
    edittransactionID2: TDBEdit;
    lbl4: TLabel;
    editnotificationID: TDBEdit;
    lbl17: TLabel;
    pnl1: TPanel;
    lbl1: TLabel;
    pnl2: TPanel;
    ImageEnMView1: TImageEnMView;
    ts2: TTabSheet;
    img1: TImage;
    lbl18: TLabel;
    grp5: TGroupBox;
    lbl19: TLabel;
    lblBackupDate: TLabel;
    lbl20: TLabel;
    lblRestoreDate: TLabel;
    btn1: TSpeedButton;
    btn2: TSpeedButton;
    grp6: TGroupBox;
    edtDropboxEmail: TEdit;
    edtDropBoxPassword: TEdit;
    lbl21: TLabel;
    lbl22: TLabel;
    btn3: TSpeedButton;
    rg2: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImageEnMView1ImageSelect(Sender: TObject; idx: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
    selected_index : Integer;
    Dropbox: TDropbox;
    MTDini : TIniFile;
    procedure onAuthorization(Sender: TObject; var ResultOk: Boolean);
      procedure OnProgress(Sender: TObject; Position, Total: Int64; var Cancel: Boolean);
  public
    { Public declarations }
    strWallpaper : string;
  end;

var
  frmSystemSettings: TfrmSystemSettings;

implementation

uses GlobalRoutines, main, uDataModule;

{$R *.dfm}

procedure TfrmSystemSettings.OnProgress(Sender: TObject; Position, Total: Int64; var Cancel: Boolean);
begin
  //pb1.Max := Total;
 // pb1.Position := Position;
  //Application.ProcessMessages;
  Cancel := False;
end;

procedure TfrmSystemSettings.onAuthorization(Sender: TObject; var ResultOk: Boolean);
begin
  ResultOk := True;
end;

procedure TfrmSystemSettings.btn1Click(Sender: TObject);
var
MTDini : TIniFile;
iniStream,appStream : TFileStream;
SystemFile, AppFile : string;
NewIniFile, NewAppFile: TDFileInfo;
Path : string;
strCustID : string;
begin
 MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
  strCustID := StrZeroInt(GetCustomerID,3);
  //pb1.Visible := True;
  Path := '/kabTRAK/';
  SystemFile := ExtractFilePath(ParamStr(0)) + 'mtd.ini';
  iniStream := TFileStream.Create(SystemFile, fmOpenRead);

  with Dropbox do
   begin

     try

        NewIniFile := Upload(iniStream, Path + ExtractFileName(SystemFile));
        if NewiniFile <> nil then
            begin
              Rename(NewIniFile,strCustID + '-' + ExtractFileName(SystemFile));
               MTDini.WriteDate('Session', 'LastBackup',Date());
              ShowMessage('System and Application Uploaded');
            end;
     finally
       MTDini.Free;
       iniStream.Free;
     end;
   end;
end;

procedure TfrmSystemSettings.btn3Click(Sender: TObject);
begin
  with frmMain do
    begin
      if not webUpdateMTD.NewVersionAvailable() then ShowMessage('You are using the current version of kabTRAK');
    end;
end;

procedure TfrmSystemSettings.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmSystemSettings.btnOKClick(Sender: TObject);
var
MTDini : TIniFile;
begin
  // Save ettings
    MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
  // Save wallpaer option

  if strWallpaper <> '' then
  begin
  with MTDini do
     begin
       MTDini.WriteString('General', 'Wallpaper',strWallpaper);
       if rg1.ItemIndex = 0  then MTDini.WriteBool('General', 'TouchscreenKeyboard',false)
         else  MTDini.WriteBool('General', 'TouchscreenKeyboard',true);

     end;
     MTDini.Free;
   end;
     if tblCounter.State in [dsInsert, dsEdit] then
    begin
      tblCounter.post;
    end;


  close;
end;

procedure TfrmSystemSettings.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSystemSettings.FormCreate(Sender: TObject);
begin
   pgcSystem.ActivePageIndex := 0;
   Dropbox := TDropbox.Create;
   Dropbox.OnUploadProgress := OnProgress;
   Dropbox.OnDownloadProgress := OnProgress;
   Dropbox.LogFileName := '_Dropbox.log';
   //Dropbox.OnAuthorizationContinueQuery := onAuthorization;

   MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
   Dropbox.Token := MTDini.ReadString('Session', 'Token', '');
   Dropbox.TokenSecret := MTDini.ReadString('Session', 'TokenSecret', '');
   edtDropboxEmail.Text := MTDini.ReadString('General', 'Appkey','');
   edtDropBoxPassword.Text := MTDini.ReadString('General', 'AppSecret','');


   pgcSystem.ActivePageIndex := 0;

  if not tblCounter.active then
   begin
     tblCounter.Connection := DM.conHenchman;
     tblCounter.Open;
   end;

  with ImageEnMView1 do
     begin
       // Update to the most modern styling
      
        ThumbWidth := 120; // Better size for thumbnails
        ThumbHeight := 120;

       GridWidth := -1; // Auto-column widths
       BorderStyle := bsNone; // Normally don't require a 3D border

     end;
end;

procedure TfrmSystemSettings.FormShow(Sender: TObject);
begin

  Application.ProcessMessages;
  Dropbox.AppKey := edtDropboxEmail.Text;
  Dropbox.AppSecret := edtDropBoxPassword.Text;
  Dropbox.AppFolderAccess := True;
  Dropbox.ClearFindCache;

   ImageEnMView1.FillFromDirectory(GetWallpaperPath, -1, False, '',False,'jpg,jpeg');
//   with frmMain.kbQwerty do
//   begin
//      Show;
//      Keyboard.LoadKeybdLayout('qwery.txt');
//      Keyboard.Width := 401;
//      Keyboard.Height := 201;
//
//   end;

end;

procedure TfrmSystemSettings.ImageEnMView1ImageSelect(Sender: TObject;
  idx: Integer);
begin
   strWallpaper := ImageEnMView1.ImageFileName[idx];

   with frmMain do
     begin
       WallPaper1.AdvImage.LoadFromFile(strWallpaper);
     end;
end;

end.
