unit uCategories;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MemDS, ComCtrls, JvExComCtrls, JvDBTreeView,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls,
  Buttons, NxDBColumns, NxColumns, NxScrollControl,
  NxCustomGridControl, NxCustomGrid, NxDBGrid, NxPageControl, StdCtrls, Mask,
  DBCtrls, AdvSmoothListBox, DBAdvSmoothListBox;

type
  TfrmCategories = class(TForm)
    tblCategories: TMyTable;
    tblSubCategories: TMyTable;
    dsCategories: TMyDataSource;
    dsSubCategories: TMyDataSource;
    pnlToolbar: TPanel;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnClose: TSpeedButton;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    NxPageControl1: TNxPageControl;
    NxTabSheet1: TNxTabSheet;
    NxTabSheet2: TNxTabSheet;
    DBAdvSmoothListBox1: TDBAdvSmoothListBox;
    lstSubCategories: TDBAdvSmoothListBox;
    tblCategoriesCatID: TIntegerField;
    tblCategoriesDescription: TStringField;
    tblCategoriesCustID: TIntegerField;
    tblSubCategoriesSubCatID: TIntegerField;
    tblSubCategoriesCatID: TIntegerField;
    tblSubCategoriesDescription: TStringField;
    tblSubCategoriesCustID: TIntegerField;
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure NxPageControl1Change(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCategories: TfrmCategories;

implementation

uses uDataModule, uCategory, uSubCategory;

{$R *.dfm}

procedure TfrmCategories.btnCloseClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmCategories.btnDeleteClick(Sender: TObject);
var
  msg : string;
begin
  case NxPageControl1.ActivePageIndex of
    0: // Del Categories
    begin
       msg := 'Do you really want to delete Category ' + tblCategoriesDescription.AsString + ' ?';
 if tblCategoriesCatID.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       tblCategories.delete;
       tblCategories.RefreshRecord;
       tblCategories.Refresh;
     end;
    end;
    1: // Del Subcategories
    begin
      msg := 'Do you really want to delete sub Category Category ' + tblSubCategoriesDescription.AsString + ' ?';
     if tblSubCategoriesSubCatID.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       tblSubCategories.delete;
       tblSubCategories.RefreshRecord;
       tblSubCategories.Refresh;
     end;

    end;
  end;
end;

procedure TfrmCategories.btnDownClick(Sender: TObject);
begin
case NxPageControl1.ActivePageIndex of
    0: tblCategories.Next;
    1: tblSubCategories.Next;

  end;

end;

procedure TfrmCategories.btnEditClick(Sender: TObject);
begin
  case NxPageControl1.ActivePageIndex of
    0: // Edit Categories
    begin
    if tblCategoriesCatID.AsVariant = null then btnNewClick(sender)   //No record selected.
      else
        begin
          screen.cursor := crHourglass;
          application.createform (TfrmGategory,frmGategory);
          frmGategory.tblCategory.Locate('CatID', tblCategoriesCatID.value, []);
          frmGategory.Caption := 'Editing ' + tblCategoriesDescription.AsString;
          frmGategory.Showmodal;
          screen.cursor := crDefault;
        end;

    end;
    1: // Edit Subcategories
    begin
      if tblSubCategoriesCatID.AsVariant = null then btnNewClick(sender)   //No record selected.
    else
        begin
          screen.cursor := crHourglass;
          application.createform (TfrmSubCategory,frmSubCategory);
          frmSubCategory.tblSubCategory.Locate('SubCatID', tblSubCategoriesSubCatID.value, []);
          frmSubCategory.Caption := 'Editing ' + tblSubCategoriesDescription.AsString;
          frmSubCategory.pnl3.Caption := 'Category : ' + tblCategoriesDescription.AsString;
          frmSubCategory.Showmodal;
          screen.cursor := crDefault;
          end;

    end;
  end;
end;

procedure TfrmCategories.btnFirstClick(Sender: TObject);
begin
  case NxPageControl1.ActivePageIndex of
    0: tblCategories.First;
    1: tblSubCategories.First;

  end;

end;

procedure TfrmCategories.btnLastClick(Sender: TObject);
begin
  case NxPageControl1.ActivePageIndex of
    0: tblCategories.Last;
    1: tblSubCategories.Last;

  end;

end;

procedure TfrmCategories.btnNewClick(Sender: TObject);
begin
   case NxPageControl1.ActivePageIndex of
    0: // Add Categories
    begin
       screen.cursor := crHourglass;
       application.createform (TfrmGategory,frmGategory);
       frmGategory.Caption := 'New Category';
       frmGategory.tblCategory.Insert;
       frmGategory.ShowModal;
       screen.cursor := crDefault;
    end;
    1: // Add Subcategories
    begin
       screen.cursor := crHourglass;
       application.createform (TfrmSubCategory,frmSubCategory);
       frmSubCategory.Caption := 'New Sub Category';
       frmSubCategory.pnl3.Caption := 'Category: ' + frmCategories.tblCategoriesDescription.AsString;
       frmSubCategory.tblSubCategory.Insert;
       frmSubCategory.ShowModal;
       screen.cursor := crDefault;
    end;

  end;
end;

procedure TfrmCategories.btnRefreshClick(Sender: TObject);
begin
  case NxPageControl1.ActivePageIndex of
    0: tblCategories.Refresh;
    1: tblSubCategories.Refresh;

  end;

end;

procedure TfrmCategories.btnUpClick(Sender: TObject);
begin
case NxPageControl1.ActivePageIndex of
    0:  tblCategories.Prior;
    1: tblSubCategories.prior;

  end;

end;

procedure TfrmCategories.FormClose(Sender: TObject; var Action: TCloseAction);
begin
tblCategories.Close;
  tblSubCategories.Close;
  Action := caFree;
end;

procedure TfrmCategories.FormCreate(Sender: TObject);
begin
  tblCategories.Open;
  tblSubCategories.Open;
  NxPageControl1.ActivePageIndex :=0;
end;

procedure TfrmCategories.NxPageControl1Change(Sender: TObject);
begin
   case NxPageControl1.ActivePageIndex of
     0: begin

        end;
     1: begin
          lstSubCategories.Header.Caption := tblCategories.FieldByName('Description').AsString;

        end;

   end;
end;

end.
