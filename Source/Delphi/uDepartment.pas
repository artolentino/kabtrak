unit uDepartment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, MemDS, DBAccess, MyAccess, StdCtrls, Mask, DBCtrls, ExtCtrls;

type
  TfrmDepartment = class(TForm)
    lbl1: TLabel;
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    editDescription: TDBEdit;
    tblDepartment: TMyTable;
    dsDepartment: TDataSource;
    tblDepartmentid: TIntegerField;
    tblDepartmentdescription: TStringField;
    tblDepartmentcustID: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDepartment: TfrmDepartment;

implementation

uses uDataModule, uDepartments;

{$R *.dfm}

procedure TfrmDepartment.btnCancelClick(Sender: TObject);
begin
   if tblDepartment.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg('There are pending operations for the current transaction. Would you like to abort the operation?',
      mtInformation, [mbYes, mbNo], 0) = mrYes then
      begin
        tblDepartment.cancel;
        Self.Close;
      end
    else
      Exit;
      end;


  close;
end;

procedure TfrmDepartment.btnOKClick(Sender: TObject);
begin
    if tblDepartment.State in [dsInsert, dsEdit] then
    begin
      tblDepartment.post;
    end;

    frmDepartments.qryDepartments.Refresh;
    Self.Close;
end;

procedure TfrmDepartment.FormCreate(Sender: TObject);
begin
  if tblDepartment.Active = false then
    begin
      tblDepartment.Connection := DM.conHenchman;
      tblDepartment.Open;
    end;

end;

end.
