(********************************************************************************

   Program                :  Henchman TRAK
   File                         :  GlobalRoutines.pas
   Description            :  Global Routines Unit
   Author                    :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)

unit GlobalRoutines;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,inifiles,
  Forms,SHELLAPI,ExtCtrls,Dialogs,Registry,Dropbox;

function GetAppVersionStr: string;
function DisplayFormattedDate(varDate :TDateTime) : string;
function InetIsOffline(Flag: Integer): Boolean; stdcall; external 'URL.DLL';
function GetIndicatorPath(): string;
function GetControlPath(): string;
function GetCustomerName(): string;
function GetJobNameLabel() : string;
function GetCustomerID(): integer;
function GetUserID(): integer;
function GetCustomerEmail(): string;
function GetCustomerContact(): string;
function GetWallpaperPath(): string;
function OpenDataConnection(): Boolean;
procedure UpdatePartNumbers();
procedure UpdateInternetIndicator(Status : boolean);
procedure UpdateDBIndicator(Status : boolean);
procedure UpdateReader(Status: Boolean);
function OpenCustomerTable() : Boolean;
function OpenCabinet() : Boolean;
procedure UpdateControls();
function Date2000(Text: string): TDateTime;
function IsAdmin(intID: Integer) : Boolean;
procedure AppRestart;
Function StrZeroInt(Number : integer;Len : integer) : string;
function GetItemCategory(itemID : string) : Integer;
function GetItemSubCategory(itemID : string) : Integer;
function IsValidEmail(const Value: string): Boolean;
//Function GetHidDevice(Vid, Pid, Fname: String): integer;
function isUserLogged(strUserID : string; strPIN : string): Boolean;

var
  FontFactor: real;
  SecurityActive: boolean;   //Global variable used so don't need to read ini file in security check.
const
  CR = #13;       // "Enter" key, "Carriage Return"
  CRLF = #13#10;  // Carriage Return + LineFeed

implementation

uses main, uDataModule, uprogressForm;


procedure UpdatePartNumbers();
begin
  DM.qry.Close;
  DM.qry.SQL.Clear;
  DM.qry.SQL.Add('SELECT * FROM tbltools');
  DM.qry.Open;

 DM.qry.First;
  while not DM.qry.EOF do
    begin
      DM.qry.Edit;
      DM.qry.FieldByName('Part__No').Value := StrZeroInt(DM.qry.FieldByName('CustID').AsInteger, 3) + StrZeroInt(DM.qry.FieldByName('id').AsInteger,3);
      DM.qry.Post;
      DM.qry.Next;
    end;
end;

function CopyCallback(TotalFileSize, TotalBytesTransferred, StreamSize,
  StreamBytesTransferred: Int64; dwStreamNumber, dwCallbackReason: DWORD;
  hSourceFile, hDestinationFile: THandle; progressform: TProgressForm ):
  DWORD; stdcall;
var
  newpos: Integer;
const
  PROCESS_CONTINUE = 0;
begin
  Result := PROCESS_CONTINUE;
  if dwCallbackReason = CALLBACK_CHUNK_FINISHED then
  begin
    newpos := Round(TotalBytesTransferred / TotalFileSize * 100);
    with progressform.pbProgress do
      if newpos <> Position then
        Position := newpos;
    Application.ProcessMessages;
  end;
end;

//function DoFilecopy(const source, target: string): Boolean;
//var
//  progressform: TProgressForm;
//begin
//  progressform := TProgressform.Create(progressForm);
//  try
//    progressform.Show;
//    Application.ProcessMessages;
//    Result := CopyFileEx(
//      PChar(source), PChar(target), @CopyCallback,
//      Pointer(progressform), @progressform.btnCancel, 0
//    );
// finally
//   progressform.Hide;
//   progressform.free;
// end;
//end;

function GetItemCategory(itemID : string) : Integer;
  begin
    with DM do
      begin
        qry.Close;
        qry.SQL.Clear;
        qry.SQL.Add('SELECT * FROM tbltools WHERE Part__No =' + QuotedStr(itemID));
        qry.Open;
        Result := qry.FieldByName('catID').AsInteger;
      end;
  end;
function GetItemSubCategory(itemID : string) : Integer;
  begin
    with DM do
      begin
        qry.Close;
        qry.SQL.Clear;
        qry.SQL.Add('SELECT * FROM tbltools WHERE Part__No =' + QuotedStr(itemID));
        qry.Open;
        Result := qry.FieldByName('subCATID').AsInteger;
      end;
  end;




function isUserLogged(strUserID : string; strPIN : string): Boolean;
 begin

  if strUserID = '' then
  begin
    result := False;
    Exit;
  end;

  if strPIN = '' then
  begin
     result := False;
     Exit;
  end;

  if (strUserID <> '') AND (strPIN <> '') then
  begin
    with DM do
      begin
          qryLoggedUser.Close;
          qryLoggedUser.ParamByName('UserID').Value := Trim(strUserID);
          qryLoggedUser.ParamByName('PIN').Value := Trim(strPIN);
          qryLoggedUser.Open;

        if qryLoggedUser.IsEmpty then
          begin
            // No Record found
           Result := False;
           Exit;
          end
        else
          begin
               if IsAdmin(qryLoggedUser.FieldByName('ID').AsInteger) then
                 begin
                   with frmMain do
                   begin
                     intUserID := qryLoggedUser.FieldByName('ID').AsInteger;
                     isLogged := True;
                     UserIsAdmin := True;
                     ledTop.Caption := 'Welcome ' + qryLoggedUser.FieldByName('FullName').AsString;
                   end;
                 end
               else
                begin
                  with frmMain do
                   begin
                     intUserID := qryLoggedUser.FieldByName('ID').AsInteger;
                     isLogged := True;
                     UserIsAdmin := False;
                     ledTop.Caption := 'Welcome ' + qryLoggedUser.FieldByName('FullName').AsString;
                   end;
                end;

                //update user info

                qryLoggedUser.Edit;
                qryLoggedUser.FieldByName('AccessCount').Value :=  qryLoggedUser.FieldByName('AccessCount').AsInteger + 1;
                qryLoggedUser.FieldByName('LastAccTime').Value := time();
                qryLoggedUser.Post;
                UpdateControls;
                result := True;

          end;

      end;
  end;


    end;


//// will return 'Correct', since the condition is true
//MyStr = IFF(TRUE, 'Correct', 'Incorrect');
//
//// will return 'Incorrect', since the condition is false
//MyStr = IFF(TALSE, 'Correct', 'Incorrect');
//
//// will return X if X  Y, otherwise returns Y
//MyInt = IFF( X  Y, X, Y);
//
//// will return TRUE, since TRUE or FALSE is TRUE
//MyBool = IFF((TRUE OR FALSE), TRUE, FALSE);
//
//// will return 0, since TRUE and FALSE is FALSE
//MyInt = IFF((TRUE AND FALSE), 1, 0);
//
//// is MyStr has a lenght grater then 0, returns its lenght, otherwise returns 0
//MyInt = IFF(Lenght(MyStr)  0, Lenght(MyStr), 0);
//
//// if 'Address:' is present on MyStr, it will return the lenght of the string, otherwise will return the string 'Not Found!'
//MyVar = IFF(Pos('Address:',MyStr)  0, Length(MyStr), 'Not found!');
//
Function IFF(C: Boolean; T,F: Variant): Variant;
Begin
     If C Then
       Result := T
     Else
       Result := F;
End;

//
// Force application to restart
// 
procedure AppRestart;
var AppName : PChar;
      Handle : Cardinal;
begin
  AppName := PChar(Application.ExeName) ;
  ShellExecute(Handle,'open', AppName, nil, nil, SW_SHOWNORMAL) ;
  Application.Terminate;
end;


//  myHidIndex:=GetHidDevice(<VendorId>, <ProductId>, <FriendlyName>);
//  
//  Returns -1 if no Hid device active or hid index number if active
//
//  This program uses 1 ListBox (DevListBox), 1 History ListBox (HistoryListBox) for HexStrings 
//  and a memo (meResults) for hex to Ascii  

//Function GetHidDevice(Vid, Pid, Fname: String): integer;
//Var
//   FriendlyName : string;
//   VendorID : string;
//   ProductID : string;
//   FoundHid : Boolean;
//   HIdIndexNo : integer;
//
//Begin
//  nrHid1.Update;
//  nrHid1.Active:=False;
//  FrmTaylorEditTest.nrHid2.Active:=False;
//
//  if nrHid1.DeviceCount=0 then
//    Begin
//     Result:=-1;
//     Exit;
//    End;
//
//  HIdIndexNo:=0;
//  FoundHid:=False;
//
//  repeat
//    nrHid1.UpdateDeviceDetails;
//    FriendlyName:=nrHid1.Device[HIdIndexNo].NameFriendly;
//    VendorID:=IntToHex(nrHid1.HidDevice.VendorID, 4);
//    ProductID:=IntToHex(nrHid1.HidDevice.ProductID, 4);
//
//    if VendorID=Vid then FoundHid:=True;
//    if ProductID=Pid then FoundHid:=True;
//    if FriendlyName=Fname then FoundHid:=True;
//    if FoundHid=True then break;
//
//    HIdIndexNo:=HIdIndexNo+1;
//    nrHid1.DeviceIndex:=HIdIndexNo;
//  until HIdIndexNo = nrHid1.DeviceCount;
//
//  if FoundHid=True then
//     begin
//       DevListBox.Clear;
//       DevListBox.Items.Add('Product Name -- '+ nrHid1.HidDevice.NameFriendly);
//       DevListBox.Items.Add('Manufacturer -- '+ nrHid1.HidDevice.Manufacturer);
//       DevListBox.Items.Add('Description  -- '+ nrHid1.HidDevice.Description);
//       DevListBox.Items.Add('Service      -- '+ nrHid1.HidDevice.Service);
//       DevListBox.Items.Add('VendorID     -- '+ IntToHex(nrHid1.HidDevice.VendorID, 4));
//       DevListBox.Items.Add('ProductID    -- '+ IntToHex(nrHid1.HidDevice.ProductID, 4));
//       DevListBox.Items.Add('S/N          -- '+ nrHid1.HidDevice.SerialNumber);
//       DevListBox.Items.Add('Ver          -- '+ IntToStr(nrHid1.HidDevice.Version) + ' [' + IntToHex(nrHid1.HidDevice.Version, 4) + 'h]');
//       DevListBox.Items.Add('Usage Page   -- '+ IntToHex(nrHid1.HidDevice.UsagePage, 4) + ' (' + GetUsagePageString(nrHid1.HidDevice.UsagePage) + ')');
//       DevListBox.Items.Add('I/O Length   -- '+ IntToStr(nrHid1.HidDevice.InputReportLength) + '/' + IntToStr(nrHid1.HidDevice.OutputReportLength));
//
//       Try
//         nrHid1.DeviceIndex:=HIdIndexNo;
//         nrHid1.Active:=True;
//       Except
//         nrHid1.Active:=False;
//         HIdIndexNo:=-1;
//       End;
//
//       if nrHid1.Active then
//         begin
//           DevListBox.Items.Add('R/W modes    -- '+ BoolToStr(nrHid1.IsReadMode,True) + ' / ' + BoolToStr(nrHid1.IsWriteMode,True));
//         end;
//     end
//   Else  // FoundHid=False
//     HIdIndexNo:=-1;
//
//  Result:=HIdIndexNo;
//End;

//
// Get Application version number embedded in the compiled binary files
//
function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
end;



function GetUserID(): integer;
begin
  with DM do
   begin
     if qryLoggedUser.Active then  Result := qryLoggedUser.FieldByName('ID').AsInteger;
   end;
end;

function GetWallpaperPath(): string;
begin
   Result := GetCurrentDir + '\Images\wallpapers\';
end;

Function StrZeroInt(Number : integer;Len : integer) : string;
var
  i,tot : Integer;
  temp : string;
begin
  temp := IntToStr(Number);
  tot := len -length(temp);
  for i := 1 to tot do
    temp := '0'+temp;
  result:=temp;
end;



function Date2000(Text: string): TDateTime;
var
  Year, Month, Day: Word;
begin   //Year 2000 date conversion.
  if Text = '02' + DateSeparator + '29' + DateSeparator + '00' then result := StrToDate('02' + DateSeparator + '29' + DateSeparator + '2000')
  else if Text = '29' + DateSeparator + '02' + DateSeparator + '00' then result := StrToDate('29' + DateSeparator + '02' + DateSeparator + '2000')
  else begin
    DecodeDate(StrToDate(Text), Year, Month, Day);
    if (Year < 1950) and (Year > 1899) then begin
      Year := Year + 100;
      if UpperCase(Copy(ShortDateFormat,1,1)) = 'D' then result := StrToDate(IntToStr(Day) + DateSeparator + IntToStr(Month) + DateSeparator + IntToStr(Year))
      else if UpperCase(Copy(ShortDateFormat,1,1)) = 'Y' then result := StrToDate(IntToStr(Year) + DateSeparator + IntToStr(Month) + DateSeparator + IntToStr(Day))
      else result := StrToDate(IntToStr(Month) + DateSeparator + IntToStr(Day) + DateSeparator + IntToStr(Year));
    end else if (Year > 2050) then begin
      Year := Year - 100;
      if UpperCase(Copy(ShortDateFormat,1,1)) = 'D' then result := StrToDate(IntToStr(Day) + DateSeparator + IntToStr(Month) + DateSeparator + IntToStr(Year))
      else if UpperCase(Copy(ShortDateFormat,1,1)) = 'Y' then result := StrToDate(IntToStr(Year) + DateSeparator + IntToStr(Month) + DateSeparator + IntToStr(Day))
      else result := StrToDate(IntToStr(Month) + DateSeparator + IntToStr(Day) + DateSeparator + IntToStr(Year));
    end else result := StrToDate(Text);
  end;
end;

function User: String;
var
  Name1: Array[0..30] of Char; Name2: Array[0..30] of Char;
  intLength: Cardinal;
begin
   intLength:= SizeOf(Name1);
  //Length := SizeOf(Name1);
  if GetUserName(Name1, intLength) then StrCopy(Name2, Name1)
  else                               StrCopy(Name2, '');
  Result := Name2;
end;

// Open customer records from the ini file

function OpenCustomerTable() : Boolean;
var
   appINI : TIniFile;
   CustID : Integer;

  begin
    //Read CustID from INI File (this is where the admin setup saves the current customer)
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
   try
     //if no last Customer return an empty string
     CustID := appINI.ReadInteger('Customer','ID',0) ;

     // if no customer found (0), return false
     if CustID <> 0  then
       begin
         with DM do
         begin
           qryCustomer.Close;
           qryCustomer.ParamByName('CustID').Value := CustID;
           qryCustomer.Open;
           // Check if the CustID is found, return true if found and false if not
           if qryCustomer.RecordCount > 0 then Result := True else Result := False;
         end;
       end
       else
        // Customer id not found in the system ini file, return false
        Result := False;
   finally
     appINI.Free;
   end;
  end;

  function OpenCabinet() : Boolean;
var
   appINI : TIniFile;
   kabID : Integer;

  begin
    //Read CustID from INI File (this is where the admin setup saves the current customer)
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
   try
     //if no last Customer return an empty string
     kabID := appINI.ReadInteger('Customer','kabID',0) ;

     // if no customer found (0), return false
     if kabID <> 0  then
       begin
         with DM do
         begin
           qryCabinet.Close;
           qryCabinet.ParamByName('kabID').Value := kabID;
           qryCabinet.Open;
           // Check if the CustID is found, return true if found and false if not
           if qryCabinet.RecordCount > 0 then Result := True else Result := False;
         end;
       end
       else
        // Customer id not found in the system ini file, return false
        Result := False;
   finally
     appINI.Free;
   end;
  end;

 function GetJobNameLabel() : string;
var
   appINI : TIniFile;
 begin
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
   try
       Result := appINI.ReadString('Customer','JobLabel','');
   finally
     appINI.Free;
   end;
 end;


procedure UpdateControls();

var
  employee : Boolean;
  admin : Boolean;
  guest : Boolean;
  logged : Boolean;

begin

  with frmMain do
   begin

      ledBottom.Caption := 'kabTRAK - ' + DM.qryCabinetLocation.AsString;
      logged := isLogged;
      if logged then   ledTop.Caption := 'Welcome ' + DM.qryLoggedUserFullName.asString;



      if (logged and UserIsAdmin) then
        begin
          pnlAdmin.Visible := True;
          pnlUser.Visible := False;
          pnlGuest.Visible := False;
        end;

        if (logged and not UserIsAdmin) then
        begin

          pnlUser.Visible := True;
          pnlAdmin.Visible := False;
          pnlGuest.Visible := False;
        end;

        if not logged then
        begin
          ledTop.Caption := 'Welcome Guest, Please present your ID Card to start your transactions';
          pnlGuest.Visible := True;
          pnlAdmin.Visible := False;
          pnlUser.Visible := False;
        end;

 end;


end;

function IsAdmin(intID : integer) : Boolean;
var
 intAccessType : Integer;

begin
   with DM do
   begin
      if qryLoggedUser.Locate('ID', intID, []) then
         begin
            intAccessType := qryLoggedUser.FieldByName('access_type').AsInteger;
            if intAccessType = 1  then Result := True else Result := False;
         end;
   end;
end;

function OpenDataConnection() : Boolean;
var
   appINI : TIniFile;
   varSchema,varUserName,varPassword,varHost : string;
   varPort : integer;
begin
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
 try
     varSchema := appINI.ReadString('Database','Schema','mtd') ;
     varUserName := appINI.ReadString('Database','Username','root') ;
     varPassword := appINI.ReadString('Database','Password','') ;
     varHost := appINI.ReadString('Database','Server','localhost') ;
     varPort := appINI.ReadInteger('Database','Port',3306) ;
   finally
     appINI.Free;
   end;
 with DM.conHenchman do
  begin
     Database := varSchema;
     Port := varPort;
     Username := varUserName;
     Password := varPassword;
     Server := varHost;
      Try
        Connected := True;
        Result := True;
      Except
        // Handle error here. Since it's a database error there will be no way of updating the notification table
       Result := False;
      end;


end;
end;



function DisplayFormattedDate(varDate :TDateTime) : string;
begin
  // Return formatted date
  result := FormatDateTime('dddd d mmmm yyyy', varDate);
end;

function GetIndicatorPath(): string;
begin
  Result := GetCurrentDir + '\Images\indicators\';
end;

function GetControlPath(): string;
begin
  Result := GetCurrentDir + '\Images\Controls\';
end;


procedure UpdateReader(Status: Boolean);
var
  image_name : string;
begin
 if not Status then image_name := 'proximity_reader_disconnected.png'
  else
     image_name := 'proximity_reader_connected.png' ;
  //update the indicator

  with frmMain do
    begin
      imgInternet.Picture.LoadFromFile(GetIndicatorPath + image_name);

      if not Status then imgProximity.Hint := 'Reader is not connected'
         else
      imgInternet.Hint := 'Reader is currently connected';
    end;

   // if not Status then CreateNotification('Reader is not connected');

end;

procedure UpdateInternetIndicator(Status: Boolean);

var
  image_name : string;
begin
  if not Status then image_name := 'internet_disconnected.png'
  else
     image_name := 'internet_connected.png' ;
  //update the indicator

  with frmMain do
    begin
      imgInternet.Picture.LoadFromFile(GetIndicatorPath + image_name);

      if not Status then imgInternet.Hint := 'The drawer is not connected to the internet'
         else
      imgInternet.Hint := 'The drawer is currently connected to the internet';
    end;
end;

 procedure UpdateDBIndicator(Status: Boolean);

var
  image_name : string;
begin
  if not Status then image_name := 'db_disconnected.png'
  else
     image_name := 'db_connected.png' ;
  //update the indicator
  with frmMain do
    begin
      imgNetwork.Picture.LoadFromFile(GetIndicatorPath + image_name);
      if not Status then imgNetwork.Hint := 'The database is Offline'
         else
      imgNetwork.Hint := 'The database is online';
    end;
end;

function GetCustomerID():integer;
var
   appINI : TIniFile;
   CustID : Integer;

  begin
  //Read CustID from INI File (this is where the admin setup saves the current customer)
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
   try
     //if no last Customer return an empty string
     CustID := appINI.ReadInteger('Customer','ID',0) ;
     Result := CustID;
   finally
     appINI.Free;
   end;
 end;

function GetCustomerName():string;

  begin
  if not OpenCustomerTable then
     begin
        // Promt to select customer
       Exit;
     end
  else
     begin
        Result := DM.qryCustomer.FieldByName('CompanyName').AsString;
     end;

 end;

function GetCustomerEmail():string;
  begin
    if not OpenCustomerTable then
      begin
       // Open customer config window
       exit;
      end
    else
      begin
        Result := DM.qryCustomer.FieldByName('Email').AsString;
      end;

 end;

 function GetCustomerContact():string;
  begin
    if not OpenCustomerTable  then
      begin
        Exit;
      end
     else
      begin
        Result := DM.qryCustomer.FieldByName('ContactPerson').AsString;
      end;
 end;

function IsValidEmail(const Value: string): Boolean;

      function CheckAllowed(const s: string): Boolean;
        var i: Integer;
        begin
          Result:= false;
          for i:= 1 to Length(s) do
            if not (s[i] in ['a'..'z',
                             'A'..'Z',
                             '0'..'9',
                             '_',
                             '-',
                             '.']) then Exit;
          Result:= true;
        end;

    var
    i: Integer;
    NamePart, ServerPart: string;
        begin
          Result:= False;
          i:=Pos('@', Value);
          if i=0 then Exit;
          NamePart:=Copy(Value, 1, i-1);
          ServerPart:=Copy(Value, i+1, Length(Value));
          if (Length(NamePart)=0) or ((Length(ServerPart)<5)) then Exit;
          i:=Pos('.', ServerPart);
          if (i=0) or (i>(Length(serverPart)-2)) then Exit;
          Result:= CheckAllowed(NamePart) and CheckAllowed(ServerPart);
        end;

function IsWrongIP(ip: string): Boolean;
var
  z, i: byte;
  st: array[1..3] of byte;
const
  ziff = ['0'..'9'];
    begin
      st[1]  := 0;
      st[2]  := 0;
      st[3]  := 0;
      z      := 0;
      Result := False;
      for i := 1 to Length(ip) do if ip[i] in ziff then
      else
      begin
        if ip[i] = '.' then
        begin
          Inc(z);
          if z < 4 then st[z] := i
          else
          begin
            IsWrongIP := True;
            Exit;
          end;
        end
        else
        begin
          IsWrongIP := True;
          Exit;
        end;
      end;
      if (z <> 3) or (st[1] < 2) or (st[3] = Length(ip)) or (st[1] + 2 > st[2]) or
        (st[2] + 2 > st[3]) or (st[1] > 4) or (st[2] > st[1] + 4) or (st[3] > st[2] + 4) then
      begin
        IsWrongIP := True;
        Exit;
      end;
      z := StrToInt(Copy(ip, 1, st[1] - 1));
      if (z > 255) or (ip[1] = '0') then
      begin
        IsWrongIP := True;
        Exit;
      end;
      z := StrToInt(Copy(ip, st[1] + 1, st[2] - st[1] - 1));
      if (z > 255) or ((z <> 0) and (ip[st[1] + 1] = '0')) then
      begin
        IsWrongIP := True;
        Exit;
      end;
      z := StrToInt(Copy(ip, st[2] + 1, st[3] - st[2] - 1));
      if (z > 255) or ((z <> 0) and (ip[st[2] + 1] = '0')) then
      begin
        IsWrongIP := True;
        Exit;
      end;
      z := StrToInt(Copy(ip, st[3] + 1, Length(ip) - st[3]));
      if (z > 255) or ((z <> 0) and (ip[st[3] + 1] = '0')) then
      begin
        IsWrongIP := True;
        Exit;
      end;
    end;

end.
