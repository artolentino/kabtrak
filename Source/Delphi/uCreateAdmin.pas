unit uCreateAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DB, DBAccess, MyAccess, MemDS, StdCtrls, ExtCtrls, Mask,
  DBCtrls;

type
  TfrmCreateAdmin = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    qryUsers: TMyQuery;
    dsUsers: TMyDataSource;
    qryCounter: TMyQuery;
    pgc1: TPageControl;
    ts1: TTabSheet;
    grpAdmin: TGroupBox;
    dbedtUserID: TDBEdit;
    dbedtPIN: TDBEdit;
    dbedtFullname: TDBEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    qryUsersID: TIntegerField;
    qryUsersUserID: TStringField;
    qryUsersPIN: TStringField;
    qryUsersFullName: TStringField;
    qryUsersEnabled: TIntegerField;
    qryUsersCreatedDate: TDateField;
    qryUsersLastAccess: TDateField;
    qryUsersLastAccTime: TDateTimeField;
    qryUsersCreatedTime: TTimeField;
    qryUsersAccessCount: TIntegerField;
    qryUsersCustID: TIntegerField;
    qryUsersaccess_type: TIntegerField;
    qryUserskabID: TIntegerField;
    qryCounterid: TIntegerField;
    qryCountertoolsID: TIntegerField;
    qryCountersupplierID: TIntegerField;
    qryCounternotificationID: TIntegerField;
    qryCounterjobID: TIntegerField;
    qryCounteremployeeID: TIntegerField;
    qryCounteruserID: TIntegerField;
    qryCounterkabID: TIntegerField;
    qryCounterkabDrawerID: TIntegerField;
    qryCounterkabBinID: TIntegerField;
    qryCountertransactionID: TIntegerField;
    qryCounterCustID: TIntegerField;
    lblCustID: TLabel;
    lblKabID: TLabel;
    procedure qryUsersAfterInsert(DataSet: TDataSet);
    procedure qryUsersAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCreateAdmin: TfrmCreateAdmin;

implementation

uses uWelcome, GlobalRoutines;

{$R *.dfm}

procedure TfrmCreateAdmin.btnOKClick(Sender: TObject);
begin
   with qryUsers do
    begin
      if State in [dsInsert, dsEdit] then
         begin
           try
           Post;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to post user detail. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;

           end;
         end;
    end;
  Close;
  with frmWelcome do
   begin
     qryUser.Close;
     qryUser.ParamByName('CustID').Value := StrToInt(lblCustID.Caption);
     qryUser.ParamByName('kabID').Value :=    StrToInt(lblKabID.Caption);
      try
           qryUser.Open;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to open user detail. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;

           end;
   end;
end;

procedure TfrmCreateAdmin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryUsers.Close;
  qryCounter.Close;
  Action := caFree;
end;

procedure TfrmCreateAdmin.qryUsersAfterInsert(DataSet: TDataSet);
begin
  with qryUsers do
  begin
    FieldByName('ID').Value := qryCounteruserID.AsInteger;
    FieldByName('Enabled').Value := 1;
    FieldByName('CreatedDate').Value := Date();
    FieldByName('CreatedTime').Value := Time();
    FieldByName('access_type').Value := 1;
    FieldByName('FullName').Value := 'Administrator';
    FieldByName('UserID').Value := StrZeroInt(StrToInt(lblCustID.Caption),3) + StrZeroInt(qryCounteruserID.AsInteger,3);
    FieldByName('kabID').Value := StrToInt(lblKabID.Caption);
    FieldByName('CustID').Value := StrToInt(lblCustID.Caption)

  end;
end;

procedure TfrmCreateAdmin.qryUsersAfterPost(DataSet: TDataSet);
begin
  with qryCounter do
    begin
      Edit;
      FieldByName('userID').Value := FieldByName('userID').AsInteger + 1;
      try
           Post;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to post counter detail. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;

           end;
    end;
end;

end.
