object frmAddkabTRAK: TfrmAddkabTRAK
  Left = 2034
  Top = 662
  BorderStyle = bsDialog
  Caption = 'Add New kabTRAK Machine'
  ClientHeight = 367
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 336
    Width = 488
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 249
    ExplicitWidth = 465
    object pnl2: TPanel
      Left = 319
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 296
      object btnOK: TButton
        Left = 6
        Top = 1
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 1
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 488
    Height = 336
    ActivePage = ts1
    Align = alClient
    TabHeight = 20
    TabOrder = 1
    TabWidth = 100
    ExplicitTop = -5
    object ts1: TTabSheet
      Caption = 'General'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dbtxt1: TDBText
        Left = 128
        Top = 74
        Width = 65
        Height = 17
        DataField = 'KabID'
        DataSource = dsCabinet
      end
      object lbl1: TLabel
        Left = 43
        Top = 74
        Width = 61
        Height = 13
        Caption = 'kabTRAK ID:'
      end
      object lbl2: TLabel
        Left = 43
        Top = 102
        Width = 32
        Height = 13
        Caption = 'Model:'
      end
      object lbl3: TLabel
        Left = 43
        Top = 131
        Width = 57
        Height = 13
        Caption = 'Description:'
      end
      object lbl4: TLabel
        Left = 43
        Top = 160
        Width = 44
        Height = 13
        Caption = 'Location:'
      end
      object bvl1: TBevel
        Left = 43
        Top = 63
        Width = 390
        Height = 1
        Shape = bsTopLine
      end
      object lbl7: TLabel
        Left = 43
        Top = 13
        Width = 64
        Height = 13
        Caption = 'Customer ID:'
      end
      object lbl8: TLabel
        Left = 43
        Top = 40
        Width = 80
        Height = 13
        Caption = 'Customer Name:'
      end
      object dbedt1: TDBEdit
        Left = 128
        Top = 127
        Width = 305
        Height = 21
        DataField = 'Description'
        DataSource = dsCabinet
        TabOrder = 0
      end
      object dbedt2: TDBEdit
        Left = 128
        Top = 156
        Width = 305
        Height = 21
        DataField = 'Location'
        DataSource = dsCabinet
        TabOrder = 1
      end
      object dbedt3: TDBEdit
        Left = 128
        Top = 98
        Width = 305
        Height = 21
        DataField = 'ModelNumber'
        DataSource = dsCabinet
        TabOrder = 2
      end
      object grp1: TGroupBox
        Left = 43
        Top = 192
        Width = 390
        Height = 92
        Caption = 'Drawers and Tool Locations'
        TabOrder = 3
        object lbl5: TLabel
          Left = 52
          Top = 32
          Width = 97
          Height = 13
          Caption = 'Number of Drawers:'
        end
        object lbl6: TLabel
          Left = 52
          Top = 61
          Width = 121
          Height = 13
          Caption = 'Number of Tools/Drawer:'
        end
        object edtkabDrawersCount1: TJvDBSpinEdit
          Left = 188
          Top = 28
          Width = 152
          Height = 21
          TabOrder = 0
          DataField = 'NumberDrawers'
          DataSource = dsCabinet
        end
        object edtkabDrawersCount2: TJvDBSpinEdit
          Left = 188
          Top = 57
          Width = 152
          Height = 21
          TabOrder = 1
          DataField = 'NumberBins'
          DataSource = dsCabinet
        end
      end
      object dbedtCustID: TDBEdit
        Left = 128
        Top = 9
        Width = 121
        Height = 21
        DataField = 'CustID'
        DataSource = dsCustomer
        ReadOnly = True
        TabOrder = 4
      end
      object dbedtCompanyName: TDBEdit
        Left = 128
        Top = 36
        Width = 305
        Height = 21
        DataField = 'CompanyName'
        DataSource = dsCustomer
        ReadOnly = True
        TabOrder = 5
      end
    end
  end
  object qryCabinet: TMyQuery
    Connection = frmWelcome.conTest
    SQL.Strings = (
      'SELECT * FROM tblitemkabs')
    AfterInsert = qryCabinetAfterInsert
    AfterPost = qryCabinetAfterPost
    MasterSource = dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 292
    Top = 48
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CustID'
        ParamType = ptInput
      end>
  end
  object qryCounter: TMyQuery
    Connection = frmWelcome.conTest
    SQL.Strings = (
      'SELECT * FROM tblcounterid WHERE CustID = :CustID')
    MasterFields = 'CustID'
    Active = True
    Left = 380
    Top = 34
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsCabinet: TMyDataSource
    DataSet = qryCabinet
    Left = 316
    Top = 122
  end
  object qryCustomer: TMyQuery
    Connection = frmWelcome.conTest
    SQL.Strings = (
      'SELECT * FROM tblcustomer WHERE CustID = :CustID')
    Active = True
    Left = 204
    Top = 98
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsCustomer: TMyDataSource
    DataSet = qryCustomer
    Left = 204
    Top = 154
  end
end
