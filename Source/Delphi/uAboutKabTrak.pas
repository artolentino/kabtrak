(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uAboutKabTrak.pas
   Description            :  About Form
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)
unit uAboutKabTrak;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage;

type
  TfrmAboutKabTrak = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    lblProductName: TLabel;
    lblVersion: TLabel;
    lblCopyright: TLabel;
    lblLicense: TLabel;
    lblCompanyName: TLabel;
    lblTrademark: TLabel;
    lblRegisteredTo: TLabel;
    btnOK: TButton;
    btnLicense: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnLicenseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAboutKabTrak: TfrmAboutKabTrak;

implementation

uses main, GlobalRoutines, uLicense;

{$R *.dfm}

procedure TfrmAboutKabTrak.btnLicenseClick(Sender: TObject);
begin
  screen.cursor := crHourglass;
  frmLicense := TfrmLicense.Create(nil);
  screen.cursor := crDefault;
  if frmLicense.ShowModal = mrOK then begin   //If upgraded license successfully, show here and change from "30 day reminder" to "About" style.
    caption := 'About kabTRAK';
    FormShow(sender);
  end else btnOK.setfocus;
end;

procedure TfrmAboutKabTrak.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Action := caFree;
end;

procedure TfrmAboutKabTrak.FormCreate(Sender: TObject);
begin
  lblVersion.Caption := 'Version ' + GetAppVersionStr;
end;

procedure TfrmAboutKabTrak.FormShow(Sender: TObject);
begin
  btnOK.setfocus;

  if frmMain.Reg.Registered = False then lblLicense.caption := '30 day evaluation license'
     else  lblLicense.caption := 'Single User license';

  if not frmMain.Reg.Registered then
    begin
      lblRegisteredTo.caption := 'You have been using kabTRAK for ' + IntToStr(frmMain.Reg.Days - frmMain.Reg.DaysLeft) + ' days.'
    end
  else
     begin
       lblRegisteredTo.caption := 'Registered to: ' + GetCustomerContact;
     end;

  if caption = 'kabTRAK 30 day evaluation' then begin   //Called from frmBS1.Show.
    lblLicense.font.style := [fsBold];
    lblRegisteredTo.Top := Round(120*FontFactor);
    lblCopyright.caption := 'To continue past 30 days you must purchase a license.';
    if (not frmMain.Reg.Registered) and (frmMain.Reg.DaysLeft >=30) then begin
      lblRegisteredTo.font.color := clMaroon;
      lblCopyright.font.color := clMaroon;
    end else begin
      lblRegisteredTo.font.color := clBlack;
      lblCopyright.font.color := clBlack;
    end;
  end else begin   //Called frmBS1.mnuAbout
    lblLicense.font.style := [];
    lblRegisteredTo.Top := Round(100*FontFactor);
    lblCopyright.caption := 'Copyright � 2014 Ar Tolentino, Henchman Product, Pty.';
      if (not frmMain.Reg.Registered) and (frmMain.Reg.DaysLeft >=30) then lblRegisteredTo.font.color := clMaroon
    else lblRegisteredTo.font.color := clBlack;
    lblCopyright.font.color := clBlack;
end;
end;
end.
