unit uToolCabinet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, DBAccess, MyAccess, MemDS, Mask, DBCtrls,
  NxPageControl;

type
  TfrmToolCabinet = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    qryToolCabinet: TMyQuery;
    dsToolCabinet: TMyDataSource;
    qryToolCabinetBinID: TStringField;
    qryToolCabinetDrawerID: TIntegerField;
    qryToolCabinetKabID: TIntegerField;
    qryToolCabinetP1Code: TStringField;
    qryToolCabinetCustID: TIntegerField;
    qryToolCabinetItemID: TIntegerField;
    pnl3: TPanel;
    qryToolCabinetitemDescription: TStringField;
    qryToolCabinetID: TIntegerField;
    qryToolCabinetPartNumber: TStringField;
    NxPageControl1: TNxPageControl;
    NxTabSheet1: TNxTabSheet;
    NxTabSheet2: TNxTabSheet;
    edititemDescription1: TDBEdit;
    edititemDescription: TDBEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    grp1: TGroupBox;
    lbl3: TLabel;
    editKabID: TDBEdit;
    lbl4: TLabel;
    lbl5: TLabel;
    editBinID: TDBEdit;
    lbl6: TLabel;
    editP1Code: TDBEdit;
    dblkcbb1: TDBLookupComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmToolCabinet: TfrmToolCabinet;

implementation

uses uDataModule, main, uTools;

{$R *.dfm}

procedure TfrmToolCabinet.btnCancelClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmToolCabinet.btnOKClick(Sender: TObject);
begin
  if qryToolCabinet.State in [dsInsert, dsEdit] then
    begin
      qryToolCabinet.post;
    end;

    frmTools.qryToolsCabinet.Refresh;
  Close;
end;

procedure TfrmToolCabinet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryToolCabinet.Close;
  Action := caFree;
end;

procedure TfrmToolCabinet.FormCreate(Sender: TObject);
begin
  qryToolCabinet.Open;
end;

end.
