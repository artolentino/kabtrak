unit uToolMovement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MemDS, TeEngine, TeeProcs, Chart, DBChart,
  ComCtrls, ExtCtrls, StdCtrls, DBCtrls, AdvDateTimePicker, Buttons,
  NxDBColumns, NxColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid,
  NxDBGrid, ImgList, pngimage;

type
  TfrmToolMovement = class(TForm)
    qryTools: TMyQuery;
    dsTools: TMyDataSource;
    pnlOptions: TPanel;
    pnl2: TPanel;
    qryTransactions: TMyQuery;
    dbtxt1: TDBText;
    dbtxt2: TDBText;
    lbl1: TLabel;
    lbl2: TLabel;
    rgOptions: TRadioGroup;
    grpDate: TGroupBox;
    btnRetrieve: TSpeedButton;
    advdtmpckrDate: TAdvDateTimePicker;
    grpDateRange: TGroupBox;
    advdtmpckrDateRangeFrom: TAdvDateTimePicker;
    lbl3: TLabel;
    lbl4: TLabel;
    advdtmpckrDateRangeTo: TAdvDateTimePicker;
    grpEmployee: TGroupBox;
    lbl5: TLabel;
    bvl1: TBevel;
    grpProject: TGroupBox;
    lbl6: TLabel;
    cbbEmployee: TComboBox;
    qryUsers: TMyQuery;
    qryJobs: TMyQuery;
    qryJobsTrailID: TIntegerField;
    qryJobsDescription: TStringField;
    qryJobsCustID: TIntegerField;
    qryJobsStartDate: TDateField;
    qryJobsEndDate: TDateField;
    qryJobsRemark: TStringField;
    cbbProjects: TComboBox;
    grdTransactions: TNextDBGrid;
    dsTransactions: TMyDataSource;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    NxDBImageColumn1: TNxDBImageColumn;
    il1: TImageList;
    NxDBTextColumn1: TNxDBTextColumn;
    NxDBDateColumn1: TNxDBDateColumn;
    nxdbtxtclmnJobName: TNxDBTextColumn;
    bvl6: TBevel;
    btnRefresh: TSpeedButton;
    pnlLegend: TPanel;
    imgOut: TImage;
    lbl7: TLabel;
    lbl8: TLabel;
    img1: TImage;
    lbl9: TLabel;
    qryUsersID: TIntegerField;
    qryUsersUserID: TStringField;
    qryUsersFullName: TStringField;
    qryUsersPIN: TStringField;
    dtmfldUsersLastAccess: TDateTimeField;
    qryUsersEnabled: TIntegerField;
    dtmfldUsersCreatedDate: TDateTimeField;
    dtmfldUsersLastAccTime: TDateTimeField;
    dtmfldUsersCreatedTime: TDateTimeField;
    qryUsersAccessCount: TIntegerField;
    qryUsersCustID: TIntegerField;
    qryUsersaccess_type: TIntegerField;
    lblAll: TLabel;
    qryToolsid: TIntegerField;
    qryToolsPart__No: TStringField;
    qryToolsDescription: TStringField;
    qryToolscatID: TIntegerField;
    qryToolssubCatID: TIntegerField;
    qryToolsLatestCost: TFloatField;
    qryToolsSerialNo: TStringField;
    qryToolsCustID: TIntegerField;
    qryToolsSupplierID: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure rgOptionsClick(Sender: TObject);
    procedure btnRetrieveClick(Sender: TObject);
    procedure cbbEmployeeChange(Sender: TObject);
    procedure cbbProjectsChange(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmToolMovement: TfrmToolMovement;

implementation

uses uDataModule, GlobalRoutines;

{$R *.dfm}

procedure TfrmToolMovement.btnDownClick(Sender: TObject);
begin
  qryTransactions.Next;
end;

procedure TfrmToolMovement.btnFirstClick(Sender: TObject);
begin
  qryTransactions.First;
end;

procedure TfrmToolMovement.btnLastClick(Sender: TObject);
begin
  qryTransactions.Last;
end;

procedure TfrmToolMovement.btnRefreshClick(Sender: TObject);
begin
  qryTransactions.Refresh;
end;

procedure TfrmToolMovement.btnRetrieveClick(Sender: TObject);
var
  intUSerID, intJobID : Integer;
begin

intUSerID := -1;
   case rgOptions.ItemIndex of
      0: begin
            qryTransactions.Close;
            qryTransactions.DeleteWhere;
            qryTransactions.Open;
            if qryTransactions.IsEmpty then
               MessageDlg('No Transaction found for' + qryTools.FieldByName('Description').ASString,  mtInformation, [mbOK],
                 0);

         end;
      1: begin
            qryTransactions.Close;
            qryTransactions.DeleteWhere;
            qryTransactions.AddWhere('tbltransdetail.TransDate = ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDate.Date)));
            qryTransactions.Open;
            if qryTransactions.IsEmpty then
               MessageDlg('No Transaction found for' + qryTools.FieldByName('Description').ASString,  mtInformation, [mbOK],
                 0);
         end;
      2: begin
            // Validate the date inputs to should be greater then from

            if (advdtmpckrDateRangeFrom.Date <  advdtmpckrDateRangeTo.Date)  then
              begin
                qryTransactions.Close;
                qryTransactions.DeleteWhere;
                qryTransactions.AddWhere('tbltransdetail.TransDate BETWEEN ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDateRangeFrom.Date)) + ' AND ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDateRangeTo.Date)) );
                qryTransactions.Open;
                if qryTransactions.IsEmpty then
               MessageDlg('No Transaction found for' + qryTools.FieldByName('Description').ASString,  mtInformation, [mbOK],
                 0);
              end
            else
              ShowMessage('Invalid date Range!');


         end;
      3: begin
           //employee
           if cbbEmployee.ItemIndex > -1 then  intUSerID := Integer(cbbEmployee.Items.Objects[cbbEmployee.ItemIndex]);
            if intUSerID > 0  then
               begin
                  qryTransactions.Close;
                  qryTransactions.DeleteWhere;
                  qryTransactions.AddWhere('tblemployeeitemtransactions.UserID = ' + IntToStr(intUSerID));
                  qryTransactions.Open;
                  if qryTransactions.IsEmpty then
               MessageDlg('No Transaction found for' + qryTools.FieldByName('Description').ASString,  mtInformation, [mbOK],
                 0);
               end;



         end;
      4: begin
            if cbbProjects.ItemIndex > -1 then  intJobID := Integer(cbbProjects.Items.Objects[cbbProjects.ItemIndex]);
             if intJobID > 0  then
               begin
                 qryTransactions.Close;
                 qryTransactions.DeleteWhere;
                 qryTransactions.AddWhere('tblemployeeitemtransactions.TrailID =' + IntToStr(intJobID) );
                 qryTransactions.Open;
                 if qryTransactions.IsEmpty then
               MessageDlg('No Transaction found for' + qryTools.FieldByName('Description').ASString,  mtInformation, [mbOK],
                 0);
               end;


         end;


   end;
end;

procedure TfrmToolMovement.btnUpClick(Sender: TObject);
begin
  qryTransactions.Prior;
end;

procedure TfrmToolMovement.cbbEmployeeChange(Sender: TObject);
begin
  if cbbEmployee.ItemIndex > 0  then btnRetrieve.Enabled := True else btnRetrieve.Enabled := False;

end;

procedure TfrmToolMovement.cbbProjectsChange(Sender: TObject);
begin
 if cbbProjects.ItemIndex > 0  then btnRetrieve.Enabled := True else btnRetrieve.Enabled := False;
end;

procedure TfrmToolMovement.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmToolMovement.FormCreate(Sender: TObject);
begin
    if not qryTransactions.Active then
    begin
      qryTransactions.Connection := DM.conHenchman;
      qryTransactions.Open;
    end;
  rgOptions.Left := 541;
  rgOptions.Top := 16;
  pnlOptions.Height := 195;
  grdTransactions.Columns.Item[3].Header.Caption := GetJobNameLabel + ' Name';
end;

procedure TfrmToolMovement.rgOptionsClick(Sender: TObject);
begin
  case rgOptions.ItemIndex of
     0: begin
          lblAll.Visible := True;
          lblAll.Top := 72;
          lblAll.Left := 32;
          btnRetrieve.Enabled := True;
          grpDate.Visible := False;
          grpDate.Left := 32;
          grpDate.Top := 72;
          grpDateRange.Visible := False;
          grpEmployee.Visible := False;
          grpProject.Visible := False;
        end;
     1: begin
          lblAll.Visible := false;
          btnRetrieve.Enabled := True;
          grpDate.Visible := True;
          grpDate.Left := 32;
          grpDate.Top := 72;
          grpDateRange.Visible := False;
          grpEmployee.Visible := False;
          grpProject.Visible := False;
        end;
     2: begin
          lblAll.Visible := false;
          btnRetrieve.Enabled := True;
          grpDateRange.Visible := True;
          grpDateRange.Left := 32;
          grpDateRange.Top := 72;
          grpDate.Visible := False;
          grpEmployee.Visible := False;
          grpProject.Visible := False;

        end;
     3: begin
          lblAll.Visible := false;
          btnRetrieve.Enabled := false;
          grpEmployee.Visible := True;
          grpEmployee.Left := 32;
          grpEmployee.Top := 72;
          grpDate.Visible := False;
          grpDateRange.Visible := False;
          grpProject.Visible := False;
          // Load selection
           //Open Query
           qryUsers.Open;
           cbbEmployee.Items.Clear;
           cbbEmployee.Items.Add('--- Select Employee ----');

           qryUsers.First;
           while not qryUsers.Eof do
             begin
               with cbbEmployee do
                 begin

                   Items.AddObject(qryUsersFullName.AsString, TObject(qryUsersID.AsInteger));
                 end;

              qryUsers.Next;
             end;
           qryUsers.Close;
           cbbEmployee.ItemIndex := 0;
        end;
     4: begin
          lblAll.Visible := false;
          btnRetrieve.Enabled := false;
          grpProject.Visible := True;
          grpProject.Caption := GetJobNameLabel;
          lbl6.Caption := 'Select ' + GetJobNameLabel;
          grpProject.Left := 32;
          grpProject.Top := 72;
          grpDate.Visible := False;
          grpDateRange.Visible := False;
          grpEmployee.Visible := False;

           //Open Query
           qryJobs.Open;
           cbbProjects.Items.Clear;
           cbbProjects.Items.Add('--- Select Job ----');

           qryJobs.First;
           while not qryJobs.Eof do
             begin
               with cbbProjects do
                 begin
                   Items.AddObject(qryJobsDescription.AsString, TObject(qryJobsTrailID.AsInteger));
                 end;

              qryJobs.Next;
             end;
           qryJobs.Close;
           cbbProjects.ItemIndex := 0;
        end;
  end;
end;

end.
