unit uDatabaseSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, ComCtrls, StdCtrls,IniFiles, Buttons,
  dbcTableDataComparer, dbcClasses, dbcSQL_Exec, dbcMySQLExec,
  dbcCustomScriptExtract, dbcMySQLScriptExtract, dbcDBStructure, dbcDBComparer,
  dbcDBEngine, dbcConnection_MyDAC, dbcMySQLDatabaseExtract, DB, DBAccess,
  MyAccess, MyBackup, DADump, MyDump;

type
   TMTDTables = Record
     tablename : string;
     strSQL  : string;
  end;

  TfrmDatabase = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    tsGeneral: TTabSheet;
    tsLocal: TTabSheet;
    tsCloud: TTabSheet;
    imgMysql: TImage;
    img1: TImage;
    rgOptions: TRadioGroup;
    lbl2: TLabel;
    grp1: TGroupBox;
    lblDatabase: TLabel;
    edtLocalSchema: TEdit;
    Label1: TLabel;
    edtLocalPort: TEdit;
    Label2: TLabel;
    edtLocalHost: TEdit;
    Label3: TLabel;
    edtLocalUsername: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtLocalPassword: TEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    edtOnlineSchema: TEdit;
    edtOnlinePort: TEdit;
    edtOnlineHost: TEdit;
    edtOnlineUsername: TEdit;
    edtOnlinePassword: TEdit;
    tsUtils: TTabSheet;
    img2: TImage;
    img3: TImage;
    btnSync: TSpeedButton;
    btnBackup: TSpeedButton;
    btn1: TSpeedButton;
    lbl1: TLabel;
    lbl3: TLabel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    Label12: TLabel;
    SpeedButton2: TSpeedButton;
    Label13: TLabel;
    conLocal: TDBCConnectionMyDAC;
    dbcmprMTD: TDBComparer;
    dbstrctrMaster: TDBStructure;
    mysqlxcLocal: TMySQLExec;
    TableDataComparer1: TTableDataComparer;
    conMaster: TDBCConnectionMyDAC;
    dbstrctrLocal: TDBStructure;
    mysqldbxtrctMaster: TMySQLDBExtract;
    mysqldbxtrctLocal: TMySQLDBExtract;
    mysqlxcMaster: TMySQLExec;
    conLocalDB: TMyConnection;
    conMasterDB: TMyConnection;
    mtdDump: TMyDump;
    mtdBackup: TMyBackup;
    MyCommand1: TMyCommand;
    pb1: TProgressBar;
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure dbcmprMTDBeforeCompare(Sender: TObject);
    procedure btnSyncClick(Sender: TObject);
    procedure dbcmprMTDAfterCompare(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBackupClick(Sender: TObject);
    procedure mtdDumpBackupProgress(Sender: TObject; ObjectName: string;
      ObjectNum, ObjectCount, Percent: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  frmDatabase: TfrmDatabase;
  mtdtables : array[1..17] of TMTDTables;

implementation

uses main, GlobalRoutines, uDataModule, uRestoreBackupProgress;

{$R *.dfm}

procedure TfrmDatabase.btnBackupClick(Sender: TObject);
var
   mtdtables : array[1..17] of TMTDTables;
   i : Integer;
   strQuery : string;
begin
   pb1.Visible := True;
  // Set up the first customer record
   with mtdtables[1] do
   begin
     tablename := 'users';
     strSQL  := 'SELECT * FROM users WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   end;

   // Set up the second and third by copying from the first
   mtdtables[2] := mtdtables[1];
   mtdtables[3] := mtdtables[1];
   mtdtables[4] := mtdtables[1];
   mtdtables[5] := mtdtables[1];
   mtdtables[6] := mtdtables[1];
   mtdtables[7] := mtdtables[1];
   mtdtables[8] := mtdtables[1];
   mtdtables[9] := mtdtables[1];
   mtdtables[10] := mtdtables[1];
   mtdtables[11] := mtdtables[1];
   mtdtables[12] := mtdtables[1];
   mtdtables[13] := mtdtables[1];
   mtdtables[14] := mtdtables[1];
   mtdtables[15] := mtdtables[1];
   mtdtables[16] := mtdtables[1];
   mtdtables[17] := mtdtables[1];

   // And then changing the first name to suit in each case
   mtdtables[2].tablename := 'tbltransdetail';
   mtdtables[3].tablename := 'tbltoolsubcatogories';
   mtdtables[4].tablename := 'tbltools';
   mtdtables[5].tablename := 'tbltoolcategories';
   mtdtables[6].tablename := 'tblsuppliers';
   mtdtables[7].tablename := 'tblnotifications';
   mtdtables[8].tablename := 'tbljobs';
   mtdtables[9].tablename := 'tblitems';
   mtdtables[10].tablename := 'tblitemkabs';
   mtdtables[11].tablename := 'tblitemkabdrawers';
   mtdtables[12].tablename := 'tblitemkabdrawerbins';
   mtdtables[13].tablename := 'tblemployees';
   mtdtables[14].tablename := 'tblemployeeitemtransactions';
   mtdtables[15].tablename := 'tbldepartments';
   mtdtables[16].tablename := 'tblcustomer';
   mtdtables[17].tablename := 'tblcounterid';

   mtdtables[2].strSQL := 'SELECT * FROM tbltransdetail WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[3].strSQL := 'SELECT * FROM tbltoolsubcatogories WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[4].strSQL := 'SELECT * FROM tbltools WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[5].strSQL := 'SELECT * FROM tbltoolcategories WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[6].strSQL := 'SELECT * FROM tblsuppliers WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[7].strSQL := 'SELECT * FROM tblnotifications WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[8].strSQL := 'SELECT * FROM tbljobs WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[9].strSQL := 'SELECT * FROM tblitems WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[10].strSQL := 'SELECT * FROM tblitemkabs WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[11].strSQL := 'SELECT * FROM tblitemkabdrawers WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[12].strSQL := 'SELECT * FROM tblitemkabdrawerbins WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[13].strSQL := 'SELECT * FROM tblemployees WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[14].strSQL := 'SELECT * FROM tblemployeeitemtransactions WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[15].strSQL := 'SELECT * FROM tbldepartments WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[16].strSQL := 'SELECT * FROM tblcustomer WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;
   mtdtables[17].strSQL := 'SELECT * FROM tblcounterid WHERE CustID = ' + DM.qryCustomer.FieldByName('CustID').AsString;

 

  try
    for i := 1 to 17 do
     with mtdtables[i] do
       begin
         //ShowMessage(IntToStr(i));
         //lbl4.Caption := 'Backing up ' + tablename;
         strQuery := strSQL;
         //ShowMessage(strSQL);
         //mtdDump.BackupQuery(SQL);
         mtdDump.BackupToFile(tablename +'.sql',strQuery);

       end;

  finally
     pb1.Position := 0;
     //lbl4.Caption := 'Done!';
  end;

end;

procedure TfrmDatabase.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmDatabase.btnOKClick(Sender: TObject);
var
  MTDIni: TiniFile;
  msg : string;
begin

  msg := 'To make the changes to take effect, kabTRAK needs to be  restarted. Would you like to restart kabTRAK now ?';
   MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
   with MTDIni do
   begin
     // Update Database section of ini file
     case rgOptions.ItemIndex of
       0 : begin
            // local database
            MTDIni.WriteBool('General','localDB' , true);

           end;
       1 : begin
            // Online Database
             MTDIni.WriteBool('General','localDB' , false);
           end;
     end;

      // Local
      MTDIni.WriteString('Database','Server' , edtLocalHost.Text);
      MTDIni.WriteInteger('Database','Port' , StrToInt(edtLocalPort.Text));
      MTDIni.WriteString('Database','Schema' , edtLocalSchema.Text);
      MTDIni.WriteString('Database','Username' , edtLocalUsername.Text);
      MTDIni.WriteString('Database','Password' , edtLocalPassword.Text);

      //Online

      MTDIni.WriteString('Cloud','Server' , edtOnlineHost.Text);
      MTDIni.WriteInteger('Cloud','Port' , StrToInt(edtOnlinePort.Text));
      MTDIni.WriteString('Cloud','Schema' , edtOnlineSchema.Text);
      MTDIni.WriteString('Cloud','Username' , edtOnlineUsername.Text);
      MTDIni.WriteString('Cloud','Password' , edtOnlinePassword.Text);

   end;

   close;
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then AppRestart;
end;

procedure TfrmDatabase.btnSyncClick(Sender: TObject);
begin
  dbcmprMTD.CompareDatabases;
end;

procedure TfrmDatabase.dbcmprMTDAfterCompare(Sender: TObject);
begin
  dbcmprMTD.ExtractAndCompareDatabases;
end;

procedure TfrmDatabase.dbcmprMTDBeforeCompare(Sender: TObject);
begin
  conLocal.Connected := True;
  conMaster.Connected := True;

end;

procedure TfrmDatabase.FormCreate(Sender: TObject);
 begin

  pgc1.ActivePageIndex := 0;

end;

procedure TfrmDatabase.FormShow(Sender: TObject);
var
  MTDIni: TiniFile;

begin
  // Load ini configuration
  MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
    with MTDIni do
    begin
     //rgOptions.Items.

    if MTDIni.ReadBool('General','localDB' , true) then
        rgOptions.ItemIndex := 0
    else
        rgOptions.ItemIndex := 1;

      // Local
      edtLocalHost.Text :=  MTDIni.ReadString('Database','Server' , '');
      edtLocalPort.Text :=  IntToStr(MTDIni.ReadInteger('Database','Port' , 3306));
      edtLocalSchema.Text :=  MTDIni.ReadString('Database','Schema' , '');
      edtLocalUsername.Text :=  MTDIni.ReadString('Database','Username' , '');
      edtLocalPassword.Text :=  MTDIni.ReadString('Database','Password' , '');

      with conLocal.Database do
           begin
              Server := edtLocalHost.Text;
              Database := edtLocalSchema.Text;
              Port := StrToInt(edtLocalPort.Text);
              Username := edtLocalUsername.Text;
              Password := edtLocalPassword.Text;

           end;

      //Online

      edtOnlineHost.Text :=  MTDIni.ReadString('Cloud','Server' , '');
      edtOnlinePort.Text :=  IntToStr(MTDIni.ReadInteger('Cloud','Port' , 3306));
      edtOnlineSchema.Text :=  MTDIni.ReadString('Cloud','Schema' , '');
      edtOnlineUsername.Text :=  MTDIni.ReadString('Cloud','Username' , '');
      edtOnlinePassword.Text :=  MTDIni.ReadString('Cloud','Password' , '');

      with conMaster.Database do
           begin
              Server := edtOnlineHost.Text;
              Database := edtOnlineSchema.Text;
              Port := StrToInt(edtOnlinePort.Text);
              Username := edtOnlineUsername.Text;
              Password := edtOnlinePassword.Text;

           end;

    end;
  MTDIni.Free;

end;

procedure TfrmDatabase.mtdDumpBackupProgress(Sender: TObject;
  ObjectName: string; ObjectNum, ObjectCount, Percent: Integer);
begin
  pb1.Position := Percent;

end;

end.
