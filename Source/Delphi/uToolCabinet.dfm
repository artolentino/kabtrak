object frmToolCabinet: TfrmToolCabinet
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'frmToolCabinet'
  ClientHeight = 254
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 223
    Width = 405
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 217
    ExplicitWidth = 463
    object pnl2: TPanel
      Left = 236
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 294
      object btnOK: TButton
        Left = 5
        Top = 6
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 6
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pnl3: TPanel
    AlignWithMargins = True
    Left = 5
    Top = 5
    Width = 395
    Height = 65
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 5
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 503
    object grp1: TGroupBox
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 389
      Height = 59
      Align = alClient
      Caption = 'Cabinet Info'
      TabOrder = 0
      ExplicitHeight = 61
      object lbl3: TLabel
        Left = 18
        Top = 22
        Width = 64
        Height = 13
        Caption = 'Cabinet No. :'
      end
      object lbl4: TLabel
        Left = 213
        Top = 22
        Width = 62
        Height = 13
        Caption = 'Drawer No. :'
      end
      object editKabID: TDBEdit
        Left = 88
        Top = 19
        Width = 90
        Height = 21
        DataField = 'KabID'
        DataSource = dsToolCabinet
        TabOrder = 0
      end
      object dblkcbb1: TDBLookupComboBox
        Left = 281
        Top = 19
        Width = 101
        Height = 21
        DataField = 'DrawerID'
        DataSource = dsToolCabinet
        ListField = 'DrawerID'
        ListSource = DM.dsDrawer
        TabOrder = 1
      end
    end
  end
  object NxPageControl1: TNxPageControl
    Left = 0
    Top = 75
    Width = 405
    Height = 148
    ActivePage = NxTabSheet1
    ActivePageIndex = 0
    Align = alClient
    TabOrder = 2
    Images = frmMain.ilTabImages
    Margin = 0
    Spacing = 0
    TabMargin = 5
    TabHeight = 32
    TabMaxWidth = 400
    TabStyle = tsClassic
    ExplicitLeft = 8
    ExplicitWidth = 618
    ExplicitHeight = 252
    object NxTabSheet1: TNxTabSheet
      Caption = 'General'
      DisplayMode = tdImageIndex
      ImageIndex = 2
      PageIndex = 0
      TabFont.Charset = DEFAULT_CHARSET
      TabFont.Color = clWindowText
      TabFont.Height = -11
      TabFont.Name = 'Tahoma'
      TabFont.Style = []
      ExplicitTop = 17
      ExplicitWidth = 300
      ExplicitHeight = 229
      object lbl1: TLabel
        Left = 30
        Top = 26
        Width = 64
        Height = 13
        Caption = 'Part Number:'
      end
      object lbl2: TLabel
        Left = 30
        Top = 58
        Width = 57
        Height = 13
        Caption = 'Description:'
      end
      object edititemDescription1: TDBEdit
        Left = 112
        Top = 53
        Width = 278
        Height = 21
        DataField = 'itemDescription'
        DataSource = dsToolCabinet
        ReadOnly = True
        TabOrder = 0
      end
      object edititemDescription: TDBEdit
        Left = 112
        Top = 26
        Width = 113
        Height = 21
        DataField = 'PartNumber'
        DataSource = dsToolCabinet
        ReadOnly = True
        TabOrder = 1
      end
    end
    object NxTabSheet2: TNxTabSheet
      Caption = 'KabTRAK Properties'
      DisplayMode = tdImageIndex
      ImageIndex = 11
      PageIndex = 1
      TabFont.Charset = DEFAULT_CHARSET
      TabFont.Color = clWindowText
      TabFont.Height = -11
      TabFont.Name = 'Tahoma'
      TabFont.Style = []
      ExplicitTop = 0
      ExplicitWidth = 100
      ExplicitHeight = 100
      object lbl5: TLabel
        Left = 26
        Top = 26
        Width = 25
        Height = 13
        Caption = 'BinID'
        FocusControl = editBinID
      end
      object lbl6: TLabel
        Left = 26
        Top = 58
        Width = 37
        Height = 13
        Caption = 'P1Code'
        FocusControl = editP1Code
      end
      object editBinID: TDBEdit
        Left = 66
        Top = 18
        Width = 303
        Height = 21
        DataField = 'BinID'
        DataSource = dsToolCabinet
        ReadOnly = True
        TabOrder = 0
      end
      object editP1Code: TDBEdit
        Left = 69
        Top = 53
        Width = 300
        Height = 21
        DataField = 'P1Code'
        DataSource = dsToolCabinet
        TabOrder = 1
      end
    end
  end
  object qryToolCabinet: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECt * FROM tblitemkabdrawerbins')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 264
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryToolCabinetBinID: TStringField
      FieldName = 'BinID'
      Origin = 'tblitemkabdrawerbins.BinID'
      Size = 50
    end
    object qryToolCabinetDrawerID: TIntegerField
      FieldName = 'DrawerID'
      Origin = 'tblitemkabdrawerbins.DrawerID'
    end
    object qryToolCabinetKabID: TIntegerField
      FieldName = 'KabID'
      Origin = 'tblitemkabdrawerbins.KabID'
    end
    object qryToolCabinetP1Code: TStringField
      FieldName = 'P1Code'
      Origin = 'tblitemkabdrawerbins.P1Code'
      Size = 5
    end
    object qryToolCabinetCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tblitemkabdrawerbins.CustID'
    end
    object qryToolCabinetItemID: TIntegerField
      FieldName = 'ItemID'
      Origin = 'tblitemkabdrawerbins.ItemID'
    end
    object qryToolCabinetitemDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'itemDescription'
      LookupDataSet = DM.qryLookupItems
      LookupKeyFields = 'id'
      LookupResultField = 'Description'
      KeyFields = 'ItemID'
      Size = 100
      Lookup = True
    end
    object qryToolCabinetID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'tblitemkabdrawerbins.ID'
    end
    object qryToolCabinetPartNumber: TStringField
      FieldKind = fkLookup
      FieldName = 'PartNumber'
      LookupDataSet = DM.qryLookupItems
      LookupKeyFields = 'id'
      LookupResultField = 'Part__No'
      KeyFields = 'ItemID'
      Lookup = True
    end
  end
  object dsToolCabinet: TMyDataSource
    DataSet = qryToolCabinet
    Left = 192
    Top = 80
  end
end
