object frmKeypad: TfrmKeypad
  Left = 0
  Top = 0
  ActiveControl = fltlbr1
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'frmKeypad'
  ClientHeight = 281
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object tchkybrd1: TTouchKeyboard
    Left = 0
    Top = 25
    Width = 305
    Height = 256
    Align = alClient
    GradientEnd = clSilver
    GradientStart = clGray
    Layout = 'NumPad'
    ExplicitLeft = 8
    ExplicitTop = 32
    ExplicitWidth = 550
    ExplicitHeight = 180
  end
  object fltlbr1: TFlatTitlebar
    Left = 0
    Top = 0
    Width = 305
    Height = 25
    ActiveTextColor = clWhite
    InactiveTextColor = 8559266
    TitlebarColor = clBlack
    Align = alTop
    Caption = 'KEYPAD'
  end
end
