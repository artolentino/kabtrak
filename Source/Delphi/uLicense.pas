(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uLicense.pas
   Description            :  Licensing Screen
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)
unit uLicense;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage;

type
  TfrmLicense = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    Panel1: TPanel;
    ProgramIcon: TImage;
    lblProductName: TLabel;
    lblVersion: TLabel;
    lblCompanyName: TLabel;
    lblTrademark: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    editRegisteredTo: TEdit;
    editRegistrationCode: TEdit;
    lblRegID: TLabel;
    edtRegistrationID: TEdit;
    lblEmail: TLabel;
    edtEmail: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLicense: TfrmLicense;

implementation

uses main;

{$R *.dfm}

procedure TfrmLicense.btnOKClick(Sender: TObject);
begin
  with frmMain do
  begin
    Reg.DoRegistration(edtRegistrationID.Text,editRegisteredTo.Text,editRegistrationCode.Text,edtEmail.text);
    if not Reg.CheckRegistered then
      begin
        ShowMessage('There was an error validating your registration information. Please try again.');
      end
  else
      begin
        ShowMessage('Registration Successful! Thank you for registering your product.');
        Close;
      end;

  end;
end;

procedure TfrmLicense.FormShow(Sender: TObject);
begin
// with frmMain.kbQwerty do
//   begin
//      Show;
//      Keyboard.LoadKeybdLayout('qwery.txt');
//      Keyboard.Width := 401;
//      Keyboard.Height := 201;
//
//   end;
  editRegisteredTo.Text := frmMain.strCompany;
  edtEmail.Text := frmMain.strEmail;
end;

end.
