unit uAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TFlatPanelUnit, pngimage, TFlatTitlebarUnit, ComCtrls,
  Buttons, StdCtrls, DB, DBAccess, MyAccess, DBCtrls, Grids, DBGrids,
  JvExExtCtrls, JvExtComponent, JvItemsPanel, JvExControls, JvOutlookBar;

type
  TfrmAdmin = class(TForm)
    fltlbrAbout: TFlatTitlebar;
    imgAbout: TImage;
    imgExit: TImage;
    fltpnlTopBottom: TFlatPanel;
    fltpnlTop1: TFlatPanel;
    btnDatasource: TSpeedButton;
    btn1: TSpeedButton;
    lbl1: TLabel;
    dsKabs: TMyDataSource;
    pgc1: TPageControl;
    ts1: TTabSheet;
    lbl2: TLabel;
    dbgrd1: TDBGrid;
    dblst1: TDBListBox;
    TabSheet1: TTabSheet;
    ts2: TTabSheet;
    procedure imgExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdmin: TfrmAdmin;

implementation

uses uDataModule;

{$R *.dfm}

procedure TfrmAdmin.imgExitClick(Sender: TObject);
begin
  Close;
end;

end.
