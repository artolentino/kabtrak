unit uCategory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, MemDS, DBAccess, MyAccess, StdCtrls, Mask, DBCtrls, ExtCtrls;

type
  TfrmGategory = class(TForm)
    tblCategory: TMyTable;
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    tblCategoryCatID: TIntegerField;
    tblCategoryDescription: TStringField;
    tblCategoryCustID: TIntegerField;
    lbl1: TLabel;
    editDescription: TDBEdit;
    ds1: TDataSource;
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGategory: TfrmGategory;

implementation

uses uDataModule, uCategories;

{$R *.dfm}

procedure TfrmGategory.btnCancelClick(Sender: TObject);
begin
 Self.Close;
end;

procedure TfrmGategory.btnOKClick(Sender: TObject);
begin
    if tblCategory.State in [dsInsert, dsEdit] then
    begin
      tblCategory.post;
    end;

    frmCategories.tblCategories.Refresh;
  Self.Close;
end;

procedure TfrmGategory.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmGategory.FormCreate(Sender: TObject);
begin
  if not tblCategory.Active then
    begin
      tblCategory.Connection := DM.conHenchman;
      tblCategory.Active := True;
    end;
end;

end.
