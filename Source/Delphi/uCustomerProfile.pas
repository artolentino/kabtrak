unit uCustomerProfile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, StdCtrls, DBCtrls, pngimage, ExtCtrls, Mask,
  ComCtrls, MemDS;

type
  TfrmCustomerProfile = class(TForm)
    lbl5: TLabel;
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pnl3: TPanel;
    pgc1: TPageControl;
    ts1: TTabSheet;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lblRequired: TLabel;
    lbl3: TLabel;
    dbedtCompanyName: TDBEdit;
    dbedtAddress1: TDBEdit;
    dbedtAddress2: TDBEdit;
    dbedtAddress3: TDBEdit;
    dbedtContactPerson: TDBEdit;
    dbedtTelNo: TDBEdit;
    dbedtEmail: TDBEdit;
    ts2: TTabSheet;
    lbl11: TLabel;
    imgIcon: TImage;
    dbtxtJobOrderLabel: TDBText;
    lbl1: TLabel;
    lbl4: TLabel;
    dbedtJobOrderLabel: TDBEdit;
    ts4: TTabSheet;
    dbmmonotes: TDBMemo;
    dsCustomer: TMyDataSource;
    qryCustCounter: TMyQuery;
    qryCustomer: TMyQuery;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryCustomerAfterInsert(DataSet: TDataSet);
    procedure qryCustomerAfterPost(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomerProfile: TfrmCustomerProfile;

implementation

uses uWelcome;

{$R *.dfm}

procedure TfrmCustomerProfile.btnCancelClick(Sender: TObject);
begin
  with qryCustomer do
    begin
      if State in  [dsInsert, dsEdit] then
        begin
          if MessageDlg('There are pending operations for the current transaction. Would you like to abort the operation?',
            mtInformation, [mbYes, mbNo], 0) = mrYes then Self.Close else Exit;

        end;
    end;
end;

procedure TfrmCustomerProfile.btnOKClick(Sender: TObject);
begin
  with qryCustomer do
    begin
      if State in [dsInsert, dsEdit] then
         begin
           try
           Post;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to post. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;

           end;
         end;

         // update dropdown control
         with frmWelcome do
         begin
              qryCustomer.Open;
              qryCustomer.RefreshRecord;
              cbb1.Items.Clear;
               cbb1.Items.Add('--- Select a customer profile ----');
               qryCustomer.First;
               while not qryCustomer.Eof do
                 begin
                   with cbb1 do
                     begin
                       Items.AddObject(qryCustomer.FieldByName('CompanyName').AsString, TObject(qryCustomer.FieldByName('CustID').AsInteger));
                     end;

                  qryCustomer.Next;
                 end;
                qryCustomer.Close;
               cbb1.ItemIndex := 0;

           end;
    end;
    Self.Close;
end;

procedure TfrmCustomerProfile.FormCreate(Sender: TObject);
begin
  // Check if the counter is already initialized
  with qryCustCounter do
    begin
      Connection := frmWelcome.conTest;
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM tblCustomerIDCounter');
      try
       Open;
       except
       on E : Exception do
          begin
            MessageDlg('Unable to open Counter table. ' + E.Message, mtError, [mbOK], 0);
            Exit;
          end;

      end;

      if IsEmpty then
        begin
          Insert;
          FieldByName('CustomerID').Value := 100;
           try
           Post;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to post the new record. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;
          end;

        end;
    end;

    with qryCustomer do
      begin
        Connection := frmWelcome.conTest;
        Close;
        SQL.Clear;
        SQL.Add('SELECT * FROM tblcustomer');
         try
           Open;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to open customer table. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;
          end;

      end;

end;

procedure TfrmCustomerProfile.qryCustomerAfterInsert(DataSet: TDataSet);
begin
  with qryCustomer do
    begin
      if Active  then
        begin
          qryCustomer.FieldByName('CustID').Value := qryCustCounter.FieldByName('CustomerID').AsInteger;
          qryCustomer.FieldByName('status').Value := 0;
        end;
    end;
end;

procedure TfrmCustomerProfile.qryCustomerAfterPost(DataSet: TDataSet);
begin
  with qryCustCounter do
    begin
      if Active  then
        begin
          Edit;
          FieldByName('CustomerID').Value := qryCustCounter.FieldByName('CustomerID').AsInteger + 1;
          try
           Post;
          except
           on E : Exception do
              begin
                MessageDlg('Unable to save record. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;
          end;
        end;

    end;
end;

end.
