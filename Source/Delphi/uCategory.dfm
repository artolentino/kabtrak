object frmGategory: TfrmGategory
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Category'
  ClientHeight = 90
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 22
    Top = 21
    Width = 57
    Height = 13
    Caption = 'Description:'
    FocusControl = editDescription
  end
  object pnl1: TPanel
    Left = 0
    Top = 59
    Width = 400
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 156
    ExplicitWidth = 463
    object pnl2: TPanel
      Left = 231
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 294
      object btnOK: TButton
        Left = 6
        Top = 1
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 1
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object editDescription: TDBEdit
    Left = 91
    Top = 18
    Width = 289
    Height = 21
    DataField = 'Description'
    DataSource = ds1
    TabOrder = 1
  end
  object tblCategory: TMyTable
    TableName = 'tbltoolcategories'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = DM.dsCustomer
    Connection = DM.conHenchman
    Left = 24
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object tblCategoryCatID: TIntegerField
      FieldName = 'CatID'
    end
    object tblCategoryDescription: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object tblCategoryCustID: TIntegerField
      FieldName = 'CustID'
    end
  end
  object ds1: TDataSource
    DataSet = tblCategory
    Left = 80
    Top = 56
  end
end
