unit uDepartments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, DB, DBAccess, MyAccess, MemDS, AdvCardList,
  DBAdvCardList, NxPageControl, NxColumns, NxDBColumns, NxScrollControl,
  NxCustomGridControl, NxCustomGrid, NxDBGrid, AdvSmoothListBox,
  DBAdvSmoothListBox;

type
  TfrmDepartments = class(TForm)
    pnl1: TPanel;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnClose: TSpeedButton;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    qryDepartments: TMyQuery;
    dsDepartments: TMyDataSource;
    lst1: TDBAdvSmoothListBox;
    qryDepartmentsid: TIntegerField;
    qryDepartmentsdescription: TStringField;
    qryDepartmentscustID: TIntegerField;
    procedure btnCloseClick(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDepartments: TfrmDepartments;

implementation

uses uDataModule, uDepartment;

{$R *.dfm}

procedure TfrmDepartments.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmDepartments.btnDeleteClick(Sender: TObject);
var
  msg : string;
begin

 msg := 'Do you really want to delete Department ' + qryDepartmentsdescription.AsString + '?';
 if qryDepartmentsid.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       qryDepartments.delete;
       qryDepartments.RefreshRecord;

     end;
end;

procedure TfrmDepartments.btnDownClick(Sender: TObject);
begin
  qryDepartments.Next;
end;

procedure TfrmDepartments.btnEditClick(Sender: TObject);
begin
  if qryDepartmentsid.AsVariant = null then btnNewClick(sender)   //No record selected.
  else begin
    screen.cursor := crHourglass;
    application.createform (TfrmDepartment,frmDepartment);
    frmDepartment.tblDepartment.Locate('id', qryDepartmentsid.value, []);
    frmDepartment.Caption := 'Editing ' + qryDepartmentsdescription.AsString;
    frmDepartment.Showmodal;
    screen.cursor := crDefault;
  end;
end;

procedure TfrmDepartments.btnFirstClick(Sender: TObject);
begin
  qryDepartments.First;
end;

procedure TfrmDepartments.btnLastClick(Sender: TObject);
begin
 qryDepartments.Last;
end;

procedure TfrmDepartments.btnNewClick(Sender: TObject);
begin
  screen.cursor := crHourglass;
  application.createform (TfrmDepartment,frmDepartment);
  frmDepartment.Caption := 'New Department Record';
  frmDepartment.tblDepartment.Insert;
  frmDepartment.ShowModal;
  screen.cursor := crDefault;
end;

procedure TfrmDepartments.btnRefreshClick(Sender: TObject);
begin
qryDepartments.RefreshRecord;
end;

procedure TfrmDepartments.btnUpClick(Sender: TObject);
begin
  qryDepartments.Prior;
end;

procedure TfrmDepartments.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryDepartments.Close;
  Action := caFree;
end;

procedure TfrmDepartments.FormCreate(Sender: TObject);
begin
  if not qryDepartments.Active then
     begin
       qryDepartments.Connection := DM.conHenchman;
       qryDepartments.Active := True;
     end;
end;

end.
