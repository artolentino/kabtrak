unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, TFlatPanelUnit, TFlatTitlebarUnit,
  TFlatSpeedButtonUnit, StdCtrls,DB, DBAccess, MyAccess, Buttons;

type
  TfrmAbout = class(TForm)
    fltlbrAbout: TFlatTitlebar;
    fltpnlTopBottom: TFlatPanel;
    imgAbout: TImage;
    btnClose: TFlatSpeedButton;
    imgLogo: TImage;
    lbl1: TLabel;
    lblCompany: TLabel;
    lblHenchman: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    StatusLabel: TLabel;
    lblTimeLimit: TLabel;
    lblTimeLimitValue: TLabel;
    lblDaysLeft: TLabel;
    lblDaysLeftValue: TLabel;
    lbl2: TLabel;
    lblEmail: TLabel;
    lblRegCode: TLabel;
    Label7: TLabel;
    btnRegister: TFlatSpeedButton;
    procedure btnCloseClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ShowStatus(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

uses GlobalRoutines, uDataModule, main;

{$R *.dfm}

procedure TfrmAbout.ShowStatus;
begin
   // Show Time Limits
    lblTimeLimit.Visible := not frmMain.Reg.Registered;
    lblDaysLeft.Visible := not frmMain.Reg.Registered;
    lblTimeLimitValue.Visible := not frmMain.Reg.Registered;
    lblDaysLeftValue.Visible := not frmMain.Reg.Registered;
    btnRegister.Visible :=not frmMain.Reg.Registered;

  lblTimeLimitValue.Caption := IntToStr(frmMain.Reg.Days);
  lblDaysLeftValue.Caption := IntToStr(frmMain.Reg.DaysLeft);
  if frmMain.Reg.Registered then
  begin
    frmAbout.Height := 287;
    StatusLabel.Caption := 'Registered';
    lblRegCode.Caption := frmMain.Reg.LicenseID;
    lblCompany.Caption := frmMain.Reg.Organization;
    lblEmail.Caption :=  GetCustomerEmail;
  end else
  begin
     frmAbout.Height := 372;
    if frmMain.Reg.Expired then StatusLabel.Caption := 'Expired' else
      StatusLabel.Caption := 'Unregistered';
      lblCompany.Caption := frmMain.Reg.Organization;
      lblEmail.Caption :=  GetCustomerEmail;
      lblRegCode.Caption := '';

  end;
end;

procedure TfrmAbout.btnCloseClick(Sender: TObject);
begin
  close;
end;

procedure TfrmAbout.FormActivate(Sender: TObject);
begin
lblCompany.Caption := GetCustomerName();
end;

procedure TfrmAbout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := caFree;
end;

end.
