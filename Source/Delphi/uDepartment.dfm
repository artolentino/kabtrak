object frmDepartment: TfrmDepartment
  Left = 0
  Top = 0
  Caption = 'Department'
  ClientHeight = 101
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 22
    Top = 21
    Width = 57
    Height = 13
    Caption = 'Description:'
    FocusControl = editDescription
  end
  object pnl1: TPanel
    Left = 0
    Top = 70
    Width = 398
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 229
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 6
        Top = 1
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 1
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object editDescription: TDBEdit
    Left = 91
    Top = 18
    Width = 289
    Height = 21
    CharCase = ecUpperCase
    DataField = 'Description'
    DataSource = dsDepartment
    TabOrder = 1
  end
  object tblDepartment: TMyTable
    TableName = 'tbldepartments'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = DM.dsCustomer
    Connection = DM.conHenchman
    Left = 24
    Top = 48
    object tblDepartmentid: TIntegerField
      FieldName = 'id'
    end
    object tblDepartmentdescription: TStringField
      FieldName = 'description'
      Size = 100
    end
    object tblDepartmentcustID: TIntegerField
      FieldName = 'custID'
    end
  end
  object dsDepartment: TDataSource
    DataSet = tblDepartment
    Left = 88
    Top = 48
  end
end
