unit login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, TFlatSpeedButtonUnit, TFlatPanelUnit, Keyboard,
  StdCtrls, TFlatEditUnit;

type
  TfrmLogin = class(TForm)
    edtPIN: TFlatEdit;
    edtUserID: TFlatEdit;
    FlatPanel1: TFlatPanel;
    imgUser: TImage;
    FlatPanel2: TFlatPanel;
    btnCancel: TFlatSpeedButton;
    btnOK: TFlatSpeedButton;
    KeyBoard: TTouchKeyboard;
    lblPIN: TLabel;
    lblUserID: TLabel;
    procedure imgExitClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edtUserIDEnter(Sender: TObject);
    procedure edtUserIDExit(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure ActivateVirtualKeyboard(Control: TWinControl; Keyboard: TTouchKeyboard);
    procedure CmFocusChanged(var Msg: TCMFocusChanged); message CM_FOCUSCHANGED;
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation



{$R *.dfm}

procedure TfrmLogin.ActivateVirtualKeyboard(Control: TWinControl; Keyboard: TTouchKeyboard);
var
  APoint : TPoint;
begin
  if Control is TCustomEdit then
  begin
    APoint := Control.ClientToScreen(Point(0,0));
    APoint := Keyboard.Parent.ScreenToClient(APoint);
    Keyboard.Left := APoint.X;
    Keyboard.Top := APoint.Y + (Control.Height);
    Keyboard.Visible := True;
  end
  else
    Keyboard.Visible := False;
end;

procedure TfrmLogin.CmFocusChanged(var Msg: TCMFocusChanged);
begin
  ActivateVirtualKeyboard(Msg.Sender, KeyBoard);
end;

procedure TfrmLogin.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmLogin.btnOKClick(Sender: TObject);
begin
  //Check for valid input
  //blank is not allowed

  if (edtUserID.Text = '') or (edtPIN.Text = '') then
     begin
       ShowMessage('Please enter your User ID and PIN to Login');

     end;
end;

procedure TfrmLogin.edtUserIDEnter(Sender: TObject);
begin
 frmLogin.Width := 604;
end;

procedure TfrmLogin.edtUserIDExit(Sender: TObject);
begin
  frmLogin.Width := 324;
end;

procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmLogin.imgExitClick(Sender: TObject);
begin
  Close;
end;

end.
