unit uNotifications;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, TFlatTitlebarUnit, Buttons, TFlatPanelUnit, DB,
  DBAccess, MyAccess, Grids, DBGrids, DBCtrls, StdCtrls, Mask, JvExMask,
  JvToolEdit, JvMaskEdit, JvDBFindEdit, Keyboard;

type
  TfrmNotifications = class(TForm)
    fltlbrAbout: TFlatTitlebar;
    imgAbout: TImage;
    imgExit: TImage;
    fltpnlTopBottom: TFlatPanel;
    fltpnlTop2: TFlatPanel;
    dsNotifications: TMyDataSource;
    dbgrd1: TDBGrid;
    fltpnlTop1: TFlatPanel;
    dbnvgr1: TDBNavigator;
    edt1: TJvDBFindEdit;
    img1: TImage;
    KeyBoard: TTouchKeyboard;
    procedure imgExitClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    procedure ActivateVirtualKeyboard(Control: TWinControl; Keyboard: TTouchKeyboard);
    procedure CmFocusChanged(var Msg: TCMFocusChanged); message CM_FOCUSCHANGED;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNotifications: TfrmNotifications;

implementation

uses uDataModule, GlobalRoutines;

{$R *.dfm}

procedure TfrmNotifications.ActivateVirtualKeyboard(Control: TWinControl; Keyboard: TTouchKeyboard);
var
  APoint : TPoint;
begin
  if Control is TCustomEdit then
  begin
    APoint := Control.ClientToScreen(Point(0,0));
    APoint := Keyboard.Parent.ScreenToClient(APoint);
    Keyboard.Left := APoint.X;
    Keyboard.Top := APoint.Y + (Control.Height);
    Keyboard.Visible := True;
  end
  else
    Keyboard.Visible := False;
end;

procedure TfrmNotifications.CmFocusChanged(var Msg: TCMFocusChanged);
begin
  ActivateVirtualKeyboard(Msg.Sender, KeyBoard);
end;

procedure TfrmNotifications.btnOkClick(Sender: TObject);
begin
 Close;
end;

procedure TfrmNotifications.imgExitClick(Sender: TObject);
begin
  close;
end;

end.
