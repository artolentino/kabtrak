unit uSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, MemDS, DBAccess, MyAccess, StdCtrls, DBCtrls, Mask, ComCtrls,
  ExtCtrls;

type
  TfrmSupplier = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    tblSupplier: TMyTable;
    tblSupplierid: TIntegerField;
    tblSupplierSupplierName: TStringField;
    tblSupplierContactPerson: TStringField;
    tblSupplierEmailAdd: TStringField;
    tblSupplierWebsite: TStringField;
    tblSupplierMobilePhone: TStringField;
    tblSupplierCustID: TIntegerField;
    lbl1: TLabel;
    editSupplierName: TDBEdit;
    dsSupplier: TDataSource;
    lbl2: TLabel;
    editContactPerson: TDBEdit;
    lbl3: TLabel;
    editEmailAdd: TDBEdit;
    lbl4: TLabel;
    editWebsite: TDBEdit;
    lbl5: TLabel;
    editMobilePhone: TDBEdit;
    lblRequired: TLabel;
    lbl6: TLabel;
    lbl8: TLabel;
    dbredt1: TDBRichEdit;
    tblSuppliernotes: TMemoField;
    tblCounter: TMyTable;
    tblSuppliersupplier_id: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tblSupplierAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure tblSupplierAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSupplier: TfrmSupplier;

implementation

uses uDataModule, main, uSuppliers, GlobalRoutines;

{$R *.dfm}

procedure TfrmSupplier.btnCancelClick(Sender: TObject);
begin
  if tblSupplier.State in [dsEdit, dsInsert] then
    begin
      if MessageDlg('There are pending  operations for this transaction. DO you really want to abort the whole operation?',
        mtWarning, [mbYes, mbNo], 0) = mrYes  then
        begin
         tblSupplier.Cancel;
         Self.Close;
        end
      else Exit;

        end;

end;

procedure TfrmSupplier.btnOKClick(Sender: TObject);
begin
   if editSupplierName.Text = '' then
   begin
     ShowMessage('Please enter valid Supplier Company name');
     Exit;
   end;

    if editContactPerson.Text = '' then
   begin
     ShowMessage('Please enter valid Contact name');
     Exit;
   end;

   if tblSupplier.State in [dsInsert, dsEdit] then
    begin
      tblSupplier.post;

    end;
      Self.Close;
   frmSuppliers.tblSuppliers.Refresh;
end;

procedure TfrmSupplier.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 tblSupplier.close;
  tblCounter.close;
  Action := caFree;
end;

procedure TfrmSupplier.FormCreate(Sender: TObject);
begin
pgc1.ActivePageIndex := 0;
  tblSupplier.Open;
  tblCounter.Open;
end;

procedure TfrmSupplier.FormKeyPress(Sender: TObject; var Key: Char);
begin

   ShowMessage(Key);
  //if Key = '' then btnCancelClick(nil);



  //if HiWord(GetKeyState(VK_ESCAPE)) <> 0 then ShowMessage('PageUp - DOWN') else ShowMessage('PageUp - UP') ;
end;

procedure TfrmSupplier.FormShow(Sender: TObject);
begin
 tblSupplier.Open;

end;

procedure TfrmSupplier.tblSupplierAfterInsert(DataSet: TDataSet);
begin
  tblSupplier.FieldByName('id').Value := tblCounter.FieldByName('supplierID').AsInteger + 1;
  tblSupplier.FieldByName('supplier_id').Value := StrZeroInt(DM.qryCustomer.FieldByName('CustID').AsInteger, 3)+ '-' + StrZeroInt(tblSupplier.FieldByName('id').AsInteger,3);
end;

procedure TfrmSupplier.tblSupplierAfterPost(DataSet: TDataSet);
begin
  tblCounter.Edit;
  tblCounter.FieldByName('supplierID').Value := tblCounter.FieldByName('supplierID').AsInteger + 1;
  tblCounter.Post;

end;

end.
