unit uRegister;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TFlatSpeedButtonUnit, ExtCtrls, TFlatPanelUnit, TFlatTitlebarUnit,
  StdCtrls, Keyboard, pngimage;

type
  TfrmRegister = class(TForm)
    fltlbrAbout: TFlatTitlebar;
    imgAbout: TImage;
    fltpnlTopBottom: TFlatPanel;
    btnClose: TFlatSpeedButton;
    RegCodeBox: TEdit;
    Label5: TLabel;
    EmailBox: TEdit;
    Label12: TLabel;
    OrganizationBox: TEdit;
    Label13: TLabel;
    Label4: TLabel;
    btnCancel: TFlatSpeedButton;
    tchkybrd1: TTouchKeyboard;
    LicenseIDBox: TEdit;
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegister: TfrmRegister;

implementation

uses uAbout, GlobalRoutines, main;

{$R *.dfm}

procedure TfrmRegister.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmRegister.btnCloseClick(Sender: TObject);
begin
if frmMain.Reg.DoRegistration(LicenseIDBox.Text, OrganizationBox.Text, RegCodeBox.Text, EmailBox.Text) = false then
    ShowMessage('Unable to register program!')
  else
    ShowMessage('Successfully Registered');
 close;

end;

procedure TfrmRegister.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Action := caFree;
end;

procedure TfrmRegister.FormShow(Sender: TObject);
begin

   OrganizationBox.Text := GetCustomerName;
   EmailBox.Text := GetCustomerEmail;


end;

end.
