object frmFindEmployees: TfrmFindEmployees
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Find Employee'
  ClientHeight = 91
  ClientWidth = 393
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 18
    Top = 24
    Width = 80
    Height = 13
    Caption = 'Employee Name:'
  end
  object pnl1: TPanel
    Left = 0
    Top = 61
    Width = 393
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 224
      Top = 0
      Width = 169
      Height = 30
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 5
        Top = 3
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 3
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object edtEmployee: TEdit
    Left = 103
    Top = 21
    Width = 276
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
end
