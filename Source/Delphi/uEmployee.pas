unit uEmployee;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Mask, Buttons, ComCtrls, ExtCtrls, DB, DBAccess,
  MyAccess, MemDS, pngimage;

type
  TfrmEmployee = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Panel3: TPanel;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    memoNotes: TDBMemo;
    Label14: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    editJobTitle: TDBEdit;
    editTel: TDBEdit;
    editEMail: TDBEdit;
    editHomePhone: TDBEdit;
    editFirstName: TDBEdit;
    Label1: TLabel;
    editMiddleName: TDBEdit;
    Label4: TLabel;
    editLastName: TDBEdit;
    Label5: TLabel;
    editExtName: TDBEdit;
    dsEmployee: TMyDataSource;
    tsEmployment: TTabSheet;
    btnGrant: TButton;
    dsUser: TMyDataSource;
    grp1: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    lbl2: TLabel;
    dblkcbb1: TDBLookupComboBox;
    tblCounter: TMyTable;
    tblCounterid: TIntegerField;
    tblCountertoolsID: TIntegerField;
    tblCountersupplierID: TIntegerField;
    tblCounternotificationID: TIntegerField;
    tblCounterjobID: TIntegerField;
    tblCounteremployeeID: TIntegerField;
    tblCounteruserID: TIntegerField;
    tblCounterkabID: TIntegerField;
    tblCounterkabDrawerID: TIntegerField;
    tblCounterkabBinID: TIntegerField;
    tblCountertransactionID: TIntegerField;
    tblCounterCustID: TIntegerField;
    edtUserID: TEdit;
    edtPIN: TEdit;
    rgAccessType: TRadioGroup;
    rgEnable: TRadioGroup;
    img1: TImage;
    qryEmployee: TMyQuery;
    tblUser: TMyTable;
    qryEmployeeID: TIntegerField;
    qryEmployeeUserID: TStringField;
    qryEmployeeLastName: TStringField;
    qryEmployeeMiddleName: TStringField;
    qryEmployeeFirstName: TStringField;
    qryEmployeeExtName: TStringField;
    qryEmployeeAddress1: TStringField;
    qryEmployeeAddress2: TStringField;
    qryEmployeeAddress3: TStringField;
    qryEmployeeDeptCode: TStringField;
    qryEmployeePosition: TStringField;
    qryEmployeeemail: TStringField;
    qryEmployeeTelNo: TStringField;
    qryEmployeeMobileNumber: TStringField;
    qryEmployeeCustID: TIntegerField;
    qryEmployeeCreateDate: TDateField;
    qryEmployeeCreateTime: TTimeField;
    tblUserUserID: TStringField;
    tblUserPIN: TStringField;
    tblUserFullName: TStringField;
    tblUserEnabled: TIntegerField;
    tblUserCreatedDate: TDateField;
    tblUserLastAccess: TDateField;
    tblUserLastAccTime: TDateTimeField;
    tblUserCreatedTime: TTimeField;
    tblUserAccessCount: TIntegerField;
    tblUserCustID: TIntegerField;
    tblUseraccess_type: TIntegerField;
    tblUserID: TIntegerField;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GrantAccess(Sender: TObject);
    procedure RevokeAccess(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tblEmployeeAfterInsert(DataSet: TDataSet);
    procedure tblEmployeeBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryEmployeeAfterInsert(DataSet: TDataSet);
    procedure qryEmployeeAfterPost(DataSet: TDataSet);
    procedure tblUserAfterInsert(DataSet: TDataSet);
    procedure tblUserAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmployee: TfrmEmployee;

implementation

uses uEmployees, uDataModule, GlobalRoutines, main;

{$R *.dfm}

procedure TfrmEmployee.btnCancelClick(Sender: TObject);
var
msg : string;
begin

 msg := 'Employee record have pending transactions. Do you really want to cancel the current operations ?' ;
  if qryEmployee.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOK then
        begin
          qryEmployee.cancel;
          Close;
        end;
    end
  else
    close;
end;

procedure TfrmEmployee.GrantAccess(Sender: TObject);
begin
  // Ctrate employee User record for kabTRAK
  // Find if the user is already in the User Table
  if not tblUser.Locate('UserID', qryEmployeeUserID.AsString, []) then
    begin
      tblUser.Insert;
      tblUserFullName.Value := qryEmployeeFirstName.AsString + ' ' + qryEmployeeLastName.AsString;
      tblUserPIN.Value := '12345';
      tblUserEnabled.Value := 1;
      tblUserCreatedDate.Value := Date();
      tblUserCreatedTime.Value := time();
      tblUseraccess_type.Value := 0;
      tblUserAccessCount.Value := 0;
      tblUser.Post;
    end;

  // Grant Access to current user
  edtUserID.Text := tblUserUserID.AsString;
  edtPIN.Text := tblUserPIN.AsString;
  rgAccessType.ItemIndex := tblUseraccess_type.AsInteger;
  rgEnable.ItemIndex := tblUserEnabled.AsInteger;



  grp1.Visible := True;
  rgEnable.Visible := True;
  rgAccessType.Visible := True;
  btnGrant.Caption := 'Revoke Access';
  btnGrant.OnClick := RevokeAccess;

end;

procedure TfrmEmployee.qryEmployeeAfterInsert(DataSet: TDataSet);
begin
  // Update EmployeeID
  qryEmployeeID.Value := tblCounteremployeeID.Value + 1;
  qryEmployeeUserID.Value :=  StrZeroInt(DM.qryCustomer.FieldByName('CustID').ASInteger, 3) + StrZeroInt(qryEmployeeID.AsInteger,3);
end;

procedure TfrmEmployee.qryEmployeeAfterPost(DataSet: TDataSet);
begin
  tblCounter.Edit;
  tblCounteremployeeID.Value := tblCounteremployeeID.Value + 1;
  tblCounter.Post;
end;

procedure TfrmEmployee.RevokeAccess(Sender: TObject);
var
msg : string;
begin
  msg := 'Do you really want to revoke the access of ' + qryEmployeeFirstName.AsString + ' ' + qryEmployeeLastName.AsString +
         ' to the kabTRAK ?' ;
  if tblUser.Locate('UserID',qryEmployeeUserID.AsString, []) then
    begin
      if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOK then
        begin
          tblUser.Delete;
          ShowMessage('Access to kabTRAK successfully revoked.');
          btnGrant.Caption := 'Grant Access';
          btnGrant.OnClick := GrantAccess;
          grp1.Visible :=false;
          rgAccessType.Visible := false;
          btnGrant.Caption := 'Grant Access';

        end
   end;

end;

procedure TfrmEmployee.tblEmployeeAfterInsert(DataSet: TDataSet);
begin
  qryEmployeeID.Value := tblCounter.FieldByName('employeeID').AsInteger + 1;
  qryEmployeeUserID.Value := StrZeroInt(qryEmployeeCustID.AsInteger,3) + StrZeroInt(qryEmployeeID.AsInteger,3);
end;

procedure TfrmEmployee.tblEmployeeBeforePost(DataSet: TDataSet);
begin
  qryEmployeeCreateDate.Value := Now();
  qryEmployeeCreateTime.Value := Time();

  tblCounter.Edit;
  tblCounter.FieldByName('employeeID').Value := tblCounter.FieldByName('employeeID').AsInteger + 1;
  tblCounter.Post;
  end;

procedure TfrmEmployee.tblUserAfterInsert(DataSet: TDataSet);
begin
  tblUserID.Value := tblCounteruserID.AsInteger + 1;
  tblUserUserID.Value := qryEmployeeUserID.AsString;
end;

procedure TfrmEmployee.tblUserAfterPost(DataSet: TDataSet);
begin
  tblCounter.Edit;
  tblCounteruserID.Value :=  tblCounteruserID.Value + 1;
  tblCounter.Post;
end;

procedure TfrmEmployee.btnOKClick(Sender: TObject);
begin
        if qryEmployee.State in [dsInsert, dsEdit] then
              begin
                qryEmployee.post;
                frmEmployees.qryEmployees.Locate('ID', qryEmployeeID.AsInteger, []);
                frmEmployees.qryEmployees.Refresh;
              end;


     if btnGrant.Caption = 'Revoke Access' then
        begin
            if tblUser.Locate('UserID',qryEmployeeUserID.AsString, []) then
            begin
                if tblUser.State in [dsEdit] then
                begin
                    tblUser.Edit;
                    tblUser.FieldByName('access_type').Value := rgAccessType.ItemIndex;
                    tblUser.FieldByName('Enabled').Value := rgEnable.ItemIndex;
                    tblUser.Post;
                end;
            end;
        end;



    Self.Close;
end;
procedure TfrmEmployee.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryEmployee.close;
  tblUser.close;
  Action := caFree;
end;



procedure TfrmEmployee.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  tblCounter.Open;
  qryEmployee.Open;
  tblUser.Open;
end;

procedure TfrmEmployee.FormShow(Sender: TObject);
begin

  PageControl1.ActivePageIndex := 0;


  // Find the current employee in the user table, if not found make grant access button visible
   if tblUser.Locate('UserID', qryEmployeeUserID.AsString, []) then
   begin
      edtUserID.Text := tblUser.FieldByName('UserID').AsString;
      edtPIN.Text := tblUser.FieldByName('PIN').AsString;
      rgAccessType.ItemIndex := tblUser.FieldByName('access_type').AsInteger;
      rgEnable.ItemIndex := tblUser.FieldByName('Enabled').AsInteger;

      rgEnable.Visible := True;
      grp1.Visible := True;
      rgAccessType.Visible := True;
      btnGrant.Caption := 'Revoke Access';
      btnGrant.OnClick := RevokeAccess;
   end
  else
   begin
      rgEnable.Visible := False;
      grp1.Visible := false;
      rgAccessType.Visible := False;
      btnGrant.Caption := 'Grant Access';
      btnGrant.OnClick := GrantAccess;
   end;

end;

end.
