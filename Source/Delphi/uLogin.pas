(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uLogin.pas
   Description            :  Login Form
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)

unit uLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, MemDS, DBAccess, MyAccess,
  AdvSmoothTouchKeyBoard, AdvMetroButton, Grids, DBGrids;

type
  TfrmLogin = class(TForm)
  //TfrmLogin = class(TForm)
    Image1: TImage;
    labUsername: TLabel;
    txtUsername: TEdit;
    labPassword: TLabel;
    txtPassword: TEdit;
    btnLogin: TButton;
    btnCancel: TButton;
    ds1: TMyDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

uses uDataModule, main, GlobalRoutines;

{$R *.dfm}

procedure TfrmLogin.btnCancelClick(Sender: TObject);
begin
  Self.close;
end;

procedure TfrmLogin.btnLoginClick(Sender: TObject);
 begin
  if isUserLogged(txtUsername.Text, txtPassword.Text) then Close else
  ShowMessage('Unable to login! Please check your credentials');


 end;


procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
