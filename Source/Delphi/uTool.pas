unit uTool;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Mask, Buttons, ComCtrls, ExtCtrls, DB, DBAccess,
  MyAccess, MemDS;

type
  TfrmTool = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Panel3: TPanel;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label14: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    editSerialNo: TDBEdit;
    editCost: TDBEdit;
    Label1: TLabel;
    editDescription: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    dsTools: TMyDataSource;
    dsCategories: TMyDataSource;
    dsSubCategories: TMyDataSource;
    dblkcbbcatID: TDBLookupComboBox;
    dblkcbbSupplierID: TDBLookupComboBox;
    dsSuppliers: TMyDataSource;
    lblPartNumber: TLabel;
    dbtxtPartNumber: TDBText;
    bvl1: TBevel;
    dblkcbbSubCat: TDBLookupComboBox;
    qryTools: TMyQuery;
    qryCategories: TMyQuery;
    qrySubCat: TMyQuery;
    qrySuppliers: TMyQuery;
    qryCounter: TMyQuery;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryToolsAfterInsert(DataSet: TDataSet);
    procedure qryToolsAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    intLastID : Integer;
  end;

var
  frmTool: TfrmTool;

implementation

uses uTools, uDataModule, GlobalRoutines, main;

{$R *.dfm}

procedure TfrmTool.btnCancelClick(Sender: TObject);
var
msg : string;
begin

 msg := 'Tools record have been modified. Do you really want to cancel the current operations ?' ;
  if qryTools.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOK then
        begin
          qryTools.cancel;
          Close;
        end;
    end
  else
    close;
end;

procedure TfrmTool.btnOKClick(Sender: TObject);
begin
    if qryTools.State in [dsInsert, dsEdit] then
    begin
      qryTools.post;
    end;

    frmTools.qryTools.Refresh;
  Close;
end;

procedure TfrmTool.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryTools.Close;
  qryCategories.Close;
  qrySubCat.Close;
  qrySuppliers.Close;

  Action := caFree;
end;

procedure TfrmTool.FormCreate(Sender: TObject);
begin
  qryTools.Open;
  qryCategories.Open;
  qrySubCat.Open;
  qrySuppliers.Open;
  qryCounter.Open;
end;

procedure TfrmTool.qryToolsAfterInsert(DataSet: TDataSet);
begin
  qryTools.FieldByName('id').Value := qryCounter.FieldByName('toolsID').AsInteger + 1;
  qryTools.FieldByName('Part__No').Value := StrZeroInt(DM.qryCustomer.FieldByName('CustID').AsInteger, 3) +  qryTools.FieldByName('id').AsString;
end;

procedure TfrmTool.qryToolsAfterPost(DataSet: TDataSet);
begin
  qryCounter.Edit;
  qryCounter.FieldByName('toolsID').Value  := qryCounter.FieldByName('toolsID').Value + 1;
  qryCounter.Post;
end;

end.
