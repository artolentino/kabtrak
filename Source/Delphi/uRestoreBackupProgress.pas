unit uRestoreBackupProgress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls;

type
  TfrmRestoreBackupProgress = class(TForm)
    pb1: TProgressBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRestoreBackupProgress: TfrmRestoreBackupProgress;

implementation

{$R *.dfm}

procedure TfrmRestoreBackupProgress.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
