object frmCreateAdmin: TfrmCreateAdmin
  Left = 2103
  Top = 627
  BorderStyle = bsDialog
  Caption = 'Create Administrator Account'
  ClientHeight = 249
  ClientWidth = 475
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 218
    Width = 475
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = -8
    ExplicitTop = 249
    ExplicitWidth = 465
    object pnl2: TPanel
      Left = 306
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 296
      object btnOK: TButton
        Left = 78
        Top = 4
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 475
    Height = 218
    ActivePage = ts1
    Align = alClient
    TabHeight = 20
    TabOrder = 1
    TabWidth = 100
    ExplicitLeft = 2
    ExplicitTop = 5
    ExplicitWidth = 455
    ExplicitHeight = 238
    object ts1: TTabSheet
      Caption = 'General'
      ExplicitLeft = 8
      ExplicitTop = 24
      ExplicitHeight = 216
      object grpAdmin: TGroupBox
        Left = 34
        Top = 23
        Width = 400
        Height = 138
        Caption = 'Create Administrator Profile'
        TabOrder = 0
        object lbl1: TLabel
          Left = 21
          Top = 42
          Width = 46
          Height = 13
          Caption = 'Fullname:'
        end
        object lbl2: TLabel
          Left = 21
          Top = 69
          Width = 40
          Height = 13
          Caption = 'User ID:'
        end
        object lbl3: TLabel
          Left = 21
          Top = 98
          Width = 21
          Height = 13
          Caption = 'PIN:'
        end
        object lblCustID: TLabel
          Left = 24
          Top = 23
          Width = 43
          Height = 13
          Caption = 'lblCustID'
        end
        object lblKabID: TLabel
          Left = 96
          Top = 23
          Width = 43
          Height = 13
          Caption = 'lblCustID'
        end
        object dbedtUserID: TDBEdit
          Left = 80
          Top = 65
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          DataField = 'UserID'
          DataSource = dsUsers
          ReadOnly = True
          TabOrder = 0
        end
        object dbedtPIN: TDBEdit
          Left = 80
          Top = 92
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          DataField = 'PIN'
          DataSource = dsUsers
          PasswordChar = '*'
          TabOrder = 1
        end
        object dbedtFullname: TDBEdit
          Left = 80
          Top = 38
          Width = 289
          Height = 21
          CharCase = ecUpperCase
          DataField = 'FullName'
          DataSource = dsUsers
          TabOrder = 2
        end
      end
    end
  end
  object qryUsers: TMyQuery
    Connection = frmWelcome.conTest
    SQL.Strings = (
      'SELECT * FROM users WHERE CustID = :CustID and kabID = :kabID')
    AfterInsert = qryUsersAfterInsert
    AfterPost = qryUsersAfterPost
    Active = True
    Left = 168
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end
      item
        DataType = ftUnknown
        Name = 'kabID'
      end>
    object qryUsersID: TIntegerField
      FieldName = 'ID'
      Origin = 'users.ID'
    end
    object qryUsersUserID: TStringField
      FieldName = 'UserID'
      Origin = 'users.UserID'
    end
    object qryUsersPIN: TStringField
      FieldName = 'PIN'
      Origin = 'users.PIN'
      Size = 10
    end
    object qryUsersFullName: TStringField
      FieldName = 'FullName'
      Origin = 'users.FullName'
      Size = 50
    end
    object qryUsersEnabled: TIntegerField
      FieldName = 'Enabled'
      Origin = 'users.Enabled'
    end
    object qryUsersCreatedDate: TDateField
      FieldName = 'CreatedDate'
      Origin = 'users.CreatedDate'
    end
    object qryUsersLastAccess: TDateField
      FieldName = 'LastAccess'
      Origin = 'users.LastAccess'
    end
    object qryUsersLastAccTime: TDateTimeField
      FieldName = 'LastAccTime'
      Origin = 'users.LastAccTime'
    end
    object qryUsersCreatedTime: TTimeField
      FieldName = 'CreatedTime'
      Origin = 'users.CreatedTime'
    end
    object qryUsersAccessCount: TIntegerField
      FieldName = 'AccessCount'
      Origin = 'users.AccessCount'
    end
    object qryUsersCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'users.CustID'
    end
    object qryUsersaccess_type: TIntegerField
      FieldName = 'access_type'
      Origin = 'users.access_type'
    end
    object qryUserskabID: TIntegerField
      FieldName = 'kabID'
      Origin = 'users.kabID'
    end
  end
  object dsUsers: TMyDataSource
    DataSet = qryUsers
    Left = 56
    Top = 232
  end
  object qryCounter: TMyQuery
    Connection = frmWelcome.conTest
    SQL.Strings = (
      'SELECT * FROM tblcounterid WHERE CustID = :CustID')
    Active = True
    Left = 336
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryCounterid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
      Origin = 'tblcounterid.id'
    end
    object qryCountertoolsID: TIntegerField
      FieldName = 'toolsID'
      Origin = 'tblcounterid.toolsID'
    end
    object qryCountersupplierID: TIntegerField
      FieldName = 'supplierID'
      Origin = 'tblcounterid.supplierID'
    end
    object qryCounternotificationID: TIntegerField
      FieldName = 'notificationID'
      Origin = 'tblcounterid.notificationID'
    end
    object qryCounterjobID: TIntegerField
      FieldName = 'jobID'
      Origin = 'tblcounterid.jobID'
    end
    object qryCounteremployeeID: TIntegerField
      FieldName = 'employeeID'
      Origin = 'tblcounterid.employeeID'
    end
    object qryCounteruserID: TIntegerField
      FieldName = 'userID'
      Origin = 'tblcounterid.userID'
    end
    object qryCounterkabID: TIntegerField
      FieldName = 'kabID'
      Origin = 'tblcounterid.kabID'
    end
    object qryCounterkabDrawerID: TIntegerField
      FieldName = 'kabDrawerID'
      Origin = 'tblcounterid.kabDrawerID'
    end
    object qryCounterkabBinID: TIntegerField
      FieldName = 'kabBinID'
      Origin = 'tblcounterid.kabBinID'
    end
    object qryCountertransactionID: TIntegerField
      FieldName = 'transactionID'
      Origin = 'tblcounterid.transactionID'
    end
    object qryCounterCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tblcounterid.CustID'
    end
  end
end
