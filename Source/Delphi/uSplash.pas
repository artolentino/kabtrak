(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uSplash.pas
   Description            :  Splash Screen
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)

unit uSplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, pngimage,inifiles;

type
  TfrmSplash = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    lblCopyright: TLabel;
    ProgramIcon: TImage;
    lblProductName: TLabel;
    lblVersion: TLabel;
    lblLicense: TLabel;
    lblCompanyName: TLabel;
    lblTrademark: TLabel;
    lblRegisteredTo: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation

uses GlobalRoutines;

{$R *.dfm}

procedure TfrmSplash.FormCreate(Sender: TObject);
var
  MTDIni: TIniFile;
  RegisteredTo1: string;
  License1: string;
    begin
     lblVersion.Caption := 'Version ' + GetAppVersionStr;
     MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
      with MTDIni do
      begin
        License1 := MTDIni.ReadString('General', 'License', '30 day evaluation license');   //Get registration code.
        RegisteredTo1 := MTDIni.ReadString('General', 'RegisteredTo', '');   //Get name registered to.
      end;
      MTDIni.Free;

      if License1 = ' ' then lblLicense.caption := '30 day evaluation license';

      if RegisteredTo1 = '' then lblRegisteredTo.caption := ''
      else lblRegisteredTo.caption := 'Registered to: ' + RegisteredTo1;
    end;

end.
