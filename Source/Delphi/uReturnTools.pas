unit uReturnTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, NxColumns, NxColumnClasses, NxScrollControl,
  NxCustomGridControl, NxCustomGrid, NxGrid, NxPageControl, DB, DBAccess,
  MyAccess, MemDS, Grids, DBGrids, JvExExtCtrls, JvExtComponent, JvItemsPanel,
  fcpanel;

type
  TfrmReturnTools = class(TForm)
    pnlToolbar: TPanel;
    btnClose: TSpeedButton;
    qryTransactions: TMyQuery;
    dsTransactions: TMyDataSource;
    NxPageControl1: TNxPageControl;
    NxTabSheet1: TNxTabSheet;
    nxtgrdTools: TNextGrid;
    grdColID: TNxTextColumn;
    grdColDescription: TNxTextColumn;
    qryTransDetail: TMyQuery;
    ds1: TMyDataSource;
    qryTransactionsTransID: TIntegerField;
    qryTransactionsToolName: TStringField;
    qryTransactionsTrailID: TIntegerField;
    qryTransactionsUserID: TIntegerField;
    qryTransactionsCustID: TIntegerField;
    qryTransactionsPart__No: TStringField;
    qryTransactionsJobName: TStringField;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    btn1: TSpeedButton;
    bvl3: TBevel;
    grdCol1: TNxTextColumn;
    pnlCabinet: TPanel;
    pnl1: TPanel;
    jvtmspnlDrawers: TJvItemsPanel;
    pnl2: TPanel;
    pnlContent: TPanel;
    qryTransactionsItemID: TStringField;
    qryTransactionsDrawerID: TStringField;
    qryDrawers: TMyQuery;
    strngfldDrawersDrawerID: TStringField;
    strngfldTransactionsBinID: TStringField;
    fcpnlCabinet: TfcPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCloseClick(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure nxtgrdToolsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure jvtmspnlDrawersItemClick(Sender: TObject; ItemIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReturnTools: TfrmReturnTools;

implementation

uses uDataModule, main;

{$R *.dfm}

procedure TfrmReturnTools.btnCloseClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmReturnTools.btnDownClick(Sender: TObject);
begin
  nxtgrdTools.SelectedRow := nxtgrdTools.SelectedRow + 1;
end;

procedure TfrmReturnTools.btnFirstClick(Sender: TObject);
begin
  nxtgrdTools.ScrollToRow(0);
end;

procedure TfrmReturnTools.btnLastClick(Sender: TObject);
begin
 nxtgrdTools.ScrollToRow(nxtgrdTools.RowCount);
end;

procedure TfrmReturnTools.btnRefreshClick(Sender: TObject);
begin
  nxtgrdTools.Refresh;
end;

procedure TfrmReturnTools.btnUpClick(Sender: TObject);
begin
  nxtgrdTools.SelectedRow := nxtgrdTools.SelectedRow -1;
end;

procedure TfrmReturnTools.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmReturnTools.FormCreate(Sender: TObject);
var
  strDrawerID : string;
begin
  //

  fcpnlCabinet.Caption := 'Cabinet ' + DM.qryCabinetKabID.AsString;
  qryDrawers.Close;
  qryDrawers.ParamByName('UserID').Value := DM.qryLoggedUser.FieldByName('ID').AsInteger;
  qryDrawers.Open;

  jvtmspnlDrawers.Items.Clear;
  qryDrawers.First;
  strDrawerID := qryDrawers.FieldByName('DrawerID').AsString;
  while not qryDrawers.Eof  do
    begin
    jvtmspnlDrawers.Items.Add(qryDrawers.FieldByName('DrawerID').AsString);
    qryDrawers.Next;
    end;

   qryTransactions.Close;
    qryTransactions.DeleteWhere;
   qryTransactions.AddWhere('tblemployeeitemtransactions.UserID = ' + DM.qryLoggedUserID.AsString);
   qryTransactions.AddWhere('tblitemkabdrawerbins.DrawerID = ' + QuotedStr(strDrawerID));
   qryTransactions.Open;

   qryTransactions.First;
   nxtgrdTools.ClearRows;
    if not qryTransactions.IsEmpty then
    begin
       while not qryTransactions.Eof  do
         begin
           qryTransDetail.Open;
           if (qryTransDetail.RecordCount = 1) and (qryTransDetail.FieldByName('TransType').Value = 0) then
           nxtgrdTools.AddCells([qryTransactions.FieldByName('BinID').AsString, qryTransactions.FieldByName('Part__No').AsString, qryTransactions.FieldByName('ToolName').AsString]);
         qryTransactions.Next;
         end;

    end;
   NxTabSheet1.Caption := 'Drawer ' + strDrawerID;
end;

procedure TfrmReturnTools.jvtmspnlDrawersItemClick(Sender: TObject;
  ItemIndex: Integer);
begin
    qryTransactions.Close;
    qryTransactions.DeleteWhere;
    qryTransactions.AddWhere('tblemployeeitemtransactions.UserID = ' + DM.qryLoggedUserID.AsString);
    qryTransactions.AddWhere('tblitemkabdrawerbins.DrawerID = ' + QuotedStr(jvtmspnlDrawers.Items.Strings[ItemIndex]));
    qryTransactions.Open;

    qryTransactions.First;
     nxtgrdTools.ClearRows;
    if not qryTransactions.IsEmpty then
      begin
        while not qryTransactions.Eof  do
         begin
           qryTransDetail.Open;
           if (qryTransDetail.RecordCount = 1) and (qryTransDetail.FieldByName('TransType').Value = 0) then
             nxtgrdTools.AddCells([qryTransactions.FieldByName('BinID').AsString, qryTransactions.FieldByName('Part__No').AsString, qryTransactions.FieldByName('ToolName').AsString]);
           qryTransactions.Next;
         end;
      end;
    NxTabSheet1.Caption := 'Drawer ' + jvtmspnlDrawers.Items.Strings[ItemIndex];
end;

procedure TfrmReturnTools.nxtgrdToolsClick(Sender: TObject);
begin
   // get partnumber

end;

end.
