unit uIssueTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, NxPageControl, DB, DBAccess, MyAccess, MemDS,
  NxDBColumns, NxColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid,
  NxDBGrid, JvExExtCtrls, JvExtComponent, JvItemsPanel, fcpanel, pngimage;

type
  TfrmIssueTools = class(TForm)
    pnlToolbar: TPanel;
    btnClose: TSpeedButton;
    btn1: TSpeedButton;
    bvl3: TBevel;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    qryTools: TMyQuery;
    dsTools: TMyDataSource;
    NxPageControl1: TNxPageControl;
    nxtbshtDrawerSheet: TNxTabSheet;
    grd1: TNextDBGrid;
    nxdbtxtclmn1: TNxDBTextColumn;
    nxdbtxtclmn2: TNxDBTextColumn;
    nxdbtxtclmn3: TNxDBTextColumn;
    qryDrawers: TMyQuery;
    pnlCabinet: TPanel;
    pnlContent: TPanel;
    fcPanel1: TfcPanel;
    dsDrawers: TMyDataSource;
    imgEmpty: TImage;
    pnl1: TPanel;
    jvtmspnlDrawers: TJvItemsPanel;
    pnl2: TPanel;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure jvtmspnlDrawersItemClick(Sender: TObject; ItemIndex: Integer);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIssueTools: TfrmIssueTools;

implementation

uses uDataModule, main, uIssue;

{$R *.dfm}

procedure TfrmIssueTools.btn1Click(Sender: TObject);
begin
  frmIssue := TfrmIssue.Create(nil);
  frmIssue.Caption := 'Issuing Tools from Drawer ' + qryTools.FieldByName('DrawerID').AsString;
  frmIssue.lbl1.Caption := 'DRAWER ' +  qryTools.FieldByName('DrawerID').AsString;
  frmIssue.ShowModal;
end;

procedure TfrmIssueTools.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmIssueTools.btnDownClick(Sender: TObject);
begin
  qryTools.Next;
end;

procedure TfrmIssueTools.btnFirstClick(Sender: TObject);
begin
 qryTools.First;

end;

procedure TfrmIssueTools.btnLastClick(Sender: TObject);
begin
  qryTools.Last;
end;

procedure TfrmIssueTools.btnRefreshClick(Sender: TObject);
begin
   qryTools.Refresh;
end;

procedure TfrmIssueTools.btnUpClick(Sender: TObject);
begin
  qryTools.Prior;
end;

procedure TfrmIssueTools.FormCreate(Sender: TObject);
var
DrawerID : string;
DrawerTabSheet : TNxTabSheet;
begin
  fcPanel1.Caption := 'Cabinet ' + DM.qryCabinetKabID.AsString;
  qryDrawers.Open;
  qryDrawers.First;

  qryTools.Close;
  qryTools.ParamByName('DrawerID').Value := qryDrawers.FieldByName('DrawerID').AsString;
  qryTools.Open;
  nxtbshtDrawerSheet.Caption := 'Drawer ' + qryDrawers.FieldByName('DrawerID').AsString;

  jvtmspnlDrawers.Items.Clear;

  while not qryDrawers.EOF do
    begin
      jvtmspnlDrawers.Items.Add(qryDrawers.FieldByName('DrawerID').AsString);
      qryDrawers.Next;
    end;
  
end;

procedure TfrmIssueTools.jvtmspnlDrawersItemClick(Sender: TObject;
  ItemIndex: Integer);

var
strDrawerID : string;
begin
  strDrawerID := jvtmspnlDrawers.Items.Strings[ItemIndex];
  qryTools.Close;
  qryTools.ParamByName('DrawerID').Value := strDrawerID;
  qryTools.Open;

  // check if empty
  if qryTools.IsEmpty then
    begin
      Application.MessageBox('Drawer is empty! Please select another drawer',  'Drawer ', MB_OK + MB_ICONINFORMATION);
      grd1.Visible := False;
      imgEmpty.Visible := True;
    end
  else
    begin
       grd1.Visible := True;;
      imgEmpty.Visible := false;
    end;

  nxtbshtDrawerSheet.Caption := 'Drawer ' + strDrawerID;
end;

end.
