unit uFindEmployee;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,DB;

type
  TfrmFindEmployees = class(TForm)
    lbl1: TLabel;
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    edtEmployee: TEdit;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFindEmployees: TfrmFindEmployees;

implementation

uses uEmployees;

{$R *.dfm}

procedure TfrmFindEmployees.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFindEmployees.btnOKClick(Sender: TObject);
begin
    if edtEmployee.Text <> '' then
     begin
      if frmEmployees.qryEmployees.Locate('Fullname' , edtEmployee.Text, [loPartialKey,loCaseInsensitive]) <> True then
       raise(Exception.Create('Employee ' + '''' + edtEmployee.Text + ''''  + ' not found'))
     end;
   Self.Close;
end;

procedure TfrmFindEmployees.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
