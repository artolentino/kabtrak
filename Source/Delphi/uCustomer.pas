(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uCustomer.pas
   Description            :  Customer Settings
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)

unit uCustomer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, StdCtrls, DBCtrls, Buttons, Mask, ComCtrls,
  ExtCtrls, pngimage,IniFiles;

type
  TfrmCustomer = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    Label13: TLabel;
    editCompanyName: TDBEdit;
    editAddressLine1: TDBEdit;
    editAddressLine2: TDBEdit;
    editAddressLine3: TDBEdit;
    editContact: TDBEdit;
    editPhone: TDBEdit;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label12: TLabel;
    TabSheet2: TTabSheet;
    memoNotes: TDBMemo;
    Label2: TLabel;
    Label3: TLabel;
    editEmail: TDBEdit;
    dsCustomer: TMyDataSource;
    Label4: TLabel;
    editJobOrderLabel: TDBEdit;
    imgIcon: TImage;
    dbtxt1: TDBText;
    lbl1: TLabel;
    btnRegister: TButton;
    edtRedID: TEdit;
    edtRegCode: TEdit;
    lblNote: TLabel;
    lblLicense: TLabel;
    lblRegisteredTo: TLabel;
    lbl2: TLabel;
    lblRequired: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomer: TfrmCustomer;

implementation

uses uDataModule, GlobalRoutines, main;

{$R *.dfm}

procedure TfrmCustomer.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomer.btnOKClick(Sender: TObject);
var
MTDIni : TIniFile;
msg : string;
begin

if (editEmail.Text =  '') or  (not IsValidEmail(editEmail.Text)) then
   begin
     ShowMessage('Please enter valid email address');
     Exit ;
   end;

if editCompanyName.Text = ''  then
  begin
    ShowMessage('Please enter valid Company name');
    Exit;
  end;

 if editJobOrderLabel.Text = ''  then
  begin
    ShowMessage('Please enter valid Job label description');
    Exit;
  end;

msg := 'To make the changes to take effect, kabTRAK needs to be  restarted. Would you like to restart kabTRAK now ?';

 MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
  with DM do
  begin
    qryCustomer.Post;
    //Save Settings to ini file
    with MTDIni do
      begin
        MTDIni.WriteString('Customer', 'Name', editCompanyName.Text);
        MTDIni.WriteString('Customer', 'Email', editEmail.Text);
        MTDIni.WriteString('Customer', 'JobLabel', editJobOrderLabel.Text);
        MTDIni.WriteString('Customer', 'Contact', editContact.Text);
        MTDIni.WriteString('General', 'RegisteredTo', editCompanyName.Text);
        MTDIni.WriteString('General', 'LicenseID', edtRedID.Text);
        MTDIni.WriteString('General', 'RegistrationCode', edtRegCode.Text);

      end;
  close;
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then AppRestart;
  end;
  MTDIni.Free;

end;

procedure TfrmCustomer.btnRegisterClick(Sender: TObject);
begin
with frmMain do
begin
  Reg.DoRegistration(edtRedID.Text,editCompanyName.Text,edtRegCode.Text,editEmail.text);
    if not Reg.CheckRegistered then
      begin
        ShowMessage('There was an error validating your registration information. Please try again.');
      end
  else
      begin
        ShowMessage('Registration Successful! Thank you for registering your product.');
      end;
end;
end;

procedure TfrmCustomer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Action := caFree;
end;

procedure TfrmCustomer.FormShow(Sender: TObject);
begin
  with DM do
  begin
    // Enter in edit mode
    qryCustomer.Edit;
  end;

  with frmMain do
  begin
     edtRedID.Text := Reg.LicenseID;
     edtRegCode.Text := Reg.RegCode;

     if not Reg.Registered then  lblLicense.caption := '30 day evaluation license'
     else  lblLicense.caption := 'Single User license';

     if not Reg.Registered then
     begin
      lblRegisteredTo.caption := 'You have been using kabTRAK for ' + IntToStr(frmMain.Reg.Days - frmMain.Reg.DaysLeft) + ' days.'
    end
  else
     begin
       lblRegisteredTo.caption := 'Registered to: ' + GetCustomerContact;
     end;

  end;

//  with frmMain.kbQwerty do
//   begin
//      Show;
//      Keyboard.LoadKeybdLayout('qwery.txt');
//       Keyboard.Width := 401;
//       Keyboard.Height := 201;
//
//   end;

end;

end.
