unit uJobOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DBCtrls, Mask, DB, DBAccess, MyAccess,
  MemDS, JvExComCtrls, JvDateTimePicker, JvDBDateTimePicker;

type
  TfrmJobOrder = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgcJobOrder: TPageControl;
    ts1: TTabSheet;
    qryJobOrder: TMyQuery;
    dsJobOrder: TMyDataSource;
    qryJobOrderTrailID: TIntegerField;
    qryJobOrderDescription: TStringField;
    qryJobOrderCustID: TIntegerField;
    qryJobOrderStartDate: TDateField;
    qryJobOrderEndDate: TDateField;
    qryJobOrderRemark: TStringField;
    lbl1: TLabel;
    editDescription: TDBEdit;
    ts2: TTabSheet;
    dbmmo1: TDBMemo;
    dbrgrpcontinues: TDBRadioGroup;
    qryJobOrdercontinues: TIntegerField;
    grpDates: TGroupBox;
    lbl2: TLabel;
    JvDBDateTimePicker1: TJvDBDateTimePicker;
    lbl3: TLabel;
    JvDBDateTimePicker2: TJvDBDateTimePicker;
    qryCounter: TMyQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure dbrgrpcontinuesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryJobOrderAfterInsert(DataSet: TDataSet);
    procedure qryJobOrderAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmJobOrder: TfrmJobOrder;

implementation

uses uDataModule, uJobOrders, main;

{$R *.dfm}

procedure TfrmJobOrder.btnCancelClick(Sender: TObject);
begin
   if qryJobOrder.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg('There are pending updates for this transaction. Would you like to abort current operations?',
        mtInformation, [mbYes, mbNo], 0) = mrYes then Self.Close else Exit;

    end;
  Self.Close;
end;

procedure TfrmJobOrder.btnOKClick(Sender: TObject);
begin
   // Check for the entry
   if editDescription.Text = '' then
     begin
       MessageDlg('Please provide the description for this entry',  mtError,
         [mbOK], 0);
       Exit;
     end;

     if dbrgrpcontinues.ItemIndex = -1 then
       begin
         MessageDlg('Please specify if it is a Regular or Recurring Task. ',  mtError,
           [mbOK], 0);
         Exit;
       end;

   if qryJobOrder.State in [dsInsert, dsEdit] then
    begin
      qryJobOrder.post;
    end;

    frmJobOrders.qryJobOrders.Refresh;
  Self.Close;
end;

procedure TfrmJobOrder.dbrgrpcontinuesChange(Sender: TObject);
begin
  if dbrgrpcontinues.ItemIndex = 1  then   grpDates.Visible := True else grpDates.Visible := False;

end;

procedure TfrmJobOrder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
qryJobOrder.Close;
  Action := caFree;
end;

procedure TfrmJobOrder.FormCreate(Sender: TObject);
begin
  qryJobOrder.Open;
  pgcJobOrder.ActivePageIndex := 0;
end;

procedure TfrmJobOrder.qryJobOrderAfterInsert(DataSet: TDataSet);
begin
  qryJobOrderTrailID.Value := qryCounter.FieldByName('jobID').Value + 1;
end;

procedure TfrmJobOrder.qryJobOrderAfterPost(DataSet: TDataSet);
begin
qryCounter.Edit;
qryCounter.FieldByName('jobID').Value := qryCounter.FieldByName('jobID').AsInteger + 1;
qryCounter.Post;
end;

end.
