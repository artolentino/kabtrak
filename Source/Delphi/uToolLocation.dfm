object frmAddToolLocation: TfrmAddToolLocation
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Tool Location'
  ClientHeight = 355
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 324
    Width = 470
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 301
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 6
        Top = 4
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 4
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 470
    Height = 324
    ActivePage = ts1
    Align = alClient
    TabHeight = 20
    TabOrder = 1
    TabWidth = 100
    object ts1: TTabSheet
      Caption = 'General'
      object lbl1: TLabel
        Left = 24
        Top = 143
        Width = 171
        Height = 13
        Caption = 'Select an Item for this tool location:'
      end
      object lbl2: TLabel
        Left = 24
        Top = 164
        Width = 39
        Height = 13
        Caption = 'P-Code:'
        FocusControl = editP1Code
      end
      object lbl5: TLabel
        Left = 232
        Top = 200
        Width = 91
        Height = 13
        Caption = 'Tool Location Code'
      end
      object dblkcbbItems: TDBLookupComboBox
        Left = 24
        Top = 160
        Width = 417
        Height = 21
        DataField = 'ItemID'
        DataSource = frmSetupCabinet.dsBin
        KeyField = 'Part__No'
        ListField = 'Description'
        ListSource = dsItems
        TabOrder = 0
      end
      object editP1Code: TDBEdit
        Left = 232
        Top = 219
        Width = 111
        Height = 21
        DataField = 'P1Code'
        DataSource = frmSetupCabinet.dsBin
        TabOrder = 1
      end
      object grp1: TGroupBox
        Left = 23
        Top = 15
        Width = 417
        Height = 122
        Caption = 'Filter Options'
        TabOrder = 2
        object lbl3: TLabel
          Left = 23
          Top = 24
          Width = 49
          Height = 13
          Caption = 'Category:'
        end
        object lbl4: TLabel
          Left = 23
          Top = 50
          Width = 70
          Height = 13
          Caption = 'Sub Category:'
        end
        object btn1: TSpeedButton
          Left = 304
          Top = 86
          Width = 97
          Height = 22
          Caption = 'Retrieve List'
          OnClick = btn1Click
        end
        object cbbCategory: TComboBox
          Left = 120
          Top = 25
          Width = 281
          Height = 21
          TabOrder = 0
          OnChange = cbbCategoryChange
        end
        object cbbSubCategory: TComboBox
          Left = 120
          Top = 52
          Width = 281
          Height = 21
          TabOrder = 1
        end
      end
      object dbrgrp1: TDBRadioGroup
        Left = 24
        Top = 200
        Width = 185
        Height = 59
        Caption = 'Physical Status'
        DataField = 'status'
        DataSource = frmSetupCabinet.dsBin
        Items.Strings = (
          'Tool is OUT'
          'Tool is IN')
        ParentBackground = True
        TabOrder = 3
        Values.Strings = (
          '0'
          '1')
      end
    end
  end
  object qryCategory: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltoolcategories')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Left = 384
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryCategoryCatID: TIntegerField
      FieldName = 'CatID'
    end
    object qryCategoryDescription: TStringField
      FieldName = 'Description'
      Size = 100
    end
    object qryCategoryCustID: TIntegerField
      FieldName = 'CustID'
    end
  end
  object qrySubCategory: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltoolsubcatogories WHERE CatID = :intCatID')
    Left = 232
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'intCatID'
      end>
    object qrySubCategorySubCatID: TIntegerField
      FieldName = 'SubCatID'
    end
    object qrySubCategoryCatID: TIntegerField
      FieldName = 'CatID'
    end
    object qrySubCategoryDescription: TStringField
      FieldName = 'Description'
      Size = 200
    end
    object qrySubCategoryCustID: TIntegerField
      FieldName = 'CustID'
    end
  end
  object dsCategories: TMyDataSource
    DataSet = qryCategory
    Left = 280
    Top = 128
  end
  object qryItems: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tbltools')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Left = 40
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsItems: TMyDataSource
    DataSet = qryItems
    Left = 80
    Top = 104
  end
  object qryBinItems: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tblitemkabdrawerbins')
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Left = 384
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
end
