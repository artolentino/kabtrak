unit uTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, Buttons, Grids, DBGrids, DB, DBAccess, MyAccess,
  DBCtrls, MemDS, StdCtrls, Mask, JvExMask, JvToolEdit, JvMaskEdit, JvDBFindEdit,
  AdvSmoothTouchKeyBoard, JvExStdCtrls, JvEdit, JvDBSearchEdit, NxDBColumns,
  NxColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid, NxDBGrid,
  NxPageControl, AdvCardList, DBAdvCardList, AdvCardListStyler,
  TFlatSpeedButtonUnit, AdvSmoothButton, ImgList, NxCollection, NxColumnClasses,
  AdvPageControl, ComCtrls, AdvGroupBox, AdvSmoothLabel;

type
  TfrmTools = class(TForm)
    pnlToolbar: TPanel;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnImport: TSpeedButton;
    dsTools: TMyDataSource;
    AdvCardListStyler1: TAdvCardListStyler;
    il1: TImageList;
    bvl1: TBevel;
    btnSuppliers: TSpeedButton;
    bvl2: TBevel;
    bvl3: TBevel;
    btnCategories: TSpeedButton;
    btnToolMovement: TSpeedButton;
    btnFindTools: TSpeedButton;
    bvl5: TBevel;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnClose: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    AdvPageControl1: TAdvPageControl;
    AdvTabSheet1: TAdvTabSheet;
    AdvTabSheet2: TAdvTabSheet;
    ToolPage: TNxPageControl;
    GridView: TNxTabSheet;
    grdTools: TNextDBGrid;
    CardView: TNxTabSheet;
    lst1: TDBAdvCardList;
    btn1: TAdvButtonsBar;
    AdvGroupBox1: TAdvGroupBox;
    AdvSmoothLabel1: TAdvSmoothLabel;
    NxPageControl1: TNxPageControl;
    NxTabSheet1: TNxTabSheet;
    grd1: TNextDBGrid;
    qryToolsCabinet: TMyQuery;
    dsToolsCabinet: TMyDataSource;
    nxdbtxtclmn2: TNxDBTextColumn;
    nxdbtxtclmn3: TNxDBTextColumn;
    nxdbtxtclmn4: TNxDBTextColumn;
    qryToolsCabinetID: TIntegerField;
    qryToolsCabinetBinID: TStringField;
    qryToolsCabinetDrawerID: TStringField;
    qryToolsCabinetKabID: TIntegerField;
    qryToolsCabinetP1Code: TStringField;
    qryToolsCabinetCustID: TIntegerField;
    qryToolsCabinetItemID: TStringField;
    qryToolsCabinetPartNum: TStringField;
    qryToolsCabinetToolDescription: TStringField;
    btnSync: TSpeedButton;
    NxDBTextColumn1: TNxDBTextColumn;
    NxDBTextColumn2: TNxDBTextColumn;
    NxDBTextColumn3: TNxDBTextColumn;
    qryTools: TMyQuery;
    qryToolsSupplierName: TStringField;
    qryToolsPart__No: TStringField;
    qryToolsDescription: TStringField;
    qryToolsToolCategory: TStringField;
    qryToolsToolSubCategory: TStringField;
    qryToolsid: TIntegerField;
    qryToolsCustID: TIntegerField;
    NxDBTextColumn4: TNxDBTextColumn;
    NxDBTextColumn5: TNxDBTextColumn;
    btnUpdatePartNumber: TSpeedButton;
    qryToolsCabinetstatus: TIntegerField;
    status: TNxDBImageColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnFindToolsClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnSuppliersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCategoriesClick(Sender: TObject);
    procedure btnToolMovementClick(Sender: TObject);
    procedure AdvPageControl1Change(Sender: TObject);
    procedure btnUpdateCatClick(Sender: TObject);
    procedure btnUpdatePartNumberClick(Sender: TObject);
   
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTools: TfrmTools;

implementation

uses uDataModule, main, uTool, uSuppliers, uFindTools, uCategories,
  uToolMovement, uToolCabinet, GlobalRoutines;

{$R *.dfm}

procedure TfrmTools.btnFirstClick(Sender: TObject);
begin
  case AdvPageControl1.ActivePageIndex of
     0 : begin
           qryTools.First;
         end;
     1: begin
           qryToolsCabinet.First;
        end;

  end;

end;

procedure TfrmTools.btnLastClick(Sender: TObject);
begin
  case AdvPageControl1.ActivePageIndex of
     0 : begin
          qryTools.Last;
         end;
     1: begin
           qryToolsCabinet.Last;
        end;

  end;

end;

procedure TfrmTools.AdvPageControl1Change(Sender: TObject);
begin
  case AdvPageControl1.ActivePageIndex of
     0 : begin
           // Navigator


         end;
     1: begin

        end;

  end;
end;

procedure TfrmTools.btnCategoriesClick(Sender: TObject);
begin
  frmCategories := TfrmCategories.Create(nil);
  frmCategories.ShowModal;
end;

procedure TfrmTools.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmTools.btnRefreshClick(Sender: TObject);
begin
case AdvPageControl1.ActivePageIndex of
     0 : begin
           qryTools.Refresh;
         end;
     1: begin
          qryToolsCabinet.Refresh;
        end;

  end;

end;

procedure TfrmTools.btnSuppliersClick(Sender: TObject);
begin
 frmSuppliers := TfrmSuppliers.Create(nil);
  frmSuppliers.ShowModal;
end;

procedure TfrmTools.btnToolMovementClick(Sender: TObject);
begin
 if qryToolsid.AsVariant = null then ShowMessage('Please select a record to view movement')
  else
  begin
    screen.cursor := crHourglass;
    application.createform (TfrmToolMovement,frmToolMovement);
    if not frmToolMovement.qryTools.Locate('Part__No' , qryToolsPart__No.AsString, []) then
       begin
         Application.MessageBox('There are no transactions found for the selected Tool', 'Warning', MB_OK + MB_ICONINFORMATION);
         Exit;
       end;
    frmToolMovement.Caption := 'Viewing ' + qryToolsPart__No.AsString  + ' - ' +  qryToolsDescription.AsString;
    frmToolMovement.Showmodal;
    screen.cursor := crDefault;
  end;
end;

procedure TfrmTools.btnDeleteClick(Sender: TObject);
var
  msg : string;
begin

case AdvPageControl1.ActivePageIndex of
     0 : begin
          msg := 'Do you really want to delete Part Number ' + qryToolsPart__No.AsString + ' with description  ' + qryToolsDescription.AsString + '?';
           if qryToolsid.AsVariant = null then exit;   //Ignore if no record selected.
             if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
               begin
                 qryTools.delete;
                 qryTools.RefreshRecord;
                 qryTools.Refresh;
               end;
         end;
     1: begin

        end;

  end;


end;

procedure TfrmTools.btnDownClick(Sender: TObject);
begin
 case AdvPageControl1.ActivePageIndex of
     0 : begin
         qryTools.Next;


         end;
     1: begin
          qryToolsCabinet.Next;
        end;

  end;

end;

procedure TfrmTools.btnEditClick(Sender: TObject);
begin
   case AdvPageControl1.ActivePageIndex of
     0 : begin
              if qryToolsid.AsVariant = null then btnNewClick(sender)   //No record selected.
           else
              begin
                screen.cursor := crHourglass;
                application.createform (TfrmTool,frmTool);
                frmTool.qryTools.Locate('Part__No', qryToolsPart__No.AsString, []);
                frmTool.Caption := 'Editing ' + qryToolsDescription.AsString;
                frmTool.Showmodal;
                screen.cursor := crDefault;
               end;
         end;
     1: begin
           if qryToolsCabinetID.AsVariant = null then btnNewClick(sender)   //No record selected.
           else
              begin
                screen.cursor := crHourglass;
                application.createform (TfrmToolCabinet,frmToolCabinet);
                frmToolCabinet.qryToolCabinet.locate('ID', qryToolsCabinetID.value, []);
                frmToolCabinet.Caption := 'Editing ' + qryToolsCabinetToolDescription.AsString;
                frmToolCabinet.Showmodal;
                screen.cursor := crDefault;
               end;
        end;

  end;


end;

procedure TfrmTools.btnFindToolsClick(Sender: TObject);
begin

case AdvPageControl1.ActivePageIndex of
     0 : begin
           frmFindTools := TfrmFindTools.Create(nil);
           frmFindTools.ShowModal;
       end;
     1: begin

        end;

  end;

end;

procedure TfrmTools.btnNewClick(Sender: TObject);
begin

  case AdvPageControl1.ActivePageIndex of
     0 : begin
          screen.cursor := crHourglass;
          application.createform (TfrmTool,frmTool);
          frmTool.Caption := 'New Tool Record';
          frmtool.qryTools.Insert;
          frmTool.ShowModal;
          screen.cursor := crDefault;
         end;
     1: begin

        end;

  end;

end;

procedure TfrmTools.btnUpClick(Sender: TObject);
begin
  case AdvPageControl1.ActivePageIndex of
     0 : begin
         qryTools.Prior;
         end;
     1: begin
          qryToolsCabinet.Prior;
        end;

  end;

end;

procedure TfrmTools.btnUpdateCatClick(Sender: TObject);
begin
   UpdatePartNumbers;
end;

procedure TfrmTools.btnUpdatePartNumberClick(Sender: TObject);
begin
  UpdatePartNumbers;
end;

procedure TfrmTools.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryTools.Close;
qryToolsCabinet.Close;
  Action := caFree;
end;

procedure TfrmTools.FormCreate(Sender: TObject);
begin
  ToolPage.ActivePageIndex := 0;
  AdvPageControl1.ActivePageIndex := 0;
  qryToolsCabinet.Close;
  qryToolsCabinet.ParamByName('kabID').Value := DM.qryCabinet.FieldByName('kabID').AsInteger;
  qryToolsCabinet.Open;
 qryTools.Open;

end;

procedure TfrmTools.FormShow(Sender: TObject);
begin
   ToolPage.ActivePageIndex := 0;
end;


end.
