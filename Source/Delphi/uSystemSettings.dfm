object frmSystemSettings: TfrmSystemSettings
  Left = 0
  Top = 0
  ActiveControl = pgcSystem
  BorderStyle = bsDialog
  Caption = 'System Settings'
  ClientHeight = 331
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 301
    Width = 534
    Height = 30
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 359
      Top = 0
      Width = 175
      Height = 30
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 0
        Top = 4
        Width = 81
        Height = 22
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 87
        Top = 4
        Width = 75
        Height = 22
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgcSystem: TPageControl
    Left = 0
    Top = 0
    Width = 534
    Height = 301
    ActivePage = ts2
    Align = alClient
    Images = frmMain.ilTabImages
    TabHeight = 36
    TabOrder = 1
    TabWidth = 120
    object ts1: TTabSheet
      Caption = '&General'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object btn3: TSpeedButton
        Left = 330
        Top = 83
        Width = 167
        Height = 33
        Caption = 'Check for Update'
        OnClick = btn3Click
      end
      object rg1: TRadioGroup
        Left = 16
        Top = 12
        Width = 308
        Height = 65
        Caption = 'Input Method'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Touchscreen Keyboard'
          'External Keyboard/Mouse')
        TabOrder = 0
      end
      object grp6: TGroupBox
        Left = 16
        Top = 83
        Width = 308
        Height = 105
        Caption = 'Dropbox Account'
        TabOrder = 1
        object lbl21: TLabel
          Left = 24
          Top = 28
          Width = 44
          Height = 13
          Caption = 'App Key:'
        end
        object lbl22: TLabel
          Left = 11
          Top = 60
          Width = 57
          Height = 13
          Caption = 'App Secret:'
        end
        object edtDropboxEmail: TEdit
          Left = 89
          Top = 28
          Width = 192
          Height = 21
          TabOrder = 0
        end
        object edtDropBoxPassword: TEdit
          Left = 89
          Top = 55
          Width = 192
          Height = 21
          TabOrder = 1
        end
      end
      object rg2: TRadioGroup
        Left = 330
        Top = 12
        Width = 167
        Height = 65
        Caption = 'Web Update'
        ItemIndex = 0
        Items.Strings = (
          'Automatic Update'
          'Manual Update')
        TabOrder = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Backup/Restore'
      ImageIndex = 1
      DesignSize = (
        526
        255)
      object img1: TImage
        Left = 442
        Top = 174
        Width = 72
        Height = 68
        Anchors = [akRight, akBottom]
        AutoSize = True
        Center = True
        Picture.Data = {
          0954506E67496D61676589504E470D0A1A0A0000000D49484452000000480000
          00440806000000222F733C000008A14944415478DAED9C0BB0555318C7BF50A8
          BC27A9480DC9940C537985E439E3CD5428728B28952EE9458F93487937A19794
          A868C6FB7AE47999913469D4308D3CBA450651236F95EBFBB5D6EEEE76E79CBB
          F7397BED7D9BE99BF94FF7ECB3F65AEBFCF75ADF73ED6A555656CA2EC92DB5F6
          1FF34D9CFDEDA618AA68A5E8AFF835A1DF514FF188628DE26EC5BF616FDC30AA
          7962049DA6B8537186FDBC58D14BF1B963725A281E579C6E3F2F548C50BC5753
          08AAABB843314CCC0AF2CB3A4537C5DB8EC8E1A1CC5334095CDFA2B85F314EB1
          314D82CE55DCA5689FA7CDDF8A418AC7622687D53951513F4F9B658A918A5792
          26E800C568C5C008F74C52DC1C0331AC5256C72D11EE9962E7FB53120475568C
          11A388A3CAF38A9B143F16480E0F8695786501F77E65499AEB8AA0269698EB0A
          FC719EAC505CAD581AF13E1EC81CC571458E4F1FE8CCD5711274AD25E7F02227
          E7C9064589E2E590EDCF533CA96818D3F86BC5ACA619C512D4528C25B83CA689
          0565B0187D924FF0A71E56ECEE60FC32C57025E9B3A80431199420FEC47E8EC8
          F1E469456F31D6CE2F7B28262BAE773CFEEF8A7B15E395A84D61083A5E718F98
          659D94BC23C66CAFB19F0F514C575C98E01CCA15C394A48F7311B4A7981583CF
          B2778213F3E47B317A89306196A2590A736005E15B6594A83FFC047510B3CC4E
          4961527E996427796BCAF3C0C20E5292CA21086D9E4979428B14B7293EB49FDB
          291E90AAF82A2D190F4127EB1F8F8AD13D49CB2F9688FB149B03DFE131972A86
          487CE63D8A60D9FA795B8C801353DA3BC1093CA518A5A8A8A61D0E6A46DC5B33
          BF6059FBE916DB18B4627D150F8951D8AEE41331C6E08D88F79DA9182B6EF524
          5980214ACC83DE856C66BE9318AFF5B0980727ED3041CC4AFDB3C03E787004BC
          840A71FB673F287A2939AFFB2FE672149B8A31B59D621A7CBE986DB222A6FE48
          9291C6B826A6FE3EA22F25E7EBE017F9420D9E166140FF22065E25C6AF7A21A6
          1F1294F3156C879645F4314331D0F37BA210E44989987C4A14BDF48F546DA70D
          8EC8F1641FC50031DBAE6E84FB086DFA2B3133F2350A1BCD7754CC54340FD1F6
          033189B44F1D131394D6621EC8D921DA12CDF754BC15673EA8919864D5A539BE
          673B61B64948FD9730397EE922A6B2D122C7F70B14374ACCF9204F70DE70EC4A
          7DD7308DD32D39EB5224C62F0789310A1051DB777DAA98EDB82D6A779593EE6A
          89A2A443A96761DA8CE4108A09F85C842EC315B3830D5C5635188CE53A276D16
          AA1154020FB4BB628732B20B8270209F902A6508516CAFD569331190C662B619
          397454038130A9E3ED7C9DB8093A4B4C15B359E03A5EE8783119C0D0655F4742
          3694CA095BAA51E03B724E37285E75411083E294E5F387D045B72BDE4F899C93
          C43CA88E79DA9035186A7F4B2C04E188E15FF40A39492630D54EF4BB84886968
          7F340F31AC438B6A1840C45E0C414788095C3B1430E96FC544DFD31D93433C46
          492A8C131B94258A1E4A52CE18311F41687F8E943491E284E818576051CCC4B4
          15631C2E2EB21FAABCA54AD233510822C024CB572BA61F436C36D1F6F973917D
          E10492B3C6598D127B55276407C62951DB450141828AA97D871106E3A917EA3B
          712E8030E22847F37B51D14749DA7676C04F501B31FA2689DCF44B624ABFCB42
          B66F65DB774D606E4407254AD2123F419789F16F0E4C60029E906E202542C8F2
          5B8E36D4E7D84AA432EA2538372C5B5F25692E04A16FAAAB8FBB94E59680B2C0
          752ABB1CCE6A97E2DC4642D08962CCF139294E04996FE7811F85B319573AB550
          C1D91DE1D7417DC4C42E69D4A03CC1D2FD25E6BC635AB25E8C5B32098B16B462
          9CFFA1049D84320C0AC76C484D54DA7FC7A630078CC76025E64BEF422E3F88E5
          0D8BCD12981425204A395E6E98632F6C33D213B81CFB26300752B06394981DBC
          FE7C9E345B8D788A14415C0E6350489110E3BD2B26E69BA668A0E82126F2A648
          88EB71A443723846CCAA599BEDCB30C12ACA9B5317C59456B209C97D5609016D
          0B3BD1B6F6BB0A31E7ABA9571D2C26B08CFBBCD21762CA3D0BF2350A9BEE6099
          67C494A6F78A617290C1CAC1173A55F1AC9804975FF08D58BDD4D4C8F1B0FC7B
          C630F626DB17A1C5FAB813663C619262EDA3DCE413E21C4CF804FB99E415962B
          1FE9E8C2D1F66F74153E5B6D294CF0DC49896CCBA1BB48B9D611E3D81130D68F
          701F05445C09FC1D52A058CB4121EF9D6BC9A4FA5988D78FEB809AC8D8BFAB26
          E530694F7C8495E918A2ED4AC555624E6E118DCF92E8E70F393B48105D21A648
          C8366D13E23EF418AB266B21D3F5BB1A58374EC3E2D835C8D1E64D31568908F9
          583156A9D0C3E01CF2442F958BC93CCC92DCF920562CDB9195BA3957872E09F2
          FC1584CC23E78A2E0AB4A1A6DF4F8CEEE1AC35D590628FAD905BA2B43DD57E66
          DCD2409BD7C4E82BAF8281CEDA94AD3397041D23A672803EAAB0D73ADB0937B6
          C44CB1D74759C479187C9A1D838754623F1326A0D7BC7C13AB8C6C017A2FEB81
          2D9704A10738C747B99984F94C7BFD5031695A740609743CE4EE3112E317DE43
          BBC21243D4CF99C755F63B52C66430713259D965D93A70ADA4517C9EC925F7CC
          AB05DE1B868CCC39C44212FE5184843B3A6E89FDCC03C28DE8E66B7381986D97
          28415890E5816BBCA3CA966365E1FD36754C8E7F5C1C4FDC07FCAAA0D37989E4
          7871C62541C46AE58AA31322A150A1EA4B993CEBBBB3AECD3C4AD0AB3044711A
          9310AC165693B4C9DA5C8D927AEB19854D38D0256D56ACE07B116B2DAEAE6192
          AF8523386D24BE5AA7440C8E2455569CD12D616E489A2084EA03517F12EF9A79
          527061320D823C21C783D977FD7AC37362566DD47760B74A9A0479C2BBF528CA
          1362EE97BC317A6F5E319DD4048210F23DAC2482DAC645F6E505A193258633D8
          3585204F701C499891DB2924CFCD7662D5C4F6FF81D434823CE1FFDCC0DA847D
          1764A925A62C64FBD0525309DA3AB69877C032927BDB9197E63407D9C042DF10
          DA6909F2848396E826D2B1757CD7675B7256BA1C7C6720C813CE0864C4D4C7B0
          7A0B8AEA2DA4544BD0AEFFA22BBFFC0F0E7058DCA84565480000000049454E44
          AE426082}
      end
      object lbl18: TLabel
        Left = 447
        Top = 155
        Width = 61
        Height = 13
        Caption = 'Powered by:'
      end
      object btn1: TSpeedButton
        Left = 16
        Top = 104
        Width = 230
        Height = 41
        Caption = '&Backup System Files to Cloud'
        OnClick = btn1Click
      end
      object btn2: TSpeedButton
        Left = 266
        Top = 104
        Width = 230
        Height = 41
        Caption = '&Restore System Files from Cloud'
      end
      object grp5: TGroupBox
        Left = 16
        Top = 16
        Width = 480
        Height = 73
        Caption = 'Backup/Restore  History'
        TabOrder = 0
        object lbl19: TLabel
          Left = 20
          Top = 29
          Width = 87
          Height = 13
          Caption = 'Last Backup Date:'
        end
        object lblBackupDate: TLabel
          Left = 134
          Top = 29
          Width = 3
          Height = 13
        end
        object lbl20: TLabel
          Left = 20
          Top = 48
          Width = 91
          Height = 13
          Caption = 'Last Restore Date:'
        end
        object lblRestoreDate: TLabel
          Left = 134
          Top = 48
          Width = 3
          Height = 13
        end
      end
    end
    object tsCounterID: TTabSheet
      Caption = 'Record Counters'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lbl8: TLabel
        Left = 192
        Top = 336
        Width = 28
        Height = 13
        Caption = 'kabID'
        FocusControl = editkabID
      end
      object lbl9: TLabel
        Left = 192
        Top = 376
        Width = 63
        Height = 13
        Caption = 'kabDrawerID'
        FocusControl = editkabDrawerID
      end
      object lbl10: TLabel
        Left = 192
        Top = 416
        Width = 42
        Height = 13
        Caption = 'kabBinID'
        FocusControl = editkabBinID
      end
      object lbl11: TLabel
        Left = 192
        Top = 456
        Width = 65
        Height = 13
        Caption = 'transactionID'
        FocusControl = edittransactionID
      end
      object lbl15: TLabel
        Left = 360
        Top = 352
        Width = 65
        Height = 13
        Caption = 'transactionID'
        FocusControl = edittransactionID1
      end
      object lbl17: TLabel
        Left = 16
        Top = 6
        Width = 262
        Height = 13
        Caption = 'Last ID Numbers (Counters)  per System Table'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object editkabID: TDBEdit
        Left = 192
        Top = 352
        Width = 134
        Height = 21
        DataField = 'kabID'
        TabOrder = 0
      end
      object editkabDrawerID: TDBEdit
        Left = 192
        Top = 392
        Width = 134
        Height = 21
        DataField = 'kabDrawerID'
        TabOrder = 1
      end
      object editkabBinID: TDBEdit
        Left = 192
        Top = 432
        Width = 134
        Height = 21
        DataField = 'kabBinID'
        TabOrder = 2
      end
      object edittransactionID: TDBEdit
        Left = 192
        Top = 472
        Width = 134
        Height = 21
        DataField = 'transactionID'
        TabOrder = 3
      end
      object edittransactionID1: TDBEdit
        Left = 360
        Top = 368
        Width = 134
        Height = 21
        DataField = 'transactionID'
        DataSource = dsCounte
        TabOrder = 4
      end
      object grp1: TGroupBox
        Left = 274
        Top = 30
        Width = 236
        Height = 116
        Caption = 'kabTRAK '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        object lbl12: TLabel
          Left = 37
          Top = 81
          Width = 18
          Height = 13
          Caption = 'Bin:'
          FocusControl = editkabID1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl13: TLabel
          Left = 37
          Top = 23
          Width = 41
          Height = 13
          Caption = 'Cabinet:'
          FocusControl = editkabDrawerID1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl14: TLabel
          Left = 37
          Top = 52
          Width = 39
          Height = 13
          Caption = 'Drawer:'
          FocusControl = editkabBinID1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object editkabID1: TDBEdit
          Left = 84
          Top = 19
          Width = 134
          Height = 21
          DataField = 'kabID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object editkabDrawerID1: TDBEdit
          Left = 84
          Top = 48
          Width = 134
          Height = 21
          DataField = 'kabDrawerID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object editkabBinID1: TDBEdit
          Left = 84
          Top = 77
          Width = 134
          Height = 21
          DataField = 'kabBinID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
      object grp2: TGroupBox
        Left = 274
        Top = 157
        Width = 236
        Height = 84
        Caption = 'Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        object lbl2: TLabel
          Left = 37
          Top = 21
          Width = 29
          Height = 13
          Caption = 'Tools:'
          FocusControl = edittoolsID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl3: TLabel
          Left = 37
          Top = 50
          Width = 42
          Height = 13
          Caption = 'Supplier:'
          FocusControl = editsupplierID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object edittoolsID: TDBEdit
          Left = 84
          Top = 17
          Width = 134
          Height = 21
          DataField = 'toolsID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object editsupplierID: TDBEdit
          Left = 84
          Top = 46
          Width = 134
          Height = 21
          DataField = 'supplierID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object grp3: TGroupBox
        Left = 16
        Top = 30
        Width = 240
        Height = 84
        Caption = 'Employees/Users'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        object lbl6: TLabel
          Left = 14
          Top = 25
          Width = 55
          Height = 13
          Caption = 'Employees:'
          FocusControl = editemployeeID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl7: TLabel
          Left = 14
          Top = 52
          Width = 50
          Height = 13
          Caption = 'User PIN::'
          FocusControl = edituserID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object editemployeeID: TDBEdit
          Left = 89
          Top = 21
          Width = 134
          Height = 21
          DataField = 'employeeID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edituserID: TDBEdit
          Left = 89
          Top = 48
          Width = 134
          Height = 21
          DataField = 'userID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object grp4: TGroupBox
        Left = 16
        Top = 120
        Width = 240
        Height = 121
        Caption = 'Transactions '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        object lbl5: TLabel
          Left = 14
          Top = 26
          Width = 57
          Height = 13
          Caption = 'Job Orders:'
          FocusControl = editjobID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl16: TLabel
          Left = 14
          Top = 53
          Width = 65
          Height = 13
          Caption = 'Transactions:'
          FocusControl = edittransactionID2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbl4: TLabel
          Left = 14
          Top = 82
          Width = 63
          Height = 13
          Caption = 'Notifications:'
          FocusControl = editnotificationID
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object editjobID: TDBEdit
          Left = 87
          Top = 22
          Width = 134
          Height = 21
          DataField = 'jobID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edittransactionID2: TDBEdit
          Left = 87
          Top = 49
          Width = 134
          Height = 21
          DataField = 'transactionID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object editnotificationID: TDBEdit
          Left = 87
          Top = 78
          Width = 134
          Height = 21
          DataField = 'notificationID'
          DataSource = dsCounte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    object tsInterface: TTabSheet
      Caption = '&Interface'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object btnSelect: TButton
        Left = 360
        Top = 295
        Width = 100
        Height = 23
        Caption = '&Save this Setting'
        TabOrder = 0
      end
      object pnl1: TPanel
        Left = 0
        Top = 0
        Width = 526
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object lbl1: TLabel
          Left = 20
          Top = 18
          Width = 211
          Height = 13
          Caption = 'Select your preferred background wallpaper'
        end
      end
      object pnl2: TPanel
        Left = 0
        Top = 37
        Width = 526
        Height = 218
        Align = alClient
        BevelOuter = bvNone
        Caption = 'pnl2'
        Padding.Left = 20
        Padding.Top = 20
        Padding.Right = 20
        Padding.Bottom = 20
        TabOrder = 2
        object ImageEnMView1: TImageEnMView
          Left = 20
          Top = 20
          Width = 486
          Height = 178
          ParentCtl3D = False
          BorderStyle = bsNone
          OnImageSelect = ImageEnMView1ImageSelect
          Style = iemsFlat
          ThumbnailsBorderColor = clBlack
          BackgroundStyle = iebsCropShadow
          ImageEnVersion = '3.1.2'
          Align = alClient
          TabOrder = 0
        end
      end
    end
  end
  object tblCounter: TMyTable
    TableName = 'tblcounterid'
    Connection = DM.conHenchman
    Left = 72
    Top = 280
    object tblCountertoolsID: TIntegerField
      FieldName = 'toolsID'
      Origin = 'tblcounterid.toolsID'
    end
    object tblCountersupplierID: TIntegerField
      FieldName = 'supplierID'
      Origin = 'tblcounterid.supplierID'
    end
    object tblCounternotificationID: TIntegerField
      FieldName = 'notificationID'
      Origin = 'tblcounterid.notificationID'
    end
    object tblCounterjobID: TIntegerField
      FieldName = 'jobID'
      Origin = 'tblcounterid.jobID'
    end
    object tblCounteremployeeID: TIntegerField
      FieldName = 'employeeID'
      Origin = 'tblcounterid.employeeID'
    end
    object tblCounteruserID: TIntegerField
      FieldName = 'userID'
      Origin = 'tblcounterid.userID'
    end
    object tblCounterkabID: TIntegerField
      FieldName = 'kabID'
      Origin = 'tblcounterid.kabID'
    end
    object tblCounterkabDrawerID: TIntegerField
      FieldName = 'kabDrawerID'
      Origin = 'tblcounterid.kabDrawerID'
    end
    object tblCounterkabBinID: TIntegerField
      FieldName = 'kabBinID'
      Origin = 'tblcounterid.kabBinID'
    end
    object tblCountertransactionID: TIntegerField
      FieldName = 'transactionID'
      Origin = 'tblcounterid.transactionID'
    end
  end
  object dsCounte: TDataSource
    DataSet = tblCounter
    Left = 32
    Top = 280
  end
end
