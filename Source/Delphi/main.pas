(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  main.pas
   Description            :  Main program
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0
**********************************************************************************)

unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TFlatPanelUnit, pngimage, TFlatSpeedButtonUnit,
  JvExExtCtrls, JvExtComponent, JvClock, TFlatButtonUnit, StdCtrls,
  TFlatTitlebarUnit,  DB, DBAccess, MyAccess, JvComponentBase,
  JvHidControllerClass, Buttons, regware4, auHTTP, reggen4,
  AdvSmoothSplashScreen, JvExControls, JvFormWallpaper,
  Grids, DBGrids,inifiles,
  AdvMetroButton, AdvSmoothTouchKeyBoard,
  jpeg, ImgList, AdvSmoothWin8Marquee, JvPanel, WallPaper, ComCtrls,
  WUpdateWiz, WUpdateLanguagesU, WUpdate, nrclasses, nrdataproc, nrusbdrvx32,
  nrusb, nrhid, RzStatus, RzBorder;

type
  TfrmMain = class(TForm)
    pnlBottom: TFlatPanel;
    imgCabinet: TImage;
    imgProximity: TImage;
    imgNetwork: TImage;
    imgInternet: TImage;
    lblCompany: TLabel;
    Timer: TTimer;
    Reg: TRegware4;
    lblDate: TLabel;
    imgEmployee: TImage;
    imgCabinetButton: TImage;
    imgChangePin: TImage;
    imgCustomer: TImage;
    imgDatabase: TImage;
    imgItems: TImage;
    imgJob: TImage;
    imgLogout: TImage;
    imgSwipe: TImage;
    imgSystemSettings: TImage;
    lblCabinet: TLabel;
    lblChangePin: TLabel;
    lblCustomer: TLabel;
    lblDatabase: TLabel;
    lblEmployee: TLabel;
    lblItems: TLabel;
    lblJob: TLabel;
    lblLogout: TLabel;
    lblSwipe: TLabel;
    lblSystemSettings: TLabel;
    fpnlTop: TFlatPanel;
    imgLogo: TImage;
    RegCodeGenerator41: TRegCodeGenerator4;
    txtAbout: TStaticText;
    pnlAdmin: TJvPanel;
    WallPaper1: TWallPaper;
    webUpdateMTD: TWebUpdate;
    WebUpdateWizard1: TWebUpdateWizard;
    WebUpdateWizardEnglish1: TWebUpdateWizardEnglish;
    ilTabImages: TImageList;
    pnlUser: TJvPanel;
    pnlGuest: TJvPanel;
    imgLogin: TImage;
    imgNotification: TImage;
    imgIssueTools: TImage;
    lblLogin: TLabel;
    lblNotifications: TLabel;
    lblToolOut: TLabel;
    ledTop: TRzLEDDisplay;
    ledBottom: TRzLEDDisplay;
    imgLogoutUser: TImage;
    btn1: TSpeedButton;
    procedure imgExitClick(Sender: TObject);
    procedure imgLoginClick(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure imgNotificationClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure imgLogoutClick(Sender: TObject);
    procedure RegExpire(Sender: TObject);
    procedure imgCustomerClick(Sender: TObject);
    procedure imgEmployeeClick(Sender: TObject);
    procedure imgChangePinClick(Sender: TObject);
    procedure imgDatabaseClick(Sender: TObject);
    procedure imgCabinetButtonClick(Sender: TObject);
    procedure imgSystemSettingsClick(Sender: TObject);
    procedure RegNagScreen(Sender: TObject);
    procedure imgItemsClick(Sender: TObject);
    procedure imgJobClick(Sender: TObject);
    procedure imgSwipeClick(Sender: TObject);
    procedure imgIssueToolsClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }

    bDLCardType     : Byte;
    boCONN:Boolean;
    ERROR_CODE:array[0..180] of String;
    boFunctionStart : Boolean;
    boLoopStart     : Boolean;
    intCustID : Integer;

  public
    { Public declarations }
    intUserID : Integer;
    strUserName: string;   //Name of user, for security login and checking if allowed to run specific menu options.
    strCompany : string;
    strEmail : string;
    isDBOnline : Boolean;
    isLogged : Boolean;
    UserIsAdmin : Boolean;
    strUserFullname, strJbLabel: string;

  end;

  const
     FERR_LIGHT = 2; //2
     FERR_SOUND = 2;      //2
     FOK_LIGHT  = 4; //4
     FOK_SOUND  = 4;      //4


var
  frmMain: TfrmMain;
  ProductName_: String ;
  SerialNumber_: widestring;
  VendorName_: widestring;
  Device_: TJvHidDevice;
  ReportID: Byte;
  Data: Pointer;
  Size: Word;
  DriverComponent: Pointer;
  IsUSBMode: Boolean;
  // LastCMD_: TedCommand;

implementation

uses GlobalRoutines, uDataModule, uAboutKabTrak, uWelcome, uSplash, uLicense,
     uLogin, uCustomer, uEmployees,uChangePin, uDatabaseSetup, uSystemSettings,
     uSetupCabinet, uTools, uUserTRansactions, uJobOrders, uReaderSetup,
  uReturnTools, uIssueTools;

{$R *.dfm}

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
msg : string;
begin

msg := 'Are you sure to terminate the application?';
if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
   begin
     Action := caFree;
     application.Terminate;
     // Close all Database connections
     with DM do
       begin
         // Customer table
         qryCustomer.Close;
         // User Table
         qryLoggedUser.Close;
         // Check other table here and close
         conHenchman.Close; // Close DB connection
       end;
   end
else
   Action := caNone;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  MTDIni: TiniFile;
  License : string;
  RegisteredTo : string;
  isLocal : Boolean;
  strWallpaper : string;

begin

    fpnlTop.Caption := 'Version ' + GetAppVersionStr;

    isDBOnline := True;
    MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
    with MTDIni do
    begin
       strWallpaper := MTDIni.ReadString('General','Wallpaper',GetWallpaperPath() + 'default.png');
       License := MTDIni.ReadString('General', 'LicenseID', '');   //Get registration code.
       if License = '' then
       begin   //If no licence code on ini file (ie. 1st use), add it as current date (to control 30 day trial).

         License := '30 day evaluation';
         MTDIni.WriteString('General', 'LicenseID', License);

       end;

        RegisteredTo := MTDIni.ReadString('General', 'RegisteredTo', '');   //Get name registered to.
        intCustID := MTDIni.ReadInteger('Customer', 'ID', 1);  // Company ID
        strCompany := MTDIni.ReadString('Customer', 'Name', '');  // Company Name
        strEmail :=  MTDIni.ReadString('Customer', 'Email', '');  // Company Email
        strJbLabel := MTDIni.ReadString('Customer', 'JobLabel', '');  // Company Email

        if strCompany = '' then  MTDIni.WriteString('Customer', 'Name', 'Henchman');   //If "Sample Data" company not yet on ini file (ie. 1st use), add it, so it shows on menu.

          //Check if actually connected
           if not DM.conHenchman.Connected then
             begin
                isDBOnline := False;
                raise Exception.Create('Critical error. Unable to connect to kabTRAK Database profile');
                 // Launch database configuratio here and reconnect

             end
          else
             begin
               //Connection successful
               // Open Company Table to set Owner
                 if not OpenCustomerTable then raise Exception.Create('Unable to open customer record');
                 if not OpenCabinet then raise Exception.Create('Unable to open cabinet record');


             end;
        end;
        MTDIni.Free;

    Wallpaper1.AdvImage.LoadFromFile(strWallpaper);



    end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  Online : Boolean;


begin

           if (strCompany = '') then  //If no companies set up yet, show frmWelcome.
                begin
                  screen.cursor := crHourglass;
                  application.createform (TfrmWelcome,frmWelcome);
                  frmSplash.hide; frmsplash.free;
                  screen.cursor := crDefault;
                  if frmWelcome.ShowModal = mrCancel then
                    begin
                      frmMain.OnClose := nil;   //Disconnect event handler to suppress this code.
                      close;   //Close application.
                    end;
                end
             else
               begin
                 lblCompany.Caption := strCompany;
                 lblDate.Caption := DisplayFormattedDate(Date());

                if InetIsOffline(0) then Online := False  else Online := True;

                 if not Online then
                   begin
                     ShowMessage('Warming! kabTRAK is not connected to the internet. Cloud update is not possible.');
                   end
                 else
                   begin
                     UpdateInternetIndicator(Online);
                   end;


                     UpdateDBIndicator(DM.conHenchman.Connected);
                     UpdateControls;
                     frmSplash.hide; frmsplash.free;
               end;

end;


procedure TfrmMain.RegExpire(Sender: TObject);
begin
  frmAboutKabTrak.btnLicenseClick(nil);
end;

procedure TfrmMain.RegNagScreen(Sender: TObject);
begin
    if not Reg.Registered then
     begin
        frmAboutKabTrak := TfrmAboutKabTrak.Create(nil);
        frmAboutKabTrak.caption := 'kabTRAK 30 day evaluation';
        frmSplash.hide;
        frmAboutKabTrak.ShowModal;
     end;
end;


procedure TfrmMain.imgCabinetButtonClick(Sender: TObject);
begin
  frmSetupCabinet := TfrmSetupCabinet.Create(nil);
  frmSetupCabinet.ShowModal;
end;

procedure TfrmMain.imgChangePinClick(Sender: TObject);
begin
  frmChangePin := TfrmChangePin.Create(nil);
  frmChangePin.ShowModal;
end;

procedure TfrmMain.imgCustomerClick(Sender: TObject);
begin
  frmCustomer := TfrmCustomer.Create(nil);
  frmCustomer.ShowModal;
end;

procedure TfrmMain.imgDatabaseClick(Sender: TObject);
begin
  frmDatabase := TfrmDatabase.Create(nil);
  frmDatabase.ShowModal;
end;

procedure TfrmMain.imgLoginClick(Sender: TObject);
begin
frmLogin := TfrmLogin.Create(nil);
frmLogin.ShowModal;
end;

procedure TfrmMain.imgEmployeeClick(Sender: TObject);
begin
  frmEmployees := TfrmEmployees.Create(nil);
  frmEmployees.ShowModal;
end;

procedure TfrmMain.imgExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.imgIssueToolsClick(Sender: TObject);
begin
  frmIssueTools := TfrmIssueTools.Create(nil);
  frmIssueTools.Caption := 'Issuing Tools to ' + DM.qryLoggedUser.FieldByName('Fullname').AsString;
  frmIssueTools.ShowModal;
end;

procedure TfrmMain.imgItemsClick(Sender: TObject);
begin
  frmTools := TfrmTools.Create(nil);
  frmTools.ShowModal;
end;

procedure TfrmMain.imgJobClick(Sender: TObject);
begin
  frmJobOrders := TfrmJobOrders.Create(nil);
  frmJobOrders.Caption := GetJobNameLabel;
  frmJobOrders.ShowModal;
end;

procedure TfrmMain.imgLogoutClick(Sender: TObject);
begin
  with frmMain do
  begin
    strUserName := '';
    intUserID := 0;
    isLogged := False;
    UserIsAdmin := False;
    UpdateControls;
  end;
end;

procedure TfrmMain.imgSwipeClick(Sender: TObject);
begin
  frmReaderSetup := TfrmReaderSetup.Create(nil);
  frmReaderSetup.ShowModal;
end;

procedure TfrmMain.imgSystemSettingsClick(Sender: TObject);
begin
  frmSystemSettings := TfrmSystemSettings.Create(nil);
  frmSystemSettings.ShowModal;
end;

procedure TfrmMain.imgNotificationClick(Sender: TObject);

begin
  frmReturnTools := TfrmReturnTools.Create(nil);
  frmReturnTools.Caption := 'Returning tools for ' + DM.qryLoggedUser.FieldByName('Fullname').AsString;
  frmReturnTools.ShowModal;
 end;

procedure TfrmMain.btn1Click(Sender: TObject);
begin
 frmWelcome := TfrmWelcome.Create(nil);
  frmWelcome.ShowModal;
end;

procedure TfrmMain.btnAboutClick(Sender: TObject);
begin
frmAboutKabTrak := TfrmAboutKabTrak.Create(nil);
frmAboutKabTrak.ShowModal;
end;

end.
