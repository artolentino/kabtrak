object frmSupplier: TfrmSupplier
  Left = 0
  Top = 0
  ActiveControl = editSupplierName
  BorderStyle = bsDialog
  Caption = 'Supplier'
  ClientHeight = 276
  ClientWidth = 396
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 246
    Width = 396
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 227
      Top = 0
      Width = 169
      Height = 30
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 5
        Top = 3
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 4
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 396
    Height = 246
    ActivePage = ts1
    Align = alClient
    Images = frmMain.ilTabImages
    TabHeight = 36
    TabOrder = 1
    TabWidth = 120
    object ts1: TTabSheet
      Caption = 'General'
      ImageIndex = 2
      object lbl1: TLabel
        Left = 38
        Top = 25
        Width = 79
        Height = 13
        Caption = 'Company Name:'
        FocusControl = editSupplierName
      end
      object lbl2: TLabel
        Left = 38
        Top = 54
        Width = 78
        Height = 13
        Caption = 'Contact Person:'
        FocusControl = editContactPerson
      end
      object lbl3: TLabel
        Left = 38
        Top = 141
        Width = 70
        Height = 13
        Caption = 'Email Address:'
        FocusControl = editEmailAdd
      end
      object lbl4: TLabel
        Left = 38
        Top = 112
        Width = 66
        Height = 13
        Caption = 'URL/Website:'
        FocusControl = editWebsite
      end
      object lbl5: TLabel
        Left = 38
        Top = 83
        Width = 67
        Height = 13
        Caption = 'Mobile Phone:'
        FocusControl = editMobilePhone
      end
      object lblRequired: TLabel
        Left = 341
        Top = 23
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl6: TLabel
        Left = 341
        Top = 52
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl8: TLabel
        Left = 340
        Top = 111
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object editSupplierName: TDBEdit
        Left = 134
        Top = 21
        Width = 200
        Height = 21
        CharCase = ecUpperCase
        DataField = 'SupplierName'
        DataSource = dsSupplier
        TabOrder = 0
      end
      object editContactPerson: TDBEdit
        Left = 134
        Top = 50
        Width = 200
        Height = 21
        CharCase = ecUpperCase
        DataField = 'ContactPerson'
        DataSource = dsSupplier
        TabOrder = 1
      end
      object editEmailAdd: TDBEdit
        Left = 134
        Top = 135
        Width = 200
        Height = 21
        DataField = 'EmailAdd'
        DataSource = dsSupplier
        TabOrder = 4
      end
      object editWebsite: TDBEdit
        Left = 134
        Top = 108
        Width = 200
        Height = 21
        DataField = 'Website'
        DataSource = dsSupplier
        TabOrder = 3
      end
      object editMobilePhone: TDBEdit
        Left = 134
        Top = 79
        Width = 158
        Height = 21
        DataField = 'MobilePhone'
        DataSource = dsSupplier
        TabOrder = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Notes'
      ImageIndex = 8
      object dbredt1: TDBRichEdit
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 382
        Height = 194
        Align = alClient
        BorderStyle = bsNone
        DataField = 'notes'
        DataSource = dsSupplier
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object tblSupplier: TMyTable
    TableName = 'tblsuppliers'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = DM.dsCustomer
    Connection = DM.conHenchman
    Active = True
    AfterInsert = tblSupplierAfterInsert
    AfterPost = tblSupplierAfterPost
    Left = 72
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object tblSupplierid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
      Origin = 'tblsuppliers.id'
    end
    object tblSupplierSupplierName: TStringField
      FieldName = 'SupplierName'
      Origin = 'tblsuppliers.SupplierName'
      Size = 100
    end
    object tblSupplierContactPerson: TStringField
      FieldName = 'ContactPerson'
      Origin = 'tblsuppliers.ContactPerson'
      Size = 50
    end
    object tblSupplierEmailAdd: TStringField
      FieldName = 'EmailAdd'
      Origin = 'tblsuppliers.EmailAdd'
      Size = 50
    end
    object tblSupplierWebsite: TStringField
      FieldName = 'Website'
      Origin = 'tblsuppliers.Website'
      Size = 100
    end
    object tblSupplierMobilePhone: TStringField
      FieldName = 'MobilePhone'
      Origin = 'tblsuppliers.MobilePhone'
    end
    object tblSupplierCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tblsuppliers.CustID'
    end
    object tblSuppliernotes: TMemoField
      FieldName = 'notes'
      Origin = 'tblsuppliers.notes'
      BlobType = ftMemo
    end
    object tblSuppliersupplier_id: TStringField
      FieldName = 'supplier_id'
      Origin = 'tblsuppliers.supplier_id'
    end
  end
  object dsSupplier: TDataSource
    DataSet = tblSupplier
    Left = 48
    Top = 240
  end
  object tblCounter: TMyTable
    TableName = 'tblcounterid'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = DM.dsCustomer
    Connection = DM.conHenchman
    Active = True
    Left = 160
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
end
