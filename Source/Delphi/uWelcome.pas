(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  uWelcome.pas
   Description            :  Welcome Screen
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)
unit uWelcome;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage, JvWizard, JvExControls, NxPageControl,
  Buttons, DB, DBAccess, MyAccess, AdvPageControl, ComCtrls, MemDS, NxColumns,
  NxDBColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid, NxDBGrid,
  Mask, JvExMask, JvSpin, JvDBSpinEdit, ImgList, DBCtrls;

type
  TfrmWelcome = class(TForm)
    JvWizard1: TJvWizard;
    Welcome: TJvWizardWelcomePage;
    lbl1: TLabel;
    wzDatabaseProfile: TJvWizardInteriorPage;
    conTest: TMyConnection;
    AdvPageControl1: TAdvPageControl;
    advtbshtLocal: TAdvTabSheet;
    advtbshtOnline: TAdvTabSheet;
    grpLocal: TGroupBox;
    Label2: TLabel;
    edtLocalHost: TEdit;
    Label1: TLabel;
    edtLocalPort: TEdit;
    lblDatabase: TLabel;
    edtLocalSchema: TEdit;
    Label3: TLabel;
    edtLocalUsername: TEdit;
    Label5: TLabel;
    edtLocalPassword: TEdit;
    btnTextLocal: TSpeedButton;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtOnlineHost: TEdit;
    edtOnlinePort: TEdit;
    edtOnlineSchema: TEdit;
    edtOnlineUserName: TEdit;
    edtOnlinePassword: TEdit;
    SpeedButton1: TSpeedButton;
    wzSelectDBProfile: TJvWizardInteriorPage;
    rgDBProfile: TRadioGroup;
    wzCustomer: TJvWizardInteriorPage;
    qryCustomer: TMyQuery;
    dsCustomer: TMyDataSource;
    btn1: TSpeedButton;
    wzCabinet: TJvWizardInteriorPage;
    qryCabinet: TMyQuery;
    qryCounter: TMyQuery;
    ilHeadImages: TImageList;
    grp5: TGroupBox;
    cbb1: TComboBox;
    grp1: TGroupBox;
    cbbkabTRAK: TComboBox;
    btnAddkabTRAK: TSpeedButton;
    lbl2: TLabel;
    lbl3: TLabel;
    dsUser: TMyDataSource;
    wzAdmin: TJvWizardInteriorPage;
    grp2: TGroupBox;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    qryUser: TMyQuery;
    dbedt1: TDBEdit;
    lbl7: TLabel;
    dbedtFullName: TDBEdit;
    dbedtFullName1: TDBEdit;
    wzFinal: TJvWizardInteriorPage;
    lbl8: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnTestLocalClick(Sender: TObject);
    procedure btnTestOnlineClick(Sender: TObject);
    procedure JvWizard1ActivePageChanged(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvWizard1FinishButtonClick(Sender: TObject);
    procedure btnAddkabTRAKClick(Sender: TObject);
    procedure JvWizard1CancelButtonClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmWelcome: TfrmWelcome;

implementation

uses uCustomerProfile, uAddkabTRAK, uCreateAdmin;


{$R *.dfm}

function ConnectToDB(strHost : string ;strDB : string ;strUsername : string ;strPassword : string; intPort : integer ) : Boolean;
  begin
    Result := True;
    with frmWelcome.conTest do
        begin
          Close;
          Server := strHost;
          Port := intPort;
          Database := strDB;
          Username := strUsername;
          Password := strPassword;
          // Catch passible error
               try
                 open;
               except
                 on E : Exception do
                   begin
                     MessageDlg('Connection error encountered. ' + E.Message , mtError, [mbOK], 0);
                     Result := False;
                   end;
                 end;

         end;
  end;

procedure TfrmWelcome.btn1Click(Sender: TObject);
begin
  frmCustomerProfile := TfrmCustomerProfile.Create(nil);
  frmCustomerProfile.qryCustomer.Insert;
  frmCustomerProfile.ShowModal;
end;

procedure TfrmWelcome.btnAddkabTRAKClick(Sender: TObject);
begin
  frmAddkabTRAK := TfrmAddkabTRAK.Create(nil);
  with frmAddkabTRAK do
    begin
       qryCustomer.Close;
       qryCustomer.ParamByName('CustID').Value :=  Integer(cbb1.Items.Objects[cbb1.ItemIndex]);
             try
               qryCustomer.Open;
             except
               on E : Exception do
               begin
                  MessageDlg('Unable to open Customer table. ' + E.Message, mtError, [mbOK], 0);
                  Exit;
               end;
             end;

       with qryCounter do
        begin
          Close;
          ParamByName('CustID').Value := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);
           try
             Open;
             except
               on E : Exception do
               begin
                  MessageDlg('Unable to open Counter table. ' + E.Message, mtError, [mbOK], 0);
                  Exit;
               end;
             end;

           if IsEmpty then
        begin
          Insert;
          FieldByName('toolsID').Value := 100;
          FieldByName('supplierID').Value := 100;
          FieldByName('notificationID').Value := 100;
          FieldByName('jobID').Value := 100;
          FieldByName('employeeID').Value := 100;
          FieldByName('userID').Value := 100;
          FieldByName('kabID').Value := 100;
          FieldByName('kabDrawerID').Value := 100;
          FieldByName('kabBinID').Value := 100;
          FieldByName('transactionID').Value := 100;
          FieldByName('CustID').Value := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);

           try
             Post;
             except
               on E : Exception do
                 begin
                   MessageDlg('Unable to post to counter table. ' + E.Message, mtError, [mbOK], 0);
                   Exit;
                 end;
             end;

        end;
        end;



    end;


  frmAddkabTRAK.qryCabinet.Insert;
  frmAddkabTRAK.qryCounter.Open;
  frmAddkabTRAK.ShowModal;
end;

procedure TfrmWelcome.btnTestLocalClick(Sender: TObject);
begin
  with conTest do
    begin
      Close;
      Server := edtLocalHost.Text;
      Port := StrToInt(edtLocalPort.Text);
      Database := edtLocalSchema.Text;
      Username := edtLocalUsername.Text;
      Password := edtLocalPassword.Text;
      Open;
    end;

    if not conTest.Connected then
      MessageDlg('Unable to connect to MYSQL Server. Please check your configuration profile. ake sure that MYSQL server is running.',
        mtError, [mbOK], 0)
     else
       MessageDlg('Local Database connection successful!',  mtInformation,
         [mbOK], 0);

end;

procedure TfrmWelcome.btnTestOnlineClick(Sender: TObject);
begin
  with conTest do
    begin
      Close;
      Server := edtOnlineHost.Text;
      Port := StrToInt(edtOnlinePort.text);
      Database := edtOnlineSchema.Text;
      Username := edtOnlineUserName.Text;
      Password := edtOnlinePassword.Text;
      Open;
    end;

    if not conTest.Connected then
      MessageDlg('Unable to connect to MYSQL Server. Please check your configuration profile. ake sure that MYSQL server is running.',
        mtError, [mbOK], 0)
     else
       MessageDlg('Online Database connection successful!',  mtInformation,
         [mbOK], 0);
end;

procedure TfrmWelcome.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmWelcome.FormCreate(Sender: TObject);
begin
  JvWizard1.ActivePageIndex := 0;
  conTest.Connected := False;
end;

procedure TfrmWelcome.JvWizard1ActivePageChanged(Sender: TObject);
var
strhost,strschema,strusername,strpassword : string;
intport,intCustID : Integer;
isDBConnected : Boolean;
begin
  case JvWizard1.ActivePageIndex of
    0: begin

       end;
    1: begin

       end;
    2: begin

        end;
    3: begin
        // Customer Profile Setup

           case rgDBProfile.ItemIndex of
              0: begin
                   // Local DB Profile selected
                   isDBConnected := ConnectToDB(edtLocalHost.Text,edtLocalSchema.Text,edtLocalUsername.Text,edtLocalPassword.Text,StrToInt(edtLocalPort.text));
                 end;

               1: begin
                   // Online DB Profile Selected
                   isDBConnected := ConnectToDB(edtOnlineHost.Text,edtOnlineSchema.Text,edtOnlineUserName.Text,edtOnlinePassword.Text,StrToInt(edtOnlinePort.text));
                 end;
           end;

           // Connect to selected database

           if isDBConnected then
            begin
               with qryCustomer do
                  begin
                     Close;
                     Connection := conTest;
                     SQL.Clear;
                     SQL.Add('SELECT * FROM tblcustomer');
                     try
                       Open;
                     except
                       on E : Exception do
                          begin
                            MessageDlg('Unable to open Customer table. ' + E.Message, mtError, [mbOK], 0);
                            Exit;
                          end;

                     end;
                end;


            end;
            // Populate the dropdown box
             cbb1.Items.Clear;
             cbb1.Items.Add('--- Select a customer profile ----');

             qryCustomer.First;
             while not qryCustomer.Eof do
               begin
                 with cbb1 do
                   begin

                     Items.AddObject(qryCustomer.FieldByName('CompanyName').AsString, TObject(qryCustomer.FieldByName('CustID').AsInteger));
                   end;

                qryCustomer.Next;
               end;
             qryCustomer.Close;
             cbb1.ItemIndex := 0;
       end;

       4: begin
       //ShowMessage('Kabtrak Profile');
           // Kabtrak Profile
               // Get selected customer ID
               if Integer(cbb1.Items.Objects[cbb1.ItemIndex]) = 0  then
                  begin
                   MessageDlg('Please select valid Customer profile first',
                     mtWarning, [mbOK], 0);
                   JvWizard1.ActivePageIndex := 3;
                    Exit;
                  end
               else  intCustID := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);

               with qryCabinet do
                 begin
                   Close;
                   SQL.Clear;
                   SQL.Add('SELECT * FROM tblitemkabs');
                   DeleteWhere;
                   AddWhere('CustID = ' + IntToStr(intCustID));
                   AddWhere('Assigned = 0');
                   try
                    Open;
                   except
                     on E : Exception do
                       begin
                         MessageDlg('Unable to open kabTRAK table. ' + E.Message, mtError, [mbOK], 0);
                         Exit;
                       end;
                   end;

                   if IsEmpty then
                     begin
                        btnAddkabTRAKClick(nil);
                       // no kabtrak has been previously setup or no available kabTRAK for this transaction
                       // Create a new record. Pop-up add form

                     end
                   else
                     begin
                     // Populate dropdown box control
                      cbbkabTRAK.Items.Clear;
                      cbbkabTRAK.Items.Add('--- Select a kabTRAK profile ----');

                       qryCabinet.First;
                           while not qryCabinet.Eof do
                             begin
                               with cbbkabTRAK do
                                 begin
                                   Items.AddObject(qryCabinet.FieldByName('Description').AsString + '-' + qryCabinet.FieldByName('Location').AsString , TObject(qryCabinet.FieldByName('kabID').AsInteger));
                                 end;

                              qryCabinet.Next;
                             end;
                        qryCabinet.Close;
                        cbbkabTRAK.ItemIndex := 0;
                     end;
                 end;




          end;
       5: begin
               if cbb1.ItemIndex = 0 then
                 begin
                   MessageDlg('Please select valid Customer profile first',
                     mtWarning, [mbOK], 0);
                   JvWizard1.ActivePageIndex := 3;
                   Exit;
                 end;

                if cbbkabTRAK.ItemIndex = 0 then
                  begin
                     MessageDlg('Please select valid kabTRAK profile first',
                     mtWarning, [mbOK], 0);
                     JvWizard1.ActivePageIndex := 4;
                     Exit;
                  end;



             with qryUser do
                begin
                  Close;
                  //ShowMessage('CustID ' + IntToStr(Integer(cbb1.Items.Objects[cbb1.ItemIndex])) + ' KabID ' + IntToStr(Integer(cbbkabTRAK.Items.Objects[cbbkabTRAK.ItemIndex])) );
                  ParamByName('CustID').Value := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);
                  ParamByName('kabID').Value := Integer(cbbkabTRAK.Items.Objects[cbbkabTRAK.ItemIndex]);
                  try
                   Open;
                  except
                   on E : Exception do
                      begin
                        MessageDlg('Unable to open User Table table. ' + E.Message, mtError, [mbOK], 0);
                        Exit;
                      end;

                  end;

                  if IsEmpty then
                    begin

                       frmCreateAdmin := TfrmCreateAdmin.Create(nil);
                       with frmCreateAdmin do
                         begin
                           lblCustID.Caption :=  IntToStr(Integer(cbb1.Items.Objects[cbb1.ItemIndex]));
                           lblKabID.Caption :=  IntToStr(Integer(cbbkabTRAK.Items.Objects[cbbkabTRAK.ItemIndex]));
                           qryCounter.Close;
                           qryCounter.ParamByName('CustID').Value := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);

                           try
                             qryCounter.Open;
                              except
                               on E : Exception do
                                  begin
                                    MessageDlg('Unable to open Counter Table table. ' + E.Message, mtError, [mbOK], 0);
                                    Exit;
                                  end;

                              end;



                           qryUsers.Close;
                           qryUsers.ParamByName('CustID').Value := Integer(cbb1.Items.Objects[cbb1.ItemIndex]);
                           qryUsers.ParamByName('kabID').Value := Integer(cbbkabTRAK.Items.Objects[cbbkabTRAK.ItemIndex]);
                             try
                              qryUsers.open;
                              except
                               on E : Exception do
                                  begin
                                    MessageDlg('Unable to open User Table table. ' + E.Message, mtError, [mbOK], 0);
                                    Exit;
                                  end;

                              end;

                             ShowMessage(qryUsers.FieldByName('CustID').AsString);
                         end;
                         frmCreateAdmin.qryUsers.Insert;
                         frmCreateAdmin.ShowModal;

                    end;
                end;

          end;
 end;

end;

procedure TfrmWelcome.JvWizard1CancelButtonClick(Sender: TObject);
begin
  // Delete all new entries
  //

end;

procedure TfrmWelcome.JvWizard1FinishButtonClick(Sender: TObject);
var

begin
   // Save configuration

   // Close all datasets
  qryUser.Close;
  qryCounter.Close;
  qryCustomer.Close;
  qryCabinet.Close;
  Self.Close;
end;

end.
