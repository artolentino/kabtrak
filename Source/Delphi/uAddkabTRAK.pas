unit uAddkabTRAK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DB, MemDS, DBAccess, MyAccess, DBCtrls,
  Mask, JvExMask, JvSpin, JvDBSpinEdit;

type
  TfrmAddkabTRAK = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    qryCabinet: TMyQuery;
    dbedt1: TDBEdit;
    dbedt2: TDBEdit;
    dbtxt1: TDBText;
    dbedt3: TDBEdit;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    dsCabinet: TMyDataSource;
    edtkabDrawersCount1: TJvDBSpinEdit;
    edtkabDrawersCount2: TJvDBSpinEdit;
    bvl1: TBevel;
    lbl7: TLabel;
    lbl8: TLabel;
    qryCustomer: TMyQuery;
    dsCustomer: TMyDataSource;
    dbedtCustID: TDBEdit;
    dbedtCompanyName: TDBEdit;
    qryCounter: TMyQuery;
    procedure qryCabinetAfterInsert(DataSet: TDataSet);
    procedure qryCabinetAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAddkabTRAK: TfrmAddkabTRAK;

implementation

uses uWelcome;

{$R *.dfm}

procedure TfrmAddkabTRAK.btnCancelClick(Sender: TObject);
begin
  with qryCabinet do
    begin
      if State in  [dsInsert, dsEdit] then
        begin
          if MessageDlg('There are pending operations for the current transaction. Would you like to abort the operation?',
            mtInformation, [mbYes, mbNo], 0) = mrYes then
            begin
             Cancel;
             Self.Close
             end
               else Exit;

        end;
    end;
end;

procedure TfrmAddkabTRAK.btnOKClick(Sender: TObject);
begin
 with qryCabinet do
    begin
      if State in [dsInsert, dsEdit] then
         begin
           try
           Post;
           except
           on E : Exception do
              begin
                MessageDlg('Unable to post. ' + E.Message, mtError, [mbOK], 0);
                Exit;
              end;

           end;
         end;

         // update dropdown control
         with frmWelcome do
         begin
              qryCabinet.Close;
              qryCabinet.SQL.Clear;
              qryCabinet.SQL.Add('SELECT * FROM tblitemkabs');
              qryCabinet.DeleteWhere;
              qryCabinet.AddWhere('CustID =' +frmAddkabTRAK.qryCabinet.FieldByName('CustID').AsString);
              try
                 qryCabinet.Open;
                 except
                 on E : Exception do
                    begin
                      MessageDlg('Unable to post. ' + E.Message, mtError, [mbOK], 0);
                      Exit;
                    end;

              end;

              qryCabinet.RefreshRecord;
              cbbkabTRAK.Items.Clear;
              cbbkabTRAK.Items.Add('--- Select a kabTRAK profile ----');
              qryCabinet.First;
               while not qryCabinet.Eof do
                 begin
                   with cbbkabTRAK do
                     begin
                       Items.AddObject(qryCabinet.FieldByName('Description').AsString + '-' + qryCabinet.FieldByName('Location').AsString, TObject(qryCabinet.FieldByName('CustID').AsInteger));
                     end;

                  qryCabinet.Next;
                 end;
                qryCabinet.Close;
               cbbkabTRAK.ItemIndex := 0;

           end;
    end;
    Self.Close;
end;

procedure TfrmAddkabTRAK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryCabinet.Close;
  qryCounter.Close;
  Action := caFree;
end;

procedure TfrmAddkabTRAK.qryCabinetAfterInsert(DataSet: TDataSet);

begin
   with qryCabinet do
     begin
       FieldByName('KabID').Value := qryCounter.FieldByName('kabID').AsInteger;
       FieldByName('CreatedDate').Value := Date();
       FieldByName('CreatedTime').Value := Time();
       FieldByName('Assigned').Value := 1;
     end;
end;

procedure TfrmAddkabTRAK.qryCabinetAfterPost(DataSet: TDataSet);
begin
  with qryCounter do
    begin
      Edit;
      FieldByName('kabID').Value := FieldByName('kabID').Value + 1;
      try
        Post;
       except
         on E : Exception do
           begin
             MessageDlg('Unable to save record. ' + E.Message, mtError, [mbOK], 0);
             Exit;
           end;
       end;
    end;
end;

end.
