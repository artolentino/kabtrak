unit uSuppliers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, MemDS, DBAccess, MyAccess, Grids, DBGrids, StdCtrls,
  JvExStdCtrls, JvEdit, JvDBSearchEdit, Buttons, ExtCtrls, Menus, AdvCardList,
  DBAdvCardList, AdvSmoothListBox, DBAdvSmoothListBox;

type
  TfrmSuppliers = class(TForm)
    pnl1: TPanel;
    dsSuppliers: TMyDataSource;
    bvl1: TBevel;
    btnNew: TSpeedButton;
    tblSuppliers: TMyTable;
    tblSuppliersid: TIntegerField;
    tblSuppliersSupplierName: TStringField;
    tblSuppliersContactPerson: TStringField;
    tblSuppliersEmailAdd: TStringField;
    tblSuppliersWebsite: TStringField;
    tblSuppliersMobilePhone: TStringField;
    tblSuppliersCustID: TIntegerField;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnFind: TSpeedButton;
    btnClose: TSpeedButton;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    lst1: TDBAdvSmoothListBox;
    procedure btnEditClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCloseClick(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSuppliers: TfrmSuppliers;

implementation

uses uSupplier, uDataModule, uFindSuppliers;

{$R *.dfm}

procedure TfrmSuppliers.btnCloseClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmSuppliers.btnDeleteClick(Sender: TObject);
var
  msg : string;
begin

 msg := 'Do you really want to delete Supplier record ' + tblSuppliersSupplierName.AsString  + '?';
 if tblSuppliersid.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       tblSuppliers.delete;
       tblSuppliers.RefreshRecord;
       tblSuppliers.Refresh;
     end;
end;

procedure TfrmSuppliers.btnDownClick(Sender: TObject);
begin
  tblSuppliers.Next;
end;

procedure TfrmSuppliers.btnEditClick(Sender: TObject);
begin
  if tblSuppliersid.AsVariant = null then btnNewClick(sender)   //No record selected.
  else begin
    screen.cursor := crHourglass;
    application.createform (TfrmSupplier,frmSupplier);
    frmSupplier.tblSupplier.Locate('id', tblSuppliersid.value, []);
    frmSupplier.Caption := 'Editing ' + tblSuppliersSupplierName.AsString;
    frmSupplier.Showmodal;
    screen.cursor := crDefault;
  end;
end;

procedure TfrmSuppliers.btnFindClick(Sender: TObject);
begin
  frmFindSuppliers := TfrmFindSuppliers.Create(nil);
  frmFindSuppliers.ShowModal;
end;

procedure TfrmSuppliers.btnFirstClick(Sender: TObject);
begin
  tblSuppliers.First;
end;

procedure TfrmSuppliers.btnLastClick(Sender: TObject);
begin
  tblSuppliers.Last;
end;

procedure TfrmSuppliers.btnNewClick(Sender: TObject);
begin
  screen.cursor := crHourglass;
  application.createform (TfrmSupplier,frmSupplier);
  frmSupplier.tblSupplier.Insert;
  frmSupplier.Caption := 'New Supplier Record';
  frmSupplier.ShowModal;
  screen.cursor := crDefault;
end;

procedure TfrmSuppliers.btnRefreshClick(Sender: TObject);
begin
  tblSuppliers.Refresh;
end;

procedure TfrmSuppliers.btnUpClick(Sender: TObject);
begin
  tblSuppliers.Prior;
end;

procedure TfrmSuppliers.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  tblSuppliers.Close;
end;

procedure TfrmSuppliers.FormCreate(Sender: TObject);
begin
  if not tblSuppliers.active then
   begin
    tblSuppliers.Connection := DM.conHenchman;
    tblSuppliers.Active := true;
  end;
end;

procedure TfrmSuppliers.FormShow(Sender: TObject);
begin
   tblSuppliers.Open;
end;

end.
