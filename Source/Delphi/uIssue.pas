unit uIssue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MemDS, StdCtrls, ExtCtrls, Buttons, RzButton;

type
  TfrmIssue = class(TForm)
    qryTransaction: TMyQuery;
    dsTransaction: TMyDataSource;
    qryTransDetail: TMyQuery;
    qryTools: TMyQuery;
    qryJobs: TMyQuery;
    dsJobs: TMyDataSource;
    pnl1: TPanel;
    pnl2: TPanel;
    pnl3: TPanel;
    grp1: TGroupBox;
    cbbJobs: TComboBox;
    qryJobsTrailID: TIntegerField;
    qryJobsDescription: TStringField;
    qryJobsCustID: TIntegerField;
    qryJobsStartDate: TDateField;
    qryJobsEndDate: TDateField;
    qryJobsRemark: TStringField;
    qryJobscontinues: TIntegerField;
    btnUnlock: TSpeedButton;
    lbl1: TLabel;
    btnOK: TRzButton;
    btnCancel: TRzButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cbbJobsChange(Sender: TObject);
    procedure btnUnlockClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIssue: TfrmIssue;

implementation

uses uDataModule, GlobalRoutines;

{$R *.dfm}

procedure TfrmIssue.btnUnlockClick(Sender: TObject);
begin
  // Unlock code here
  // parameter to communicate with the hardware
  // DrwwerID
  // Unlock Code
  // Wait for the Device's response if command is sent if successful or not
  // if not successful the door will stay locked




end;

procedure TfrmIssue.cbbJobsChange(Sender: TObject);
begin
  if cbbJobs.ItemIndex <> 0 then btnUnlock.Enabled := True else btnUnlock.Enabled := False;
end;

procedure TfrmIssue.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmIssue.FormCreate(Sender: TObject);
begin
  grp1.Caption := 'Please select a ' +  GetJobNameLabel + ' for this transaction. ';
  //Open Query
           qryJobs.Open;
           cbbJobs.Items.Clear;
           cbbJobs.Items.Add('--- Select ' + GetJobNameLabel + ' for this transaction ----');

           qryJobs.First;
           while not qryJobs.Eof do
             begin
               with cbbJobs do
                 begin

                   Items.AddObject(qryJobsDescription.AsString, TObject(qryJobsTrailID.AsString));
                 end;

              qryJobs.Next;
             end;
           qryJobs.Close;
           cbbJobs.ItemIndex := 0;
end;

procedure TfrmIssue.btnOKClick(Sender: TObject);
begin
  // Get the list of items taken by the user
  // Check the tool location signal if empy
  // and get the item ID of each tools taken
  // and record to transaction table

  // lock the drawer again
end;

procedure TfrmIssue.btnCancelClick(Sender: TObject);
begin
  Self.Close;
end;

end.
