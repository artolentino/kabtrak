unit uJobOrders;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MemDS, AdvSmoothListBox, DBAdvSmoothListBox,
  NxPageControl, ExtCtrls, Buttons;

type
  TfrmJobOrders = class(TForm)
    pnlToolbar: TPanel;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnClose: TSpeedButton;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    dsJobs: TMyDataSource;
    lst1: TDBAdvSmoothListBox;
    qryJobOrders: TMyQuery;
    qryTRansactions: TMyQuery;
    qryJobOrdersTrailID: TIntegerField;
    qryJobOrdersDescription: TStringField;
    qryJobOrdersCustID: TIntegerField;
    qryJobOrdersStartDate: TDateField;
    qryJobOrdersEndDate: TDateField;
    qryJobOrdersRemark: TStringField;
    qryTRansactionsTransID: TIntegerField;
    qryTRansactionsUserID: TIntegerField;
    qryTRansactionsTransType: TIntegerField;
    qryTRansactionsItemID: TIntegerField;
    qryTRansactionsTransDate: TDateField;
    qryTRansactionsTransTime: TDateTimeField;
    qryTRansactionsRemarks: TStringField;
    qryTRansactionsTrailID: TIntegerField;
    qryTRansactionsCustID: TIntegerField;
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmJobOrders: TfrmJobOrders;

implementation

uses uDataModule, GlobalRoutines, uJobOrder;

{$R *.dfm}

procedure TfrmJobOrders.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmJobOrders.btnDeleteClick(Sender: TObject);
var
  msg,msg2 : string;
begin
if qryJobOrdersTrailID.AsVariant = null then exit;   //Ignore if no record selected.

 // check if the Job order number is in transaction table
  if qryTRansactions.Locate('TrailID', qryJobOrders.FieldByName('TrailID').asInteger, []) then
     begin
      msg2 := 'There are existing Tool transaction for this ' + GetJobNameLabel + ' and it cannot be deleted';
      ShowMessage(msg2);
      Exit;
     end
  else
     begin
        msg := 'Do you really want to delete ' +  GetJobNameLabel + ' Number ' + qryJobOrdersTrailID.AsString + ' with description  ' + qryJobOrdersDescription.AsString + '?';
        if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
       begin
         qryJobOrders.delete;
         qryJobOrders.RefreshRecord;
         qryJobOrders.Refresh;
       end;
     end;


end;

procedure TfrmJobOrders.btnDownClick(Sender: TObject);
begin
  qryJobOrders.Next;
end;

procedure TfrmJobOrders.btnEditClick(Sender: TObject);
begin
    if qryJobOrdersTrailID.AsVariant = null then btnNewClick(sender)   //No record selected.
  else begin
    screen.cursor := crHourglass;
    application.createform (TfrmJobOrder,frmJobOrder);
    frmJobOrder.qryJobOrder.Locate('TrailID', qryJobOrdersTrailID.value, []);
    frmJobOrder.Caption := 'Editing ' + frmJobOrder.qryJobOrderDescription.AsString;
    frmJobOrder.Showmodal;
    screen.cursor := crDefault;
  end;
end;

procedure TfrmJobOrders.btnFirstClick(Sender: TObject);
begin
  qryJobOrders.First;
end;

procedure TfrmJobOrders.btnLastClick(Sender: TObject);
begin
 qryJobOrders.Last;
end;

procedure TfrmJobOrders.btnNewClick(Sender: TObject);
begin
  screen.cursor := crHourglass;
  application.createform (TfrmJobOrder,frmJobOrder);
  frmJobOrder.Caption := 'New ' + GetJobNameLabel + ' Record';
  frmJobOrder.grpDates.Visible := True;
  frmJobOrder.qryJobOrder.Insert;
  frmJobOrder.ShowModal;
  screen.cursor := crDefault;
end;

procedure TfrmJobOrders.btnRefreshClick(Sender: TObject);
begin
  qryJobOrders.Refresh;
end;

procedure TfrmJobOrders.btnUpClick(Sender: TObject);
begin
  qryJobOrders.Prior;
end;

procedure TfrmJobOrders.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
