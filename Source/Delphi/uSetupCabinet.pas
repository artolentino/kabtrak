unit uSetupCabinet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, MemDS, DBAccess, MyAccess, ComCtrls, StdCtrls, ExtCtrls, Grids,
  DBGrids, pngimage, Mask, AdvSpin, nrcommbox, Buttons, nrclasses, nrhid,
  nrlogfile,IniFiles, NxDBColumns, NxColumns, NxScrollControl,
  NxCustomGridControl, NxCustomGrid, NxDBGrid, RzPanel, ImgList, NxEdit, RzEdit,
  RzDBEdit, RzDBSpin, DBCtrls;

type
  TfrmSetupCabinet = class(TForm)
    dsDrawers: TMyDataSource;
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pnl3: TPanel;
    btnConnect: TSpeedButton;
    btn1: TSpeedButton;
    btnClose: TSpeedButton;
    pgcCabinet: TPageControl;
    ts1: TTabSheet;
    lblDeviceList: TLabel;
    nrDeviceBox: TnrDeviceBox;
    mmoDeviceInfo: TMemo;
    ts2: TTabSheet;
    tsTools: TTabSheet;
    lbl1: TLabel;
    nrLogCabinet: TnrLogFile;
    nrCabinet: TnrHid;
    RzGroupBox1: TRzGroupBox;
    grd1: TNextDBGrid;
    RzGroupBox2: TRzGroupBox;
    grd2: TNextDBGrid;
    nxdbtxtclmn1: TNxDBTextColumn;
    ilStatusDrawer: TImageList;
    qryBins: TMyQuery;
    dsBin: TMyDataSource;
    qryDrawers: TMyQuery;
    il1StatusBin: TImageList;
    nxdbtxtclmn2: TNxDBTextColumn;
    nxdbtxtclmn3: TNxDBTextColumn;
    NxDBImageColumn1: TNxDBImageColumn;
    qryDrawersID: TIntegerField;
    qryDrawersDrawerID: TStringField;
    qryDrawersKabID: TStringField;
    qryDrawersLockCode: TStringField;
    qryDrawersRemarks: TStringField;
    qryDrawersUserID: TIntegerField;
    dtmfldDrawersCreatedDate: TDateTimeField;
    dtmfldDrawersCreatedTime: TDateTimeField;
    dtmfldDrawersLastAccess: TDateTimeField;
    dtmfldDrawersLastAccessTime: TDateTimeField;
    qryDrawersM1ID: TStringField;
    qryDrawersCustID: TIntegerField;
    qryDrawersDrawerCode: TIntegerField;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    bvl1: TBevel;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    nxdbtxtclmn4: TNxDBTextColumn;
    nxdbtxtclmn5: TNxDBTextColumn;
    nxdbnmbrclmnTNxDBNumberColumn2: TNxDBImageColumn;
    tsCabinet: TTabSheet;
    grp1: TGroupBox;
    lbl2: TLabel;
    lbl3: TLabel;
    nxdbtxtclmn6: TNxDBTextColumn;
    nxdbtxtclmn7: TNxDBTextColumn;
    qryCounter: TMyQuery;
    qryCounterCustID: TIntegerField;
    qryCounterkabDrawerID: TIntegerField;
    qryCounterkabBinID: TIntegerField;
    edtNumberDrawers: TRzDBSpinEdit;
    edt1: TRzDBSpinEdit;
    qryBinsBinID: TStringField;
    qryBinsID: TIntegerField;
    qryBinsDrawerID: TStringField;
    qryBinsKabID: TIntegerField;
    qryBinsstatus: TIntegerField;
    qryBinsDescription: TStringField;
    qryBinsPart__No: TStringField;
    qryBinsCustID: TIntegerField;
    qryBinsP1Code: TStringField;
    lbl4: TLabel;
    edit1: TDBEdit;
    qryBinsItemID: TStringField;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    btn2: TSpeedButton;
    qryDrawersstatus: TIntegerField;
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure nrDeviceBoxChange(Sender: TObject);
    procedure pgcCabinetChange(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure qryDrawersAfterInsert(DataSet: TDataSet);
    procedure qryBinsAfterInsert(DataSet: TDataSet);
    procedure btnRefreshClick(Sender: TObject);
    procedure qryDrawersAfterPost(DataSet: TDataSet);
    procedure qryBinsAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSetupCabinet: TfrmSetupCabinet;

implementation

uses uDataModule, main, GlobalRoutines, uDrawer, uToolLocation;

{$R *.dfm}

procedure TfrmSetupCabinet.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmSetupCabinet.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmSetupCabinet.btnDeleteClick(Sender: TObject);
var
 msg : string;
begin
  case pgcCabinet.ActivePageIndex of
     2: begin
         // Drawers
         msg := 'Do you really want to delete Drawer ' + qryDrawersDrawerID.AsString  + '?';
           if qryDrawersID.AsVariant = null then exit;   //Ignore if no record selected.
             if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
               begin
                 qryDrawers.delete;
                 qryDrawers.Refresh;
               end;
        end;
     3: begin
          msg := 'Do you really want to delete Tool at location  ' + qryBinsBinID.AsString  + '?';
           if qryBinsBinID.AsVariant = null then exit;   //Ignore if no record selected.
             if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
               begin
                 qryBins.delete;
                 qryBins.Refresh;
               end;
        end;

  end;
end;

procedure TfrmSetupCabinet.btnDownClick(Sender: TObject);
begin
  case pgcCabinet.ActivePageIndex of
     2: begin
           qryDrawers.Next;
        end;
     3: begin
           qryBins.Next;
        end;

  end;
end;

procedure TfrmSetupCabinet.btnEditClick(Sender: TObject);
begin
  case pgcCabinet.ActivePageIndex of
     2: begin
         //Drawers
           if qryDrawersID.AsVariant = null then btnNewClick(sender)   //No record selected.
           else
              begin
                screen.cursor := crHourglass;
                application.createform (TfrmDrawer,frmDrawer);
                frmDrawer.Caption := 'Editing ' + qryDrawersDrawerID.AsString;
                frmDrawer.Showmodal;
                screen.cursor := crDefault;
               end;
        end;
     3: begin
            screen.cursor := crHourglass;
            application.createform (TfrmAddToolLocation,frmAddToolLocation);
            frmAddToolLocation.Caption := 'Editing ' + qryBinsBinID.AsString;
            with frmAddToolLocation do

            begin
              // category
              qryCategory.Open;
              cbbCategory.Items.Clear;
              cbbCategory.Items.Add('--- Select a Category ----');

              qryCategory.First;
               while not qryCategory.Eof do
                 begin
                   with cbbCategory do
                     begin

                       Items.AddObject(qryCategoryDescription.AsString, TObject(qryCategoryCatID.AsInteger));
                     end;

                  qryCategory.Next;
                 end;
                qryCategory.Close;
                cbbCategory.ItemIndex := GetItemCategory(qryBinsItemID.AsString);
                //subcategory
               qrySubCategory.close;
               qrySubCategory.ParamByName('intCatID').Value := Integer(cbbCategory.Items.Objects[cbbCategory.ItemIndex]);
               qrySubCategory.Open;
               cbbSubCategory.Items.Clear;
               cbbSubCategory.Items.Add('--- Select a Sub Category ----');

               qrySubCategory.First;
               while not qrySubCategory.Eof do
                 begin
                   with cbbSubCategory do
                     begin

                       Items.AddObject(qrySubCategoryDescription.AsString, TObject(qrySubCategorySubCatID.AsInteger));
                     end;

                  qrySubCategory.Next;
                 end;
               qrySubCategory.Close;
               cbbSubCategory.ItemIndex := GetItemSubCategory(qryBinsItemID.AsString);

               qryItems.Open;
               qryItems.Locate('Part__No',qryBinsItemID.AsString, [] );

            end;

            frmAddToolLocation.ShowModal;
            screen.cursor := crDefault;
        end;

  end;
end;

procedure TfrmSetupCabinet.btnNewClick(Sender: TObject);
begin
  case pgcCabinet.ActivePageIndex of
     2: begin
         //Drawers
         qryDrawers.Open;

          if qryDrawers.RecordCount >= DM.qryCabinet.FieldByName('NumberDrawers').AsInteger then
            begin
               MessageDlg('Maximum allowable number of Drawers per kabTRAK have been reached.',
                 mtWarning, [mbOK], 0);
              Exit;
            end
        else

          begin
            screen.cursor := crHourglass;
            application.createform (TfrmDrawer,frmDrawer);
            frmDrawer.Caption := 'New Drawer';
            qryDrawers.Insert;
            frmDrawer.ShowModal;
            screen.cursor := crDefault;
          end;
        end;
     3: begin
          if qryDrawersID.AsVariant = null then
            begin
              MessageDlg('No reference drawer found for this tool location. Please create your drawer records first.',
                mtWarning, [mbOK], 0);
              exit;
            end
        else
           begin
          //Bins
              if qryBins.RecordCount >= DM.qryCabinet.FieldByName('NumberBins').AsInteger then
                begin
                  MessageDlg('Maximum allowable number of locaton per Drawer have been reached.',
                    mtWarning, [mbOK], 0);
                  Exit;
                end
           end;

            screen.cursor := crHourglass;
            application.createform (TfrmAddToolLocation,frmAddToolLocation);
            frmAddToolLocation.Caption := 'New Tool Location';
            qryBins.Insert;
            frmAddToolLocation.ShowModal;
            screen.cursor := crDefault;


        end;

  end;
end;

procedure TfrmSetupCabinet.btnOKClick(Sender: TObject);
var
MTDIni : TIniFile;
begin
  if DM.qryCabinet.State in [dsInsert, dsEdit] then
    begin
      DM.qryCabinet.post;
    end;
MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');

 with MTDIni do
      begin
             begin
               MTDIni.WriteString('Cabinet', 'Type', 'USB');
               MTDIni.WriteString('Cabinet', 'Description', nrCabinet.HidDevice.Description);
               MTDIni.WriteString('Cabinet', 'Manufacturer', nrCabinet.HidDevice.Manufacturer);
               MTDIni.WriteString('Cabinet', 'Manufacturer', nrCabinet.HidDevice.Manufacturer);
               MTDIni.WriteString('Cabinet', 'VendorID', IntToHex(nrCabinet.HidDevice.VendorID, 4));
               MTDIni.WriteString('Cabinet', 'ProductID', IntToHex(nrCabinet.HidDevice.ProductID, 4));

             end;

      end;
  MTDIni.Free;

  Self.Close;
end;

procedure TfrmSetupCabinet.btnRefreshClick(Sender: TObject);
begin
   case pgcCabinet.ActivePageIndex of
     2: begin
           qryDrawers.Refresh;
        end;
     3: begin
           qryBins.Refresh;
        end;

  end;
end;

procedure TfrmSetupCabinet.btnUpClick(Sender: TObject);
begin
  case pgcCabinet.ActivePageIndex of
     2: begin
          qryDrawers.Prior;
        end;
     3: begin
           qryBins.Prior;
        end;

  end;
end;

procedure TfrmSetupCabinet.FormCreate(Sender: TObject);
begin
  pgcCabinet.ActivePageIndex := 0;
  qryDrawers.Open;
  qryBins.Open;
  RzGroupBox1.Caption := 'Cabinet - ' +  DM.qryCabinet.FieldByName('KabID').AsString;
  RzGroupBox2.Caption := 'Drawer - ' + DM.qryDrawer.FieldByName('DrawerID').AsString;
end;

procedure TfrmSetupCabinet.FormShow(Sender: TObject);
begin
//  with frmMain.kbQwerty do
//   begin
//      Show;
//      Keyboard.LoadKeybdLayout('qwery.txt');
//      Keyboard.Width := 401;
//      Keyboard.Height := 201;
//
//   end;
end;

procedure TfrmSetupCabinet.nrDeviceBoxChange(Sender: TObject);
begin
  mmoDeviceInfo.Lines.Clear;
      mmoDeviceInfo.Lines.Add('Friendly Name :' +  nrCabinet.HidDevice.NameFriendly);
      mmoDeviceInfo.Lines.Add('Description :' +  nrCabinet.HidDevice.Description);
      mmoDeviceInfo.Lines.Add('Manufacturer :' +  nrCabinet.HidDevice.Manufacturer);
      mmoDeviceInfo.Lines.Add('Service :' +  nrCabinet.HidDevice.Service);
end;

procedure TfrmSetupCabinet.pgcCabinetChange(Sender: TObject);
begin
  case pgcCabinet.ActivePageIndex of
      0: begin
          // Connection Tab
          pnlNavigator.Visible := False;
          btnNew.Visible := false;
          btnEdit.Visible := false;
          btnDelete.Visible := false;
         end;
      1: begin
          // Cabinet Config
          pnlNavigator.Visible := False;
          btnNew.Visible := false;
          btnEdit.Visible := false;
          btnDelete.Visible := false;
         end;
      2: begin
          pnlNavigator.Visible := true;
          btnNew.Visible := true;
          btnEdit.Visible := true;
          btnDelete.Visible := True;

         end;
      3: begin
          RzGroupBox2.Caption := ' Drawer ' + qryDrawers.FieldByName('DrawerID').AsString;
          pnlNavigator.Visible := true;
          btnNew.Visible := true;
          btnEdit.Visible := true;
          btnDelete.Visible := True;

         end;
  end;
end;

procedure TfrmSetupCabinet.qryBinsAfterInsert(DataSet: TDataSet);
begin
  // Generate id
  qryBinsID.Value := qryCounterkabBinID.Value + 1;
  qryBinsBinID.Value := StrZeroInt(DM.qryCustomer.FieldByName('CustID').AsInteger, 3) + '-' + StrZeroInt(qryDrawersID.AsInteger,3) + '-' +  StrZeroInt(qryBinsID.AsInteger, 4);
  qryBinsCustID.Value := DM.qryCustomer.FieldByName('CustID').AsInteger;
  qryBinsKabID.Value := DM.qryCabinet.FieldByName('kabID').AsInteger;
  qryBinsstatus.Value := 0;
end;

procedure TfrmSetupCabinet.qryBinsAfterPost(DataSet: TDataSet);
begin
  qryCounter.Edit;
  qryCounterkabBinID.Value := qryCounterkabBinID.Value + 1;
  qryCounter.Post;
end;

procedure TfrmSetupCabinet.qryDrawersAfterInsert(DataSet: TDataSet);
begin
  // Generate ID
  qryDrawersID.Value := qryCounterkabDrawerID.Value + 1;
  qryDrawersDrawerID.Value := StrZeroInt(DM.qryCabinet.FieldByName('KabID').AsInteger, 3) + '-' + StrZeroInt(qryDrawersID.AsInteger, 3);
  qryDrawers.FieldByName('CreatedDate').Value := Date();
  qryDrawers.FieldByName('CreatedTime').Value := Time();

end;

procedure TfrmSetupCabinet.qryDrawersAfterPost(DataSet: TDataSet);
begin
  qryCounter.Edit;
  qryCounterkabDrawerID.Value := qryCounterkabDrawerID.Value + 1;
  qryCounter.Post;
end;

end.
