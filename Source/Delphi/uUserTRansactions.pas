unit uUserTRansactions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, DB, DBAccess, MyAccess, MemDS, ExtCtrls, ComCtrls,
  NxDBColumns, NxColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid,
  NxDBGrid, StdCtrls, AdvDateTimePicker, Buttons, DBCtrls, pngimage, Grids,
  DBGrids;

type
  TfrmUserTransaction = class(TForm)
    pnlOptions: TPanel;
    dbtxtFullname: TDBText;
    dbtxtDepartment: TDBText;
    lbl1: TLabel;
    lbl2: TLabel;
    btnRetrieve: TSpeedButton;
    bvl1: TBevel;
    grpDate: TGroupBox;
    advdtmpckrDate: TAdvDateTimePicker;
    grpDateRange: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    advdtmpckrDateRangeFrom: TAdvDateTimePicker;
    advdtmpckrDateRangeTo: TAdvDateTimePicker;
    grpTools: TGroupBox;
    lbl5: TLabel;
    cbbTools: TComboBox;
    grpProject: TGroupBox;
    lbl6: TLabel;
    cbbProjects: TComboBox;
    pnl2: TPanel;
    grdTransactions: TNextDBGrid;
    NxDBImageColumn1: TNxDBImageColumn;
    NxDBDateColumn1: TNxDBDateColumn;
    nxdbtxtclmn1: TNxDBTextColumn;
    nxdbtxtclmnJobName: TNxDBTextColumn;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    bvl6: TBevel;
    btnRefresh: TSpeedButton;
    rgOptions: TRadioGroup;
    qryEmployee: TMyQuery;
    dsEmployees: TMyDataSource;
    qryTransactions: TMyQuery;
    qryJobs: TMyQuery;
    dsTransactions: TMyDataSource;
    il1: TImageList;
    nxdbtxtclmn2: TNxDBTextColumn;
    nxdbtxtclmn3: TNxDBTextColumn;
    pnlLegend: TPanel;
    imgOut: TImage;
    lbl7: TLabel;
    lbl8: TLabel;
    img1: TImage;
    lbl9: TLabel;
    qryTools: TMyQuery;
    strngfldToolsItemID: TStringField;
    qryToolsid: TIntegerField;
    strngfldToolsDescription: TStringField;
    qryToolsCustID: TIntegerField;
    ds1: TMyDataSource;
    lblAllTracsactions: TLabel;
    qryEmployeeEnabled: TIntegerField;
    qryEmployeeLastAccess: TDateField;
    qryEmployeeLastName: TStringField;
    qryEmployeeFirstName: TStringField;
    qryEmployeeDepartment: TStringField;
    qryEmployeeDeptCode: TStringField;
    qryEmployeeFullName: TStringField;
    qryEmployeeAccessCount: TIntegerField;
    qryEmployeeCustID: TIntegerField;
    qryEmployeeID: TIntegerField;
    qryJobsTrailID: TIntegerField;
    qryJobsDescription: TStringField;
    qryJobsCustID: TIntegerField;
    qryJobsStartDate: TDateField;
    qryJobsEndDate: TDateField;
    qryJobsRemark: TStringField;
    qryJobscontinues: TIntegerField;
    img2: TImage;
    procedure FormCreate(Sender: TObject);
    procedure rgOptionsClick(Sender: TObject);
    procedure cbbToolsChange(Sender: TObject);
    procedure btnRetrieveClick(Sender: TObject);
    procedure cbbProjectsChange(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUserTransaction: TfrmUserTransaction;

implementation

uses uDataModule, GlobalRoutines;

{$R *.dfm}

procedure TfrmUserTransaction.btnDownClick(Sender: TObject);
begin
  qryTransactions.Next;
end;

procedure TfrmUserTransaction.btnFirstClick(Sender: TObject);
begin
  qryTransactions.First;
end;

procedure TfrmUserTransaction.btnLastClick(Sender: TObject);
begin
  qryTransactions.Last;
end;

procedure TfrmUserTransaction.btnRefreshClick(Sender: TObject);
begin
  qryTransactions.Refresh;
end;

procedure TfrmUserTransaction.btnRetrieveClick(Sender: TObject);
var
 intJobID : Integer;
 intToolsID : integer;
 strItemID : string;
begin

//intToolsID := -1;
   case rgOptions.ItemIndex of
      0: begin
            qryTransactions.Close;
            qryTransactions.DeleteWhere;
           // qryTransactions.AddWhere('tbltransdetail.TransDate = ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDate.Date)));
            qryTransactions.Open;
         end;
      1: begin
            qryTransactions.Close;
            qryTransactions.DeleteWhere;
            qryTransactions.AddWhere('tbltransdetail.TransDate = ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDate.Date)));
            qryTransactions.Open;
         end;
      2: begin
            // Validate the date inputs to should be greater then from

            if (advdtmpckrDateRangeFrom.Date <  advdtmpckrDateRangeTo.Date)  then
              begin
                qryTransactions.Close;
                qryTransactions.DeleteWhere;
                qryTransactions.AddWhere('tbltransdetail.TransDate BETWEEN ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDateRangeFrom.Date)) + ' AND ' + QuotedStr(FormatDateTime('YYYY-MM-DD',advdtmpckrDateRangeTo.Date)) );
                qryTransactions.Open;
              end
            else
              MessageDlg('You have an invalid date range.',  mtWarning,
                [mbOK], 0);

         end;
      3: begin
           //Tools
          if cbbTools.ItemIndex > 0  then
               begin
                  qryTransactions.Close;
                  qryTransactions.DeleteWhere;
                  qryTransactions.AddWhere('tbltools.id = ' + IntToStr(Integer(cbbTools.Items.Objects[cbbTools.ItemIndex])));
                  qryTransactions.Open;
               end;



         end;
      4: begin
            if cbbProjects.ItemIndex > -1 then  intJobID := Integer(cbbProjects.Items.Objects[cbbProjects.ItemIndex]);
             if intJobID > 0  then
               begin
                 qryTransactions.Close;
                 qryTransactions.DeleteWhere;
                 qryTransactions.AddWhere('tblemployeeitemtransactions.TrailID =' + IntToStr(intJobID) );
                 qryTransactions.Open;
               end;


         end;


   end;
end;


procedure TfrmUserTransaction.btnUpClick(Sender: TObject);
begin
  qryTransactions.Prior;
end;

procedure TfrmUserTransaction.cbbProjectsChange(Sender: TObject);
begin
  if cbbProjects.ItemIndex > 0  then btnRetrieve.Enabled := True else btnRetrieve.Enabled := False;
end;

procedure TfrmUserTransaction.cbbToolsChange(Sender: TObject);
begin
  if cbbTools.ItemIndex > 0  then btnRetrieve.Enabled := True else btnRetrieve.Enabled := False;

end;

procedure TfrmUserTransaction.FormCreate(Sender: TObject);
begin

   rgOptions.Items.Clear;
   rgOptions.Items.Add('All Transactions');
   rgOptions.Items.Add('By Date');
   rgOptions.Items.Add('By Date Range');
   rgOptions.Items.Add('By Tool Usage');
   rgOptions.Items.Add('By ' + GetJobNameLabel);



  rgOptions.Left := 805;
  rgOptions.Top := 16;
  pnlOptions.Height := 195;
  grdTransactions.Columns.Item[5].Header.Caption := GetJobNameLabel + ' Name';
  lblAllTracsactions.Visible := true;
end;

procedure TfrmUserTransaction.rgOptionsClick(Sender: TObject);
begin
  case rgOptions.ItemIndex of
     0: begin
          btnRetrieve.Enabled := True;
          lblAllTracsactions.Visible := true;
          lblAllTracsactions.Left := 32;
          lblAllTracsactions.Top := 72;
          grpDateRange.Visible := False;
          grpTools.Visible := False;
          grpProject.Visible := False;
          grpDate.Visible := false;
        end;
     1: begin
          lblAllTracsactions.Visible := false;
          btnRetrieve.Enabled := True;
          grpDate.Visible := True;
          grpDate.Left := 32;
          grpDate.Top := 72;
          grpDateRange.Visible := False;
          grpTools.Visible := False;
          grpProject.Visible := False;

        end;
     2: begin
          lblAllTracsactions.Visible := false;
          btnRetrieve.Enabled := True;
          grpDateRange.Visible := True;
          grpDateRange.Left := 32;
          grpDateRange.Top := 72;
          grpDate.Visible := False;
          grpTools.Visible := False;
          grpProject.Visible := False;


        end;
     3: begin
          lblAllTracsactions.Visible := false;
          btnRetrieve.Enabled := false;
          grpTools.Visible := True;
          grpTools.Left := 32;
          grpTools.Top := 72;
          grpDate.Visible := False;
          grpDateRange.Visible := False;
          grpProject.Visible := False;

          // Load selection
           //Open Query
           qryTools.Open;
           cbbTools.Items.Clear;
           cbbTools.Items.Add('--- Select a Tool ----');

           qryTools.First;
           while not qryTools.Eof do
             begin
               with cbbTools do
                 begin

                   Items.AddObject(qryTools.FieldByName('Description').AsString, TObject(qryTools.FieldByName('id').AsInteger));
                 end;

              qryTools.Next;
             end;
           qryTools.Close;
           cbbTools.ItemIndex := 0;
        end;
     4: begin
          lblAllTracsactions.Visible := false;
          btnRetrieve.Enabled := false;
          grpProject.Visible := True;
          grpProject.Caption := GetJobNameLabel;
          lbl6.Caption := 'Select ' + GetJobNameLabel;
          grpProject.Left := 32;
          grpProject.Top := 72;
          grpDate.Visible := False;
          grpDateRange.Visible := False;
          grpTools.Visible := False;


           //Open Query
           qryJobs.Open;
           cbbProjects.Items.Clear;
           cbbProjects.Items.Add('--- Select Job ----');

           qryJobs.First;
           while not qryJobs.Eof do
             begin
               with cbbProjects do
                 begin
                   Items.AddObject(qryJobsDescription.AsString, TObject(qryJobsTrailID.AsInteger));
                 end;

              qryJobs.Next;
             end;
           qryJobs.Close;
           cbbProjects.ItemIndex := 0;
        end;
  end;
end;

end.
