object frmJobOrder: TfrmJobOrder
  Left = 0
  Top = 0
  Caption = 'Job Order'
  ClientHeight = 325
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 294
    Width = 502
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 333
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 5
        Top = 4
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 4
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgcJobOrder: TPageControl
    Left = 0
    Top = 0
    Width = 502
    Height = 294
    ActivePage = ts1
    Align = alClient
    Images = frmMain.ilTabImages
    TabHeight = 36
    TabOrder = 1
    TabWidth = 120
    object ts1: TTabSheet
      Caption = 'General'
      ImageIndex = 2
      object lbl1: TLabel
        Left = 29
        Top = 20
        Width = 57
        Height = 13
        Caption = 'Description:'
        FocusControl = editDescription
      end
      object editDescription: TDBEdit
        Left = 94
        Top = 16
        Width = 370
        Height = 21
        CharCase = ecUpperCase
        DataField = 'Description'
        DataSource = dsJobOrder
        TabOrder = 0
      end
      object dbrgrpcontinues: TDBRadioGroup
        Left = 29
        Top = 51
        Width = 435
        Height = 69
        Caption = 'Regular/Recurring'
        DataField = 'continues'
        DataSource = dsJobOrder
        Items.Strings = (
          'Regular/Recurring'
          'Project Based')
        ParentBackground = True
        TabOrder = 1
        Values.Strings = (
          '0'
          '1')
        OnChange = dbrgrpcontinuesChange
      end
      object grpDates: TGroupBox
        Left = 29
        Top = 126
        Width = 435
        Height = 101
        Caption = 'Dates'
        TabOrder = 2
        Visible = False
        object lbl2: TLabel
          Left = 19
          Top = 27
          Width = 54
          Height = 13
          Caption = 'Start Date:'
        end
        object lbl3: TLabel
          Left = 19
          Top = 61
          Width = 48
          Height = 13
          Caption = 'End Date:'
        end
        object JvDBDateTimePicker1: TJvDBDateTimePicker
          Left = 79
          Top = 23
          Width = 186
          Height = 21
          TabOrder = 0
          DropDownDate = 41883.000000000000000000
          DataField = 'StartDate'
          DataSource = dsJobOrder
        end
        object JvDBDateTimePicker2: TJvDBDateTimePicker
          Left = 79
          Top = 57
          Width = 186
          Height = 21
          TabOrder = 1
          DropDownDate = 41883.000000000000000000
          DataField = 'EndDate'
          DataSource = dsJobOrder
        end
      end
    end
    object ts2: TTabSheet
      Caption = 'Notes'
      ImageIndex = 8
      object dbmmo1: TDBMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 488
        Height = 242
        Align = alClient
        DataField = 'Remark'
        DataSource = dsJobOrder
        TabOrder = 0
      end
    end
  end
  object qryJobOrder: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'select * from tbljobs')
    AfterInsert = qryJobOrderAfterInsert
    AfterPost = qryJobOrderAfterPost
    MasterSource = DM.dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Active = True
    Left = 312
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryJobOrderTrailID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'TrailID'
      Origin = 'tbljobs.TrailID'
    end
    object qryJobOrderDescription: TStringField
      FieldName = 'Description'
      Origin = 'tbljobs.Description'
      Size = 150
    end
    object qryJobOrderCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tbljobs.CustID'
    end
    object qryJobOrderStartDate: TDateField
      FieldName = 'StartDate'
      Origin = 'tbljobs.StartDate'
    end
    object qryJobOrderEndDate: TDateField
      FieldName = 'EndDate'
      Origin = 'tbljobs.EndDate'
    end
    object qryJobOrderRemark: TStringField
      FieldName = 'Remark'
      Origin = 'tbljobs.Remark'
      Size = 255
    end
    object qryJobOrdercontinues: TIntegerField
      FieldName = 'continues'
      Origin = 'tbljobs.continues'
    end
  end
  object dsJobOrder: TMyDataSource
    DataSet = qryJobOrder
    Left = 392
    Top = 136
  end
  object qryCounter: TMyQuery
    Connection = DM.conHenchman
    SQL.Strings = (
      'SELECT * FROM tblcounterid')
    MasterSource = DM.dsCustomer
    Active = True
    Left = 336
    Top = 192
  end
end
