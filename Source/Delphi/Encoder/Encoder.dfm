object Form1: TForm1
  Left = 0
  Top = 0
  Width = 383
  Height = 186
  Caption = 'Encoder - Very Simple Example'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 79
    Height = 13
    Caption = 'Encoder Position'
  end
  object Label2: TLabel
    Left = 8
    Top = 88
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object Label4: TLabel
    Left = 304
    Top = 88
    Width = 31
    Height = 13
    Caption = 'Label4'
  end
  object PhidgetEncoder1: TPhidgetEncoder
    Left = 176
    Top = 96
    Width = 32
    Height = 32
    OnAttach = PhidgetEncoder1Attach
    OnDetach = PhidgetEncoder1Detach
    OnPositionChange = PhidgetEncoder1PositionChange
    ControlData = {00090000}
  end
  object TrackBar1: TTrackBar
    Left = 8
    Top = 32
    Width = 361
    Height = 45
    TabOrder = 1
  end
end
