unit Encoder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, OleCtrls, Phidget21COM_TLB, ComCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    PhidgetEncoder1: TPhidgetEncoder;
    TrackBar1: TTrackBar;
    procedure PhidgetEncoder1PositionChange(ASender: TObject; Index, Time,
      EncoderDisplacement: Integer);
    procedure PhidgetEncoder1Detach(Sender: TObject);
    procedure PhidgetEncoder1Attach(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  TrackBar1.Frequency := 50;
  TrackBar1.Min := -1000;
  TrackBar1.Max := 1000;
  Label2.Caption := 'Unattached';
  Label4.Caption := '';
  PhidgetEncoder1.Open(-1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  PhidgetEncoder1.Enabled[0] := False;
  PhidgetEncoder1.Close;
end;

procedure TForm1.PhidgetEncoder1Attach(Sender: TObject);
begin
  Label2.Caption := 'Attached';
  PhidgetEncoder1.Enabled[0] := True;
  PhidgetEncoder1.EncoderPosition[0] := 0;
  TrackBar1.Position := 0;
end;

procedure TForm1.PhidgetEncoder1Detach(Sender: TObject);
begin
  Label2.Caption := 'Unattached';
end;

procedure TForm1.PhidgetEncoder1PositionChange(ASender: TObject; Index, Time,
  EncoderDisplacement: Integer);
begin
  TrackBar1.Position := TrackBar1.Position + EncoderDisplacement;
  Label4.Caption := IntToStr(PhidgetEncoder1.EncoderPosition[0]);
end;

end.
