object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 541
  Width = 477
  object conHenchman: TMyConnection
    Database = 'mtd'
    Username = 'root'
    Server = 'localhost'
    LoginPrompt = False
    Left = 48
    Top = 32
  end
  object mysqlmntr1: TMySQLMonitor
    Options = [moDialog, moSQLMonitor, moDBMonitor, moCustom, moHandled]
    TraceFlags = [tfQPrepare, tfQExecute, tfQFetch, tfError, tfConnect, tfTransact, tfService, tfMisc, tfParams]
    Left = 136
    Top = 32
  end
  object dsCustomer: TMyDataSource
    DataSet = qryCustomer
    Left = 120
    Top = 336
  end
  object tblLookUpSupplier: TMyTable
    TableName = 'tblsuppliers'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = dsCustomer
    Connection = conHenchman
    Left = 248
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsLookupSupplier: TMyDataSource
    DataSet = tblLookUpSupplier
    Left = 352
    Top = 104
  end
  object tblLookUpToolCategories: TMyTable
    TableName = 'tbltoolcategories'
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    MasterSource = dsCustomer
    Connection = conHenchman
    Left = 232
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsLookupToolCategories: TMyDataSource
    DataSet = tblLookUpToolCategories
    Left = 376
    Top = 168
  end
  object tblToolSubCat: TMyTable
    TableName = 'tbltoolsubcatogories'
    MasterSource = dsLookupToolCategories
    Connection = conHenchman
    Left = 360
    Top = 256
  end
  object dsToolSubCat: TMyDataSource
    DataSet = tblToolSubCat
    Left = 272
    Top = 256
  end
  object tblLookupDept: TMyTable
    TableName = 'tbldepartments'
    MasterFields = 'CustID'
    DetailFields = 'custID'
    MasterSource = dsCustomer
    Connection = conHenchman
    Left = 256
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
  end
  object dsDept: TMyDataSource
    DataSet = tblLookupDept
    Left = 344
    Top = 40
  end
  object qryCustomer: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'SELECT * FROM tblcustomer WHERE CustID = :CustID')
    Left = 56
    Top = 336
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CustID'
        ParamType = ptInput
        Value = 0
      end>
  end
  object qryLoggedUser: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'SELECT * FROM users WHERE UserID = :UserID AND PIN = :PIN')
    MasterSource = dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Left = 56
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'UserID'
      end
      item
        DataType = ftUnknown
        Name = 'PIN'
      end
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryLoggedUserID: TIntegerField
      FieldName = 'ID'
    end
    object qryLoggedUserUserID: TStringField
      FieldName = 'UserID'
      Size = 50
    end
    object qryLoggedUserFullName: TStringField
      FieldName = 'FullName'
      Size = 50
    end
    object qryLoggedUserPIN: TStringField
      FieldName = 'PIN'
      Size = 50
    end
    object dtmfldLoggedUserLastAccess: TDateTimeField
      FieldName = 'LastAccess'
    end
    object qryLoggedUserEnabled: TIntegerField
      FieldName = 'Enabled'
    end
    object dtmfldLoggedUserCreatedDate: TDateTimeField
      FieldName = 'CreatedDate'
    end
    object dtmfldLoggedUserLastAccTime: TDateTimeField
      FieldName = 'LastAccTime'
    end
    object dtmfldLoggedUserCreatedTime: TDateTimeField
      FieldName = 'CreatedTime'
    end
    object qryLoggedUserAccessCount: TIntegerField
      FieldName = 'AccessCount'
    end
    object qryLoggedUserCustID: TIntegerField
      FieldName = 'CustID'
    end
    object qryLoggedUseraccess_type: TIntegerField
      FieldName = 'access_type'
    end
  end
  object qryCabinet: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'SELECT * FROM tblitemkabs WHERE kabID = :kabID')
    MasterSource = dsCustomer
    MasterFields = 'CustID'
    DetailFields = 'CustID'
    Left = 128
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'kabID'
      end
      item
        DataType = ftUnknown
        Name = 'CustID'
      end>
    object qryCabinetKabID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'KabID'
      Origin = 'tblitemkabs.KabID'
    end
    object qryCabinetDescription: TStringField
      FieldName = 'Description'
      Origin = 'tblitemkabs.Description'
      Size = 255
    end
    object qryCabinetLocation: TStringField
      FieldName = 'Location'
      Origin = 'tblitemkabs.Location'
      Size = 255
    end
    object dtmfldCabinetCreatedDate: TDateTimeField
      FieldName = 'CreatedDate'
      Origin = 'tblitemkabs.CreatedDate'
    end
    object dtmfldCabinetCreatedTime: TDateTimeField
      FieldName = 'CreatedTime'
      Origin = 'tblitemkabs.CreatedTime'
    end
    object dtmfldCabinetLastAccess: TDateTimeField
      FieldName = 'LastAccess'
      Origin = 'tblitemkabs.LastAccess'
    end
    object dtmfldCabinetLastAccessTime: TDateTimeField
      FieldName = 'LastAccessTime'
      Origin = 'tblitemkabs.LastAccessTime'
    end
    object qryCabinetUserID: TIntegerField
      FieldName = 'UserID'
      Origin = 'tblitemkabs.UserID'
    end
    object qryCabinetModelNumber: TStringField
      FieldName = 'ModelNumber'
      Origin = 'tblitemkabs.ModelNumber'
      Size = 50
    end
    object qryCabinetCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tblitemkabs.CustID'
    end
    object qryCabinetAssigned: TIntegerField
      FieldName = 'Assigned'
      Origin = 'tblitemkabs.Assigned'
    end
    object qryCabinetNumberDrawers: TIntegerField
      FieldName = 'NumberDrawers'
      Origin = 'tblitemkabs.NumberDrawers'
    end
    object qryCabinetNumberBins: TIntegerField
      FieldName = 'NumberBins'
      Origin = 'tblitemkabs.NumberBins'
    end
  end
  object dsCabinet: TMyDataSource
    DataSet = qryCabinet
    Left = 184
    Top = 408
  end
  object qryLookupItems: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'Select * from tbltools')
    MasterSource = dsCustomer
    Left = 264
    Top = 328
  end
  object dsLookupItems: TMyDataSource
    DataSet = qryLookupItems
    Left = 352
    Top = 328
  end
  object qryDrawer: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'SELECT * FROM tblitemkabdrawers')
    MasterSource = dsCabinet
    MasterFields = 'KabID'
    DetailFields = 'KabID'
    Left = 128
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'KabID'
        ParamType = ptInput
      end>
    object qryDrawerID: TIntegerField
      FieldName = 'ID'
      Origin = 'tblitemkabdrawers.ID'
    end
    object qryDrawerDrawerID: TStringField
      FieldName = 'DrawerID'
      Origin = 'tblitemkabdrawers.DrawerID'
    end
    object qryDrawerKabID: TStringField
      FieldName = 'KabID'
      Origin = 'tblitemkabdrawers.KabID'
      Size = 50
    end
    object qryDrawerLockCode: TStringField
      FieldName = 'LockCode'
      Origin = 'tblitemkabdrawers.LockCode'
    end
    object qryDrawerStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'tblitemkabdrawers.Status'
    end
    object qryDrawerRemarks: TStringField
      FieldName = 'Remarks'
      Origin = 'tblitemkabdrawers.Remarks'
      Size = 255
    end
    object qryDrawerUserID: TIntegerField
      FieldName = 'UserID'
      Origin = 'tblitemkabdrawers.UserID'
    end
    object dtmfldDrawerCreatedDate: TDateTimeField
      FieldName = 'CreatedDate'
      Origin = 'tblitemkabdrawers.CreatedDate'
    end
    object dtmfldDrawerCreatedTime: TDateTimeField
      FieldName = 'CreatedTime'
      Origin = 'tblitemkabdrawers.CreatedTime'
    end
    object dtmfldDrawerLastAccess: TDateTimeField
      FieldName = 'LastAccess'
      Origin = 'tblitemkabdrawers.LastAccess'
    end
    object dtmfldDrawerLastAccessTime: TDateTimeField
      FieldName = 'LastAccessTime'
      Origin = 'tblitemkabdrawers.LastAccessTime'
    end
    object qryDrawerM1ID: TStringField
      FieldName = 'M1ID'
      Origin = 'tblitemkabdrawers.M1ID'
      Size = 10
    end
    object qryDrawerCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tblitemkabdrawers.CustID'
    end
    object qryDrawerDrawerCode: TIntegerField
      FieldName = 'DrawerCode'
      Origin = 'tblitemkabdrawers.DrawerCode'
    end
  end
  object dsDrawer: TMyDataSource
    DataSet = qryDrawer
    Left = 192
    Top = 464
  end
  object qryBin: TMyQuery
    Connection = conHenchman
    SQL.Strings = (
      'SELECT * FROM tblitemkabdrawerbins')
    MasterSource = dsDrawer
    MasterFields = 'DrawerID'
    DetailFields = 'DrawerID'
    Left = 280
    Top = 472
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DrawerID'
      end>
  end
  object dsBin: TMyDataSource
    DataSet = qryBin
    Left = 336
    Top = 472
  end
  object qry: TMyQuery
    Connection = conHenchman
    Left = 112
    Top = 192
  end
end
