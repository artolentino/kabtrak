unit uReaderSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Mask, AdvSpin, AdvCombo, Buttons,
  nrcommbox, nrclasses, nrhid, nrlogfile, nrcomm, IniFiles;

type
  TfrmReaderSetup = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    pnl3: TPanel;
    ts2: TTabSheet;
    ts3: TTabSheet;
    rgConnection: TRadioGroup;
    grpSerial: TGroupBox;
    lbl1: TLabel;
    edtportfrom1: TAdvSpinEdit;
    lbl2: TLabel;
    edtportfrom2: TAdvSpinEdit;
    lblDeviceList: TLabel;
    btnNew: TSpeedButton;
    btn1: TSpeedButton;
    nrLogFile1: TnrLogFile;
    nrUSB: TnrHid;
    nrDeviceBox: TnrDeviceBox;
    mmoDeviceInfo: TMemo;
    nrcmSerial: TnrComm;
    btnClose: TSpeedButton;
    grp1: TGroupBox;
    lbl3: TLabel;
    rbContinues: TRadioButton;
    lbl4: TLabel;
    edtHoldTime: TAdvSpinEdit;
    lbl5: TLabel;
    edtLockOut: TAdvSpinEdit;
    lbl6: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure rgConnectionClick(Sender: TObject);
    procedure nrUSBAfterReceive(Com: TObject; Buffer: Pointer;
      Received: Cardinal);
    procedure btnNewClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure nrDeviceBoxChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReaderSetup: TfrmReaderSetup;

implementation

{$R *.dfm}

procedure TfrmReaderSetup.btn1Click(Sender: TObject);
begin
  nrUSB.Active := False;
end;

procedure TfrmReaderSetup.btnCancelClick(Sender: TObject);
begin
   self.Close;
end;

procedure TfrmReaderSetup.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmReaderSetup.btnNewClick(Sender: TObject);
begin
  nrUSB.Active := True;
end;

procedure TfrmReaderSetup.btnOKClick(Sender: TObject);
var
MTDIni : TIniFile;
begin
MTDIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mtd.ini');
 with MTDIni do
      begin
        if rgConnection.ItemIndex = 0 then
             begin
               MTDIni.WriteString('Reader', 'Type', 'USB');
               MTDIni.WriteString('Reader', 'Description', nrUSB.HidDevice.Description);
               MTDIni.WriteString('Reader', 'Manufacturer', nrUSB.HidDevice.Manufacturer);
               MTDIni.WriteString('Reader', 'Manufacturer', nrUSB.HidDevice.Manufacturer);
               MTDIni.WriteString('Reader', 'VendorID', IntToHex(nrUSB.HidDevice.VendorID, 4));
               MTDIni.WriteString('Reader', 'ProductID', IntToHex(nrUSB.HidDevice.ProductID, 4));

             end
         else
             begin
               MTDIni.WriteString('Reader', 'Type', 'Serial');
               MTDIni.WriteString('Reader', 'Serial Port', nrcmSerial.ComName);
               MTDIni.WriteString('Reader', 'COM Port', VarToStr(nrcmSerial.ComPort));
             end;

               MTDIni.WriteInteger('Reader', 'HoldTime', edtHoldTime.Value );
               MTDIni.WriteInteger('Reader', 'LockOut', edtLockOut.Value );
               MTDIni.WriteBool('Reader', 'ContinuesRead', rbContinues.Checked );

      end;
  MTDIni.Free;

  Self.Close;
end;

procedure TfrmReaderSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmReaderSetup.FormShow(Sender: TObject);
begin
  rgConnectionClick(nil);
end;

procedure TfrmReaderSetup.nrDeviceBoxChange(Sender: TObject);
begin
  if nrDeviceBox.nrComm = nrUSB then
  begin
      mmoDeviceInfo.Lines.Clear;
      mmoDeviceInfo.Lines.Add('Friendly Name :' +  nrUSB.HidDevice.NameFriendly);
      mmoDeviceInfo.Lines.Add('Description :' +  nrUSB.HidDevice.Description);
      mmoDeviceInfo.Lines.Add('Manufacturer :' +  nrUSB.HidDevice.Manufacturer);
      mmoDeviceInfo.Lines.Add('Service :' +  nrUSB.HidDevice.Service);
  end;

  if nrDeviceBox.nrComm = nrcmSerial then
  begin
      mmoDeviceInfo.Lines.Clear;
      mmoDeviceInfo.Lines.Add('Friendly Name :' +  nrcmSerial.ComName);
      
  end;
end;

procedure TfrmReaderSetup.nrUSBAfterReceive(Com: TObject; Buffer: Pointer;
  Received: Cardinal);
begin
//  s := 'HID Report is received:  ' + IntToStr(Received) + ' bytes ';
//  for i := 0 to Received - 1
//    do s := s + IntToHex(Byte(PAnsiChar(Buffer)[i]),2) + ' ';
end;

procedure TfrmReaderSetup.rgConnectionClick(Sender: TObject);
begin
  case rgConnection.ItemIndex of
    0: begin
         mmoDeviceInfo.Lines.Clear;
         nrDeviceBox.nrComm := nrUSB;
         grpSerial.Visible := False;
         lblDeviceList.Top := 103;
         nrDeviceBox.Top := 122;
         mmoDeviceInfo.Top := 149;
         frmReaderSetup.Height := 480;
       end;
    1: begin
        mmoDeviceInfo.Lines.Clear;
        nrcmSerial.Update;
        nrDeviceBox.nrComm := nrcmSerial;
        with grpSerial do
          begin
            Top := 103;
            Visible := True;
          end;
          frmReaderSetup.Height := 538;
         lblDeviceList.Top := 169;
         nrDeviceBox.Top := 188;
         mmoDeviceInfo.Top := 215;

       end;
    
  end;
end;

end.
