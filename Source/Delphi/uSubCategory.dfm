object frmSubCategory: TfrmSubCategory
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Sub Category'
  ClientHeight = 120
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 24
    Top = 55
    Width = 53
    Height = 13
    Caption = 'Description'
    FocusControl = editDescription
  end
  object pnl1: TPanel
    Left = 0
    Top = 89
    Width = 406
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 156
    ExplicitWidth = 463
    object pnl2: TPanel
      Left = 237
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 231
      object btnOK: TButton
        Left = 6
        Top = 1
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 1
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object editDescription: TDBEdit
    Left = 96
    Top = 52
    Width = 289
    Height = 21
    DataField = 'Description'
    DataSource = ds1
    TabOrder = 1
  end
  object pnl3: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 482
  end
  object tblSubCategory: TMyTable
    TableName = 'tbltoolsubcatogories'
    MasterFields = 'CatID'
    DetailFields = 'CatID'
    MasterSource = frmCategories.dsCategories
    Connection = DM.conHenchman
    Active = True
    Left = 128
    Top = 88
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CatID'
        ParamType = ptInput
      end>
    object tblSubCategorySubCatID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'SubCatID'
      Origin = 'tbltoolsubcatogories.SubCatID'
    end
    object tblSubCategoryCatID: TIntegerField
      FieldName = 'CatID'
      Origin = 'tbltoolsubcatogories.CatID'
    end
    object tblSubCategoryDescription: TStringField
      FieldName = 'Description'
      Origin = 'tbltoolsubcatogories.Description'
      Size = 200
    end
    object tblSubCategoryCustID: TIntegerField
      FieldName = 'CustID'
      Origin = 'tbltoolsubcatogories.CustID'
    end
  end
  object ds1: TDataSource
    DataSet = tblSubCategory
    Left = 80
    Top = 88
  end
end
