unit uToolLocation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, DBCtrls, Mask, DB, DBAccess, MyAccess,
  MemDS, Buttons;

type
  TfrmAddToolLocation = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    lbl1: TLabel;
    dblkcbbItems: TDBLookupComboBox;
    lbl2: TLabel;
    editP1Code: TDBEdit;
    grp1: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    cbbCategory: TComboBox;
    cbbSubCategory: TComboBox;
    qryCategory: TMyQuery;
    qrySubCategory: TMyQuery;
    dsCategories: TMyDataSource;
    qryCategoryCatID: TIntegerField;
    qryCategoryDescription: TStringField;
    qryCategoryCustID: TIntegerField;
    qrySubCategorySubCatID: TIntegerField;
    qrySubCategoryCatID: TIntegerField;
    qrySubCategoryDescription: TStringField;
    qrySubCategoryCustID: TIntegerField;
    btn1: TSpeedButton;
    qryItems: TMyQuery;
    dsItems: TMyDataSource;
    lbl5: TLabel;
    qryBinItems: TMyQuery;
    dbrgrp1: TDBRadioGroup;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cbbCategoryChange(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAddToolLocation: TfrmAddToolLocation;

implementation

uses uDataModule, uSetupCabinet;

{$R *.dfm}

procedure TfrmAddToolLocation.btn1Click(Sender: TObject);

begin
  // Check Filters
  if (cbbCategory.ItemIndex = 0) AND (cbbSubCategory.ItemIndex = 0) then
     begin
       // no filters
        qryItems.Close;
        qryItems.DeleteWhere;
        qryItems.Open;

     end;

    if (cbbCategory.ItemIndex <> 0) AND (cbbSubCategory.ItemIndex = 0) then
        begin
          qryItems.Close;
          qryItems.DeleteWhere;
          qryItems.AddWhere('CatID = ' + IntToStr(Integer(cbbCategory.Items.Objects[cbbCategory.ItemIndex])));
          qryItems.Open;
        end;

     if (cbbCategory.ItemIndex = 0) AND (cbbSubCategory.ItemIndex <> 0) then
        begin
          qryItems.Close;
          qryItems.DeleteWhere;
          qryItems.AddWhere('SubCATID = ' + IntToStr(Integer(cbbSubCategory.Items.Objects[cbbSubCategory.ItemIndex])));
          qryItems.Open;
        end;

      if (cbbCategory.ItemIndex <> 0) AND (cbbSubCategory.ItemIndex <> 0) then
        begin
          qryItems.Close;
          qryItems.DeleteWhere;
          qryItems.AddWhere('CatID = ' + IntToStr(Integer(cbbCategory.Items.Objects[cbbCategory.ItemIndex])));
          qryItems.AddWhere('SubCATID = ' + IntToStr(Integer(cbbSubCategory.Items.Objects[cbbSubCategory.ItemIndex])));
          qryItems.Open;
        end;

end;

procedure TfrmAddToolLocation.btnCancelClick(Sender: TObject);
var
msg : string;
begin

 msg := 'Tool location record have been modified. Do you really want to cancel the current operations ?' ;
  if frmSetupCabinet.qryBins.State in [dsInsert, dsEdit] then
    begin
      if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOK then
        begin
          frmSetupCabinet.qryBins.cancel;
          Close;
        end;
    end
  else
    close;
end;

procedure TfrmAddToolLocation.btnOKClick(Sender: TObject);
begin


  if (qryBinItems.Locate('ItemID', qryItems.FieldByName('Part__No').AsString, [])) AND (frmSetupCabinet.qryBins.State in [dsInsert]) then
     begin
      ShowMessage('Duplicate record found. Aborting post. Please select valid item');
      exit;
     end
 else
   begin

        if frmSetupCabinet.qryBins.State in [dsInsert, dsEdit] then
          begin
            //frmSetupCabinet.qryBinsstatus.Value := rgStatus.ItemIndex;
            frmSetupCabinet.qryBins.post;
          end;
          frmSetupCabinet.qryBins.Refresh;
          Close;
   end;
end;

procedure TfrmAddToolLocation.cbbCategoryChange(Sender: TObject);
begin
   // Sub Categories
           qrySubCategory.close;
           qrySubCategory.ParamByName('intCatID').Value := Integer(cbbCategory.Items.Objects[cbbCategory.ItemIndex]);
           qrySubCategory.Open;
           cbbSubCategory.Items.Clear;
           cbbSubCategory.Items.Add('--- Select a Sub Category ----');

           qrySubCategory.First;
           while not qrySubCategory.Eof do
             begin
               with cbbSubCategory do
                 begin

                   Items.AddObject(qrySubCategoryDescription.AsString, TObject(qrySubCategorySubCatID.AsInteger));
                 end;

              qrySubCategory.Next;
             end;
           qrySubCategory.Close;
           cbbSubCategory.ItemIndex := 0;
end;

procedure TfrmAddToolLocation.FormActivate(Sender: TObject);
begin
   if frmSetupCabinet.qryBins.State in [dsInsert] then
      begin

   //Open Query
           qryCategory.Open;
           cbbCategory.Items.Clear;
           cbbCategory.Items.Add('--- Select a Category ----');

           qryCategory.First;
           while not qryCategory.Eof do
             begin
               with cbbCategory do
                 begin

                   Items.AddObject(qryCategoryDescription.AsString, TObject(qryCategoryCatID.AsInteger));
                 end;

              qryCategory.Next;
             end;
           qryCategory.Close;
           cbbCategory.ItemIndex := 0;

           cbbSubCategory.Items.Clear;
           cbbSubCategory.Items.Add('--- Select a Sub Category ----');
           cbbSubCategory.ItemIndex := 0;


      end;


end;

procedure TfrmAddToolLocation.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Action := caFree;
end;

procedure TfrmAddToolLocation.FormCreate(Sender: TObject);
begin
  qryBinItems.Close;
  qryBinItems.DeleteWhere;
  qryBinItems.AddWhere('KabID = ' + DM.qryCabinet.FieldByName('kabID').ASString);
  qryBinItems.Open;
end;

end.
