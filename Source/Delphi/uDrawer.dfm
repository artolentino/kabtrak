object frmDrawer: TfrmDrawer
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Add Drawer'
  ClientHeight = 256
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 225
    Width = 307
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pnl2: TPanel
      Left = 138
      Top = 0
      Width = 169
      Height = 31
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnOK: TButton
        Left = 6
        Top = 4
        Width = 75
        Height = 23
        Caption = '&OK'
        Default = True
        TabOrder = 0
        OnClick = btnOKClick
      end
      object btnCancel: TButton
        Left = 86
        Top = 4
        Width = 75
        Height = 23
        Caption = '&Cancel'
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 307
    Height = 225
    ActivePage = ts1
    Align = alClient
    TabHeight = 20
    TabOrder = 1
    TabWidth = 100
    object ts1: TTabSheet
      Caption = 'General'
      object lbl3: TLabel
        Left = 28
        Top = 31
        Width = 67
        Height = 13
        Caption = 'Drawer Code:'
        FocusControl = editDrawerCode
      end
      object editDrawerCode: TDBEdit
        Left = 108
        Top = 28
        Width = 163
        Height = 21
        DataField = 'DrawerCode'
        DataSource = frmSetupCabinet.dsDrawers
        TabOrder = 0
      end
      object grp1: TGroupBox
        Left = 28
        Top = 55
        Width = 243
        Height = 105
        Caption = 'Lock Codes ( HEX )'
        TabOrder = 1
        object lbl1: TLabel
          Left = 28
          Top = 29
          Width = 25
          Height = 13
          Caption = 'Lock:'
          FocusControl = editLockCode
        end
        object lbl2: TLabel
          Left = 28
          Top = 58
          Width = 35
          Height = 13
          Caption = 'Unlock:'
          FocusControl = editM1ID
        end
        object editLockCode: TDBEdit
          Left = 80
          Top = 25
          Width = 134
          Height = 21
          DataField = 'LockCode'
          DataSource = frmSetupCabinet.dsDrawers
          TabOrder = 0
        end
        object editM1ID: TDBEdit
          Left = 80
          Top = 52
          Width = 134
          Height = 21
          DataField = 'M1ID'
          DataSource = frmSetupCabinet.dsDrawers
          TabOrder = 1
        end
      end
    end
  end
end
