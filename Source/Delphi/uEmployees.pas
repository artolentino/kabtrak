unit uEmployees;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, Buttons, Grids, DBGrids, DB, DBAccess, MyAccess,
  DBCtrls, MemDS, StdCtrls, Mask, JvExMask, JvToolEdit, JvMaskEdit, JvDBFindEdit,
  AdvSmoothTouchKeyBoard, JvExStdCtrls, JvEdit, JvDBSearchEdit, JvExControls,
  JvLabel, JvDBControls, AdvCardList, DBAdvCardList, NxPageControl, NxDBColumns,
  NxColumns, NxScrollControl, NxCustomGridControl, NxCustomGrid, NxDBGrid;

type
  TfrmEmployees = class(TForm)
    Panel1: TPanel;
    btnNew: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    btnImport: TSpeedButton;
    dsEmployees: TMyDataSource;
    bvl1: TBevel;
    btn2: TSpeedButton;
    bvl2: TBevel;
    btn3: TSpeedButton;
    bvl3: TBevel;
    btnTransactions: TSpeedButton;
    bvl4: TBevel;
    btn5: TSpeedButton;
    bvl5: TBevel;
    pnlNavigator: TPanel;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnFirst: TSpeedButton;
    btnLast: TSpeedButton;
    btnRefresh: TSpeedButton;
    bvl6: TBevel;
    btnClose: TSpeedButton;
    NxPageControl1: TNxPageControl;
    GridView: TNxTabSheet;
    CardView: TNxTabSheet;
    btnBar: TAdvButtonsBar;
    lst1: TDBAdvCardList;
    grd1: TNextDBGrid;
    nxdbnmbrclmnTNxDBNumberColumn3: TNxDBTextColumn;
    NxDBTextColumn1: TNxDBTextColumn;
    NxDBTextColumn2: TNxDBTextColumn;
    NxDBTextColumn3: TNxDBTextColumn;
    NxDBTextColumn4: TNxDBTextColumn;
    qryEmployees: TMyQuery;
    qryEmployeesID: TIntegerField;
    qryEmployeesUserID: TStringField;
    qryEmployeesLastName: TStringField;
    qryEmployeesMiddleName: TStringField;
    qryEmployeesFirstName: TStringField;
    qryEmployeesExtName: TStringField;
    qryEmployeesAddress1: TStringField;
    qryEmployeesAddress2: TStringField;
    qryEmployeesAddress3: TStringField;
    qryEmployeesDeptCode: TStringField;
    qryEmployeesPosition: TStringField;
    qryEmployeesemail: TStringField;
    qryEmployeesTelNo: TStringField;
    qryEmployeesMobileNumber: TStringField;
    qryEmployeesCustID: TIntegerField;
    qryEmployeesCreateDate: TDateField;
    qryEmployeesCreateTime: TTimeField;
    qryEmployeesFullname: TStringField;
    qryEmployeesstrDept: TStringField;
    procedure mnuExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnuDeleteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);

    procedure btnCloseClick(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure NxPageControl1Change(Sender: TObject);
    procedure qryEmployeesCalcFields(DataSet: TDataSet);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btnTransactionsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
   
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmployees: TfrmEmployees;

implementation

uses uDataModule, uEmployee, main, uDepartments, uFindEmployee,
  uUserTRansactions;

{$R *.dfm}

procedure TfrmEmployees.btn2Click(Sender: TObject);
begin
 frmDepartments := TfrmDepartments.Create(nil);
  frmDepartments.ShowModal;
end;

procedure TfrmEmployees.btn3Click(Sender: TObject);
begin
 frmFindEmployees := TfrmFindEmployees.Create(nil);
  frmFindEmployees.ShowModal;
end;

procedure TfrmEmployees.btnTransactionsClick(Sender: TObject);
var
 strMessage,strCaption : string;
begin
  if qryEmployeesUserID.AsVariant = null then ShowMessage('Please select a User to view')
  else
  begin
    screen.cursor := crHourglass;
    application.createform (TfrmUserTransaction,frmUserTransaction);
    frmUserTransaction.qryEmployee.Open;
    if frmUserTransaction.qryEmployee.Locate('ID', qryEmployeesID.value, []) then
      begin
        frmUserTransaction.qryTransactions.DeleteWhere;
        frmUserTransaction.qryTransactions.AddWhere('tblemployeeitemtransactions.UserID =' + qryEmployeesID.AsString);
        frmUserTransaction.Caption := 'Viewing ' + qryEmployeesFullname.AsString + ' Transactions history';
        frmUserTransaction.Showmodal;
        screen.cursor := crDefault;
      end
    else
      begin
        strMessage := 'There are no transactions found for ' + qryEmployeesFullname.AsString;
        Application.MessageBox(PChar(strMessage), 'Warning', MB_OK + MB_ICONINFORMATION);

        screen.cursor := crDefault;
        Exit;
      end;


  end;
end;

procedure TfrmEmployees.btnCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmEmployees.btnDeleteClick(Sender: TObject);
var
  msg : string;
begin

 msg := 'Do you really want to delete Employee Records of ' + qryEmployeesFirstName.AsString + ' ' + qryEmployeesLastName.AsString + '?';
 if qryEmployeesID.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       qryEmployees.delete;
       qryEmployees.RefreshRecord;
       qryEmployees.Refresh;
     end;
end;

procedure TfrmEmployees.btnDownClick(Sender: TObject);
begin
 qryEmployees.Next;
end;

procedure TfrmEmployees.btnEditClick(Sender: TObject);
begin
   if qryEmployeesID.AsVariant = null then btnNewClick(sender)   //No record selected.
  else begin
    screen.cursor := crHourglass;
    application.createform (TfrmEmployee,frmEmployee);
    frmEmployee.qryEmployee.Locate('ID', qryEmployeesID.value, []);
    frmEmployee.Caption := 'Editing ' + qryEmployeesFirstName.AsString + ' ' + qryEmployeesLastName.AsString;
    frmEmployee.Showmodal;
    screen.cursor := crDefault;
  end;

end;

procedure TfrmEmployees.btnFirstClick(Sender: TObject);
begin
  qryEmployees.First;
end;

procedure TfrmEmployees.btnLastClick(Sender: TObject);
begin
  qryEmployees.Last;
end;

procedure TfrmEmployees.btnNewClick(Sender: TObject);
begin
 screen.cursor := crHourglass;
  application.createform (TfrmEmployee,frmEmployee);
  frmEmployee.qryEmployee.Insert;
  frmEmployee.Caption := 'New Employee Record';
  frmEmployee.ShowModal;
  screen.cursor := crDefault;
end;

procedure TfrmEmployees.btnRefreshClick(Sender: TObject);
begin
  qryEmployees.Refresh;
end;

procedure TfrmEmployees.btnUpClick(Sender: TObject);
begin
 qryEmployees.Prior;
end;

procedure TfrmEmployees.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryEmployees.Close;
  Action := caFree;
end;

procedure TfrmEmployees.FormCreate(Sender: TObject);
begin
  NxPageControl1.ActivePageIndex := 0;
  qryEmployees.Open;

end;

procedure TfrmEmployees.FormShow(Sender: TObject);
begin
   qryEmployees.Open;
   NxPageControl1.ActivePageIndex := 0;

end;


procedure TfrmEmployees.mnuDeleteClick(Sender: TObject);
var
  msg : string;
begin

 msg := 'Do you really want to delete Employee Records of ' + qryEmployeesFirstName.AsString + ' ' + qryEmployeesLastName.AsString + '?';
 if qryEmployeesID.AsVariant = null then exit;   //Ignore if no record selected.
   if MessageDlg(msg, mtConfirmation, [mbOk, mbCancel], 0) = mrOk then
     begin
       qryEmployees.delete;
       qryEmployees.RefreshRecord;
       qryEmployees.Refresh;
     end;

end;

procedure TfrmEmployees.mnuExitClick(Sender: TObject);
begin
 Close;
end;



procedure TfrmEmployees.NxPageControl1Change(Sender: TObject);
begin
  case NxPageControl1.ActivePageIndex of
     0: begin


        end;
     1: begin
          lst1.SetFocus;
        end;
  end;
end;



procedure TfrmEmployees.qryEmployeesCalcFields(DataSet: TDataSet);
begin
  qryEmployeesFullname.Value := qryEmployeesFirstName.AsString + ' ' + qryEmployeesLastName.AsString;
end;

end.
