(********************************************************************************

   Program                :  Henchman TRAK
   File                   :  mtd.dpr
   Description            :  MTD Proogram
   Author                 :  Gabriel Tolentino/Henchman Products
   Development enviroment :  Embarcadero RAD Studio 2010
   Revisions	            :
   Version                :  1.0

**********************************************************************************)
program mtd;

uses
  EMemLeaks,
  EResLeaks,
  ESendMailSMTP,
  EDialogWinAPIMSClassic,
  EDialogWinAPIEurekaLogDetailed,
  EDialogWinAPIStepsToReproduce,
  EDebugExports,
  EFixSafeCallException,
  EMapWin32,
  EAppVCL,
  ExceptionLog7,
  Forms,
  main in 'Source\Delphi\main.pas' {frmMain},
  GlobalRoutines in 'Source\Delphi\GlobalRoutines.pas',
  uDataModule in 'Source\Delphi\uDataModule.pas' {DM: TDataModule},
  uSplash in 'Source\Delphi\uSplash.pas' {frmSplash},
  uAboutKabTrak in 'Source\Delphi\uAboutKabTrak.pas' {frmAboutKabTrak},
  uWelcome in 'Source\Delphi\uWelcome.pas' {frmWelcome},
  uLicense in 'Source\Delphi\uLicense.pas' {frmLicense},
  uLogin in 'Source\Delphi\uLogin.pas' {frmLogin},
  uCustomer in 'Source\Delphi\uCustomer.pas' {frmCustomer},
  uChangePin in 'Source\Delphi\uChangePin.pas' {frmChangePin},
  uDatabaseSetup in 'Source\Delphi\uDatabaseSetup.pas' {frmDatabase},
  uSystemSettings in 'Source\Delphi\uSystemSettings.pas' {frmSystemSettings},
  uSetupCabinet in 'Source\Delphi\uSetupCabinet.pas' {frmSetupCabinet},
  uTools in 'Source\Delphi\uTools.pas' {frmTools},
  uTool in 'Source\Delphi\uTool.pas' {frmTool},
  uEmployees in 'Source\Delphi\uEmployees.pas' {frmEmployees},
  uEmployee in 'Source\Delphi\uEmployee.pas' {frmEmployee},
  uSuppliers in 'Source\Delphi\uSuppliers.pas' {frmSuppliers},
  uSupplier in 'Source\Delphi\uSupplier.pas' {frmSupplier},
  uFindTools in 'Source\Delphi\uFindTools.pas' {frmFindTools},
  uFindSuppliers in 'Source\Delphi\uFindSuppliers.pas' {frmFindSuppliers},
  uCategories in 'Source\Delphi\uCategories.pas' {frmCategories},
  uCategory in 'Source\Delphi\uCategory.pas' {frmGategory},
  uSubCategory in 'Source\Delphi\uSubCategory.pas' {frmSubCategory},
  uToolMovement in 'Source\Delphi\uToolMovement.pas' {frmToolMovement},
  uDepartments in 'Source\Delphi\uDepartments.pas' {frmDepartments},
  uDepartment in 'Source\Delphi\uDepartment.pas' {frmDepartment},
  uFindEmployee in 'Source\Delphi\uFindEmployee.pas' {frmFindEmployees},
  Dropbox in 'Source\Delphi\Dropbox.pas',
  uUserTRansactions in 'Source\Delphi\uUserTRansactions.pas' {frmUserTransaction},
  uJobOrders in 'Source\Delphi\uJobOrders.pas' {frmJobOrders},
  uJobOrder in 'Source\Delphi\uJobOrder.pas' {frmJobOrder},
  uReaderSetup in 'Source\Delphi\uReaderSetup.pas' {frmReaderSetup},
  uToolCabinet in 'Source\Delphi\uToolCabinet.pas' {frmToolCabinet},
  uIssueTools in 'Source\Delphi\uIssueTools.pas' {frmIssueTools},
  uReturnTools in 'Source\Delphi\uReturnTools.pas' {frmReturnTools},
  uDrawer in 'Source\Delphi\uDrawer.pas' {frmDrawer},
  uToolLocation in 'Source\Delphi\uToolLocation.pas' {frmAddToolLocation},
  uIssue in 'Source\Delphi\uIssue.pas' {frmIssue},
  superobject in 'Source\Delphi\superobject.pas',
  uprogressForm in 'Source\Delphi\uprogressForm.pas' {progressForm},
  uRestoreBackupProgress in 'Source\Delphi\uRestoreBackupProgress.pas' {frmRestoreBackupProgress},
  uCustomerProfile in 'Source\Delphi\uCustomerProfile.pas' {frmCustomerProfile},
  uAddkabTRAK in 'Source\Delphi\uAddkabTRAK.pas' {frmAddkabTRAK},
  uCreateAdmin in 'Source\Delphi\uCreateAdmin.pas' {frmCreateAdmin};

{$R *.res}

begin
  frmSplash := TfrmSplash.Create(Application);
  if frmSplash.lblLicense.caption <> '30 day evaluation license' then
  begin
    frmSplash.Show;
    frmSplash.Update;
  end;
  Application.Initialize;
  Application.Title := 'kabTRAK';
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;

end.


