SELECT
  mtd.tblemployeeitemtransactions.TransID,
  mtd.tblemployeeitemtransactions.UserID,
  mtd.tblemployeeitemtransactions.TransType,
  mtd.tblemployeeitemtransactions.ItemID,
  mtd.tblemployeeitemtransactions.TransDate,
  mtd.tblemployeeitemtransactions.TransTime,
  mtd.users.FullName,
  mtd.tbltools.Description AS ToolName,
  mtd.tbltools.Part__No,
  mtd.tbljobs.Description AS JobName
FROM
  mtd.tblemployeeitemtransactions,
  mtd.users,
  mtd.tbljobs,
  mtd.tbltools
WHERE
  mtd.tbljobs.TrailID = mtd.tblemployeeitemtransactions.TransID AND
  mtd.users.UserID = mtd.tblemployeeitemtransactions.UserID AND
  mtd.tbltools.id = mtd.tblemployeeitemtransactions.ItemID
