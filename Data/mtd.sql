/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mtd

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2014-09-15 11:28:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tblcounterid
-- ----------------------------
DROP TABLE IF EXISTS `tblcounterid`;
CREATE TABLE `tblcounterid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `toolsID` int(11) DEFAULT NULL,
  `supplierID` int(11) DEFAULT NULL,
  `notificationID` int(11) DEFAULT NULL,
  `jobID` int(11) DEFAULT NULL,
  `employeeID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `kabID` int(11) DEFAULT NULL,
  `kabDrawerID` int(11) DEFAULT NULL,
  `kabBinID` int(11) DEFAULT NULL,
  `transactionID` int(11) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblcounterid
-- ----------------------------
INSERT INTO `tblcounterid` VALUES ('1', '100', '102', '100', '100', '105', '103', '100', '100', '100', '100', '1');

-- ----------------------------
-- Table structure for tblcustomer
-- ----------------------------
DROP TABLE IF EXISTS `tblcustomer`;
CREATE TABLE `tblcustomer` (
  `CustID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(255) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `TelNo` varchar(45) DEFAULT NULL,
  `FaxNo` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `ContactPerson` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `notes` text,
  `JobOrderLabel` varchar(25) DEFAULT NULL,
  `Address3` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CustID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblcustomer
-- ----------------------------
INSERT INTO `tblcustomer` VALUES ('1', 'Qantas Airways', 'Australia', null, null, 'user@qantas.com', 'ALisdair Chambers', 'Active', null, 'Trail', null, null, null);

-- ----------------------------
-- Table structure for tbldepartments
-- ----------------------------
DROP TABLE IF EXISTS `tbldepartments`;
CREATE TABLE `tbldepartments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `custID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbldepartments
-- ----------------------------
INSERT INTO `tbldepartments` VALUES ('1', 'Sales Department', '1');
INSERT INTO `tbldepartments` VALUES ('2', 'Production De[artment', '1');

-- ----------------------------
-- Table structure for tblemployeeitemtransactions
-- ----------------------------
DROP TABLE IF EXISTS `tblemployeeitemtransactions`;
CREATE TABLE `tblemployeeitemtransactions` (
  `TransID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `TransType` int(11) DEFAULT NULL,
  `ItemID` int(11) DEFAULT NULL,
  `TransDate` datetime DEFAULT NULL,
  `TransTime` datetime DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `TrailID` int(11) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblemployeeitemtransactions
-- ----------------------------

-- ----------------------------
-- Table structure for tblemployees
-- ----------------------------
DROP TABLE IF EXISTS `tblemployees`;
CREATE TABLE `tblemployees` (
  `ID` int(11) NOT NULL,
  `UserID` varchar(20) DEFAULT NULL,
  `FirstName` varchar(25) DEFAULT NULL,
  `MiddleName` varchar(25) DEFAULT NULL,
  `LastName` varchar(25) DEFAULT NULL,
  `ExtName` varchar(25) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Address3` varchar(100) DEFAULT NULL,
  `DeptCode` varchar(20) DEFAULT NULL,
  `Position` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `TelNo` varchar(20) DEFAULT NULL,
  `MobileNumber` varchar(20) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `CreateTime` time DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblemployees
-- ----------------------------
INSERT INTO `tblemployees` VALUES ('103', '001103', 'ALISDAIR', null, 'CHAMBERS', null, null, null, null, '1', 'DIRECTOR', 'alisdairchamber@qantasairways.com', null, null, '1', null, null);
INSERT INTO `tblemployees` VALUES ('104', '001104', 'GABRIEL', 'APIGO', 'TOLENTINO', null, null, null, null, '2', 'PROGRAMMER', 'artolentino@gmail.com', null, null, '1', null, null);

-- ----------------------------
-- Table structure for tblitemkabdrawerbins
-- ----------------------------
DROP TABLE IF EXISTS `tblitemkabdrawerbins`;
CREATE TABLE `tblitemkabdrawerbins` (
  `ID` int(11) NOT NULL,
  `BinID` varchar(50) NOT NULL,
  `DrawerID` varchar(25) DEFAULT NULL,
  `KabID` int(11) DEFAULT NULL,
  `P1Code` varchar(5) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `ItemID` varchar(25) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblitemkabdrawerbins
-- ----------------------------

-- ----------------------------
-- Table structure for tblitemkabdrawers
-- ----------------------------
DROP TABLE IF EXISTS `tblitemkabdrawers`;
CREATE TABLE `tblitemkabdrawers` (
  `ID` int(11) NOT NULL,
  `DrawerID` varchar(20) NOT NULL,
  `KabID` varchar(50) DEFAULT NULL,
  `LockCode` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedTime` datetime DEFAULT NULL,
  `LastAccess` datetime DEFAULT NULL,
  `LastAccessTime` datetime DEFAULT NULL,
  `M1ID` varchar(10) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `DrawerCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblitemkabdrawers
-- ----------------------------

-- ----------------------------
-- Table structure for tblitemkabs
-- ----------------------------
DROP TABLE IF EXISTS `tblitemkabs`;
CREATE TABLE `tblitemkabs` (
  `KabID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedTime` datetime DEFAULT NULL,
  `LastAccess` datetime DEFAULT NULL,
  `LastAccessTime` datetime DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `ModelNumber` varchar(50) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `Assigned` int(1) DEFAULT '0',
  `NumberDrawers` int(11) DEFAULT NULL,
  `NumberBins` int(11) DEFAULT NULL,
  PRIMARY KEY (`KabID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblitemkabs
-- ----------------------------
INSERT INTO `tblitemkabs` VALUES ('1', 'kabTRAK', 'Station A', '2014-08-01 14:39:55', '2014-08-01 14:40:00', null, null, '1', null, '1', '0', '10', '120');
INSERT INTO `tblitemkabs` VALUES ('2', 'kabTRAK', 'Station B', '2014-08-01 14:40:29', '2014-08-01 14:40:33', null, null, '1', null, '1', '0', '10', '120');

-- ----------------------------
-- Table structure for tbljobs
-- ----------------------------
DROP TABLE IF EXISTS `tbljobs`;
CREATE TABLE `tbljobs` (
  `TrailID` int(11) NOT NULL,
  `Description` varchar(150) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Remark` varchar(255) DEFAULT NULL,
  `continues` int(11) DEFAULT NULL,
  PRIMARY KEY (`TrailID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbljobs
-- ----------------------------
INSERT INTO `tbljobs` VALUES ('101', 'REGULAR MAINTENANCE', '1', null, null, null, '0');

-- ----------------------------
-- Table structure for tblnotifications
-- ----------------------------
DROP TABLE IF EXISTS `tblnotifications`;
CREATE TABLE `tblnotifications` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) DEFAULT NULL,
  `nDate` date DEFAULT NULL,
  `read` int(11) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblnotifications
-- ----------------------------

-- ----------------------------
-- Table structure for tblsuppliers
-- ----------------------------
DROP TABLE IF EXISTS `tblsuppliers`;
CREATE TABLE `tblsuppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` varchar(20) DEFAULT NULL,
  `SupplierName` varchar(100) DEFAULT NULL,
  `ContactPerson` varchar(50) DEFAULT NULL,
  `EmailAdd` varchar(50) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tblsuppliers
-- ----------------------------
INSERT INTO `tblsuppliers` VALUES ('101', '001-101', 'LEKKO PRODUCTS', 'ALISDAIR CHAMBERS', null, 'http://www.lekko.co', null, '1', null);
INSERT INTO `tblsuppliers` VALUES ('102', '001-102', 'HENCHMAN PRODUCTS', 'ALISDAIR CHAMBERS', null, 'http://www.henchman.com', null, '1', null);

-- ----------------------------
-- Table structure for tbltoolcategories
-- ----------------------------
DROP TABLE IF EXISTS `tbltoolcategories`;
CREATE TABLE `tbltoolcategories` (
  `CatID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltoolcategories
-- ----------------------------

-- ----------------------------
-- Table structure for tbltools
-- ----------------------------
DROP TABLE IF EXISTS `tbltools`;
CREATE TABLE `tbltools` (
  `id` int(11) NOT NULL,
  `Part__No` varchar(20) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `catID` int(11) DEFAULT NULL,
  `subCatID` int(11) DEFAULT NULL,
  `LatestCost` float(8,2) DEFAULT NULL,
  `SerialNo` varchar(25) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `SupplierID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltools
-- ----------------------------

-- ----------------------------
-- Table structure for tbltoolsubcatogories
-- ----------------------------
DROP TABLE IF EXISTS `tbltoolsubcatogories`;
CREATE TABLE `tbltoolsubcatogories` (
  `SubCatID` int(11) NOT NULL AUTO_INCREMENT,
  `CatID` int(11) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SubCatID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltoolsubcatogories
-- ----------------------------

-- ----------------------------
-- Table structure for tbltransdetail
-- ----------------------------
DROP TABLE IF EXISTS `tbltransdetail`;
CREATE TABLE `tbltransdetail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TransID` int(11) DEFAULT NULL,
  `TransDate` date DEFAULT NULL,
  `TransType` int(11) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbltransdetail
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `UserID` varchar(20) DEFAULT NULL,
  `PIN` varchar(10) DEFAULT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `Enabled` int(11) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `LastAccess` date DEFAULT NULL,
  `LastAccTime` timestamp NULL DEFAULT NULL,
  `CreatedTime` time DEFAULT NULL,
  `AccessCount` int(11) DEFAULT NULL,
  `CustID` int(11) DEFAULT NULL,
  `access_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1111', '1234', 'Administrator', '1', '2014-09-14', '2014-09-09', '0000-00-00 00:00:00', '21:00:00', '43', '1', '1');
INSERT INTO `users` VALUES ('102', '001104', '12345', 'GABRIEL TOLENTINO', '1', '2014-09-15', null, null, '09:25:35', '0', '1', '0');
INSERT INTO `users` VALUES ('103', '001103', '12345', 'ALISDAIR CHAMBERS', '1', '2014-09-15', null, null, '09:28:33', '0', '1', '0');
