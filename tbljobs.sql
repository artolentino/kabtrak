-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:21 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table tblnotifications
-- 

LOCK TABLES tblnotifications WRITE;
INSERT INTO tblnotifications(ID, Description, nDate, read, CustID) VALUES
  (1, 'Repair and Maintenance', 1, NULL, NULL, NULL, 0),
  (2, 'Job Order 2', 1, NULL, NULL, NULL, 0),
  (3, 'Job Order 3', 1, NULL, NULL, NULL, 0);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
