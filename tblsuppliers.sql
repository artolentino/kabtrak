-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:21 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table tbltoolcategories
-- 

LOCK TABLES tbltoolcategories WRITE;
INSERT INTO tbltoolcategories(CatID, Description, CustID) VALUES
  (1, '', 'Henchman Products', 'Alisdair Chambers', NULL, 'http://www.lekko.co', NULL, 1, NULL),
  (100, '001-100', 'LEKKO PRODUCTS', 'ALISDAIR CHAMBERS', NULL, 'http://www.lekko.co', NULL, 1, NULL),
  (101, '001-101', 'iSECURE INTEGRATED BUSINESS SOLUTION', 'EMIL T. LEGASPI JR.', '', 'jttp://www.onlnebarreview.co', '', 1, NULL);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
