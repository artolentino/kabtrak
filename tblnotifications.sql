-- MyDAC version: 5.80.0.47
-- MySQL server version: 5.6.16
-- MySQL client version: 4.1.3 Direct
-- Script date 9/12/2014 3:10:21 PM
-- ---------------------------------------------------------------------- 
-- Server: localhost
-- Database: mtd

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Dumping data for table tblsuppliers
-- 

LOCK TABLES tblsuppliers WRITE;
INSERT INTO tblsuppliers(id, supplier_id, SupplierName, ContactPerson, EmailAdd, Website, MobilePhone, CustID, notes) VALUES
  (1, 'The drawer is not connected', '2014-07-31 13:11:55', 0, 1),
  (2, 'The Smart Card Reader is Offline!', '2014-07-31 13:12:13', 1, 1);
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
